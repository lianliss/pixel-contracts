const { ethers } = require("hardhat");
// const { expect } = require("chai");

// Helper function to create an array of test token IDs with given rarities
async function createTestTokenIds(contract, owner, types) {
    const tokenIds = [];
    for (let i = 0; i < types.length; i++) {
        const tokenId = i + 1;
        await contract.mint(owner.address, types[i]);
        tokenIds.push(tokenId);
    }
    return tokenIds;
}

// Helper function to grant roles
async function grantRoles(contract, roles, grantees) {
    for (let i = 0; i < roles.length; i++) {
        await contract.grantRole(roles[i], grantees[i]);
    }
}

async function increaseTime(duration) {
  await ethers.provider.send("evm_increaseTime", [duration]);
  await ethers.provider.send("evm_mine", []);
}

// Helper function to deploy and configure contracts
async function deployAndSetupContracts() {
    const MockERC20 = await ethers.getContractFactory("MockERC20");
    const MockPXLsNFTV5 = await ethers.getContractFactory("PXLsNFTV5");
    const PXLsNFTV5Editor = await ethers.getContractFactory("PXLsNFTV5Editor");
    const PXLsCoreV5Math = await ethers.getContractFactory("PXLsCoreV5Math");
    const PXLsCoreV5 = await ethers.getContractFactory("PXLsCoreV5");
    const PXLsCoreV5Settings = await ethers.getContractFactory("PXLsCoreV5Settings");
    const PXLsSlotsV5 = await ethers.getContractFactory("PXLsSlotsV5");
    const PXLsSlotsV5Settings = await ethers.getContractFactory("PXLsSlotsV5Settings");
    const PXLsChestV5 = await ethers.getContractFactory("PXLsChestV5");
    const Forge = await ethers.getContractFactory("Forge");
    const MiningDrone = await ethers.getContractFactory("MiningDrone");

    // Deploy contracts
    const pixel = await MockERC20.deploy("Pixel", "PXL");
    const pixelDust = await MockERC20.deploy("Pixel Dust", "PXLD");
    const nftCollection = await MockPXLsNFTV5.deploy();
    const nftCollectionEditor = await PXLsNFTV5Editor.deploy(nftCollection.address);;
    const coreV5Math = await PXLsCoreV5Math.deploy();
    const coreV5 = await PXLsCoreV5.deploy(coreV5Math.address);
    const coreV5Settings = await PXLsCoreV5Settings.deploy(coreV5Math.address);
    const slotsV5 = await PXLsSlotsV5.deploy();
    const slotsV5Settings = await PXLsSlotsV5Settings.deploy(slotsV5.address);
    const chest = await PXLsChestV5.deploy();
    const forge = await Forge.deploy(
        coreV5.address,
        pixelDust.address,
        nftCollection.address,
        nftCollection.address,
        nftCollection.address
    );
    const miningDrone = await MiningDrone.deploy(
      pixelDust.address,
      coreV5.address,
      coreV5Math.address,
      slotsV5.address
    );

    await Promise.all([
        pixel.deployed(),
        pixelDust.deployed(),
        nftCollection.deployed(),
        nftCollectionEditor.deployed(),
        coreV5Math.deployed(),
        coreV5.deployed(),
        coreV5Settings.deployed(),
        slotsV5.deployed(),
        slotsV5Settings.deployed(),
        chest.deployed(),
        forge.deployed(),
        miningDrone.deployed()
    ]);

    return {
        pixel,
        pixelDust,
        nftCollection,
        nftCollectionEditor,
        coreV5Math,
        coreV5,
        coreV5Settings,
        slotsV5,
        slotsV5Settings,
        chest,
        forge,
        miningDrone
    };
}

async function grantAllRoles(contracts, owner, slotsV5Settings, user) {
    const {
        miningDrone,
        forge,
        nftCollection,
        nftCollectionEditor,
        chest,
        coreV5,
        coreV5Math,
        slotsV5,
    } = contracts;

    // Grant all roles
    await grantRoles(chest, [await chest.MULTIPLIER_ROLE()], [slotsV5.address]);
    await grantRoles(forge, [await forge.SLOT_EDITOR_ROLE()], [slotsV5.address]);
    await grantRoles(nftCollection, [await nftCollection.MINTER_ROLE()], [forge.address]);
    await grantRoles(nftCollection, [await nftCollection.BURNER_ROLE()], [forge.address]);
    await grantRoles(nftCollection, [await nftCollection.PROXY_ROLE(), await nftCollection.MINTER_ROLE()], [nftCollectionEditor.address, owner.address]);
    // await grantRoles(nftCollection, [await nftCollection.PROXY_ROLE()], [nftCollectionEditor.address]);
    await grantRoles(coreV5, [await coreV5.MULTIPLIER_ROLE(),await coreV5.BURNER_ROLE() ], [slotsV5.address, miningDrone.address]);
    await grantRoles(slotsV5, [await nftCollection.PROXY_ROLE()], [slotsV5Settings.address]);

    // Setting up Forge contract
    await forge.setSlot0ParameterId(17);
    await forge.setSlot1ParameterId(18);
    await forge.addTypeIdsByRarity([1, 5, 7], 0)
    await forge.addTypeIdsByRarity([2, 6], 1)
    await forge.addTypeIdsByRarity([3], 2)
    await forge.addTypeIdsByRarity([4], 3)
    await forge.addTypeIdsByRarity([5], 4)

    // console.log(await forge.getTypeIdsByRarityPaginated(0, 2, 2))

    // Setting up MiningDrone contract
    await miningDrone.setParameterIdSlot0(19);
    await miningDrone.setParameterIdSlot1(20);
    await miningDrone.setParameterIdDroneSpeed(21);
    await miningDrone.setParameterIdDroneDuration(22);

    await nftCollection.setTransfersDisabled(false)

    // PARAMS: typeName, _tokenURI, slots, collectionId, rarity, soulbound, disposable
    // Regular blend NFT
    // await nftCollectionEditor.createType("nftCollection", "testURI", [], 1, 0, false, true);
    await nftCollectionEditor.createType("nftCollection", "testURI", [], 1, 1, false, true);
    await nftCollectionEditor.createType("nftCollection", "testURI", [], 1, 2, false, true);
    await nftCollectionEditor.createType("nftCollection", "testURI", [], 1, 3, false, true);
    await nftCollectionEditor.createType("nftCollection", "testURI", [], 1, 4, false, true);
    await nftCollectionEditor.createType("nftCollection", "testURI", [], 1, 5, false, true);
    // Forge slot NFT
    await nftCollectionEditor.createType("Extruder", "testURI", [0], 1, 1, false, true);
    await nftCollectionEditor.createType("Synthesis", "testURI", [1], 1, 1, false, true);
    // Drone NFT
    await nftCollectionEditor.createType("Drone", "testURI", [2], 2, 1, false, true);
    // MiningDrone slot NFT
    await nftCollectionEditor.createType("TrainingModule", "testURI", [3], 2, 1, false, true);
    await nftCollectionEditor.createType("Battery", "testURI", [4], 2, 1, false, true);

    // Setting up NFT's params
    // Extruder
    nftTypeId0 = 5;
    const parameters0 = [17]
    const multipliers0 = [10120] // 1.012
    const adductors0 = [0]
    await nftCollectionEditor.addVariant(nftTypeId0, parameters0, multipliers0, adductors0)

    // Synthesis
    nftTypeId1 = 6;
    const parameters1 = [18]
    const multipliers1 = [10120] // 1.012
    const adductors1 = [0]
    await nftCollectionEditor.addVariant(nftTypeId1, parameters1, multipliers1, adductors1)

    //TODO!!
    // Training module
    nftTypeId2 = 8;
    const parameters2 = [19]
    const multipliers2 = [10300] // 1.030
    const adductors2 = [0]
    await nftCollectionEditor.addVariant(nftTypeId2, parameters2, multipliers2, adductors2)

    //TODO!!
    // Battery
    nftTypeId3 = 9;
    const parameters3 = [20]
    const multipliers3 = [10300] // 1.030
    const adductors3 = [0]
    await nftCollectionEditor.addVariant(nftTypeId3, parameters3, multipliers3, adductors3)

    //TODO!!
    // Drone's speed + Duration +
    nftTypeId4 = 7;
    const parameters4 = [21, 22]
    const multipliers4 = [2, 32400] // 2
    const adductors4 = [0, 0]
    await nftCollectionEditor.addVariant(nftTypeId4, parameters4, multipliers4, adductors4)

    //TODO!!
    // // Drone's duration
    // nftTypeId5 = 7;
    // const parameters5 = [22]
    // const multipliers5 = [32400] // 9 hours in sec
    // const adductors5 = [0]
    // await nftCollectionEditor.addVariant(nftTypeId5, parameters5, multipliers5, adductors5)


    // Setting up CoreV5
    userId = 1;
    const userAddress = user.address
    const parent = 0;
    await coreV5.grantAccess(userId, userAddress, parent);
    await coreV5Math.setBaseSpeed("10000000000000000")
    await coreV5Math.setDiffuculty("100000")

    // Setting up SlotsV5
    await slotsV5Settings.setCoreAddress(coreV5.address)
    await slotsV5Settings.setNFTAddress(nftCollection.address)
    await slotsV5Settings.setChestAddress(chest.address)

    await slotsV5Settings.setRegisteredParameters(23)

    const titleSlot0 = "Extruder"
    const sgbPriceSlot0 = 1
    const pixelPriceSlot0 = 0
    const requireSlotSlot0 = 0;
    await slotsV5Settings.createSlot(titleSlot0, sgbPriceSlot0, pixelPriceSlot0, requireSlotSlot0)

    const titleSlot1 = "Synthesis"
    const sgbPriceSlot1 = 1
    const pixelPriceSlot1 = 0
    const requireSlotSlot1 = 0;
    await slotsV5Settings.createSlot(titleSlot1, sgbPriceSlot1, pixelPriceSlot1, requireSlotSlot1)

    const titleSlot2 = "Drone"
    const sgbPriceSlot2 = 1
    const pixelPriceSlot2 = 0
    const requireSlotSlot2 = 0;
    await slotsV5Settings.createSlot(titleSlot2, sgbPriceSlot2, pixelPriceSlot2, requireSlotSlot2)

    const titleSlot3 = "TrainingModule"
    const sgbPriceSlot3 = 1
    const pixelPriceSlot3 = 0
    const requireSlotSlot3 = 0;
    await slotsV5Settings.createSlot(titleSlot3, sgbPriceSlot3, pixelPriceSlot3, requireSlotSlot3)

    const titleSlot4 = "Battery"
    const sgbPriceSlot4 = 1
    const pixelPriceSlot4 = 0
    const requireSlotSlot4 = 0;
    await slotsV5Settings.createSlot(titleSlot4, sgbPriceSlot4, pixelPriceSlot4, requireSlotSlot4)

    await slotsV5Settings.setParameterReceiver(parameters0[0], forge.address);
    await slotsV5Settings.setParameterReceiver(parameters1[0], forge.address);
    await slotsV5Settings.setParameterReceiver(parameters2[0], miningDrone.address);
    await slotsV5Settings.setParameterReceiver(parameters3[0], miningDrone.address);
    await slotsV5Settings.setParameterReceiver(parameters4[0], miningDrone.address);
    await slotsV5Settings.setParameterReceiver(parameters4[1], miningDrone.address);


}

describe("Forge Contract", function () {
    let owner, user;
    let contracts;

    beforeEach(async function () {
        [owner, user] = await ethers.getSigners();
        contracts = await deployAndSetupContracts();
        await grantAllRoles(contracts, owner, contracts.slotsV5Settings, user);
    });

    // it("should burn dust, slots, blend and mint a crafted NFT", async function () {
    //   const { expect } =  await import("chai");

    //   const { forge, nftCollection, pixelDust, slotsV5 } = contracts;

    //   // Mint tokens and set approvals
    //   const slotTokenIds = await createTestTokenIds(nftCollection, user, [5, 6]);
    //   const blendTokenIds = await createTestTokenIds(nftCollection, user, [1, 2, 3, 4, 5, 1, 2]);

    //   await nftCollection.connect(user).setApprovalForAll(forge.address, true);
    //   await nftCollection.connect(user).approve(slotsV5.address, slotTokenIds[0]);

    //   await pixelDust.mint(user.address, ethers.utils.parseEther("1000000"));
    //   await pixelDust.connect(user).approve(forge.address, ethers.utils.parseEther("1000000"));

    //   // Equip slot and craft
    //   const slotTypeId0 = 0;
    //   await slotsV5.connect(user).unlockSlotSGB(slotTypeId0, { value: 1 });
    //   await slotsV5.connect(user)["equip(uint256,uint8,uint256)"](slotTokenIds[0], slotTypeId0, 0);

    //   // Lite craft test
    //   console.log(await forge.connect(user).calculateRarityDistribution(ethers.utils.parseEther("1"), [blendTokenIds[1]], user.address))
    //   await forge.connect(user).craft(ethers.utils.parseEther("1"), [blendTokenIds[1]]);

    //   // Heavy craft test
    //   await forge.connect(user).craft(
    //     ethers.utils.parseEther("1000"),
    //     [blendTokenIds[2], blendTokenIds[3], blendTokenIds[4], blendTokenIds[5], blendTokenIds[6]]
    //   );

    //   // Check crafted NFTs
    //   expect(parseInt(await contracts.nftCollection.balanceOf(user.address))).to.equal(4);
    // });

    it("should burn dust, slots, blend and mint a crafted NFT", async function () {
      const { expect } =  await import("chai");

      const { miningDrone, nftCollection, nftCollectionEditor, pixel, slotsV5, coreV5, pixelDust } = contracts;

      // Mint tokens and set approvals
      // const slotTokenIds = await createTestTokenIds(nftCollection, user, [9]);
      const droneNftIds = await createTestTokenIds(nftCollection, user, [7])

      await nftCollection.connect(user).approve(slotsV5.address, droneNftIds[0]);
      // await nftCollection.connect(user).approve(slotsV5.address, slotTokenIds[0]);

      // await pixel.mint(user.address, ethers.utils.parseEther("1000000"));
      // await pixel.connect(user).approve(slotsV5.address, ethers.utils.parseEther("1000000"));
      await coreV5.mintForOne(userId, ethers.utils.parseEther("1000000"))
      await pixelDust.mint(miningDrone.address, ethers.utils.parseEther("1000000"))

      // console.log(await nftCollection.getTokenType(slotTokenIds[0]))
      // Equip slot Drone
      const slotTypeId2 = 2;
      await slotsV5.connect(user).unlockSlotSGB(slotTypeId2, { value: 1 });
      await slotsV5.connect(user)["equip(uint256,uint8,uint256)"](droneNftIds[0], slotTypeId2, 0);

      // // Equip slot Battery
      // const slotTypeId4 = 4;
      // await slotsV5.connect(user).unlockSlotSGB(slotTypeId4, { value: 1 });
      // await slotsV5.connect(user)["equip(uint256,uint8,uint256)"](slotTokenIds[0], slotTypeId4, 0);

      console.log(await miningDrone.calculateMineUpgradePrice(user.address))
      await miningDrone.connect(user).upgradeMine()

      await miningDrone.connect(user).dispatchDrone();

      await increaseTime("32400");
      await miningDrone.connect(user).returnDroneAndClaimReward();
      // expect(await miningDrone.someState(user.address)).to.equal(expectedState);
      console.log("TOTAL CLAIMED", await pixelDust.balanceOf(user.address))



      await miningDrone.connect(user).dispatchDrone();
      await increaseTime("32400");
      await miningDrone.connect(user).returnDroneAndClaimReward();
      console.log("TOTAL CLAIMED", await pixelDust.balanceOf(user.address))


    });
});
