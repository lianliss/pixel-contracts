import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { loadFixture, time } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

import type { Signers } from "../types";
import { deployLockFixture } from "./Marketplace.fixture";

describe("Marketplace", function () {
  before(async function () {
    this.signers = {} as Signers;

    const signers = await ethers.getSigners();
    this.signers.admin = signers[0];

    this.loadFixture = loadFixture;
  });

  describe("Deployment", function () {
    beforeEach(async function () {
      const { marketplace, owner, feesCollector, feesCollectorCutPerMillion } =
        await this.loadFixture(deployLockFixture);
      this.marketplace = marketplace;
      this.owner = owner;
      this.feesCollector = feesCollector;
      this.feesCollectorCutPerMillion = feesCollectorCutPerMillion;
    });

    it("Should set the right feesCollector, feesCollectorCutPerMillion", async function () {
      expect(await this.marketplace.feesCollector()).to.equal(this.feesCollector.address);
      expect(await this.marketplace.feesCollectorCutPerMillion()).to.equal(this.feesCollectorCutPerMillion);
    });

    it("Should set the right owner", async function () {
      expect(await this.marketplace.owner()).to.equal(this.owner.address);
    });
  });

  describe("Orders", function () {
    beforeEach(async function () {
      const { marketplace, nft, owner, feesCollector, feesCollectorCutPerMillion } =
        await this.loadFixture(deployLockFixture);
      this.marketplace = marketplace;
      this.nft = nft;
      this.owner = owner;
      this.feesCollector = feesCollector;
      this.feesCollectorCutPerMillion = feesCollectorCutPerMillion;

      await this.nft.mint(this.owner.address, 0);
    });

    describe("Creating", function () {
      it("Test", async function () {
        await expect(this.marketplace.createOrder(await this.nft.getAddress(), 1, 100, (await time.latest()) + 10000))
          .to.emit(this.marketplace, "OrderCreated")
          .withArgs(anyValue, 1, this.owner.address, await this.nft.getAddress(), 100, anyValue);
        // NFT locked in marketplace
        expect(await this.nft.ownerOf(1)).to.be.eq(await this.marketplace.getAddress());

        await expect(this.marketplace.cancelOrder(await this.nft.getAddress(), 1))
          .to.emit(this.marketplace, "OrderCancelled")
          .withArgs(anyValue, 1, this.owner.address, await this.nft.getAddress());
        // NFT returned back
        expect(await this.nft.ownerOf(1)).to.be.eq(this.owner.address);

        const [, , user] = await ethers.getSigners();
        await this.marketplace.createOrder(await this.nft.getAddress(), 1, 100, (await time.latest()) + 10000);
        const tx = this.marketplace.connect(user).executeOrder(await this.nft.getAddress(), 1, 100, { value: 100 });
        await expect(tx).to.changeEtherBalances([this.owner, this.feesCollector, user], [99, 1, -100]);
        await expect(tx)
          .to.emit(this.marketplace, "OrderSuccessful")
          .withArgs(anyValue, 1, this.owner.address, await this.nft.getAddress(), 100, user.address);
        // NFT transferred
        expect(await this.nft.ownerOf(1)).to.be.eq(user.address);
      });

      it("Should revert with the right error if invalid ERC721 contract", async function () {
        await expect(
          this.marketplace.createOrder(await this.owner.address, 1, 100, (await time.latest()) + 10000),
        ).to.be.revertedWith("Marketplace#_requireNFT: INVALID_NFT_ADDRESS");
      });

      it("Should revert with the right error if non existent token", async function () {
        await expect(
          this.marketplace.createOrder(await this.nft.getAddress(), 0, 100, (await time.latest()) + 10000),
        ).to.be.revertedWithCustomError(this.nft, "ERC721NonexistentToken");
      });
    });

    // describe("Events", function () {
    //   it("Should emit an event on withdrawals", async function () {
    //     await time.increaseTo(this.unlockTime);
    //
    //     await expect(this.lock.withdraw()).to.emit(this.lock, "Withdrawal").withArgs(this.lockedAmount, anyValue); // We accept any value as `when` arg
    //   });
    // });
    //
    // describe("Transfers", function () {
    //   it("Should transfer the funds to the owner", async function () {
    //     await time.increaseTo(this.unlockTime);
    //
    //     await expect(this.lock.withdraw()).to.changeEtherBalances(
    //       [this.owner, this.lock],
    //       [this.lockedAmount, -this.lockedAmount],
    //     );
    //   });
    // });
  });
});
