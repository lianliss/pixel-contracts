import { ethers } from "hardhat";

import type { Marketplace, Marketplace__factory, PixelNFT__factory } from "../../types";

export async function deployLockFixture() {
  // Contracts are deployed using the first signer/account by default
  const [owner, feesCollector] = await ethers.getSigners();

  const feesCollectorCutPerMillion = 10000;

  const marketplaceFactory = (await ethers.getContractFactory("Marketplace")) as Marketplace__factory;
  const marketplace = (await marketplaceFactory.deploy(
    await owner.getAddress(),
    ethers.ZeroAddress, // Native token
    await feesCollector.getAddress(),
    feesCollectorCutPerMillion,
  )) as Marketplace;

  const nftFactory = (await ethers.getContractFactory("PixelNFT")) as PixelNFT__factory;
  const nft = await nftFactory.deploy();

  await nft.createType("", "", [], 0, 0);
  await nft.grantRole(await nft.OPERATOR_ROLE(), await marketplace.getAddress());
  await marketplace.addNftCollection(await nft.getAddress());

  return { marketplace, nft, owner, feesCollector, feesCollectorCutPerMillion };
}
