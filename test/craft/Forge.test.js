const { ethers } = require("hardhat");
// const { expect } = require("chai");

// Helper function to create an array of test token IDs with given rarities
async function createTestTokenIds(contract, owner, rarities) {
    const tokenIds = [];
    for (let i = 0; i < rarities.length; i++) {
        const tokenId = i + 1;
        await contract.mint(owner.address, rarities[i]);
        tokenIds.push(tokenId);
    }
    return tokenIds;
}

// Helper function to grant roles
async function grantRoles(contract, roles, grantees) {
    for (let i = 0; i < roles.length; i++) {
        await contract.grantRole(roles[i], grantees[i]);
    }
}

// Helper function to deploy and configure contracts
async function deployAndSetupContracts() {
    const MockERC20 = await ethers.getContractFactory("MockERC20");
    const MockPXLsNFTV5 = await ethers.getContractFactory("PXLsNFTV5");
    const PXLsNFTV5Editor = await ethers.getContractFactory("PXLsNFTV5Editor");
    const PXLsCoreV5Math = await ethers.getContractFactory("PXLsCoreV5Math");
    const PXLsCoreV5 = await ethers.getContractFactory("PXLsCoreV5");
    const PXLsCoreV5Settings = await ethers.getContractFactory("PXLsCoreV5Settings");
    const PXLsSlotsV5 = await ethers.getContractFactory("PXLsSlotsV5");
    const PXLsSlotsV5Settings = await ethers.getContractFactory("PXLsSlotsV5Settings");
    const PXLsChestV5 = await ethers.getContractFactory("PXLsChestV5");
    const Forge = await ethers.getContractFactory("Forge");

    // Deploy contracts
    const pixelDust = await MockERC20.deploy("Pixel Dust", "PXLD");
    const slotCollection = await MockPXLsNFTV5.deploy();
    const blendCollection = await MockPXLsNFTV5.deploy();
    const craftedCollection = await MockPXLsNFTV5.deploy();
    const slotCollectionEditor = await PXLsNFTV5Editor.deploy(slotCollection.address);
    const blendCollectionEditor = await PXLsNFTV5Editor.deploy(blendCollection.address);
    const craftedCollectionEditor = await PXLsNFTV5Editor.deploy(craftedCollection.address);
    const coreV5Math = await PXLsCoreV5Math.deploy();
    const coreV5 = await PXLsCoreV5.deploy(coreV5Math.address);
    const coreV5Settings = await PXLsCoreV5Settings.deploy(coreV5Math.address);
    const slotsV5 = await PXLsSlotsV5.deploy();
    const slotsV5Settings = await PXLsSlotsV5Settings.deploy(slotsV5.address);
    const chest = await PXLsChestV5.deploy();
    const forge = await Forge.deploy(
        coreV5.address,
        pixelDust.address,
        blendCollection.address,
        slotCollection.address,
        craftedCollection.address
    );

    await Promise.all([
        pixelDust.deployed(),
        slotCollection.deployed(),
        blendCollection.deployed(),
        craftedCollection.deployed(),
        slotCollectionEditor.deployed(),
        blendCollectionEditor.deployed(),
        craftedCollectionEditor.deployed(),
        coreV5Math.deployed(),
        coreV5.deployed(),
        coreV5Settings.deployed(),
        slotsV5.deployed(),
        slotsV5Settings.deployed(),
        chest.deployed(),
        forge.deployed(),
    ]);

    return {
        pixelDust,
        slotCollection,
        blendCollection,
        craftedCollection,
        slotCollectionEditor,
        blendCollectionEditor,
        craftedCollectionEditor,
        coreV5Math,
        coreV5,
        coreV5Settings,
        slotsV5,
        slotsV5Settings,
        chest,
        forge,
    };
}

async function grantAllRoles(contracts, owner, slotsV5Settings, user) {
    const {
        forge,
        slotCollection,
        blendCollection,
        craftedCollection,
        slotCollectionEditor,
        blendCollectionEditor,
        craftedCollectionEditor,
        chest,
        coreV5,
        slotsV5,
    } = contracts;

    // Grant all roles
    await grantRoles(chest, [await chest.MULTIPLIER_ROLE()], [slotsV5.address]);
    await grantRoles(forge, [await forge.SLOT_EDITOR_ROLE()], [slotsV5.address]);
    await grantRoles(craftedCollection, [await craftedCollection.MINTER_ROLE()], [forge.address]);
    await grantRoles(blendCollection, [await blendCollection.BURNER_ROLE()], [forge.address]);
    await grantRoles(slotCollection, [await slotCollection.PROXY_ROLE(), await slotCollection.MINTER_ROLE()], [slotCollectionEditor.address, owner.address]);
    await grantRoles(blendCollection, [await slotCollection.PROXY_ROLE()], [blendCollectionEditor.address]);
    await grantRoles(craftedCollection, [await slotCollection.PROXY_ROLE()], [craftedCollectionEditor.address]);
    await grantRoles(coreV5, [await coreV5.MULTIPLIER_ROLE()], [slotsV5.address]);
    await grantRoles(slotsV5, [await slotCollection.PROXY_ROLE()], [slotsV5Settings.address]);

    // Setting up Forge contract
    await forge.setSlot0ParameterId(9);
    await forge.setSlot1ParameterId(10);

    await slotCollection.setTransfersDisabled(false)

    // PARAMS: typeName, _tokenURI, slots, collectionId, rarity, soulbound, disposable
    await slotCollectionEditor.createType("slotCollection", "testURI", [0,1], 1, 0, false, true);
    await slotCollectionEditor.createType("slotCollection", "testURI", [0,1], 1, 1, false, true);
    await slotCollectionEditor.createType("slotCollection", "testURI", [0,1], 1, 2, false, true);
    await slotCollectionEditor.createType("slotCollection", "testURI", [0,1], 1, 3, false, true);
    await slotCollectionEditor.createType("slotCollection", "testURI", [0,1], 1, 4, false, true);
    await slotCollectionEditor.createType("slotCollection", "testURI", [0,1], 1, 5, false, true);
    await blendCollectionEditor.createType("blendCollection", "testURI", [], 1, 0, false, true);
    await blendCollectionEditor.createType("blendCollection", "testURI", [], 2, 1, false, true);
    await blendCollectionEditor.createType("blendCollection", "testURI", [], 2, 2, false, true);
    await blendCollectionEditor.createType("blendCollection", "testURI", [], 2, 3, false, true);
    await blendCollectionEditor.createType("blendCollection", "testURI", [], 2, 4, false, true);
    await blendCollectionEditor.createType("blendCollection", "testURI", [], 2, 5, false, true);
    await craftedCollectionEditor.createType("craftedCollection", "testURI", [], 1, 0, false, true);
    await craftedCollectionEditor.createType("craftedCollection", "testURI", [], 3, 1, false, true);
    await craftedCollectionEditor.createType("craftedCollection", "testURI", [], 3, 2, false, true);
    await craftedCollectionEditor.createType("craftedCollection", "testURI", [], 3, 3, false, true);
    await craftedCollectionEditor.createType("craftedCollection", "testURI", [], 3, 4, false, true);
    await craftedCollectionEditor.createType("craftedCollection", "testURI", [], 3, 5, false, true);

    // Setting up NFT's params
    slotTypeId0 = 0;
    const parameters0 = [9]
    const multipliers0 = [10120] // 1.012
    const adductors0 = [0]
    await slotCollectionEditor.addVariant(slotTypeId0, parameters0, multipliers0, adductors0)

    slotTypeId1 = 1;
    const parameters1 = [9]
    const multipliers1 = [10120] // 1.012
    const adductors1= [0]
    await slotCollectionEditor.addVariant(slotTypeId1, parameters1, multipliers1, adductors1)

    // Setting up CoreV5
    const userId = 1;
    const userAddress = user.address
    const parent = 0;
    await coreV5.grantAccess(userId, userAddress, parent);

    // Setting up SlotsV5
    await slotsV5Settings.setCoreAddress(coreV5.address)
    await slotsV5Settings.setNFTAddress(slotCollection.address)
    await slotsV5Settings.setChestAddress(chest.address)

    const titleSlot0 = "Extruder"
    const sgbPriceSlot0 = 1
    const pixelPriceSlot0 = 0
    const requireSlotSlot0 = 0;
    await slotsV5Settings.createSlot(titleSlot0, sgbPriceSlot0, pixelPriceSlot0, requireSlotSlot0)

    const titleSlot1 = "Synthesis"
    const sgbPriceSlot1 = 1
    const pixelPriceSlot1 = 0
    const requireSlotSlot1 = 0;
    await slotsV5Settings.createSlot(titleSlot1, sgbPriceSlot1, pixelPriceSlot1, requireSlotSlot1)

    await slotsV5Settings.setParameterReceiver(parameters0[0], forge.address);
}

describe("Forge Contract", function () {
    let owner, user;
    let contracts;

    beforeEach(async function () {
        [owner, user] = await ethers.getSigners();
        contracts = await deployAndSetupContracts();
        await grantAllRoles(contracts, owner, contracts.slotsV5Settings, user);
    });

    it("should burn dust, slots, blend and mint a crafted NFT", async function () {
      const { expect } =  await import("chai");

      const { forge, slotCollection, blendCollection, pixelDust, slotsV5 } = contracts;

      // Mint tokens and set approvals
      const slotTokenIds = await createTestTokenIds(slotCollection, user, [1, 2]);
      const blendTokenIds = await createTestTokenIds(blendCollection, user, [1, 2, 3, 4, 5, 1]);

      await blendCollection.connect(user).setApprovalForAll(forge.address, true);
      await slotCollection.connect(user).approve(slotsV5.address, slotTokenIds[0]);

      await pixelDust.mint(user.address, ethers.utils.parseEther("1000000"));
      await pixelDust.connect(user).approve(forge.address, ethers.utils.parseEther("1000000"));

      // Equip slot and craft
      const slotTypeId0 = 0;
      await slotsV5.connect(user).unlockSlotSGB(slotTypeId0, { value: 1 });
      await slotsV5.connect(user)["equip(uint256,uint8,uint256)"](slotTokenIds[0], slotTypeId0, 0);

      // Lite craft test
      console.log(await forge.connect(user).calculateRarityDistribution(ethers.utils.parseEther("1"), [blendTokenIds[0]], user.address))
      await forge.connect(user).craft(ethers.utils.parseEther("1"), [blendTokenIds[0]]);

      // Heavy craft test
      await forge.connect(user).craft(
        ethers.utils.parseEther("1000"),
        [blendTokenIds[1], blendTokenIds[2], blendTokenIds[3], blendTokenIds[4], blendTokenIds[5]]
      );

      // Check crafted NFTs
      expect(parseInt(await contracts.craftedCollection.balanceOf(user.address))).to.equal(2);
    });
});
