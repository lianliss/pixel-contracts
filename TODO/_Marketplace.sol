// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Checker.sol";
import "@openzeppelin/contracts/utils/introspection/IERC165.sol";

contract Marketplace is Ownable, Pausable {
    using Address for address;

    struct Order {
        // Order ID
        bytes32 id;
        // Owner of the NFT
        address seller;
        // NFT registry address
        address nftAddress;
        // Price (in wei) for the published item
        uint256 price;
        // Time when this sale ends
        uint256 expiresAt;
    }

    struct Offer {
        // Offer ID
        bytes32 id;
        // Buyer address
        address buyer;
        // NFT registry address
        address nftAddress;
        // Price (in wei)
        uint256 price;
        // Time when this offer ends
        uint256 expiresAt;
    }

    // From ERC721 registry assetId to Order (to avoid asset collision)
    mapping(address => mapping(uint256 => Order)) public orderByAssetId;
    // From ERC721 registry assetId to Offer (to avoid asset collision)
    mapping(address => mapping(uint256 => Offer)) public offerByAssetId;

    address public feesCollector;

    uint256 public feesCollectorCutPerMillion;
    uint256 public publicationFeeInWei;
    IERC20 public feeCurrency = address(0); // ZERO address for fees in coin

    // EVENTS
    event OrderCreated(
        bytes32 id,
        uint256 indexed assetId,
        address indexed seller,
        address nftAddress,
        uint256 priceInWei,
        uint256 expiresAt
    );
    event OrderSuccessful(
        bytes32 id,
        uint256 indexed assetId,
        address indexed seller,
        address nftAddress,
        uint256 totalPrice,
        address indexed buyer
    );
    event OrderCancelled(bytes32 id, uint256 indexed assetId, address indexed seller, address nftAddress);
    event OfferCreated(
        bytes32 id,
        uint256 indexed assetId,
        address indexed buyer,
        address nftAddress,
        uint256 priceInWei,
        uint256 expiresAt
    );
    event OfferSuccessful(
        bytes32 id,
        uint256 indexed assetId,
        address indexed buyer,
        address nftAddress,
        uint256 totalPrice,
        address indexed seller
    );
    event OfferCancelled(bytes32 id, uint256 indexed assetId, address indexed buyer, address nftAddress);

    event ChangedPublicationFee(uint256 publicationFee);
    event ChangedFeesCollectorCutPerMillion(uint256 feesCollectorCutPerMillion);
    event FeesCollectorSet(address indexed oldFeesCollector, address indexed newFeesCollector);
    event SetFeeCurrency(address newFeeCurrency);

    /**
     * @dev Initialize this contract. Acts as a constructor
     * @param _owner - owner
     * @param _feesCollector - fees collector
     * @param _feesCollectorCutPerMillion - fees collector cut per million
     */
    constructor(address _owner, address _feesCollector, uint256 _feesCollectorCutPerMillion) Ownable(_owner) {
        // Address init
        setFeesCollector(_feesCollector);

        // Fee init
        setFeesCollectorCutPerMillion(_feesCollectorCutPerMillion);
    }

    function setFeeCurrency(address _feeCurrency) external onlyOwner {
        feeCurrency = IERC20(_feeCurrency);
        emit SetFeeCurrency(_feeCurrency);
    }

    /**
     * @dev Sets the publication fee that's charged to users to publish items
     * @param _publicationFee - Fee amount in wei this contract charges to publish an item
     */
    function setPublicationFee(uint256 _publicationFee) external onlyOwner {
        publicationFeeInWei = _publicationFee;
        emit ChangedPublicationFee(publicationFeeInWei);
    }

    /**
     * @dev Sets the share cut for the fees collector of the contract that's
     *  charged to the seller on a successful sale
     * @param _feesCollectorCutPerMillion - fees for the collector
     */
    function setFeesCollectorCutPerMillion(uint256 _feesCollectorCutPerMillion) public onlyOwner {
        feesCollectorCutPerMillion = _feesCollectorCutPerMillion;

        require(
            feesCollectorCutPerMillion < 1000000,
            "Marketplace#setFeesCollectorCutPerMillion: FEES_MUST_BE_BETWEEN_0_AND_999999"
        );

        emit ChangedFeesCollectorCutPerMillion(feesCollectorCutPerMillion);
    }

    /**
     * @notice Set the fees collector
     * @param _newFeesCollector - fees collector
     */
    function setFeesCollector(address _newFeesCollector) public onlyOwner {
        require(_newFeesCollector != address(0), "Marketplace#setFeesCollector: INVALID_FEES_COLLECTOR");

        emit FeesCollectorSet(feesCollector, _newFeesCollector);
        feesCollector = _newFeesCollector;
    }

    /**
     * @dev Creates a new order
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order (in hours)
     */
    function createOrder(
        address nftAddress,
        uint256 assetId,
        uint256 priceInWei,
        uint256 expiresAt
    ) public payable whenNotPaused {
        _createOrder(nftAddress, assetId, priceInWei, expiresAt);

        // Check if there's a publication fee and
        // transfer the amount to marketplace owner
        if (publicationFeeInWei > 0) {
            require(publicationFeeInWei == msg.value, "Marketplace#createOrder: INSUFFICIENT_FUND");
            (bool sent, ) = payable(feesCollector).call{ value: publicationFeeInWei }("");
            require(sent, "Marketplace#createOrder: TRANSFER_FAILED");
        }
    }

    /**
     * @dev Cancel an already published order
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function cancelOrder(address nftAddress, uint256 assetId) public whenNotPaused {
        _cancelOrder(nftAddress, assetId);
    }

    /**
     * @dev Executes the sale for a published NFT
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function executeOrder(address nftAddress, uint256 assetId) public payable whenNotPaused {
        _executeOrder(nftAddress, assetId, msg.value);
    }

    /**
     * @dev Creates a new order
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order
     */
    function _createOrder(address nftAddress, uint256 assetId, uint256 priceInWei, uint256 expiresAt) internal {
        _requireERC721(nftAddress);

        address sender = _msgSender();

        IERC721 nftRegistry = IERC721(nftAddress);
        address assetOwner = nftRegistry.ownerOf(assetId);

        require(sender == assetOwner, "Marketplace#_createOrder: NOT_ASSET_OWNER");
        require(
            nftRegistry.getApproved(assetId) == address(this) ||
                nftRegistry.isApprovedForAll(assetOwner, address(this)),
            "The contract is not authorized to manage the asset"
        );
        require(priceInWei > 0, "Price should be bigger than 0");
        require(expiresAt > block.timestamp + 1 minutes, "Marketplace#_createOrder: INVALID_EXPIRES_AT");

        bytes32 orderId = keccak256(abi.encodePacked(block.timestamp, assetOwner, assetId, nftAddress, priceInWei));

        orderByAssetId[nftAddress][assetId] = Order({
            id: orderId,
            seller: assetOwner,
            nftAddress: nftAddress,
            price: priceInWei,
            expiresAt: expiresAt
        });

        emit OrderCreated(orderId, assetId, assetOwner, nftAddress, priceInWei, expiresAt);
    }

    /**
     * @dev Cancel an already published order
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function _cancelOrder(address nftAddress, uint256 assetId) internal returns (Order memory) {
        address sender = _msgSender();
        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_cancelOrder: INVALID_ORDER");
        require(order.seller == sender || sender == owner(), "Marketplace#_cancelOrder: UNAUTHORIZED_USER");

        bytes32 orderId = order.id;
        address orderSeller = order.seller;
        address orderNftAddress = order.nftAddress;
        delete orderByAssetId[nftAddress][assetId];

        emit OrderCancelled(orderId, assetId, orderSeller, orderNftAddress);

        return order;
    }

    /**
     * @dev Executes the sale for a published NFT
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     * @param price - Order price
     */
    function _executeOrder(address nftAddress, uint256 assetId, uint256 price) internal returns (Order memory) {
        _requireERC721(nftAddress);

        address sender = _msgSender();

        IERC721 nftRegistry = IERC721(nftAddress);

        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_executeOrder: ASSET_NOT_FOR_SALE");

        require(order.seller != address(0), "Marketplace#_executeOrder: INVALID_SELLER");
        require(order.seller != sender, "Marketplace#_executeOrder: SENDER_IS_SELLER");
        require(order.price == price, "Marketplace#_executeOrder: PRICE_MISMATCH");
        require(block.timestamp < order.expiresAt, "Marketplace#_executeOrder: ORDER_EXPIRED");
        require(order.seller == nftRegistry.ownerOf(assetId), "Marketplace#_executeOrder: SELLER_NOT_OWNER");

        delete orderByAssetId[nftAddress][assetId];

        uint256 feesCollectorShareAmount = (price * feesCollectorCutPerMillion) / 1000000;

        // Fees collector share
        {
            if (feesCollectorShareAmount > 0) {
                (bool sentFees, ) = payable(feesCollector).call{ value: feesCollectorShareAmount }("");
                require(sentFees, "Marketplace#_executeOrder: TRANSFER_FEES_TO_FEES_COLLECTOR_FAILED");
            }
        }

        // Transfer sale amount to seller
        (bool sent, ) = payable(order.seller).call{ value: price - feesCollectorShareAmount }("");
        require(sent, "Marketplace#_executeOrder: TRANSFER_AMOUNT_TO_SELLER_FAILED");

        // Transfer asset owner
        nftRegistry.transferFrom(order.seller, sender, assetId);

        emit OrderSuccessful(order.id, assetId, order.seller, nftAddress, price, sender);

        return order;
    }

    /**
     * @dev Creates a new offer
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order (in hours)
     */
    function createOffer(
        address nftAddress,
        uint256 assetId,
        uint256 priceInWei,
        uint256 expiresAt
    ) public payable whenNotPaused {
        require(priceInWei + publicationFeeInWei == msg.value, "Marketplace#createOffer: INSUFFICIENT_FUND");
        _createOffer(nftAddress, assetId, priceInWei, expiresAt);

        // Check if there's a publication fee and
        // transfer the amount to marketplace owner
        if (publicationFeeInWei > 0) {
            (bool sent, ) = payable(feesCollector).call{ value: publicationFeeInWei }("");
            require(sent, "Marketplace#createOffer: TRANSFER_FAILED");
        }
    }

    /**
     * @dev Cancel an already published offer
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function cancelOffer(address nftAddress, uint256 assetId) public whenNotPaused {
        _cancelOffer(nftAddress, assetId);
    }

    /**
     * @dev Executes the sale for a published offer
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function executeOffer(address nftAddress, uint256 assetId) public whenNotPaused {
        _executeOffer(nftAddress, assetId);
    }

    /**
     * @dev Creates a new offer
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order
     */
    function _createOffer(address nftAddress, uint256 assetId, uint256 priceInWei, uint256 expiresAt) internal {
        _requireERC721(nftAddress);

        address sender = _msgSender();

        require(priceInWei > 0, "Price should be bigger than 0");
        require(expiresAt > block.timestamp + 1 minutes, "Marketplace#_createOffer: INVALID_EXPIRES_AT");

        bytes32 offerId = keccak256(abi.encodePacked(block.timestamp, sender, assetId, nftAddress, priceInWei));

        offerByAssetId[nftAddress][assetId] = Offer({
            id: offerId,
            buyer: sender,
            nftAddress: nftAddress,
            price: priceInWei,
            expiresAt: expiresAt
        });

        emit OfferCreated(offerId, assetId, sender, nftAddress, priceInWei, expiresAt);
    }

    /**
     * @dev Cancel an already published offer
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function _cancelOffer(address nftAddress, uint256 assetId) internal returns (Offer memory) {
        address sender = _msgSender();
        Offer memory offer = offerByAssetId[nftAddress][assetId];

        require(offer.id != 0, "Marketplace#_cancelOffer: INVALID_OFFER");
        require(offer.buyer == sender || sender == owner(), "Marketplace#_cancelOffer: UNAUTHORIZED_USER");

        bytes32 offerId = offer.id;
        address offerBuyer = offer.buyer;
        address orderNftAddress = offer.nftAddress;
        delete offerByAssetId[nftAddress][assetId];

        emit OfferCancelled(offerId, assetId, offerBuyer, orderNftAddress);

        return offer;
    }

    /**
     * @dev Executes the sale for a published offer
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function _executeOffer(address nftAddress, uint256 assetId) internal returns (Offer memory) {
        _requireERC721(nftAddress);

        address sender = _msgSender();

        IERC721 nftRegistry = IERC721(nftAddress);

        Offer memory offer = offerByAssetId[nftAddress][assetId];

        require(offer.id != 0, "Marketplace#_executeOffer: ASSET_NOT_FOR_SALE");

        require(offer.buyer != address(0), "Marketplace#_executeOffer: INVALID_BUYER");
        require(offer.buyer != sender, "Marketplace#_executeOffer: SENDER_IS_BUYER");
        require(block.timestamp < offer.expiresAt, "Marketplace#_executeOffer: OFFER_EXPIRED");
        require(sender == nftRegistry.ownerOf(assetId), "Marketplace#_executeOffer: SELLER_NOT_OWNER");

        uint256 price = offer.price;

        delete offerByAssetId[nftAddress][assetId];

        uint256 feesCollectorShareAmount = (price * feesCollectorCutPerMillion) / 1000000;

        // Fees collector share
        {
            if (feesCollectorShareAmount > 0) {
                (bool sentFees, ) = payable(feesCollector).call{ value: feesCollectorShareAmount }("");
                require(sentFees, "Marketplace#_executeOffer: TRANSFER_FEES_TO_FEES_COLLECTOR_FAILED");
            }
        }

        // Transfer sale amount to seller
        (bool sent, ) = payable(sender).call{ value: price - feesCollectorShareAmount }("");
        require(sent, "Marketplace#_executeOffer: TRANSFER_AMOUNT_TO_SELLER_FAILED");

        // Transfer asset owner
        nftRegistry.transferFrom(sender, offer.buyer, assetId);

        emit OfferSuccessful(offer.id, assetId, offer.buyer, nftAddress, price, sender);

        return offer;
    }

    function _requireERC721(address nftAddress) internal view {
        require(
            ERC165Checker.supportsERC165(nftAddress) &&
                IERC165(nftAddress).supportsInterface(type(IERC721).interfaceId),
            "Marketplace#_requireERC721: INVALID_ERC721_IMPLEMENTATION"
        );
    }
}
