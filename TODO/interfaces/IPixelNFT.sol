// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import "@openzeppelin/contracts/interfaces/draft-IERC6093.sol";
import "../libraries/PixelLibrary.sol";

struct Modifier {
    uint8 parameterId; // One of ACTION constant
    uint24 mul; // Multiplier with PERCENT_DECIMALS decimals
    uint256 add; // Adductor
}

struct ItemType {
    Modifier[][] variants; // Each variant can have a multiple modifiers
    string tokenURI;
    string name;
    uint8[] slots; // Purpose slots list
    uint collectionId;
    uint8 rarity;
}

interface IPixelNFT is IERC721Metadata, IERC721Errors {

    event CollectionCreated(uint indexed collectionId, string collectionName);
    event TypeCreated(uint256 index, string name);
    event VariantAdded(uint256 typeId, uint256 variantId);
    event VariantsCleared(uint256 indexed typeId);
    event Mint(address indexed owner, uint256 indexed typeId, uint256 tokenId);
    event Burn(address indexed owner, uint256 tokenId);

    function createCollection(string calldata collectionName) external;
    function getCollections() external view returns (string[] memory);
    function createType(string calldata typeName, string calldata _tokenURI, uint8[] calldata slots, uint collectionId, uint8 rarity) external;
    function addVariant(uint256 typeId, uint8[] calldata parameters, uint24[] calldata multipliers, uint256[] calldata adductors) external;
    function clearVariants(uint256 typeId) external;
    function getTypes() external view returns(ItemType[] memory);
    function getTokenType(uint256 tokenId) external view returns (ItemType memory);
    function getOwnerTokensCount(address owner) external view returns (uint256);
    function getOwnerTokenAt(address owner, uint256 index) external view returns (uint256);
    function getOwnerTokens(address owner, uint offset, uint limit) external view returns (uint256[] memory, ItemType[] memory);
    function tokenURI(uint256 tokenId) external view returns (string memory);
    function setBaseURI(string calldata newValue) external returns (string memory);
    function setBaseSuffix(string calldata newValue) external returns (string memory);
    function mint(address to, uint256 typeId) external returns (uint);
    function burn(uint256 tokenId) external;
    function balanceOf(address owner) external view returns (uint256);
    function ownerOf(uint256 tokenId) external view returns (address);
    function name() external pure returns (string memory);
    function symbol() external pure returns (string memory);
    function approve(address to, uint256 tokenId) external;
    function getApproved(uint256 tokenId) external view returns (address);
    function transferFrom(address from, address to, uint256 tokenId) external;

}
