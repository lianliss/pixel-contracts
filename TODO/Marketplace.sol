// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "./interfaces/IPixelNFT.sol";

contract Marketplace is Ownable, Pausable {
    using Address for address;
    using EnumerableSet for EnumerableSet.AddressSet;

    IERC20 public acceptedToken;
    EnumerableSet.AddressSet private _nftCollectionList;

    struct Order {
        // Order ID
        bytes32 id;
        // Owner of the NFT
        address seller;
        // NFT registry address
        address nftAddress;
        // Price (in wei) for the published item
        uint256 price;
        // Time when this sale ends
        uint256 expiresAt;
    }

    // From ERC721 registry assetId to Order (to avoid asset collision)
    mapping(address => mapping(uint256 => Order)) public orderByAssetId;

    address public feesCollector;

    uint256 public feesCollectorCutPerMillion;
    uint256 public publicationFeeInWei;

    // EVENTS
    event OrderCreated(
        bytes32 id,
        uint256 indexed assetId,
        address indexed seller,
        address nftAddress,
        uint256 priceInWei,
        uint256 expiresAt
    );
    event OrderSuccessful(
        bytes32 id,
        uint256 indexed assetId,
        address indexed seller,
        address nftAddress,
        uint256 totalPrice,
        address indexed buyer
    );
    event OrderCancelled(bytes32 id, uint256 indexed assetId, address indexed seller, address nftAddress);

    event ChangedPublicationFee(uint256 publicationFee);
    event ChangedFeesCollectorCutPerMillion(uint256 feesCollectorCutPerMillion);
    event FeesCollectorSet(address indexed oldFeesCollector, address indexed newFeesCollector);
    event NftCollectionAdded(address indexed nftAddress);
    event NftCollectionRemoved(address indexed nftAddress);

    /**
     * @dev Initialize this contract. Acts as a constructor
     * @param _owner - owner
     * @param _acceptedToken - Address of the ERC20 accepted for this marketplace
     * @param _feesCollector - fees collector
     * @param _feesCollectorCutPerMillion - fees collector cut per million
     */
    constructor(
        address _owner,
        address _acceptedToken,
        address _feesCollector,
        uint256 _feesCollectorCutPerMillion
    ) Ownable(_owner) {
        acceptedToken = IERC20(_acceptedToken);

        // Address init
        setFeesCollector(_feesCollector);

        // Fee init
        setFeesCollectorCutPerMillion(_feesCollectorCutPerMillion);
    }

    /**
     * @dev Sets the publication fee that's charged to users to publish items
     * @param _publicationFee - Fee amount in wei this contract charges to publish an item
     */
    function setPublicationFee(uint256 _publicationFee) external onlyOwner {
        publicationFeeInWei = _publicationFee;
        emit ChangedPublicationFee(publicationFeeInWei);
    }

    /**
     * @dev Sets the share cut for the fees collector of the contract that's
     *  charged to the seller on a successful sale
     * @param _feesCollectorCutPerMillion - fees for the collector
     */
    function setFeesCollectorCutPerMillion(uint256 _feesCollectorCutPerMillion) public onlyOwner {
        feesCollectorCutPerMillion = _feesCollectorCutPerMillion;

        require(
            feesCollectorCutPerMillion < 1000000,
            "Marketplace#setFeesCollectorCutPerMillion: FEES_MUST_BE_BETWEEN_0_AND_999999"
        );

        emit ChangedFeesCollectorCutPerMillion(feesCollectorCutPerMillion);
    }

    /**
     * @notice Set the fees collector
     * @param _newFeesCollector - fees collector
     */
    function setFeesCollector(address _newFeesCollector) public onlyOwner {
        require(_newFeesCollector != address(0), "Marketplace#setFeesCollector: INVALID_FEES_COLLECTOR");

        emit FeesCollectorSet(feesCollector, _newFeesCollector);
        feesCollector = _newFeesCollector;
    }

    /**
     * @notice Add new nft collection
     * @param nftCollection - nft address
     */
    function addNftCollection(address nftCollection) public onlyOwner {
        require(_nftCollectionList.add(nftCollection), "Marketplace#addNftCollection: INVALID_NFT_ADDRESS");
        emit NftCollectionAdded(nftCollection);
    }

    /**
     * @notice Add new nft collection
     * @param nftCollection - nft address
     */
    function removeNftCollection(address nftCollection) public onlyOwner {
        require(_nftCollectionList.remove(nftCollection), "Marketplace#removeNftCollection: INVALID_NFT_ADDRESS");
        emit NftCollectionRemoved(nftCollection);
    }

    function nftCollectionList() public view returns (address[] memory) {
        return _nftCollectionList.values();
    }

    /**
     * @dev Creates a new order
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order (in hours)
     */
    function createOrder(
        address nftAddress,
        uint256 assetId,
        uint256 priceInWei,
        uint256 expiresAt
    ) public payable whenNotPaused {
        _createOrder(nftAddress, assetId, priceInWei, expiresAt);
    }

    /**
     * @dev Cancel an already published order
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function cancelOrder(address nftAddress, uint256 assetId) public whenNotPaused {
        _cancelOrder(nftAddress, assetId);
    }

    /**
     * @dev Executes the sale for a published NFT
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     * @param price - Order price
     */
    function executeOrder(address nftAddress, uint256 assetId, uint256 price) public payable whenNotPaused {
        if (address(acceptedToken) == address(0)) price = msg.value;
        _executeOrder(nftAddress, assetId, price);
    }

    /**
     * @dev Creates a new order
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order
     */
    function _createOrder(address nftAddress, uint256 assetId, uint256 priceInWei, uint256 expiresAt) internal {
        _requireNFT(nftAddress);

        address sender = _msgSender();

        IPixelNFT nftRegistry = IPixelNFT(nftAddress);
        address assetOwner = nftRegistry.ownerOf(assetId);

        require(sender == assetOwner, "Marketplace#_createOrder: NOT_ASSET_OWNER");
        require(priceInWei > 0, "Price should be bigger than 0");
        require(expiresAt > block.timestamp + 1 minutes, "Marketplace#_createOrder: INVALID_EXPIRES_AT");

        bytes32 orderId = keccak256(abi.encodePacked(block.timestamp, assetOwner, assetId, nftAddress, priceInWei));

        orderByAssetId[nftAddress][assetId] = Order({
            id: orderId,
            seller: assetOwner,
            nftAddress: nftAddress,
            price: priceInWei,
            expiresAt: expiresAt
        });

        // Check if there's a publication fee and
        // transfer the amount to marketplace owner
        if (publicationFeeInWei > 0) {
            _transferFund(sender, feesCollector, publicationFeeInWei, "Marketplace#_createOrder: TRANSFER_FEE_FAILED");
        }

        // Transfer asset
        nftRegistry.transferFrom(sender, address(this), assetId);

        emit OrderCreated(orderId, assetId, assetOwner, nftAddress, priceInWei, expiresAt);
    }

    /**
     * @dev Cancel an already published order
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function _cancelOrder(address nftAddress, uint256 assetId) internal {
        address sender = _msgSender();
        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_cancelOrder: INVALID_ORDER");
        require(order.seller == sender || sender == owner(), "Marketplace#_cancelOrder: UNAUTHORIZED_USER");

        // Transfer asset
        IPixelNFT nftRegistry = IPixelNFT(nftAddress);
        nftRegistry.transferFrom(address(this), sender, assetId);

        bytes32 orderId = order.id;
        address orderSeller = order.seller;
        address orderNftAddress = order.nftAddress;
        delete orderByAssetId[nftAddress][assetId];

        emit OrderCancelled(orderId, assetId, orderSeller, orderNftAddress);
    }

    /**
     * @dev Executes the sale for a published NFT
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     * @param price - Order price
     */
    function _executeOrder(address nftAddress, uint256 assetId, uint256 price) internal {
        address sender = _msgSender();

        IPixelNFT nftRegistry = IPixelNFT(nftAddress);

        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_executeOrder: ASSET_NOT_FOR_SALE");

        require(order.seller != address(0), "Marketplace#_executeOrder: INVALID_SELLER");
        require(order.seller != sender, "Marketplace#_executeOrder: SENDER_IS_SELLER");
        require(order.price == price, "Marketplace#_executeOrder: PRICE_MISMATCH");
        require(block.timestamp < order.expiresAt, "Marketplace#_executeOrder: ORDER_EXPIRED");

        delete orderByAssetId[nftAddress][assetId];

        uint256 feesCollectorShareAmount = (price * feesCollectorCutPerMillion) / 1000000;

        // Fees collector share
        {
            if (feesCollectorShareAmount > 0) {
                _transferFund(
                    sender,
                    feesCollector,
                    feesCollectorShareAmount,
                    "Marketplace#_executeOrder: TRANSFER_FEES_TO_FEES_COLLECTOR_FAILED"
                );
            }
        }

        // Transfer sale amount to seller
        _transferFund(
            sender,
            order.seller,
            price - feesCollectorShareAmount,
            "Marketplace#_executeOrder: TRANSFER_AMOUNT_TO_SELLER_FAILED"
        );

        // Transfer asset owner
        nftRegistry.transferFrom(address(this), sender, assetId);

        emit OrderSuccessful(order.id, assetId, order.seller, nftAddress, price, sender);
    }

    function _requireNFT(address nftAddress) internal view {
        require(_nftCollectionList.contains(nftAddress), "Marketplace#_requireNFT: INVALID_NFT_ADDRESS");
    }

    function _transferFund(address from, address to, uint256 amount, string memory errMsg) internal {
        if (address(acceptedToken) == address(0)) {
            if (to == address(this)) {
                require(msg.value == amount, errMsg);
            } else {
                (bool sent, ) = payable(to).call{ value: amount }("");
                require(sent, errMsg);
            }
        } else {
            require(acceptedToken.transferFrom(from, to, amount), errMsg);
        }
    }
}
