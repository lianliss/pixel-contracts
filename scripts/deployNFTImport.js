const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 5 * 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {

  },
  songbird: {
    coreV5: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
  },
  flare: {

  },
  swisstest: {

  },
  skaletest: {
    coreV5: '0xF064c145bD903DFa7BEB0B79482263509Dc2F6d3',
  },
  skale: {
    nft: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    nftEditor: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
  },
  unit0: {
    nft: '0xF2c5eC1FEc65bdaf4b4e5028CbC25d022a5aF0bC',
    nftEditor: '0xb8ad5Ab21FC4bD1814D7bc8f5157D0E30C5efcb1',
  },
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    NFTMassImport: new ContractParams([
      () => old[networkName].nft,
      () => old[networkName].nftEditor,
    ]),
  };
  console.log('CONTRACTS', contracts);

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      //await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '/contracts#address-tabs');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
