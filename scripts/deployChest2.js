const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {
    core: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
    nft: '0x3cB1B33daF18c0fC9B60984031F05125500645fb',
    chest: '0x9D662b61886F397beC9bC78196A9d94610423834',
    slots: '0xD84e2811310C2B874BCe0CDB56697ee41d2E7eaA',
    quest: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
  },
  songbird: {
    core: '0x39838abf66af299401030e1B82be7848301FbB94',
    nft: '0xEd7D651E59f9acA6Aac165A6964Cb17514c0126e',
    chest: '0x8f4F9e4e9B377a3E4E4c5BE038c637fC8966FC1d',
    slots: '0xB6E6333B9637c1eF255FeC0dE6eCfe1063eca62f',
    quest: '0x85b19506C4F72ACDCdb7D08005960B59784766fF',
    PXLsCoreV5: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    PXLsNFTV5: '0x41a7435EF2CBd77df7C6966Af4E62a9B12416398',
    PXLsSlotsV5: '0xd820347ff282864e84a593ACF47E1FEBAE4268AF',
    multipool: '0x0f96cfB894F26bbC8b73C55139C968d9b8E2D777',
  },
  flare: {
    core: '0xf1F13Faa8c9EcD983f4776eEF5Cb63b966234926',
    nft: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    chest: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
    slots: '0xC8F4d52e4B87E3709b2af8D889E2081221B508AD',
    quest: '0xD33fc9AC0dF1B1A6177fed43Ee7aA910C6D9c95E',
  },
  swisstest: {
    core: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    nft: '0x177f380DA3be649836fE28571cA574f24602b8b6',
    chest: '0x3EF62f8C15594c87F42808968109F31d13000b4D',
    slots: '0x297c76FED3436d65cf4361686f63B4b8E5Ea4198',
    quest: '0x70F510E960d839aA0019Bb6Ee38Bc7ADc45B97FB',
  },
  skaletest: {
    core: '0xc3C8aF5Cf071d35a2AeF21eF2369D2ea28109886',
    nft: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
    chest: '0x3cB1B33daF18c0fC9B60984031F05125500645fb',
    slots: '0x9D662b61886F397beC9bC78196A9d94610423834',
    quest: '0x8F0Ce4a7bD41E9C59A74ba2185458CF67b4a327E',
  },
  skale: {
    PXLsCoreV5: '0x049eEE4028Fb4fF8591Ea921F15843Cd2d9e9f94',
    erc20: '0xE0595a049d02b7674572b0d59cd4880Db60EDC50',
    market: '0x9b9c4f527736fE53156199e36C6F4d7A8a195842',
    slotsV5: '0xDC056ba51981709E054F58310aE12B5164d200a4',
    PXLsNFTV5: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
  },
  unit0: {
    PXLsCoreV5: '0x10b641D9fa73f1564e8d52641aE72DEC5B13d7Bc',
    erc20: ZERO_ADDRESS,
    market: '0xA3D177F5B457e02048fAE06b854f686e5BFf939c',
    slotsV5: '0x6B92C9B89c51a81e0fc70c2A501C976cC168Af53',
    PXLsNFTV5: '0xF2c5eC1FEc65bdaf4b4e5028CbC25d022a5aF0bC',
  },
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    Multipool: new ContractParams([]),
    Chest2V5: new ContractParams([
      () => old[networkName].PXLsCoreV5,
      () => old[networkName].PXLsNFTV5,
      // () => old[networkName].multipool,
      () => contracts.Multipool.address,
    ]),
  };

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '#code');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
