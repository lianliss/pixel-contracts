const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 5 * 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {
    coreV5: '0xA40F6AF5A9b43E4E713A4e3f4d71C918933916a3',
  },
  songbird: {
    coreV5: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
  },
  flare: {
    coreV5: '0x033691A4Bef2A0e5638aFA98d527ec13D4BfDd4C',
  },
  swisstest: {
    coreV5: '0x8831d1AD3FA11c2A9DBdC88235eAd2b58c9FCA93',
  },
  skaletest: {
    coreV5: '0xF064c145bD903DFa7BEB0B79482263509Dc2F6d3',
  },
  skale: {
    coreV5: '0x049eEE4028Fb4fF8591Ea921F15843Cd2d9e9f94',
  },
  unit0: {
    coreV5: '0x10b641D9fa73f1564e8d52641aE72DEC5B13d7Bc',
  }
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    CoinBalances: new ContractParams([]),
    Balances: new ContractParams([]),
  };
  console.log('CONTRACTS', contracts);

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      //await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '/contracts#address-tabs');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
