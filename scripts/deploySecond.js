const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 5 * 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {

  },
  songbird: {
    slotsV5: '0xd820347ff282864e84a593ACF47E1FEBAE4268AF',
    coreV5: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    jade: '0xB7a4379C4041F58F18107BE46650517B0a161dE9',
  },
  flare: {

  },
  swisstest: {

  },
  skaletest: {

  },
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    // SecondMinerV5: new ContractParams([
    //   'JADE',
    //   'JADE Shard',
    //   '1000000000000000000000000',
    //   () => old[networkName].coreV5,
    // ]),
    // SecondMinerV5Migration: new ContractParams([
    //   () => contracts.SecondMinerV5.address,
    //   () => old[networkName].jade,
    //   () => old[networkName].coreV5,
    // ]),
    PXLsSlotsV5Settings: new ContractParams([
      () => old[networkName].slotsV5,
    ]),
  };
  console.log('CONTRACTS', contracts);

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      //await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '/contracts#address-tabs');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
