const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {
    core: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
    nft: '0x3cB1B33daF18c0fC9B60984031F05125500645fb',
    chest: '0x9D662b61886F397beC9bC78196A9d94610423834',
    slots: '0xD84e2811310C2B874BCe0CDB56697ee41d2E7eaA',
    quest: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
  },
  songbird: {
    core: '0x39838abf66af299401030e1B82be7848301FbB94',
    nft: '0xEd7D651E59f9acA6Aac165A6964Cb17514c0126e',
    chest: '0x8f4F9e4e9B377a3E4E4c5BE038c637fC8966FC1d',
    slots: '0xB6E6333B9637c1eF255FeC0dE6eCfe1063eca62f',
    quest: '0x85b19506C4F72ACDCdb7D08005960B59784766fF',
    PXLsCoreV5: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    PXLsSlotsV5: '0xd820347ff282864e84a593ACF47E1FEBAE4268AF',
  },
  flare: {
    core: '0xf1F13Faa8c9EcD983f4776eEF5Cb63b966234926',
    nft: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    chest: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
    slots: '0xC8F4d52e4B87E3709b2af8D889E2081221B508AD',
    quest: '0xD33fc9AC0dF1B1A6177fed43Ee7aA910C6D9c95E',
  },
  swisstest: {
    core: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    nft: '0x177f380DA3be649836fE28571cA574f24602b8b6',
    chest: '0x3EF62f8C15594c87F42808968109F31d13000b4D',
    slots: '0x297c76FED3436d65cf4361686f63B4b8E5Ea4198',
    quest: '0x70F510E960d839aA0019Bb6Ee38Bc7ADc45B97FB',
  },
  skaletest: {
    core: '0xc3C8aF5Cf071d35a2AeF21eF2369D2ea28109886',
    nft: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
    chest: '0x3cB1B33daF18c0fC9B60984031F05125500645fb',
    slots: '0x9D662b61886F397beC9bC78196A9d94610423834',
    quest: '0x8F0Ce4a7bD41E9C59A74ba2185458CF67b4a327E',
  },
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    PXLsCoreV5Math: new ContractParams(),
    PXLsCoreV5: new ContractParams([
      () => contracts.PXLsCoreV5Math.address,
    ]),
    PXLsCoreV5Settings: new ContractParams([
      () => contracts.PXLsCoreV5.address,
    ]),
    PXLsNFTV5: new ContractParams(),
    PXLsNFTV5Editor: new ContractParams([
      () => contracts.PXLsNFTV5.address,
    ]),
    // PXLsChestV5: new ContractParams(),
    // PXLsFreeChestV5: new ContractParams([
    //   () => contracts.PXLsCoreV5.address,
    //   () => contracts.PXLsChestV5.address,
    // ]),
    PXLsSlotsV5: new ContractParams(),
    PXLsSlotsV5Settings: new ContractParams([
      () => contracts.PXLsSlotsV5.address,
    ]),
    
    // PXLQuest: new ContractParams([]),
    
    // PXLsNFTV5Migration: new ContractParams([
    //   () => contracts.PXLsNFTV5.address,
    //   () => contracts.PXLsNFTV5Editor.address,
    //   () => old[networkName].nft,
    // ]),
    // PXLsChestV5Migration: new ContractParams([
    //   () => contracts.PXLsChestV5.address,
    //   () => old[networkName].chest,
    // ]),
    // PXLsSlotsV5Migration: new ContractParams([
    //   () => contracts.PXLsSlotsV5.address,
    //   () => old[networkName].slots,
    //   () => old[networkName].nft,
    //   () => contracts.PXLsCoreV5.address,
    //   () => contracts.PXLsNFTV5Migration.address,
    // ]),
    // PXLsCoreV5Migration: new ContractParams([
    //   () => contracts.PXLsCoreV5.address,
    //   () => old[networkName].core,
    //   () => contracts.PXLsSlotsV5Migration.address,
    // ]),
    SlotsCalculateSelf: new ContractParams([
      () => contracts.PXLsSlotsV5.address,
      () => contracts.PXLsCoreV5.address,
    ]),
  };

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '#code');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
