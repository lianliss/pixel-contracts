const hre = require("hardhat");
const fs = require("fs");

async function main() {
  const networkName = hre.network.name;

  const configPath = "./config.json";
  const config = JSON.parse(fs.readFileSync(configPath, "utf-8"));

  if (!config[networkName]) {
    config[networkName] = {};
  }

  console.log(`Deploying PixelDust contract to ${networkName}...`);

  const PixelDust = await hre.ethers.getContractFactory("PXLDust");

  const pixelDust = await PixelDust.deploy();
  await pixelDust.deployed();

  console.log(`PixelDust contract deployed at: ${pixelDust.address}`);

  config[networkName].pixelDustAddress = pixelDust.address;

  fs.writeFileSync(configPath, JSON.stringify(config, null, 2), "utf-8");
  console.log(`Updated configuration saved to ${configPath}`);
  
  await hre.run("verify:verify", {
    address: pixelDust.address,
    constructorArguments: args,
  });
  console.log("");
  console.log("Dust");
  console.log(pixelDust.address);
  console.log(customChain.urls.browserURL + "/address/" + pixelDust.address + '/contracts#address-tabs');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error("Deployment failed:", error);
    process.exit(1);
  });
