const hre = require("hardhat");
const fs = require("fs");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 5 * 1000));
}

const PARAMETER_ID_SLOT_0 = 19;
const PARAMETER_ID_SLOT_1 = 0;

async function main() {
  const networkName = hre.network.name;

  const configPath = "./config.json";
  const config = JSON.parse(fs.readFileSync(configPath, "utf-8"));

  if (!config[networkName]) {
    throw new Error(`No configuration found for network: ${networkName}`);
  }

  const {
    coreAddress,
    coreMathAddress,
    pixelDustAddress,
    pixelNftV5Address,
    slotsV5Address
  } = config[networkName];

  console.log(`Deploying Forge contract to ${networkName}...`);
  console.log(`Using addresses:
    PixelDust: ${pixelDustAddress}
    Core: ${coreAddress}
    CoreMath: ${coreMathAddress}
    Slots: ${slotsV5Address}
  `);

  const Forge = await hre.ethers.getContractFactory("MiningDrone");
  const args = [
    pixelDustAddress,
    coreAddress,
    coreMathAddress,
    slotsV5Address,
  ]
  const forge = await Forge.deploy(...args);

  await forge.deployed();

  console.log(`MiningDrone contract deployed at: ${forge.address}`);

  await hre.run("verify:verify", {
    address: forge.address,
    constructorArguments: args,
  });
  console.log("");
  console.log("MiningDrone");
  console.log(forge.address);
  console.log(customChain.urls.browserURL + "/address/" + forge.address + '/contracts#address-tabs');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error("Deployment failed:", error);
    process.exit(1);
  });
