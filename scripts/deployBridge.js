const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {
    core: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
    nft: '0x3cB1B33daF18c0fC9B60984031F05125500645fb',
    chest: '0x9D662b61886F397beC9bC78196A9d94610423834',
    slots: '0xD84e2811310C2B874BCe0CDB56697ee41d2E7eaA',
    quest: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
  },
  songbird: {
    core: '0x39838abf66af299401030e1B82be7848301FbB94',
    nft: '0xEd7D651E59f9acA6Aac165A6964Cb17514c0126e',
    chest: '0x8f4F9e4e9B377a3E4E4c5BE038c637fC8966FC1d',
    slots: '0xB6E6333B9637c1eF255FeC0dE6eCfe1063eca62f',
    quest: '0x85b19506C4F72ACDCdb7D08005960B59784766fF',
  },
  flare: {
    core: '0xf1F13Faa8c9EcD983f4776eEF5Cb63b966234926',
    nft: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    chest: '0xD6a42EDb99Bd9a3387CC37A86d3D27c34780b9B6',
    slots: '0xC8F4d52e4B87E3709b2af8D889E2081221B508AD',
    quest: '0xD33fc9AC0dF1B1A6177fed43Ee7aA910C6D9c95E',
  },
  swisstest: {
    core: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    nft: '0x177f380DA3be649836fE28571cA574f24602b8b6',
    chest: '0x3EF62f8C15594c87F42808968109F31d13000b4D',
    slots: '0x297c76FED3436d65cf4361686f63B4b8E5Ea4198',
    quest: '0x70F510E960d839aA0019Bb6Ee38Bc7ADc45B97FB',
  },
  skaletest: {
    core: '0xc3C8aF5Cf071d35a2AeF21eF2369D2ea28109886',
    nft: '0xa6FfF50C671023eCbd83F4a259bB0fDA20faEbC4',
    chest: '0x3cB1B33daF18c0fC9B60984031F05125500645fb',
    slots: '0x9D662b61886F397beC9bC78196A9d94610423834',
    quest: '0x8F0Ce4a7bD41E9C59A74ba2185458CF67b4a327E',
  },
  polygon: {
    meson: '0x25aB3Efd52e6470681CE037cD546Dc60726948D3',
  },
  skale: {
    meson: '0x529c2945c257CC737A15733d43a1fD1cBBe79C67',
  }
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    WrapToMesonBridge: new ContractParams([
      () => old[networkName].meson,
    ]),
  };

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '#code');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
