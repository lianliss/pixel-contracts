const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 5 * 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  coston2: {
    coreV5: '0xA40F6AF5A9b43E4E713A4e3f4d71C918933916a3',
    nftV5: '0x97765542584b0FcDd215026aDcd64923731336c4',
    erc20: ZERO_ADDRESS,
  },
  songbird: {
    coreV5: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    nftV5: '0x41a7435EF2CBd77df7C6966Af4E62a9B12416398',
    slotsV5: '0xd820347ff282864e84a593ACF47E1FEBAE4268AF',
    payableMath: '0xDbBBbc62345903D27fb66Cc7d6C457EeAbcc5Ba5',
    jade: '0xf4e4697Cc38f73B334daE3D41cAcAC489eFF3439',
    erc20: ZERO_ADDRESS,
  },
  flare: {
    coreV5: '0xA40F6AF5A9b43E4E713A4e3f4d71C918933916a3',
    nftV5: '0xc8b4454C681169B8Cf80E0EfF821ACD6C2b7C79D',
    erc20: ZERO_ADDRESS,
  },
  swisstest: {
    coreV5: '0x8831d1AD3FA11c2A9DBdC88235eAd2b58c9FCA93',
    nftV5: '0x52eeAe19a497291961Ffee91750D3a03106A12Bc',
    erc20: ZERO_ADDRESS,
  },
  skaletest: {
    coreV5: '0xF064c145bD903DFa7BEB0B79482263509Dc2F6d3',
    nftV5: '0x4DcBde5cD1CFF0667D7E9c6ab366d596a987fe2d',
    erc20: '0x6c71319b1F910Cf989AD386CcD4f8CC8573027aB',
  },
  skale: {
    coreV5: '0x049eEE4028Fb4fF8591Ea921F15843Cd2d9e9f94',
    erc20: '0xE0595a049d02b7674572b0d59cd4880Db60EDC50',
    market: '0x9b9c4f527736fE53156199e36C6F4d7A8a195842',
    slotsV5: '0xDC056ba51981709E054F58310aE12B5164d200a4',
    nftV5: '0xcB48dF8e2FE472D8Be277348683bBD401Cab6201',
    payableMath: '0x17399B64E191283B542B2aeb27bcDE0b909572C6',
    zargates: '0xbedcCEbB051AD2b8ED8bBEF2C87499278Aeb0345',
  },
  unit0: {
    coreV5: '0x10b641D9fa73f1564e8d52641aE72DEC5B13d7Bc',
    erc20: ZERO_ADDRESS,
    market: '0xA3D177F5B457e02048fAE06b854f686e5BFf939c',
    slotsV5: '0x6B92C9B89c51a81e0fc70c2A501C976cC168Af53',
    nftV5: '0xF2c5eC1FEc65bdaf4b4e5028CbC25d022a5aF0bC',
    zargates: '0xF1f63cbed6396A69112333B725Cc55612ab136a1',
  },
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    PayableMath: new ContractParams([]),
    PayableCore: new ContractParams([
      () => old[networkName].coreV5,
      () => contracts.PayableMath.address,
      () => old[networkName].slotsV5,
    ]),
    // PayableSecond: new ContractParams([
    //   () => old[networkName].jade,
    //   () => old[networkName].coreV5,
    //   () => old[networkName].payableMath,
    // ]),
    PayableFloat: new ContractParams([
      () => old[networkName].zargates,
      () => old[networkName].coreV5,
      () => contracts.PayableMath.address,
    ]),
    AutoclaimNFT: new ContractParams([
      () => old[networkName].coreV5,
      () => old[networkName].nftV5,
      () => contracts.PayableMath.address,
      // () => old[networkName].payableMath,
    ]),
    AutoclaimCraft: new ContractParams([
      () => old[networkName].coreV5,
      () => old[networkName].nftV5,
    ]),
    CoreV5Autoclaim: new ContractParams([
      () => old[networkName].coreV5,
    ]),
  };
  console.log('CONTRACTS', contracts);

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      //await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '/contracts#address-tabs');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
