const hre = require("hardhat");
const { Etherscan } = require("@nomicfoundation/hardhat-verify/etherscan");

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 1000));
}

class ContractParams {
  constructor(args = [], onDeploy = address => {}) {
    this.args = args;
    this.onDeploy = onDeploy;
  }
}

const old = {
  songbird: {
    v4Core: '0x39838abf66af299401030e1B82be7848301FbB94',
    v4Nft: '0xEd7D651E59f9acA6Aac165A6964Cb17514c0126e',
    v4Slots: '0xB6E6333B9637c1eF255FeC0dE6eCfe1063eca62f',
    v5Core: '0x7B3D7cfEe6BD2B3042B8F04D90b137D9Ed06Ea74',
    v5Nft: '0x41a7435EF2CBd77df7C6966Af4E62a9B12416398',
    v5Slots: '0xd820347ff282864e84a593ACF47E1FEBAE4268AF',
    coreMigration: '0xC841aD3749AedB307256B9EE2CCD88a9065fbc07',
    slotsMigration: '0xBd8B4F2CBc464454092F9adE584c6579877Fa39d',
    nftMigration: '0x36591b546Cb53B9f699be8A418922Bf70A7cc2C1',
  },
}

async function main() {

  let contracts;

  const networkName = hre.network.name;
  console.log("");
  console.log('DEPLOY TO', networkName);
  const customChain = hre.config
    .etherscan
    .customChains
    .find(chain => chain.network === networkName);

  contracts = {
    V5BackendMigration: new ContractParams([
      () => old[networkName].coreMigration,
      () => old[networkName].slotsMigration,
      () => old[networkName].nftMigration,
      () => old[networkName].v4Core,
      () => old[networkName].v4Slots,
      () => old[networkName].v4Nft,
      () => old[networkName].v5Core,
      () => old[networkName].v5Slots,
    ]),
  };

  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const args = (params.args || []).map(argument => {
      return typeof argument === 'function'
        ? argument()
        : argument;
    });
    params.args = args;

    const contract = await hre.ethers.deployContract(name, args);
    await contract.deployTransaction.wait();
    const address = contract.deployTransaction.creates;
    params.address = address;

    console.log(`${name} deployed to:`);
    console.log(address);
    try {
      if (!customChain) continue;
      await delay();
      await hre.run("verify:verify", {
        address,
        constructorArguments: args,
      });
      console.log("Contract verified to");
      console.log(customChain.urls.browserURL + "/address/" + address);
    } catch (error) {
      console.error("Error veryfing Contract. Reason:", error);
    }
    console.log("");
  }
  for (let i = 0; i < Object.keys(contracts).length; i++) {
    const name = Object.keys(contracts)[i];
    const params = contracts[name];
    const {address} = params;
    console.log("");
    console.log(name);
    console.log(address);
    console.log(customChain.urls.browserURL + "/address/" + address + '#code');
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
