const hre = require("hardhat");
const fs = require("fs");

const PARAMETER_ID_SLOT_0 = 19;
const PARAMETER_ID_SLOT_1 = 0;

async function main() {
  const networkName = hre.network.name;

  const configPath = "./config.json";
  const config = JSON.parse(fs.readFileSync(configPath, "utf-8"));

  if (!config[networkName]) {
    throw new Error(`No configuration found for network: ${networkName}`);
  }

  const {
    coreAddress,
    pixelDustAddress,
    pixelNftV5Address,
    slotsV5Address
  } = config[networkName];

  console.log(`Deploying Forge contract to ${networkName}...`);
  console.log(`Using addresses:
    Core: ${coreAddress}
    PixelDust: ${pixelDustAddress}
    BlendCollection: ${pixelNftV5Address}
    SlotCollection: ${pixelNftV5Address}
    CraftedCollection: ${pixelNftV5Address}
  `);

  const Forge = await hre.ethers.getContractFactory("Forge");
  const args = [
    coreAddress,
    pixelDustAddress,
    pixelNftV5Address,
    pixelNftV5Address,
    pixelNftV5Address,
  ]
  const forge = await Forge.deploy(...args);

  await forge.deployed();

  console.log(`Forge contract deployed at: ${forge.address}`);

  await hre.run("verify:verify", {
    address: forge.address,
    constructorArguments: args,
  });
  console.log("");
  console.log("Forge");
  console.log(forge.address);
  console.log(customChain.urls.browserURL + "/address/" + forge.address + '/contracts#address-tabs');

  // Grant all needed roles.
  const nftCollectionV5 = await hre.ethers.getContractAt("PXLsNFTV5", pixelNftV5Address)
  await forge.grantRole(await forge.SLOT_EDITOR_ROLE(), slotsV5Address);
  await nftCollectionV5.grantRole(await nftCollectionV5.MINTER_ROLE(), forge.address);
  await nftCollectionV5.grantRole(await nftCollectionV5.BURNER_ROLE(), forge.address);
  console.log(`All roles have been granted!`);

  // Setting up Forge contract
  await forge.setSlot0ParameterId(PARAMETER_ID_SLOT_0);
  await forge.setSlot1ParameterId(PARAMETER_ID_SLOT_1);
  console.log(`All slots have been set!`);

  config[networkName].forgeAddress = forge.address;

  fs.writeFileSync(configPath, JSON.stringify(config, null, 2), "utf-8");
  console.log(`Updated configuration saved to ${configPath}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error("Deployment failed:", error);
    process.exit(1);
  });
