require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require('hardhat-ignore-warnings');
require("hardhat-contract-sizer");
// require("hardhat-gas-reporter");
const accounts = require('../accounts');
const account = accounts.pixel;

const networks = {
  localhost: {
    url: "http://127.0.0.1:8545"
  },
  eth: {
    url: "https://rpc.ankr.com/eth/6c2f34a42715fa4c50762b0069a7a658618c752709b7db32f7bfe442741117eb",
    chainId: 1,
    gasPrice: 36000000000,
    accounts: [account.privateKey]
  },
  bsc: {
    url: "https://bsc-dataseed1.defibit.io/",
    chainId: 56,
    gasPrice: 20000000000,
    accounts: [account.privateKey]
  },
  polygon: {
    url: "https://polygon-rpc.com",
    chainId: 137,
    gasPrice: 140000000000,
    accounts: [account.privateKey]
  },
  arbitrum: {
    url: "https://arb1.arbitrum.io/rpc",
    chainId: 42161,
    gasPrice: 10000000,
    accounts: [account.privateKey]
  },
  mumbai: {
    url: "https://rpc-mumbai.maticvigil.com/",
    chainId: 80001,
    gasPrice: 20000000000,
    accounts: [account.privateKey]
  },
  test: {
    url: "https://bsc-testnet.web3api.com/v1/KBR2FY9IJ2IXESQMQ45X76BNWDAW2TT3Z3",
    chainId: 97,
    gasPrice: 11000000000,
    accounts: [account.privateKey]
  },
  songbird: {
    url: "https://rpc.ankr.com/flare_songbird",
    chainId: 19,
    gasPrice: 25000000000,
    accounts: [account.privateKey]
  },
  flare: {
    url: "https://rpc.ankr.com/flare/6f42bec11ea561bcb237c8f54c51be2012f0c8b26529d2364028c48b56ad1430",
    chainId: 14,
    gasPrice: 50000000000,
    accounts: [account.privateKey]
  },
  coston2: {
    url: "https://coston2-api.flare.network/ext/bc/C/rpc",
    chainId: 114,
    gasPrice: 50000000000,
    accounts: [account.privateKey]
  },
  skale: {
    url: "https://mainnet.skalenodes.com/v1/elated-tan-skat",
    chainId: 2046399126,
    gasPrice: 100000,
    accounts: [account.privateKey]
  },
  skaletest: {
    url: "https://testnet.skalenodes.com/v1/juicy-low-small-testnet",
    chainId: 1444673419,
    gasPrice: 10000000000,
    accounts: [account.privateKey]
  },
  swisstest: {
    url: "https://testnet-swisstronik-evm.konsortech.xyz",
    chainId: 1291,
    gasPrice: 8,
    accounts: [account.privateKey]
  },
  unit0: {
    url: "https://rpc.unit0.dev",
    chainId: 88811,
    gasPrice: 1600000000,
    accounts: [account.privateKey]
  }
};

module.exports = {
  solidity: {
    version: "0.8.24",
    settings: {
      viaIR: false,
      optimizer: {
        enabled: true,
        runs: 1
      },
      evmVersion: 'paris',
    }
  },
  networks: networks,
  etherscan: {
    enabled: true,
    apiKey: accounts.arbitrum,
    customChains: [
      {
        network: "polygon",
        chainId: 137,
        urls: {
          apiURL: "https://api.polygonscan.com/api",
          browserURL: "https://polygonscan.com"
        }
      },
      {
        network: "arbitrum",
        chainId: 42161,
        urls: {
          apiURL: "https://api.arbiscan.io/api",
          browserURL: "https://arbiscan.io/"
        }
      },
      {
        network: "songbird",
        chainId: 19,
        urls: {
          apiURL: "https://songbird-explorer.flare.network/api",
          browserURL: "https://songbird-explorer.flare.network"
        }
      },
      {
        network: "flare",
        chainId: 14,
        urls: {
          apiURL: "https://flare-explorer.flare.network/api",
          browserURL: "https://flare-explorer.flare.network"
        }
      },
      {
        network: "coston2",
        chainId: 114,
        urls: {
          apiURL: "https://coston2-explorer.flare.network/api",
          browserURL: "https://coston2-explorer.flare.network"
        }
      },
      {
        network: "skale",
        chainId: 2046399126,
        urls: {
          apiURL: "https://elated-tan-skat.explorer.mainnet.skalenodes.com/api",
          browserURL: "https://elated-tan-skat.explorer.mainnet.skalenodes.com"
        }
      },
      {
        network: "skaletest",
        chainId: 1444673419,
        urls: {
          apiURL: "https://juicy-low-small-testnet.explorer.testnet.skalenodes.com/api",
          browserURL: "https://juicy-low-small-testnet.explorer.testnet.skalenodes.com"
        }
      },
      {
        network: "swisstest",
        chainId: 1291,
        urls: {
          apiURL: "https://explorer-evm.testnet.swisstronik.com/api",
          browserURL: "https://explorer-evm.testnet.swisstronik.com"
        }
      },
      {
        network: "unit0",
        chainId: 88811,
        urls: {
          apiURL: "https://explorer.unit0.dev/api",
          browserURL: "https://explorer.unit0.dev"
        }
      },
    ]
  }
};
