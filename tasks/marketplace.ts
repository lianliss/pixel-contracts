import { task } from "hardhat/config";
import type { TaskArguments } from "hardhat/types";

task("task:deployMarketplace", "Deploys Marketplace Contract")
  .addParam("owner", "Owner")
  .addParam("acceptedToken", "Address of the ERC20 accepted for this marketplace")
  .addParam("feesCollector", "Fees collector")
  .addParam("feesCollectorCutPerMillion", "Fees collector cut per million")
  .setAction(async function (taskArguments: TaskArguments, { ethers, run }) {
    const signers = await ethers.getSigners();
    const marketplaceFactory = await ethers.getContractFactory("Marketplace");
    console.log(`Deploying Marketplace...`);
    const marketplace = await marketplaceFactory
      .connect(signers[0])
      .deploy(
        taskArguments.owner,
        taskArguments.acceptedToken,
        taskArguments.feesCollector,
        taskArguments.feesCollectorCutPerMillion,
      );
    await marketplace.waitForDeployment();
    console.log("Marketplace deployed to: ", await marketplace.getAddress());
    await run("verify:verify", {
      address: await marketplace.getAddress(),
      constructorArguments: [
        taskArguments.owner,
        taskArguments.acceptedToken,
        taskArguments.feesCollector,
        taskArguments.feesCollectorCutPerMillion,
      ],
    });
  });

task("task:deploy", "Deploys NFT Contract")
  .addParam("name")
  .addParam("symbol")
  .setAction(async function (taskArguments: TaskArguments, { ethers, run }) {
    const signers = await ethers.getSigners();
    const mockERC721Factory = await ethers.getContractFactory("MockERC721");
    console.log(`Deploying NFT...`);
    const nft = await mockERC721Factory.connect(signers[0]).deploy(taskArguments.name, taskArguments.symbol);
    await nft.waitForDeployment();
    console.log("Marketplace deployed to: ", await nft.getAddress());
    await run("verify:verify", {
      address: await nft.getAddress(),
      constructorArguments: [taskArguments.name, taskArguments.symbol],
    });
  });
