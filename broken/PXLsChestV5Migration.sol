//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Chest/ChestV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";

struct OldChest {
    uint[][RARITY_COUNT][] items;
    uint[RARITY_COUNT] ratios;
}

struct OldMarketItem {
    uint chestId;
    uint count;
    uint pixelPrice;
    uint sgbPrice;
}

interface IPXLChest {
    function getChests() external view returns (OldChest[] memory);
    function getMarket() external view returns (OldMarketItem[] memory);
    function getChestsCount() external view returns (uint);
    function getChest(uint chestId) external view returns (OldChest memory);
}

/// @title Pixel Shard Slots Migration
/// @author Danil Sakhinov
/// @notice Required PXLsChestV5 role PROXY_ROLE
contract PXLsChestV5Migration is ChestV5Context, ProxyImplementation {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    IPXLChest private _oldChest;
    bool public migrated;

    /// @param chestAddress PXLsChestV5 new
    /// @param oldChestAddress PXLChest old
    constructor(
        address chestAddress,
        address oldChestAddress
        ) ProxyImplementation(chestAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        _oldChest = IPXLChest(oldChestAddress);
    }

    /// Internal methods

    // function migrateChests(OldChest[] memory chests) internal {
    //     for (uint chestId; chestId < chests.length; chestId++) {
    //         OldChest memory chest = chests[chestId];
    //         _chests.push(Chest(
    //             chest.items,
    //             chest.ratios,
    //             chestId,
    //             0,
    //             0,
    //             ""
    //         ));
    //     }
    // }

    // function migrateMarket(OldMarketItem[] memory marketItems) internal {
    //     for (uint marketId; marketId < marketItems.length; marketId++) {
    //         OldMarketItem memory market = marketItems[marketId];
    //         _market.push(MarketItem(
    //             marketId,
    //             market.chestId,
    //             market.count,
    //             market.pixelPrice,
    //             market.sgbPrice
    //         ));
    //     }
    // }

    /// Proxy methods

    function xMigrate(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        (bool isProxy,) = routedDelegate("xMigrate(bytes)", encoded);
        if (isProxy) {
            (
                address oldChestAddress
            ) = abi.decode(encoded, (address));
            // IPXLChest old = IPXLChest(oldChestAddress);

            // for (uint chestId; chestId < old.getChestsCount(); chestId++) {
            //     OldChest memory chest = old.getChest(chestId);
            // }
            //migrateMarket(old.getMarket());
        }
    }

    function migrate() public onlyRole(BACKEND_ROLE) {
        require(!migrated, "Already migrated");

        _oldChest.getChest(0);

        // for (uint chestId; chestId < _oldChest.getChestsCount(); chestId++) {
        //     OldChest memory chest = _oldChest.getChest(chestId);
        // }
        // xMigrate(abi.encode(
        //     address(_oldChest)
        // ));
    }

}