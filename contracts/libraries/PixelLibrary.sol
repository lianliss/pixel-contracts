//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

uint8 constant PARAM_SPEED_MUL = 0;
uint8 constant PARAM_SIZE_MUL = 1;
uint8 constant PARAM_REF_MUL = 2;
uint8 constant PARAM_SPEED_ADDER = 3;
uint8 constant PARAM_SIZE_ADDER = 4;
uint8 constant PARAM_REF_ADDER = 5;
uint8 constant PARAM_SPEED_PRICE = 6;
uint8 constant PARAM_SIZE_PRICE = 7;
uint8 constant PARAM_REF_PRICE = 8;
uint8 constant PARAM_RARITY_PIXEL = 9;
uint8 constant PARAM_RARITY_COMMON = 10;
uint8 constant PARAM_RARITY_UNCOMMON = 11;
uint8 constant PARAM_RARITY_RARE = 12;
uint8 constant PARAM_RARITY_EPIC = 13;
uint8 constant PARAM_RARITY_LEGENDARY = 14;
uint8 constant PARAM_CHANCE = 15; // Drop chance action
uint8 constant PARAM_MINING_MULT = 16; // Mining Multiplicator

uint8 constant RARITY_COUNT = 6;
uint8 constant RARITY_PIXEL = 0;
uint8 constant RARITY_COMMON = 1;
uint8 constant RARITY_UNCOMMON = 2;
uint8 constant RARITY_RARE = 3;
uint8 constant RARITY_EPIC = 4;
uint8 constant RARITY_LEGENDARY = 5;

uint8 constant PERCENT_DECIMALS = 4;
uint constant PERCENT_PRECISION = 10**PERCENT_DECIMALS;