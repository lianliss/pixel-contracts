//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/Proxy/Proxy.sol";
import "contracts/Proxy/ProxyImplementation.sol";

interface IProxyContext is IProxy {
    
    function publicCall() external;
}

contract ProxyContext is IProxyContext, Proxy {
    uint public storedId;

    event ContextCall();
    event ContextCallPublic();

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    function publicCall() public {
        emit ContextCallPublic();
    }
}

contract Logic is ProxyImplementation {
    uint public storedId;
    
    event This(address sender, address current);
    event Set(uint newValue);
    event LogicCall();
    event LogicCallPublic();

    uint64 public comeVar = 4444;

    constructor(address proxyAddress) ProxyImplementation(proxyAddress) {
    }

    function privateCall() private {
        emit LogicCall();
    }

    function publicCall() public {
        emit LogicCallPublic();
    }

    function getSomeVar() private view returns (uint64) {
        return comeVar;
    }

    function _update(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xUpdate(bytes)", encoded);
        if (isProxy) {
            (uint64 newId) = abi.decode(encoded, (uint64));
            storedId = newId;
            emit Set(storedId);
            privateCall();
            IProxyContext core = IProxyContext(address(this));
            core.publicCall();
        }
    }
    function xUpdate(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _update(encoded);
    }
    function update(uint64 newId) public {
        bytes memory encoded = abi.encode(newId);
        _update(encoded);
        comeVar = newId;
    }
    
}