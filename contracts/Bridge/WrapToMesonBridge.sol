//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";

interface IMesonMinimal {
    function tokenForIndex(uint8 tokenIndex) external view returns (address token);
    function postSwapFromContract(uint256 encodedSwap, uint200 postingValue, address fromContract)
        payable external;
}

/// @notice Wrap for Meson bridge with extra fee
contract WrapToMesonBridge is AccessControl {

    IMesonMinimal public meson;
    address public feeReceiver;
    uint constant public FEE_DECIMALS = 6;
    uint public feeSize = 10**FEE_DECIMALS;

    constructor(address mesonAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        meson = IMesonMinimal(mesonAddress);
        feeReceiver = _msgSender();
    }

    event FeeSent(address indexed from, address indexed token, address indexed feeReceiver, uint fromAmount, uint fee);
    event FeeReceiverSet(address indexed newReceiver);
    event FeeSizeSet(uint newFeeSize);
    event MesonSet(address mesonAddress);

    function setFeeReceiver(address newReceiver) public onlyRole(DEFAULT_ADMIN_ROLE) {
        feeReceiver = newReceiver;
        emit FeeReceiverSet(newReceiver);
    }

    /// @notice Set extra fee size with 6 digits of decimals
    function setFeeSize(uint newFeeSize) public onlyRole(DEFAULT_ADMIN_ROLE) {
        feeSize = newFeeSize;
        emit FeeSizeSet(newFeeSize);
    }

    function setMesonAddress(address newMesonAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        meson = IMesonMinimal(newMesonAddress);
        emit MesonSet(newMesonAddress);
    }

    function transferToMeson(uint256 encodedSwap, address initiator) payable external {
        address tokenAddr = getSwapTokenAddress(encodedSwap);
        require(tokenAddr != address(0), "Unsupported token");

        uint200 postingValue = (uint200(uint160(initiator)) << 40) + 1;

        if (tokenAddr == address(0x1)) {
            // Core tokens (the token used to pay gas fees)
            uint256 amount = getSwapAmountFrom(encodedSwap, 18);
            uint fee = getTokenFee(18);
            require(amount + fee <= msg.value, "Tx value does not match the amount");
            (bool sent,) = feeReceiver.call{value: fee}("");
            require(sent, "Can't send coins");
            emit FeeSent(_msgSender(), tokenAddr, feeReceiver, amount, fee);

            meson.postSwapFromContract{value: amount}(encodedSwap, postingValue, address(this));
        } else {
            // ERC20 tokens
            IERC20Metadata tokenContract = IERC20Metadata(tokenAddr);

            uint8 tokenDecimals = tokenContract.decimals();
            uint256 amount = getSwapAmountFrom(encodedSwap, tokenDecimals);

            // Get additional fee
            uint fee = getTokenFee(tokenDecimals);
            require(tokenContract.balanceOf(_msgSender()) >= amount + fee, "Not enough funds");
            tokenContract.transferFrom(_msgSender(), feeReceiver, fee);
            emit FeeSent(_msgSender(), tokenAddr, feeReceiver, amount, fee);

            // Option to transfer tokens directly from user's address
            tokenContract.transferFrom(_msgSender(), address(this), amount);
            tokenContract.approve(address(meson), amount);

            meson.postSwapFromContract(encodedSwap, postingValue, address(this));
        }
    }

    function getSwapAmountFrom(uint256 encodedSwap, uint8 tokenDecimals) public pure returns (uint256 amount) {
        uint256 amountInMeson = (encodedSwap >> 208) & 0xFFFFFFFFFF;

        if (tokenDecimals == 6) {
            amount = amountInMeson;
        } else if (tokenDecimals >= 6) {
            amount = amountInMeson * 10 ** (tokenDecimals - 6);
        } else {
            require(amountInMeson % (10 ** (6 - tokenDecimals)) == 0, "Decimals overflow");
            amount = amountInMeson / 10 ** (6 - tokenDecimals);
        }
    }

    function getSwapTokenIndex(uint256 encodedSwap) public pure returns (uint8) {
        return uint8(encodedSwap);
    }

    function getSwapTokenAddress(uint256 encodedSwap) public view returns (address) {
        uint8 tokenIndex = getSwapTokenIndex(encodedSwap);
        return meson.tokenForIndex(tokenIndex);
    }

    function getTokenFee(uint8 tokenDecimals) public view returns (uint256) {
        return feeSize * 10**tokenDecimals / 10**FEE_DECIMALS;
    }
}