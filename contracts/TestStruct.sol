//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

contract TestStruct {

    event One();
    event Two();

}