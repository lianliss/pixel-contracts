//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPXLSlots {

    struct SlotType {
        string title;
        uint sgbPrice;
        uint pixelPrice;
        uint8 requireSlot;
    }

    struct Slot {
        uint tokenId;
        uint variantId;
        bool isEnabled;
    }

    event Recieved(address indexed account, uint amount);
    event SlotUnlocked(uint64 indexed userId, uint slotTypeId);

    function setRegisteredParameters(uint8 amount) external;
    function getRegisteredParameters() external view returns (uint8);
    function setReceiver(address payable account) external;
    function setCoreAddress(address newAddress) external;
    function setChestAddress(address newAddress) external;
    function setNFTAddress(address newAddress) external;

    function getCustomReceiver(uint8 parameterId) external view returns (address);
    function getSlotsTypes() external view returns (SlotType[] memory);
    function getUserSlots(uint64 userId) external view returns (Slot[] memory);
    function unlockSlot(uint8 slotTypeId) external;
    function unlockSlotSGB(uint8 slotTypeId) external payable;
    function equip(uint tokenId, uint8 slotTypeId, uint variantId) external;
    function unequip(uint8 slotTypeId) external;

}