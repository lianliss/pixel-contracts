//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct SecondStorage {
    uint claimTimestamp;
    uint claimed;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    bool disabled;
}

interface IPXLSecondMiner {

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Claim(uint64 indexed id, address account, uint amount);
    event Burn(uint64 indexed id, address account, uint amount);
    event BurnTotalSupply(address account, uint amount);
    event ReferReward(uint64 indexed claimerId, uint64 indexed id, uint64 indexed child, address account, uint amount);
    event SetAccessController(address controller);
    event SetBurner(address controller);
    event SetClaimsDisabled(address caller, bool value);
    event SetRefersDisabled(address caller, bool value);
    event SetStorageDisabled(uint64 indexed id, address caller, bool value);
    event Reinvest(uint64 indexed id, address account, uint value);

}