//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/libraries/PixelLibrary.sol";

struct Chest {
    uint[][RARITY_COUNT][] items;
    uint[RARITY_COUNT] ratios;
}

struct MarketItem {
    uint chestId;
    uint count;
    uint pixelPrice;
    uint sgbPrice;
}

interface IPXLChest {
    event ItemDrop(address indexed account, uint indexed chestId, uint indexed typeId, uint tokenId);
    event BonusDrop(address indexed account, uint64 indexed userId, uint amount);
    event BuyChestPixel(address indexed account, uint indexed marketItem, uint indexed chestId, uint price);
    event BuyChestSGB(address indexed account, uint indexed marketItem, uint indexed chestId, uint price);
    event SendChest(address indexed account, uint indexed chestId);
    
    function setUserChances(uint64 userId, uint24[RARITY_COUNT] calldata chances) external;
    function sendChest(uint chestId, address account) external;
    function getChestsCount() external view returns (uint);
    function addChestItem(uint chestIndex, uint slotIndex, uint rarity, uint typeId) external returns (uint);
}