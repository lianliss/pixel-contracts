//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct Storage {
    address account;
    uint64 parent;
    bool disabled;
    uint claimTimestamp;
    uint claimed;
    uint refStorage;
    uint8 sizeLevel;
    uint8 speedLevel;
    uint8 referralLevel;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint24 referralMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    uint256 referralAdder;
    uint24 speedPrice;
    uint24 sizePrice;
    uint24 referralPrice;
    uint activity;
}

interface IPXLsCoreV4 {

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Claim(uint64 indexed id, address account, uint amount);
    event Burn(uint64 indexed id, address account, uint amount);
    event BurnTotalSupply(address account, uint amount);
    event ReferReward(uint64 indexed claimerId, uint64 indexed id, uint64 indexed child, address account, uint amount);
    event LevelUpSize(uint64 indexed id, address account, uint level, uint price);
    event LevelUpSpeed(uint64 indexed id, address account, uint level, uint price);
    event LevelUpReferral(uint64 indexed id, address account, uint level, uint price);
    event GrantAccess(uint64 indexed id, address account, address controller);
    event SetParent(uint64 indexed id, uint64 indexed parent);
    event SetAccessController(address controller);
    event SetBurner(address controller);
    event SetClaimsDisabled(address caller, bool value);
    event SetRefersDisabled(address caller, bool value);
    event SetStorageDisabled(uint64 indexed id, address caller, bool value);
    event Reinvest(uint64 indexed id, address account, uint value);

    function getStorage(uint64 id) external view returns (
        Storage memory user,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
        );

    function getAddressId(address account) external view returns (uint64);
    function getIdAddress(uint64 id) external view returns (address);
    function mintForOne(uint64 id, uint amount) external;
    function burn(address account, uint amount) external;
    function setMultipliers(uint64 id, uint24[3] calldata mult, uint256[3] calldata add, uint24[3] calldata price) external;
}