//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";
import "contracts/V4/interfaces/IPixelNFT.sol";

contract PXLDrill is AccessControl {

    bytes32 public constant SETTER_ROLE = keccak256("SETTER_ROLE");
    uint[] private _key = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    string[] private _numerals = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

    string public tokenURI = "https://gateway.dedrive.io/v1/access/c93ec587c42a4d94807233abf3e1df06";
    uint public collectionId = 4;

    IPXLsCoreV4 private _core;
    IPixelNFT private _nft;

    mapping(uint drillLevel => uint itemType) private _levels;
    mapping(uint64 userId => bool isReceived) public recieved;

    event ReceiveDrill(address indexed account, uint indexed itemTypeId);

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(SETTER_ROLE, _msgSender());
    }

    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV4(newAddress);
    }

    function setNFTAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _nft = IPixelNFT(newAddress);
    }

    function getRomanNumber(uint n) public view returns (string memory) {
        string memory res = "";
        for (uint i = 0; i < _key.length; i++) {
            while (n >= _key[i]) {
                n -= _key[i];
                res = string.concat(res, _numerals[i]);
            } 
        }
        return res;
    }

    function _getLevelItemType(uint8 drillLevel) internal returns (uint) {
        if (_levels[drillLevel] > 0) {
            return _levels[drillLevel];
        }
        uint itemTypeId = _nft.getTypesLength();
        string memory typeName = string.concat("Drill ", getRomanNumber(uint(drillLevel)));
        uint8 rarity = drillLevel / 10 + 1;
        if (rarity > 5) {
            rarity = 5;
        }
        uint8[] memory slots = new uint8[](1);
        slots[0] = 6;
        uint8[] memory parameters = new uint8[](1);
        parameters[0] = 0;
        uint24[] memory multipliers = new uint24[](1);
        multipliers[0] = uint24(drillLevel) * 100 + 10**4;
        uint256[] memory adductors = new uint256[](1);
        adductors[0] = 0;

        _nft.createType(typeName, tokenURI, slots, collectionId, rarity);
        _nft.addVariant(itemTypeId, parameters, multipliers, adductors);
        _levels[drillLevel] = itemTypeId;
        return itemTypeId;
    }

    function getLevelItemType(uint8 drillLevel) public onlyRole(SETTER_ROLE) returns (uint) {
        return _getLevelItemType(drillLevel);
    }

    function sendDrill(uint64 userId, uint8 drillLevel) public onlyRole(SETTER_ROLE) {
        address account = _core.getIdAddress(userId);
        require(userId > 0, "Unknown user");
        require(account != address(0), "Unauthorized user");
        require(!recieved[userId], "Drill already received");

        require(drillLevel > 0, "Drill level must not equal zero");
        uint typeId = _getLevelItemType(drillLevel);
        _nft.mint(account, typeId);
        recieved[userId] = true;
        emit ReceiveDrill(account, typeId);
    }
    
}