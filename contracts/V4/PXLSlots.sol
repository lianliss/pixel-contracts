//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V4/interfaces/IPixelNFT.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";
import "contracts/V4/interfaces/IPXLChest.sol";
import "contracts/V4/interfaces/IParameterReceiver.sol";
import "contracts/V4/interfaces/IPXLSlots.sol";

contract PXLSlots is IPXLSlots, AccessControl {

    uint8 private _registeredParameters = 16;
    mapping (uint8 parameterId => IParameterReceiver) private _customReceivers;
    SlotType[] private _slotTypes;
    mapping (uint64 userId => mapping(uint8 slotTypeId => Slot slot)) private _slots;
    mapping (uint64 userId => mapping(uint8 slotTypeId => mapping(uint8 parameterId => Modifier))) private _slotsMods;
    address payable private _receiver;
    IPXLsCoreV4 private _core;
    IPXLChest private _chest;
    IPixelNFT private _nft;
    IERC20 private _USDT;
    bool private _isUseUSDT = false;

    IPXLSlots[] private _oldContracts;

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _receiver = payable(_msgSender());

        createSlot("Pocket 1", 15*10**18, 5*10**18, 0); // 0
        createSlot("Pocket 2", 30*10**18, 10*10**18, 0); // 1
        createSlot("Backpack", 45*10**18, 15*10**18, 1); // 2
        createSlot("Pocket 3", 60*10**18, 20*10**18, 2); // 3
        createSlot("Belt", 75*10**18, 25*10**18, 3); // 4
        createSlot("Pocket 4", 90*10**18, 30*10**18, 4); // 5
        createSlot("Artifact", 0, 1*10**18, 0); // 6

        _oldContracts.push(IPXLSlots(0x9123f4C946029810b7606912D68ad81BFEBf1e7A));
        _oldContracts.push(IPXLSlots(0x1F120F6A888C068F7fE8190a4b47C00F748F952B));
    }

    receive() external payable {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
    }

    function setUSDT(address usdt) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _USDT = IERC20(usdt);
    }

    function setUseUSDT(bool isUse) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _isUseUSDT = isUse;
    }

    function setRegisteredParameters(uint8 amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _registeredParameters = amount;
    }

    function getRegisteredParameters() public view returns (uint8) {
        return _registeredParameters;
    }

    function setReceiver(address payable account) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _receiver = account;
    }

    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV4(newAddress);
    }

    function setChestAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _chest = IPXLChest(newAddress);
    }

    function setNFTAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _nft = IPixelNFT(newAddress);
    }

    function getCustomReceiver(uint8 parameterId) public view returns (address) {
        return address(_customReceivers[parameterId]);
    }

    function createSlot(string memory title, uint sgbPrice, uint pixelPrice, uint8 requireSlot) public onlyRole(EDITOR_ROLE) {
        SlotType storage newSlot = _slotTypes.push();
        newSlot.title = title;
        newSlot.sgbPrice = sgbPrice;
        newSlot.pixelPrice = pixelPrice;
        newSlot.requireSlot = requireSlot;
    }

    function updateSlot(uint slotTypeId, string calldata title, uint sgbPrice, uint pixelPrice, uint8 requireSlot) public onlyRole(EDITOR_ROLE) {
        SlotType storage slotType = _slotTypes[slotTypeId];
        slotType.title = title;
        slotType.sgbPrice = sgbPrice;
        slotType.pixelPrice = pixelPrice;
        slotType.requireSlot = requireSlot;
    }

    function setParameterReceiver(uint8 parameterId, address receiver) public onlyRole(EDITOR_ROLE) {
        _customReceivers[parameterId] = IParameterReceiver(receiver);
    }

    function getSlotsTypes() public view returns (SlotType[] memory) {
        return _slotTypes;
    }

    function getUserSlots(uint64 userId) public view returns (Slot[] memory) {
        Slot[] memory slots = new Slot[](_slotTypes.length);
        for (uint8 slotTypeId; slotTypeId < _slotTypes.length; slotTypeId++) {
            slots[slotTypeId] = _slots[userId][slotTypeId];
        }
        return slots;
    }

    function unlockSlot(uint8 slotTypeId) public {
        uint64 id = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(id) == _msgSender(), "Not authorized");
        Slot storage slot = _slots[id][slotTypeId];
        require(!slot.isEnabled, "Slot is already unlocked");
        SlotType storage slotType = _slotTypes[slotTypeId];
        require(slotTypeId == 0
        || slotType.requireSlot == 0
        || _slots[id][slotType.requireSlot].isEnabled == true, "Required slot is not unlocked");
        if (slotType.pixelPrice > 0) {
            _core.burn(_msgSender(), slotType.pixelPrice);
            slot.isEnabled = true;
            emit SlotUnlocked(id, slotTypeId);
        } else {
            revert("Slot is unreachable");
        }
    }

    function unlockSlotSGB(uint8 slotTypeId) public payable {
        uint64 id = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(id) == _msgSender(), "Not authorized");
        Slot storage slot = _slots[id][slotTypeId];
        require(!slot.isEnabled, "Slot is already unlocked");
        SlotType storage slotType = _slotTypes[slotTypeId];
        require(slotTypeId == 0
        || slotType.requireSlot == 0
        || _slots[id][slotType.requireSlot].isEnabled == true, "Required slot is not unlocked");
        if (slotType.sgbPrice > 0) {
            if (_isUseUSDT) {
                require(_USDT.transferFrom(
                    _msgSender(),
                    _receiver,
                    slotType.sgbPrice
                ), "Can't transfer USDT");
                emit Recieved(_msgSender(), slotType.sgbPrice);
            } else {
                require(msg.value >= slotType.sgbPrice, "Not enough SGB");
                (bool sent,) = _receiver.call{value: msg.value}("");
                require(sent, "Failed to send Ether");
                emit Recieved(_msgSender(), msg.value);
            }
            slot.isEnabled = true;
            emit SlotUnlocked(id, slotTypeId);
        } else {
            revert("Slot is unreachable");
        }
    }

    function _equip(uint tokenId, uint8 slotTypeId, uint variantId, uint64 id, address account) private {
        Slot storage slot = _slots[id][slotTypeId];
        require(slot.isEnabled, "Slot is not enabled");
        require(slot.tokenId == 0, "The slot is already occupied by the item");
        require(_nft.ownerOf(tokenId) == account, "Not an item owner");
        ItemType memory itemType = _nft.getTokenType(tokenId);
        require(variantId < itemType.variants.length, "Unreachable variant");
        uint8 itemSlotIndex;
        while (itemSlotIndex < itemType.slots.length
        && itemType.slots[itemSlotIndex] != slotTypeId) {
            itemSlotIndex++;
        }
        require(itemSlotIndex < itemType.slots.length, "Wrong slot");
        // Transfer item
        _nft.transferFrom(account, address(this), tokenId);
        // Fill slot
        slot.tokenId = tokenId;
        slot.variantId = variantId;
        // Save modifier
        Modifier[] memory variant = itemType.variants[variantId];
        for (uint modIndex; modIndex < variant.length; modIndex++) {
            Modifier memory mod = itemType.variants[variantId][modIndex];
            _slotsMods[id][slotTypeId][mod.parameterId] = mod;
        }
        // Calculate with other slots
        _calculate(id);
    }

    function equip(uint tokenId, uint8 slotTypeId, uint variantId) public {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");
        _equip(tokenId, slotTypeId, variantId, id, account);
    }

    function equipPayable(uint tokenId, uint8 slotTypeId, uint variantId) public payable {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");

        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);

        _equip(tokenId, slotTypeId, variantId, id, account);
    }

    function equip(uint tokenId, uint8 slotTypeId, uint variantId, uint64 id) public onlyRole(EDITOR_ROLE) {
        address account = _core.getIdAddress(id);
        _equip(tokenId, slotTypeId, variantId, id, account);

    }

    function _unequip(uint8 slotTypeId, uint64 id, address account) private {
        Slot storage slot = _slots[id][slotTypeId];
        require(slot.tokenId > 0 && _nft.ownerOf(slot.tokenId) == address(this), "The slot is empty");
        ItemType memory itemType = _nft.getTokenType(slot.tokenId);
        uint variantId = slot.variantId;
        // Transfer item
        _nft.transferFrom(address(this), account, slot.tokenId);
        // Clear slot
        slot.tokenId = 0;
        slot.variantId = 0;
        // Clear modifier
        Modifier[] memory variant = itemType.variants[variantId];
        for (uint modIndex; modIndex < variant.length; modIndex++) {
            uint8 parameterId = itemType.variants[variantId][modIndex].parameterId;
            delete _slotsMods[id][slotTypeId][parameterId];
        }
        // Calculate with other slots
        _calculate(id);
    }

    function unequip(uint8 slotTypeId) public {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");
        _unequip(slotTypeId, id, account);
    }

    function unequipPayable(uint8 slotTypeId) public payable {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");

        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
        
        _unequip(slotTypeId, id, account);
    }

    function unequip(uint8 slotTypeId, uint64 id) public onlyRole(EDITOR_ROLE) {
        address account = _core.getIdAddress(id);
        _unequip(slotTypeId, id, account);
    }

    function _calculate(uint64 userId) internal {
        Modifier[] memory params = new Modifier[](_registeredParameters);
        uint24[] memory minuses = new uint24[](_registeredParameters);
        uint24[] memory pluses = new uint24[](_registeredParameters);
        bool storageChanged = false;
        bool chancesChanged = false;
        for (uint8 parameterId; parameterId < _registeredParameters; parameterId++) {
            params[parameterId].mul = uint24(PERCENT_PRECISION);
            for (uint8 slotTypeId; slotTypeId < _slotTypes.length; slotTypeId++) {
                Modifier memory mod = _slotsMods[userId][slotTypeId][parameterId];
                if (mod.mul > 0) {
                    if (mod.mul > uint24(PERCENT_PRECISION)) {
                        pluses[parameterId] +=  mod.mul - uint24(PERCENT_PRECISION);
                    } else if (mod.mul < uint24(PERCENT_PRECISION)) {
                        minuses[parameterId] += uint24(PERCENT_PRECISION) - mod.mul;
                    }
                }
                if (mod.add > 0) {
                    params[parameterId].add += mod.add;
                }
            }
            if (pluses[parameterId] > minuses[parameterId]) {
                params[parameterId].mul += pluses[parameterId] - minuses[parameterId];
            } else if (pluses[parameterId] < minuses[parameterId]) {
                uint24 minus = minuses[parameterId] - pluses[parameterId];
                if (minus < uint24(PERCENT_PRECISION)) {
                    params[parameterId].mul -= minus;
                } else {
                    params[parameterId].mul = uint24(PERCENT_PRECISION);
                }
            }
            // Collect changes or send a new parameter to receiver
            if (parameterId >= 0 && parameterId <= PARAM_REF_PRICE) {
                storageChanged = true;
            } else if (parameterId >= PARAM_RARITY_COMMON && parameterId <= PARAM_RARITY_LEGENDARY) {
                chancesChanged = true;
            } else if (address(_customReceivers[parameterId]) != address(0)) {
                _customReceivers[parameterId]
                    .updateParameter(parameterId, params[parameterId].mul, params[parameterId].add);
            }
        }
        
        if (storageChanged) {
            _core.setMultipliers(
                userId,
                [
                    params[PARAM_SPEED_MUL].mul,
                    params[PARAM_SIZE_MUL].mul,
                    params[PARAM_REF_MUL].mul
                ],
                [
                    params[PARAM_SPEED_ADDER].add,
                    params[PARAM_SIZE_ADDER].add,
                    params[PARAM_REF_ADDER].add
                ],
                [
                    params[PARAM_SPEED_PRICE].mul,
                    params[PARAM_SIZE_PRICE].mul,
                    params[PARAM_REF_PRICE].mul
                ]
            );
        }
        if (chancesChanged) {
            _chest.setUserChances(userId, [
                params[PARAM_RARITY_PIXEL].mul,
                params[PARAM_RARITY_COMMON].mul,
                params[PARAM_RARITY_UNCOMMON].mul,
                params[PARAM_RARITY_RARE].mul,
                params[PARAM_RARITY_EPIC].mul,
                params[PARAM_RARITY_LEGENDARY].mul
            ]);
        }
    }

    function _migrate(uint64 id) private {
        for (uint i; i < _oldContracts.length; i++) {
            IPXLSlots from = _oldContracts[i];
            Slot[] memory slots = from.getUserSlots(id);
            for (uint8 slotId; slotId < slots.length; slotId++) {
                if (slots[slotId].isEnabled) {
                    _slots[id][slotId].isEnabled = true;
                }
                if (slots[slotId].tokenId > 0) {
                    _equip(slots[slotId].tokenId, slotId, slots[slotId].variantId, id, address(from));
                }
            }
        }
    }

    function migrate() public {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");
        _migrate(id);
    }

    function migrate(uint64 id) public onlyRole(EDITOR_ROLE) {
        _migrate(id);
    }

}