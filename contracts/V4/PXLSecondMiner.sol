//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";
import "contracts/V4/interfaces/IPXLSecondMiner.sol";
import "contracts/V4/interfaces/IParameterReceiver.sol";

/// @title Second Pixel Shard miner
/// @author Danil Sakhinov
/// @notice Like ERC20 but transfers disabled
/// @notice Only Pixel Wallet users allowed
contract PXLSecondMiner is IPXLSecondMiner, IParameterReceiver, AccessControl {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    bytes32 public constant CLAIMER_ROLE = keccak256("CLAIMER_ROLE");

    address payable private _receiver;
    mapping(uint64 => uint) private _balances;

    string constant private _symbol = "JADEs";
    string constant private _name = "JADE Shard";
    uint8 constant private _decimals = 18;
    uint private _totalSupply;
    IERC20 private _USDT;
    IPXLsCoreV4 private _core;

    uint private constant _PRECISION = 10**_decimals;
    uint private constant _PERCENT_DECIMALS = 4;
    uint private constant _PERCENT_PRECISION = 10**_PERCENT_DECIMALS;
    uint private constant _INITIAL_SUPPLY = 1000000 * 10**(_decimals);
    uint private constant _HOUR = 60 * 60;
    uint private constant _DAY = _HOUR * 24;

    uint private constant SPEED_PARAMETER = 0;
    uint private constant SIZE_PARAMETER = 1;

    uint private _claimed;
    uint private _burned;

    bool private _claimsDisabled;
    bool private _refersDisabled;

    uint private _difficulty = 10;
    uint private _burnRate = _PERCENT_PRECISION;

    uint8 public minSpeedLevel = 0;
    uint8 public minSizeLevel = 0;

    // Tokens per hour
    uint baseSpeed = 2 * 10**15; // 0.002 PXLs
    uint baseSize = 6 * _PRECISION;

    mapping (uint64 userId => SecondStorage) private _users;
    uint8[2] private _parameters;

    // Referral rewards to parents
    uint[] parentsPercents = [
        100, // 1% to first parent
        0 // 0% to second parent
    ];



    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(CLAIMER_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
        _grantRole(MULTIPLIER_ROLE, _msgSender());
        _totalSupply = _INITIAL_SUPPLY;

        _receiver = payable(_msgSender());
    }

    function setCore(address coreAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV4(coreAddress);
    }

    function _getCoreStorage(uint64 id) internal view returns (Storage memory) {
        (
            Storage memory _storage,,,,,,,
        ) = _core.getStorage(id);
        return _storage;
    }

    modifier onlyStorageOwner(uint64 id) {
        require(_getCoreStorage(id).account == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    event Recieved(address indexed account, uint amount);
    receive() external payable {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
    }

    function setReceiver(address payable account) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _receiver = account;
    }

    function setUSDT(address usdt) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _USDT = IERC20(usdt);
    }

    function setBurnRate(uint burnRate) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _burnRate = burnRate;
    }

    function _getSpeed(Storage memory coreUser, uint64 id) internal view returns (uint) {
        SecondStorage storage user = _users[id];
        uint24 mult = user.speedMultiplicator == 0
            ? uint24(_PERCENT_PRECISION)
            : user.speedMultiplicator;
        if (_parameters[SPEED_PARAMETER] == 0) {
            mult = coreUser.speedMultiplicator;
        }
        uint256 adder = _parameters[SPEED_PARAMETER] == 0
            ? coreUser.speedAdder
            : user.speedAdder;
        return (coreUser.speedLevel + 1) * baseSpeed
        * mult / _PERCENT_PRECISION
        / _HOUR
        + adder;
    }

    function _getSize(Storage memory coreUser, uint64 id) internal view returns (uint) {
        SecondStorage storage user = _users[id];
        uint24 mult = user.sizeMultiplicator == 0
            ? uint24(_PERCENT_PRECISION)
            : user.sizeMultiplicator;
        if (_parameters[SIZE_PARAMETER] == 0) {
            mult = coreUser.sizeMultiplicator;
        }
        uint256 adder = _parameters[SIZE_PARAMETER] == 0
            ? coreUser.sizeAdder
            : user.sizeAdder;
        return (coreUser.sizeLevel + 1) * baseSpeed
        * baseSize / _PRECISION
        * mult / _PERCENT_PRECISION
            + adder;
    }

    /// @notice Burn storage tokens
    /// @param id Wallet ID
    /// @param value Tokens amount to burn
    function _burn(uint64 id, uint256 value) internal {
        require(_balances[id] >= value, "Can't burn more");
        uint burnValue = value * _burnRate / _PERCENT_PRECISION;
        uint reinvestValue = value - burnValue;
        _totalSupply -= burnValue;
        _balances[id] -= value;
        _burned += burnValue;
        address userAddress = _core.getIdAddress(id);
        emit Burn(id, userAddress, burnValue);
        if (reinvestValue > 0) {
            emit Reinvest(id, userAddress, reinvestValue);
        }
    }

    /// @notice The administrator can reset the storage
    /// @param id Wallet ID
    function burnStorage(uint64 id) public onlyRole(BURNER_ROLE) {
        Storage memory user = _getCoreStorage(id);
        uint balance = _balances[id];
        _burn(id, balance);
        emit Transfer(user.account, address(0), balance);
    }

    /// @notice Burner contract can burn a specific amount of tokens from an account
    /// @param account Storage owner
    /// @param amount Tokens amount to burn
    function burn(address account, uint amount) public onlyRole(BURNER_ROLE) {
        uint64 id = _core.getAddressId(account);
        Storage memory coreUser = _getCoreStorage(id);
        require(_balances[id] >= amount, "Not enough funds");
        require(coreUser.account == account, "Address have no storage");
        _burn(id, amount);
        emit Transfer(account, address(0), amount);
    }

    /// @notice Transfer tokens to storage account
    /// @param id Wallet ID
    /// @param amount Tokens amount to claim
    function _claim(uint64 id, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _users[id].claimed += amount;
        _balances[id] += amount;
    }

    /// @notice ERC20 format
    function totalSupply() public view virtual returns (uint256) {
        return _totalSupply;
    }

    /// @notice ERC20 format
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /// @notice ERC20 format
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /// @notice ERC20 format
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /// @notice Gets account balance
    /// @param account Address linked to storage
    /// @return Tokens amount
    /// @dev Balances linked to Wallet ID only
    function balanceOf(address account) public view virtual returns (uint256) {
        uint64 id = _core.getAddressId(account);
        return _core.getIdAddress(id) == account
            ? _balances[id]
            : 0;
    }

    function _allowMining(uint64 id) internal returns (bool) {
        SecondStorage storage user = _users[id];
        require(!user.disabled, "This token mining is disabled for this user");
        if (user.claimTimestamp == 0) {
            Storage memory coreUser = _getCoreStorage(id);
            require(coreUser.sizeLevel >= minSizeLevel, "Minimum storage level required");
            require(coreUser.speedLevel >= minSpeedLevel, "Minimum drill level required");
            user.claimTimestamp = block.timestamp;
        }
        return true;
    }

    function _checkRequirements(uint64 id) internal view returns (bool) {
        Storage memory coreUser = _getCoreStorage(id);
        return coreUser.sizeLevel >= minSizeLevel
        && coreUser.speedLevel >= minSpeedLevel;
    }

    /// @notice Gets mined tokens since last claim
    /// @param id Wallet ID
    /// @return Tokens amount
    function _getMinedTokens(uint64 id) internal view returns (uint) {
        SecondStorage storage user = _users[id];
        Storage memory coreUser = _getCoreStorage(id);
        uint limit = _getSize(coreUser, id);
        if (user.claimTimestamp == 0) {
            return 0;
        }
        uint time = block.timestamp - user.claimTimestamp;
        uint amount = _getSpeed(coreUser, id)
            * time;
        return amount > limit && limit != 0
            ? limit
            : amount;
    }

    /// @notice Claim reward by storage
    /// @notice It also sends rewards to parents
    /// @param id Wallet ID
    function _claimReward(uint64 id, uint additional) internal {
        require(!_claimsDisabled, "Claims disabled");
        uint mined = _getMinedTokens(id) + additional;
        SecondStorage storage current = _users[id];
        if (current.claimTimestamp == 0) {
            _allowMining(id);
            return;
        }
        if (mined == 0) return;
        Storage memory coreUser = _getCoreStorage(id);
        require(!coreUser.disabled, "Storage disabled");
        require(!current.disabled, "This mining is disabled for this user");
        _claim(id, mined);
        current.claimTimestamp = block.timestamp;
        emit Claim(id, coreUser.account, mined);
        emit Transfer(address(this), coreUser.account, mined);

        uint index = 0;
        uint64 previous = id;
        while (index < parentsPercents.length
        && coreUser.parent != 0
            && !_refersDisabled) {
            uint64 receiver = coreUser.parent;
            coreUser = _getCoreStorage(id);
            uint reward = mined * parentsPercents[index] / _PERCENT_PRECISION;
            if (_claimed + reward <= _totalSupply
                && coreUser.account != address(0)
                && !_users[receiver].disabled) {
                _claim(id, reward);
                emit ReferReward(id, receiver, previous, coreUser.account, reward);
            }
            index++;
            previous = receiver;
        }
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param id Wallet ID
    function claimReward(uint64 id) public payable onlyStorageOwner(id) {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
        _claimReward(id, 0);
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param id Wallet ID
    function claimFor(uint64 id) public onlyRole(CLAIMER_ROLE) {
        _claimReward(id, 0);
    }

    /// @notice Mint amount of tokens to storage and distribute it to refers
    /// @param id Wallet ID
    /// @param amount Amount of tokens to mint
    /// @dev Calls claim
    function mintWithClaim(uint64 id, uint amount) public onlyRole(MINTER_ROLE) {
        _claimReward(id, amount);
    }

    /// @notice Mint amount of tokens only for one account
    /// @param id Wallet ID
    /// @param amount Amount of tokens to mint
    function mintForOne(uint64 id, uint amount) public onlyRole(MINTER_ROLE) {
        _claim(id, amount);
        emit Transfer(address(this), _core.getIdAddress(id), amount);
    }

    /// @notice Returns total claimed amount by storages
    /// @return Claimed tokens amount
    function totalClaimed() public view virtual returns (uint) {
        return _claimed;
    }

    /// @notice Returns total burned amount by storages
    /// @return Burned tokens amount
    function totalBurned() public view virtual returns (uint) {
        return _burned;
    }

    /// @notice Contract owner can disable claims
    function disableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = true;
        emit SetClaimsDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable claims again
    function enableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = false;
        emit SetClaimsDisabled(_msgSender(), false);
    }

    /// @notice Contract owner can disable parents rewards
    function disableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = true;
        emit SetRefersDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable parents rewards again
    function enableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = false;
        emit SetRefersDisabled(_msgSender(), false);
    }

    /// @notice Contract owner set base tokens per hour value
    function setBaseSpeed(uint speed) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSpeed = speed;
    }

    function setBaseSize(uint size) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSize = size;
    }

    function setDiffuculty(uint value) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _difficulty = value;
    }

    function setMinLevels(uint8 minSpeed, uint8 minSize) public onlyRole(DEFAULT_ADMIN_ROLE) {
        minSpeedLevel = minSpeed;
        minSizeLevel = minSize;
    }

    /// @notice Owner can set a new parents percents
    /// @param _percents Array of percents
    function setParentsPercents(uint[2] calldata _percents) public onlyRole(DEFAULT_ADMIN_ROLE) {
        parentsPercents = _percents;
    }

    /// @notice Storage info getter
    /// @param id Wallet ID
    function getStorage(uint64 id) public view returns (
        SecondStorage memory user,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint balance,
        uint coins
    ) {
        SecondStorage storage _user = _users[id];
        Storage memory coreUser = _getCoreStorage(id);
        user = _user;
        mined = _getMinedTokens(id);
        sizeLimit = _getSize(coreUser, id);
        rewardPerSecond = _getSpeed(coreUser, id);
        balance = _balances[id];
        coins = coreUser.account.balance;
    }

    /// @notice Admins can burn amount tokens from total supply
    /// @param amount Amount of tokens to burn
    function burnTotalSupply(uint amount) public onlyRole(BURNER_ROLE) {
        _totalSupply -= amount;
        emit BurnTotalSupply(_msgSender(), amount);
    }

    function setParameters(uint8[2] calldata parameters) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _parameters = parameters;
    }

    function updateParameter(uint8 parameterId, uint24 multiplier, uint256 adder) public onlyRole(MULTIPLIER_ROLE) {
        /// TODO: Fix it with migration
        address account = tx.origin;
        uint64 userId = _core.getAddressId(account);
        if (userId == 0) return;
        SecondStorage storage user = _users[userId];
        if (_checkRequirements(userId)) {
            _claimReward(userId, 0);
        }
        if (parameterId == _parameters[SPEED_PARAMETER]) {
            user.speedMultiplicator = multiplier;
            user.speedAdder = adder;
        } else if (parameterId == _parameters[SIZE_PARAMETER]) {
            user.sizeMultiplicator = multiplier;
            user.sizeAdder = adder;
        }
    }

    function disableMining(uint64 userId) public onlyRole(MULTIPLIER_ROLE) {
        SecondStorage storage user = _users[userId];
        user.disabled = true;
        user.claimTimestamp = 0;
    }

    function enableMining(uint64 userId) public onlyRole(MULTIPLIER_ROLE) {
        SecondStorage storage user = _users[userId];
        user.disabled = false;
        user.claimTimestamp = block.timestamp;
    }

}