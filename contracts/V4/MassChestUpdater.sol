//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/V4/interfaces/IPXLChest.sol";

contract MassChestUpdater is AccessControl {
    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    IPXLChest private _chest = IPXLChest(0x8f4F9e4e9B377a3E4E4c5BE038c637fC8966FC1d);

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
    }

    function addChestItems(uint chestIndex, uint slotIndex, uint rarity, uint[] calldata typeId) public onlyRole(EDITOR_ROLE) {
        for (uint i; i < typeId.length; i++) {
            _chest.addChestItem(chestIndex, slotIndex, rarity, typeId[i]);
        }
    }

    function setChest(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _chest = IPXLChest(newAddress);
    }

}