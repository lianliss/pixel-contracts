//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";
import "contracts/V4/interfaces/IPXLChest.sol";

contract PXLFreeChest is AccessControl {

    bytes32 public constant SETTER_ROLE = keccak256("SETTER_ROLE");

    mapping(uint64 id => mapping(uint chestId => bool)) private _userFreeChest;
    mapping(uint64 id => uint) private _lastFreeClaimTimestamp;
    uint private _weeklyChestId;
    IPXLsCoreV4 private _core;
    IPXLChest private _chest;

    uint private constant _HOUR = 60 * 60;
    uint private constant _DAY = _HOUR * 24;
    uint private constant _WEEK = _DAY * 7;

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(SETTER_ROLE, _msgSender());
    }

    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV4(newAddress);
    }

    function setChestAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _chest = IPXLChest(newAddress);
    }

    function setWeeklyChest(uint chestId) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _weeklyChestId = chestId;
    }

    function setUsersFreeChest(uint64[] calldata id, uint chestId) public onlyRole(SETTER_ROLE) {
        for (uint i; i < id.length; i++) {
            _userFreeChest[id[i]][chestId] = true;
        }
    }

    event FreeChestClaimed(uint64 indexed userId, address indexed account, uint indexed chestId);
    function claimFreeChest(uint chestId) public {
        uint64 id = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(id) == _msgSender(), "Not authorized");
        require(_userFreeChest[id][chestId] == true, "Free chest not available");
        _userFreeChest[id][chestId] = false;
        _chest.sendChest(chestId, _msgSender());
        emit FreeChestClaimed(id, _msgSender(), chestId);
    }

    function getUserFreeChests(uint64 id) public view returns (uint[] memory) {
        uint chestsCount = _chest.getChestsCount();
        uint[] memory chests = new uint[](chestsCount);
        for (uint chestId; chestId < chestsCount; chestId++) {
            if (_userFreeChest[id][chestId] == true) {
                chests[chestId] = chestId;
            }
        }
        return chests;
    }

    event WeeklyChestClaimed(uint64 indexed userId, address indexed account, uint indexed chestId);
    function claimWeeklyChest() public {
        uint64 id = _core.getAddressId(_msgSender());
        (
            Storage memory user,,,,,,,
        ) = _core.getStorage(id);
        require(user.account == _msgSender(), "Not authorized");
        require(user.activity >= _WEEK, "Too short activity");
        uint last = _lastFreeClaimTimestamp[id];
        uint time = block.timestamp - last;
        require(last == 0 || time >= _WEEK, "The current weekly chest has already been claimed");

        _lastFreeClaimTimestamp[id] = block.timestamp;
        _chest.sendChest(_weeklyChestId, _msgSender());
        emit WeeklyChestClaimed(id, _msgSender(), _weeklyChestId);
    }

    function getLastFreeClaim(uint64 userId) public view returns (uint) {
        return _lastFreeClaimTimestamp[userId];
    }
}