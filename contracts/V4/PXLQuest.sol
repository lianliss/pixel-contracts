//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";
import "contracts/V4/interfaces/IPXLChest.sol";

struct Quest {
    uint questId;
    uint chestId;
    uint PXLs;
    uint maxIterations;
}

struct UserQuest {
    uint questId;
    uint chestId;
    uint PXLs;
    uint iterations;
    uint maxIterations;
}

contract PXLQuest is AccessControl {

    bytes32 public constant SETTER_ROLE = keccak256("SETTER_ROLE");

    mapping(uint64 userId => mapping(uint questId => uint iterations)) private _userQuest;
    mapping(uint questId => Quest) private _quests;
    uint[] public questsList;
    IPXLsCoreV4 private _core;
    IPXLChest private _chest;

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(SETTER_ROLE, _msgSender());
    }

    event UpdateQuest(uint indexed questId, uint chestId, uint PXLs, uint maxIterations);
    event QuestComplete(uint indexed questId, uint64 indexed userId);

    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV4(newAddress);
    }

    function setChestAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _chest = IPXLChest(newAddress);
    }

    function updateQuest(uint questId, uint chestId, uint PXLs, uint maxIterations) public onlyRole(DEFAULT_ADMIN_ROLE) {
        if (_quests[questId].questId == 0) {
            questsList.push(questId);
        }
        _quests[questId] = Quest(questId, chestId, PXLs, maxIterations);
        emit UpdateQuest(questId, chestId, PXLs, maxIterations);
    }

    function setQuestComplete(uint64 userId, uint questId) public onlyRole(SETTER_ROLE) {
        Quest memory quest = _quests[questId];
        require(_userQuest[userId][questId] < quest.maxIterations, "Quest already completed");
        require(quest.questId == questId, "Undefined quest");
        address account = _core.getIdAddress(userId);
        require(account != address(0), "Unknown user");
        if (quest.chestId > 0) {
            _chest.sendChest(quest.chestId, account);
        }
        if (quest.PXLs > 0) {
            _core.mintForOne(userId, quest.PXLs);
        }
        _userQuest[userId][questId]++;
        emit QuestComplete(questId, userId);
    }

    function getQuestsLength() public view returns(uint) {
        return questsList.length;
    }

    function getUserQuests(uint64 userId, uint offset, uint limit) public view returns(UserQuest[] memory) {
        uint length = questsList.length - offset;
        if (limit < length) {
            length = limit;
        }
        UserQuest[] memory quests = new UserQuest[](length);
        for (uint i = offset; i < questsList.length && i < offset + limit; i++) {
            quests[i].questId = questsList[i];
            quests[i].chestId = _quests[questsList[i]].chestId;
            quests[i].PXLs = _quests[questsList[i]].PXLs;
            quests[i].iterations = _userQuest[userId][questsList[i]];
            quests[i].maxIterations = _quests[questsList[i]].maxIterations;
        }
        return quests;
    }
    
    function markUsersQuestsComplete(uint64[] calldata users, uint questId) public onlyRole(SETTER_ROLE) {
        Quest storage quest = _quests[questId];
        for (uint i; i < users.length; i++) {
            _userQuest[users[i]][questId] = quest.maxIterations;
            emit QuestComplete(questId, users[i]);
        }
    }
    
}