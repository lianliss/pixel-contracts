//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V4/interfaces/IPixelNFT.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";
import "contracts/V4/interfaces/IPXLChest.sol";

contract PXLChest is IPXLChest, AccessControl {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");
    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");

    Chest[] private _chests;
    MarketItem[] private _market;
    mapping(uint64 id => uint24[RARITY_COUNT]) private _chances;
    IPXLsCoreV4 private _core;
    IPixelNFT private _nft;
    address payable private _receiver;

    uint private _baseValue = PERCENT_PRECISION;
    uint private _maxPXLsBonus = 2 * 10**18;
    IERC20 private _USDT;
    bool private _isUseUSDT = false;

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(MULTIPLIER_ROLE, _msgSender());
        _receiver = payable(_msgSender());
    }

    event Recieved(address indexed account, uint amount);
    receive() external payable {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
    }

    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV4(newAddress);
    }

    function setNFTAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _nft = IPixelNFT(newAddress);
    }

    function setReceiver(address payable account) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _receiver = account;
    }

    function setUSDT(address usdt) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _USDT = IERC20(usdt);
    }

    function setUseUSDT(bool isUse) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _isUseUSDT = isUse;
    }

    function setMaxPXLsBonus(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _maxPXLsBonus = amount;
    }

    function createChest(uint[RARITY_COUNT] calldata ratios) public onlyRole(EDITOR_ROLE) returns (uint) {
        _chests.push();
        uint index = _chests.length - 1;
        _chests[index].ratios = ratios;
        return index;
    }

    function setChestRatios(uint chestId, uint[RARITY_COUNT] calldata ratios) public onlyRole(EDITOR_ROLE) {
        _chests[chestId].ratios = ratios;
    }

    function addChestItem(uint chestIndex, uint slotIndex, uint rarity, uint typeId) public onlyRole(EDITOR_ROLE) returns (uint) {
        for (uint i = _chests[chestIndex].items.length; i <= slotIndex; i++) {
            _chests[chestIndex].items.push();
        }
        _chests[chestIndex].items[slotIndex][rarity].push(typeId);
        return _chests[chestIndex].items[slotIndex][rarity].length - 1;
    }

    function clearChestItems(uint chestIndex) public onlyRole(EDITOR_ROLE) {
        delete _chests[chestIndex].items;
    }

    function getChest(uint chestId) public view returns (Chest memory) {
        return _chests[chestId];
    }

    function getChestsCount() public view returns (uint) {
        return _chests.length;
    }

    function getChests() public view returns (Chest[] memory) {
        return _chests;
    }

    function createMarketItem(uint chestId, uint count, uint pixelPrice, uint sgbPrice) public onlyRole(EDITOR_ROLE) returns (uint) {
        MarketItem storage item = _market.push();
        item.chestId = chestId;
        item.count = count;
        item.pixelPrice = pixelPrice;
        item.sgbPrice = sgbPrice;
        return _market.length - 1;
    }

    function updateMarketItem(uint itemId, uint pixelPrice, uint sgbPrice) public onlyRole(EDITOR_ROLE) {
        _market[itemId].pixelPrice = pixelPrice;
        _market[itemId].sgbPrice = sgbPrice;
    }

    function setMarketItemCount(uint itemId, uint count) public onlyRole(EDITOR_ROLE) {
        _market[itemId].count = count;
    }

    function getMarketItem(uint itemId) public view returns (MarketItem memory) {
        return _market[itemId];
    }

    function getMarketItemsCount() public view returns (uint) {
        return _market.length;
    }

    function getMarket() public view returns (MarketItem[] memory) {
        return _market;
    }

    function setUserChances(uint64 userId, uint24[RARITY_COUNT] calldata chances) public onlyRole(MULTIPLIER_ROLE) {
        _chances[userId] = chances;
    }

    function _getChance(uint chestId, uint8 rarity, uint64 userId) public view returns (uint) {
        Chest storage chest = _chests[chestId];
        uint24 userChance = _chances[userId][rarity];
        if (userChance >= PERCENT_PRECISION) {
            userChance -= uint24(PERCENT_PRECISION);
        } else {
            userChance = 0;
        }
        uint startRarity = RARITY_PIXEL;
        while (chest.ratios[startRarity + 1] == 0) {
            startRarity++;
        }
        if (rarity >= startRarity && rarity <= RARITY_LEGENDARY) {
            uint ratio = chest.ratios[rarity];
            if (ratio == 0) return 0;
            uint numenator = _baseValue;
            if (rarity >= 1) {
                for (uint8 step = rarity - 1; step >= startRarity; step--) {
                    uint prevChances = _getChance(chestId, step, userId);
                    if (prevChances >= numenator) return 0;
                    numenator -= prevChances;
                    if (step == 0) break;
                }
            }
            return numenator * PERCENT_PRECISION / ratio
                + userChance;
        } else {
            return 0;
        }
    }

    uint private _randomCounter;
    function _pseudoRandom(uint mod) private returns (uint) {
        _randomCounter++;
        return uint(keccak256(abi.encodePacked(block.prevrandao, block.timestamp, _randomCounter)))
            % mod;
    }

    function _rollRarity(uint chestId, uint64 userId) public returns (uint) {
        uint rand = _pseudoRandom(_baseValue);
        uint chance;
        for (uint8 rarity = RARITY_LEGENDARY; rarity >= RARITY_PIXEL; rarity--) {
            chance += _getChance(chestId, rarity, userId);
            if (rand <= chance) {
                return rarity;
            }
        }
        return RARITY_PIXEL;
    }

    function _rollDrop(uint chestId, address account, bool isUseItems) internal {
        uint64 userId = isUseItems
            ? _core.getAddressId(account)
            : 0;
        Chest storage chest = _chests[chestId];
        for (uint slotId; slotId < chest.items.length; slotId++) {
            uint rarity = _rollRarity(chestId, userId);
            if (rarity > RARITY_PIXEL) {
                uint itemIndex = _pseudoRandom(chest.items[slotId][rarity].length);
                uint typeId = chest.items[slotId][rarity][itemIndex];
                uint tokenId = _nft.mint(account, typeId);
                emit ItemDrop(account, chestId, typeId, tokenId);
            } else if (userId > 0) {
                uint bonus = _pseudoRandom(_maxPXLsBonus + 1);
                _core.mintForOne(userId, bonus);
                emit BonusDrop(account, userId, bonus);
            }
        }
    }

    function buyChest(uint itemId) public payable {
        MarketItem storage item = _market[itemId];
        require(item.count > 0, "Not enough chests left");
        if (item.pixelPrice > 0) {
            _core.burn(_msgSender(), item.pixelPrice);
            item.count--;
            _rollDrop(item.chestId, _msgSender(), true);
            emit BuyChestPixel(_msgSender(), itemId, item.chestId, item.pixelPrice);
        } else if (item.sgbPrice > 0) {
            if (_isUseUSDT) {
                require(_USDT.transferFrom(
                    _msgSender(),
                    _receiver,
                    item.sgbPrice
                ), "Can't transfer USDT");
                emit Recieved(_msgSender(), item.sgbPrice);
            } else {
                require(msg.value >= item.sgbPrice, "Not enough SGB");
                (bool sent,) = _receiver.call{value: msg.value}("");
                require(sent, "Failed to send Ether");
                emit Recieved(_msgSender(), msg.value);
            }
            
            item.count--;
            _rollDrop(item.chestId, _msgSender(), true);
            emit BuyChestSGB(_msgSender(), itemId, item.chestId, item.sgbPrice);
        } else {
            revert("Item is not tradable");
        }
    }

    function sendChest(uint chestId, address account) public onlyRole(MINTER_ROLE) {
        _rollDrop(chestId, account, true);
        emit SendChest(account, chestId);
    }

}