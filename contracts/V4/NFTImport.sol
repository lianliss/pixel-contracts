//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V4/interfaces/IPixelNFT.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract NFTImport is AccessControl {

    struct Import {
        uint sourceId;
        string typeName;
        string tokenURI;
        uint8[] slots;
        uint sourceCollectionId;
        string collectionName;
        uint8 rarity;
        uint8[] parameters;
        uint24[] multipliers;
        uint256[] adductors;
    }

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    mapping(uint => uint) public collections;
    mapping(uint => uint) public types;

    IPixelNFT public nft;

    event NewCollectionMapping(uint sourceCollectionId, uint collectionId);
    event NewTypeMapping(uint sourceTypeId, uint typeId);

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
    }

    function setDestinationContract(address destination) public onlyRole(EDITOR_ROLE) {
        nft = IPixelNFT(destination);
    }

    function setCollectionMapping(uint sourceId, uint destinationId) public onlyRole(EDITOR_ROLE) {
        collections[sourceId] = destinationId;
        emit NewCollectionMapping(sourceId, destinationId);
    }

    function setCollectionMapping(uint sourceId, string calldata name) public onlyRole(EDITOR_ROLE) returns(uint) {
        uint index = nft.getCollections().length;
        require(index > 0, "First collection with index = 0 must be created manually");
        nft.createCollection(name);
        setCollectionMapping(sourceId, index);
        return index;
    }

    function importType(
        uint sourceId,
        string calldata typeName,
        string calldata _tokenURI,
        uint8[] calldata slots,
        uint sourceCollectionId,
        string calldata collectionName,
        uint8 rarity) public onlyRole(EDITOR_ROLE) {
            require(types[sourceId] == 0, "Already imported");
            require(sourceCollectionId > 0, "Can't import from zero collection");
            if (collections[sourceCollectionId] == 0) {
                setCollectionMapping(sourceCollectionId, collectionName);
            }
            uint index = nft.getTypesLength();
            require(index > 0, "First type with index = 0 must be created manually");
            nft.createType(typeName, _tokenURI, slots, collections[sourceCollectionId], rarity);
            types[sourceId] = index;
            emit NewTypeMapping(sourceId, index);
        }

    function importTypeModifiers(uint256 sourceId, uint8[] calldata parameters, uint24[] calldata multipliers, uint256[] calldata adductors) public onlyRole(EDITOR_ROLE) {
        uint typeId = types[sourceId];
        require(typeId > 0, "Can't import modifiers for unknown type");
        nft.addVariant(typeId, parameters, multipliers, adductors);
    }

    function importStruct(Import calldata data) public onlyRole(EDITOR_ROLE) {
        importType(
            data.sourceId,
            data.typeName,
            data.tokenURI,
            data.slots,
            data.sourceCollectionId,
            data.collectionName,
            data.rarity
        );
        importTypeModifiers(
            data.sourceId,
            data.parameters,
            data.multipliers,
            data.adductors
        );
    }

}