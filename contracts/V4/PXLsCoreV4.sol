//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V4/interfaces/IPXLsCoreV4.sol";

interface IPXLs3 {
    function getStorage(uint64 id) external view returns (
        uint claimTimestamp,
        uint8 sizeLevel,
        uint8 speedLevel,
        uint claimed,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        address account,
        uint256 balance,
        uint multiplicator,
        uint64 parent,
        uint coins
    );
}

/// @title Pixel Shard token
/// @author Danil Sakhinov
/// @notice Like ERC20 but transfers disabled
/// @notice Only Pixel Wallet users allowed
contract PXLsCoreV4 is IPXLsCoreV4, AccessControl {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    bytes32 public constant GRANTER_ROLE = keccak256("GRANTER_ROLE");
    bytes32 public constant CLAIMER_ROLE = keccak256("CLAIMER_ROLE");

    address payable private _receiver;
    mapping(uint64 => Storage) private _storages;
    mapping(address => uint64) private _accounts;
    mapping(uint64 => uint) private _balances;

    string constant private _symbol = "PXLs";
    string constant private _name = "Pixel Shard";
    uint8 constant private _decimals = 18;
    uint private _totalSupply;
    IERC20 private _USDT;

    uint private constant _PRECISION = 10**_decimals;
    uint private constant _PERCENT_DECIMALS = 4;
    uint private constant _PERCENT_PRECISION = 10**_PERCENT_DECIMALS;
    uint private constant _INITIAL_SUPPLY = 2768888888 * 10**(_decimals - 2);
    uint private constant _HOUR = 60 * 60;
    uint private constant _DAY = _HOUR * 24;

    uint private _claimed;
    uint private _burned;

    bool private _claimsDisabled;
    bool private _refersDisabled;

    uint private _difficulty = 25;
    uint private _burnRate = _PERCENT_PRECISION;

    // Tokens per hour
    uint baseSpeed = 1 * 10**16; // 0.01 PXLs
    uint baseSize = 4 * _PRECISION;

    // Referral rewards to parents
    uint[] parentsPercents = [
    2000, // 20% to first parent
    500 // 5% to second ancestor
    ];



    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(GRANTER_ROLE, _msgSender());
        _grantRole(CLAIMER_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
        _grantRole(MULTIPLIER_ROLE, _msgSender());
        _totalSupply = _INITIAL_SUPPLY;

        _receiver = payable(_msgSender());
    }

    modifier onlyStorageOwner(uint64 id) {
        require(_storages[id].account == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    event Recieved(address indexed account, uint amount);
    receive() external payable {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
    }

    function setReceiver(address payable account) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _receiver = account;
    }

    function setUSDT(address usdt) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _USDT = IERC20(usdt);
    }

    function setBurnRate(uint burnRate) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _burnRate = burnRate;
    }

    function setMultipliers(uint64 id, uint24[3] calldata mult, uint256[3] calldata add, uint24[3] calldata price) public onlyRole(MULTIPLIER_ROLE) {
        _claimReward(id, 0);
        Storage storage user = _storages[id];
        user.speedMultiplicator = mult[0];
        user.sizeMultiplicator = mult[1];
        user.referralMultiplicator = mult[2];
        user.speedAdder = add[0];
        user.sizeAdder = add[1];
        user.referralAdder = add[2];
        user.speedPrice = price[0];
        user.sizePrice = price[1];
        user.referralPrice = price[2];
    }

    function _getSpeed(Storage storage user) internal view returns (uint) {
        return (user.speedLevel + 1) * baseSpeed
        * user.speedMultiplicator / _PERCENT_PRECISION
        / _HOUR
            + user.speedAdder;
    }

    function _getSpeedPrice(Storage storage user) internal view returns (uint) {
        uint nextLevel = user.speedLevel + 2;
        return nextLevel**2 * _difficulty * baseSpeed
        * user.speedPrice / _PERCENT_PRECISION;
    }

    function _getSize(Storage storage user) internal view returns (uint) {
        return (user.sizeLevel + 1) * baseSpeed
        * baseSize / _PRECISION
        * user.sizeMultiplicator / _PERCENT_PRECISION
            + user.sizeAdder;
    }

    function _getSizePrice(Storage storage user) internal view returns (uint) {
        uint nextLevel = user.sizeLevel + 2;
        return nextLevel**2 * _difficulty * baseSpeed
        * baseSize / _PRECISION
        * user.sizePrice / _PERCENT_PRECISION;
    }

    function _getReferralSize(Storage storage user) internal view returns (uint) {
        uint8 currentLevel = user.referralLevel + 1;
        return (
            currentLevel * baseSpeed
            * baseSize / _PRECISION
            + _PRECISION * 2
            + (parentsPercents[0] * _PRECISION / _PERCENT_PRECISION * currentLevel)
            + (parentsPercents[1] * _PRECISION / _PERCENT_PRECISION * currentLevel)
        )
        * user.referralMultiplicator / _PERCENT_PRECISION
            + user.referralAdder;
    }

    function _getReferralPrice(Storage storage user) internal view returns (uint) {
        uint nextLevel = user.referralLevel + 2;
        return nextLevel**2 * _difficulty
        * baseSpeed * baseSize / _PRECISION
        * (_PERCENT_PRECISION + parentsPercents[0] * nextLevel) / _PERCENT_PRECISION
        * (_PERCENT_PRECISION + parentsPercents[1] * nextLevel) / _PERCENT_PRECISION
        * user.referralPrice / _PERCENT_PRECISION;
    }

    /// @notice Burn storage tokens
    /// @param id Wallet ID
    /// @param value Tokens amount to burn
    function _burn(uint64 id, uint256 value) internal {
        require(_balances[id] >= value, "Can't burn more");
        uint burnValue = value * _burnRate / _PERCENT_PRECISION;
        uint reinvestValue = value - burnValue;
        _totalSupply -= burnValue;
        _balances[id] -= value;
        _burned += burnValue;
        emit Burn(id, _storages[id].account, burnValue);
        if (reinvestValue > 0) {
            emit Reinvest(id, _storages[id].account, reinvestValue);
        }
    }

    /// @notice The administrator can reset the storage
    /// @param id Wallet ID
    function burnStorage(uint64 id) public onlyRole(BURNER_ROLE) {
        Storage storage user = _storages[id];
        uint balance = _balances[id];
        _burn(id, balance);
        user.sizeLevel = 0;
        user.speedLevel = 0;
        user.referralLevel = 0;
        user.refStorage = 0;
        emit Transfer(user.account, address(0), balance);
    }

    /// @notice Burner contract can burn a specific amount of tokens from an account
    /// @param account Storage owner
    /// @param amount Tokens amount to burn
    function burn(address account, uint amount) public onlyRole(BURNER_ROLE) {
        uint64 id = getAddressId(account);
        require(_balances[id] >= amount, "Not enough funds");
        require(_storages[id].account == account, "Address have no storage");
        _burn(id, amount);
        emit Transfer(account, address(0), amount);
    }

    /// @notice Admin can change a storage parent
    /// @param id Wallet ID
    /// @param parent New parent
    function changeParent(uint64 id, uint64 parent) public onlyRole(GRANTER_ROLE) {
        _storages[id].parent = parent;
        emit SetParent(id, parent);
    }

    /// @notice Transfer tokens to storage account
    /// @param id Wallet ID
    /// @param amount Tokens amount to claim
    function _claim(uint64 id, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _storages[id].claimed += amount;
        _balances[id] += amount;
    }

    function _fillRef(uint64 id, uint amount) internal returns (uint) {
        Storage storage user = _storages[id];
        if (_getReferralSize(user) <= user.refStorage) {
            return 0;
        }
        uint limit = _getReferralSize(user) - user.refStorage;
        uint value = limit < amount
            ? limit
            : amount;
        if (value > 0) {
            user.refStorage += value;
        }
        return value;
    }

    /// @notice ERC20 format
    function totalSupply() public view virtual returns (uint256) {
        return _totalSupply;
    }

    /// @notice ERC20 format
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /// @notice ERC20 format
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /// @notice ERC20 format
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /// @notice Gets account balance
    /// @param account Address linked to storage
    /// @return Tokens amount
    /// @dev Balances linked to Wallet ID only
    function balanceOf(address account) public view virtual returns (uint256) {
        uint64 id = getAddressId(account);
        return _storages[id].account == account
            ? _balances[id]
            : 0;
    }

    /// @notice Gets Wallet ID linked to a storage
    /// @param account Address linked to storage
    /// @return Wallet ID
    function getAddressId(address account) public view returns (uint64) {
        return _accounts[account];
    }

    function getIdAddress(uint64 id) public view returns (address) {
        return _storages[id].account;
    }

    /// @notice Gets mined tokens since last claim
    /// @param id Wallet ID
    /// @return Tokens amount
    function _getMinedTokens(uint64 id) internal view returns (uint) {
        Storage storage user = _storages[id];
        uint limit = _getSize(user);
        uint time = block.timestamp - user.claimTimestamp;
        uint amount = _getSpeed(user)
            * time;
        return amount > limit && limit != 0
            ? limit
            : amount;
    }

    function _updateActivity(Storage storage user) internal {
        uint claimSeconds = block.timestamp - user.claimTimestamp;
        if (claimSeconds < _DAY) {
            user.activity += claimSeconds;
        } else {
            user.activity = 0;
        }
    }

    /// @notice Claim reward by storage
    /// @notice It also sends rewards to parents
    /// @param id Wallet ID
    function _claimReward(uint64 id, uint additional) internal {
        require(!_claimsDisabled, "Claims disabled");
        uint mined = _getMinedTokens(id) + additional;
        if (mined == 0) return;
        Storage storage current = _storages[id];
        require(!current.disabled, "Storage disabled");
        _claim(id, mined);
        _updateActivity(current);
        current.claimTimestamp = block.timestamp;
        emit Claim(id, current.account, mined);
        emit Transfer(address(this), current.account, mined);

        uint index = 0;
        uint64 previous = id;
        while (index < parentsPercents.length
        && current.parent != 0
            && !_refersDisabled) {
            uint64 receiver = current.parent;
            current = _storages[receiver];
            uint reward = mined * parentsPercents[index] / _PERCENT_PRECISION;
            if (_claimed + reward <= _totalSupply && current.account != address(0)) {
                uint filled = _fillRef(receiver, reward);
                if (filled > 0) {
                    emit ReferReward(id, receiver, previous, current.account, filled);
                }
            }
            index++;
            previous = receiver;
        }
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param id Wallet ID
    function claimReward(uint64 id) public payable onlyStorageOwner(id) {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
        _claimReward(id, 0);
    }

    function claimReferral(uint64 id) public onlyStorageOwner(id) {
        require(!_claimsDisabled, "Claims disabled");
        Storage storage user = _storages[id];
        require(!user.disabled, "Storage disabled");
        uint mined = user.refStorage;
        if (mined == 0) return;
        _claim(id, mined);
        user.refStorage = 0;
        emit Claim(id, user.account, mined);
        emit Transfer(address(this), user.account, mined);
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param id Wallet ID
    function claimFor(uint64 id) public onlyRole(CLAIMER_ROLE) {
        _claimReward(id, 0);
    }

    function claimForRef(uint64 id) public onlyRole(CLAIMER_ROLE) {
        require(!_claimsDisabled, "Claims disabled");
        Storage storage user = _storages[id];
        require(!user.disabled, "Storage disabled");
        uint mined = user.refStorage;
        if (mined == 0) return;
        _claim(id, mined);
        user.refStorage = 0;
        emit Claim(id, user.account, mined);
        emit Transfer(address(this), user.account, mined);
    }

    /// @notice Mint amount of tokens to storage and distribute it to refers
    /// @param id Wallet ID
    /// @param amount Amount of tokens to mint
    /// @dev Calls claim
    function mintWithClaim(uint64 id, uint amount) public onlyRole(MINTER_ROLE) {
        _claimReward(id, amount);
    }

    /// @notice Mint amount of tokens only for one account
    /// @param id Wallet ID
    /// @param amount Amount of tokens to mint
    function mintForOne(uint64 id, uint amount) public onlyRole(MINTER_ROLE) {
        _claim(id, amount);
        emit Transfer(address(this), _storages[id].account, amount);
    }

    /// @notice Promotes storage to a new size level
    /// @param id Wallet ID
    /// @param levelPrice Promoting cost
    function _promoteSizeLevel(uint64 id, uint levelPrice) internal {
        Storage storage user = _storages[id];
        _claimReward(id, 0);
        user.sizeLevel++;
        emit LevelUpSize(id, user.account, user.sizeLevel, levelPrice);
    }

    /// @notice Admin can promote storage size for free
    /// @param id Wallet ID
    function promoteSizeLevel(uint64 id) public onlyRole(GRANTER_ROLE) {
        _promoteSizeLevel(id, 0);
    }

    /// @notice Storage owner can buy size level up
    /// @param id Wallet ID
    function buySizeLevel(uint64 id) public onlyStorageOwner(id) {
        Storage storage user = _storages[id];
        uint levelPrice = _getSizePrice(user);
        require(balanceOf(user.account) >= levelPrice, "Not enough funds");

        _burn(id, levelPrice);
        _promoteSizeLevel(id, levelPrice);
        emit Transfer(user.account, address(0), levelPrice);
    }

    /// @notice Promotes storage to a new speed level
    /// @param id Wallet ID
    /// @param levelPrice Promoting cost
    function _promoteSpeedLevel(uint64 id, uint levelPrice) internal {
        Storage storage user = _storages[id];
        _claimReward(id, 0);
        user.speedLevel++;
        emit LevelUpSpeed(id, user.account, user.speedLevel, levelPrice);
    }

    /// @notice Admin can promote storage speed for free
    /// @param id Wallet ID
    function promoteSpeedLevel(uint64 id) public onlyRole(GRANTER_ROLE) {
        _promoteSpeedLevel(id, 0);
    }

    /// @notice Storage owner can buy speed level up
    /// @param id Wallet ID
    function buySpeedLevel(uint64 id) public onlyStorageOwner(id) {
        Storage storage user = _storages[id];
        uint levelPrice = _getSpeedPrice(user);
        require(balanceOf(user.account) >= levelPrice, "Not enough funds");

        _burn(id, levelPrice);
        _promoteSpeedLevel(id, levelPrice);
        emit Transfer(user.account, address(0), levelPrice);
    }

    /// @notice Promotes storage to a new speed level
    /// @param id Wallet ID
    /// @param levelPrice Promoting cost
    function _promoteReferralLevel(uint64 id, uint levelPrice) internal {
        Storage storage user = _storages[id];
        user.referralLevel++;
        emit LevelUpReferral(id, user.account, user.referralLevel, levelPrice);
    }

    /// @notice Admin can promote storage speed for free
    /// @param id Wallet ID
    function promoteReferralLevel(uint64 id) public onlyRole(GRANTER_ROLE) {
        _promoteReferralLevel(id, 0);
    }

    /// @notice Storage owner can buy speed level up
    /// @param id Wallet ID
    function buyReferralLevel(uint64 id) public onlyStorageOwner(id) {
        Storage storage user = _storages[id];
        uint levelPrice = _getReferralPrice(user);
        require(balanceOf(user.account) >= levelPrice, "Not enough funds");

        _burn(id, levelPrice);
        _promoteReferralLevel(id, levelPrice);
        emit Transfer(user.account, address(0), levelPrice);
    }

    /// @notice Access controller can create storage for Pixel Wallet user on his address
    /// @param id Wallet ID
    /// @param account Wallet address
    /// @param parent Pixel Wallet inviter
    function grantAccess(uint64 id, address account, uint64 parent) public onlyRole(GRANTER_ROLE) {
        _accounts[account] = id;
        _storages[id].account = account;
        if (_storages[id].claimTimestamp == 0) {
            _storages[id].claimTimestamp = block.timestamp;
            _storages[id].speedMultiplicator = uint24(_PERCENT_PRECISION);
            _storages[id].sizeMultiplicator = uint24(_PERCENT_PRECISION);
            _storages[id].referralMultiplicator = uint24(_PERCENT_PRECISION);
            _storages[id].speedPrice = uint24(_PERCENT_PRECISION);
            _storages[id].sizePrice = uint24(_PERCENT_PRECISION);
            _storages[id].referralPrice = uint24(_PERCENT_PRECISION);
        }
        if (parent != 0 && parent != id) {
            _storages[id].parent = parent;
            emit SetParent(id, parent);
        }
        emit GrantAccess(id, account, _msgSender());
    }

    /// @notice Admins can disable storage
    /// @param id Wallet ID
    function disableStorage(uint64 id) public onlyRole(GRANTER_ROLE) {
        _storages[id].disabled = true;
        emit SetStorageDisabled(id, _msgSender(), true);
    }

    /// @notice Admins can enable storage again
    /// @param id Wallet ID
    function enableStorage(uint64 id) public onlyRole(GRANTER_ROLE) {
        _storages[id].disabled = false;
        emit SetStorageDisabled(id, _msgSender(), false);
    }

    /// @notice Returns total claimed amount by storages
    /// @return Claimed tokens amount
    function totalClaimed() public view virtual returns (uint) {
        return _claimed;
    }

    /// @notice Returns total burned amount by storages
    /// @return Burned tokens amount
    function totalBurned() public view virtual returns (uint) {
        return _burned;
    }

    /// @notice Contract owner can disable claims
    function disableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = true;
        emit SetClaimsDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable claims again
    function enableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = false;
        emit SetClaimsDisabled(_msgSender(), false);
    }

    /// @notice Contract owner can disable parents rewards
    function disableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = true;
        emit SetRefersDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable parents rewards again
    function enableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = false;
        emit SetRefersDisabled(_msgSender(), false);
    }

    /// @notice Contract owner set base tokens per hour value
    function setBaseSpeed(uint speed) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSpeed = speed;
    }

    function setBaseSize(uint size) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSize = size;
    }

    function setDiffuculty(uint value) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _difficulty = value;
    }

    /// @notice Owner can set a new parents percents
    /// @param _percents Array of percents
    function setParentsPercents(uint[2] calldata _percents) public onlyRole(DEFAULT_ADMIN_ROLE) {
        parentsPercents = _percents;
    }

    /// @notice Storage info getter
    /// @param id Wallet ID
    function getStorage(uint64 id) public view returns (
        Storage memory user,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
    ) {
        Storage storage _user = _storages[id];
        user = _user;
        prices[0] = _getSpeedPrice(_user);
        prices[1] = _getSizePrice(_user);
        prices[2] = _getReferralPrice(_user);
        mined = _getMinedTokens(id);
        sizeLimit = _getSize(_user);
        rewardPerSecond = _getSpeed(_user);
        refLimit = _getReferralSize(_user);
        balance = _balances[id];
        coins = user.account.balance;
    }

    /// @notice Migrate storage from old contract
    /// @dev Can't migrate a single storage twice
    function migrateStorage(
        uint64 id,
        uint64 parent,
        address account,
        uint claimTimestamp,
        uint8 sizeLevel,
        uint8 speedLevel,
        uint claimed,
        uint balance
    ) public onlyRole(GRANTER_ROLE) {
        Storage storage user = _storages[id];
        _balances[id] = balance > 0 ? balance + _PRECISION : balance;
        _accounts[account] = id;
        if (parent != 0 && parent != id) {
            user.parent = parent;
            emit SetParent(id, parent);
        }
        user.account = account;
        user.claimTimestamp = claimTimestamp == 0
            ? block.timestamp
            : claimTimestamp;
        user.sizeLevel = sizeLevel > 0 ? sizeLevel + 1 : 0;
        user.speedLevel = speedLevel > 0 ? speedLevel + 1 : 0;
        user.claimed = claimed;
        _claimed += claimed;
        user.speedMultiplicator = uint24(_PERCENT_PRECISION);
        user.sizeMultiplicator = uint24(_PERCENT_PRECISION);
        user.referralMultiplicator = uint24(_PERCENT_PRECISION);
        user.speedPrice = uint24(_PERCENT_PRECISION);
        user.sizePrice = uint24(_PERCENT_PRECISION);
        user.referralPrice = uint24(_PERCENT_PRECISION);

        emit GrantAccess(id, account, _msgSender());
    }

    /// @notice Admins can burn amount tokens from total supply
    /// @param amount Amount of tokens to burn
    function burnTotalSupply(uint amount) public onlyRole(BURNER_ROLE) {
        _totalSupply -= amount;
        emit BurnTotalSupply(_msgSender(), amount);
    }

    /// @notice Migrate storages from PXLs v2
    function migrateMany(
        uint64[] calldata users,
        address source
    ) public onlyRole(GRANTER_ROLE) {
        IPXLs3 old = IPXLs3(source);
        for (uint i; i < users.length; i++) {
            uint64 id = users[i];
            (
                uint claimTimestamp,
                uint8 sizeLevel,
                uint8 speedLevel,
                uint claimed,
                ,
                ,
                ,
                address account,
                uint256 balance,
                ,
                uint64 parent,
            ) = old.getStorage(id);
            if (account == address(0)) continue;
            migrateStorage(
                id,
                parent,
                account,
                claimTimestamp,
                sizeLevel,
                speedLevel,
                claimed,
                balance
            );
        }
    }

}