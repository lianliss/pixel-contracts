//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/libraries/PixelLibrary.sol";
import "contracts/V4/interfaces/IPixelNFT.sol";

contract PixelNFT is IPixelNFT, AccessControl {
    using Strings for uint256;
    using EnumerableSet for EnumerableSet.UintSet;

    string private constant _name = "Pixel Item NFT";
    string private constant _symbol = "PXLNFT";
    string private _baseURI = "";
    string private _baseSuffix = ".png";
    bool private _transfersDisabled = true;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant OPERATOR_ROLE = keccak256("OPERATOR_ROLE");

    mapping(uint256 tokenId => address) private _owners;
    mapping(address owner => uint256) private _balances;
    mapping(uint256 tokenId => address) private _tokenApprovals;

    mapping(address owner => EnumerableSet.UintSet) private _ownerTokens;

    ItemType[] private _types;
    string[] private _collections;
    mapping(uint256 tokenId => uint256) private _tokensTypes;
    uint256 private _tokenIdCounter;

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
    }

    function createCollection(string calldata collectionName) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _collections.push(collectionName);
        emit CollectionCreated(_collections.length - 1, collectionName);
    }

    function getCollections() public view returns (string[] memory) {
        return _collections;
    }

    function createType(string calldata typeName, string calldata _tokenURI, uint8[] calldata slots, uint collectionId, uint8 rarity) public onlyRole(DEFAULT_ADMIN_ROLE) {
        ItemType storage newType = _types.push();
        newType.tokenURI = _tokenURI;
        newType.name = typeName;
        newType.slots = slots;
        newType.collectionId = collectionId;
        newType.rarity = rarity;
        emit TypeCreated(_types.length - 1, typeName);
    }

    function addVariant(uint256 typeId, uint8[] calldata parameters, uint24[] calldata multipliers, uint256[] calldata adductors) public onlyRole(DEFAULT_ADMIN_ROLE) {
        ItemType storage t = _types[typeId];
        Modifier[] storage v = t.variants.push();
        for (uint8 i; i < parameters.length; i++) {
            v.push(Modifier(parameters[i], multipliers[i], adductors[i]));
        }
        emit VariantAdded(typeId, t.variants.length - 1);
    }

    function clearVariants(uint256 typeId) public onlyRole(DEFAULT_ADMIN_ROLE) {
        delete _types[typeId].variants;
        emit VariantsCleared(typeId);
    }

    function getTypesLength() public view returns (uint) {
        return _types.length;
    }

    function getTypes(uint offset, uint limit) public view returns(ItemType[] memory) {
        ItemType[] memory types = new ItemType[](limit);
        for (uint i; i < limit; i++) {
            types[i] = _types[i + offset];
        }
        return types;
    }

    function getTypes() public view returns(ItemType[] memory) {
        return _types;
    }

    function getTokenType(uint256 tokenId) public view returns (ItemType memory) {
        return _types[_tokensTypes[tokenId]];
    }

    function getTokenTypes(uint256[] calldata tokenIds) public view returns (ItemType[] memory) {
        ItemType[] memory types = new ItemType[](tokenIds.length);
        for (uint i; i < tokenIds.length; i++) {
            types[i] = _types[_tokensTypes[tokenIds[i]]];
        }
        return types;
    }

    function getOwnerTokensCount(address owner) public view returns (uint256) {
        return _ownerTokens[owner].length();
    }

    function getOwnerTokenAt(address owner, uint256 index) public view returns (uint256) {
        return _ownerTokens[owner].at(index);
    }

    function getOwnerTokens(address owner, uint offset, uint limit) public view returns (uint256[] memory, ItemType[] memory) {
        uint256 length = getOwnerTokensCount(owner);
        uint256 count = offset + limit > length
            ? length - offset
            : limit;
        uint256[] memory tokens = new uint256[](count);
        ItemType[] memory types = new ItemType[](count);
        for (uint256 i; i < count; i++) {
            tokens[i] = getOwnerTokenAt(owner, i + offset);
            types[i] = getTokenType(tokens[i]);
        }
        return (tokens, types);
    }

    function tokenURI(uint256 tokenId) public view override returns (string memory) {
        _requireOwned(tokenId);

        string memory baseURI = _baseURI;
        uint256 typeId = _tokensTypes[tokenId];
        ItemType storage tokenType = _types[typeId];
        return bytes(tokenType.tokenURI).length == 0
        ? string.concat(baseURI, typeId.toString(), _baseSuffix)
        : tokenType.tokenURI;
    }

    function setBaseURI(string calldata newValue) public onlyRole(DEFAULT_ADMIN_ROLE) returns (string memory) {
        return _baseURI = newValue;
    }

    function setBaseSuffix(string calldata newValue) public onlyRole(DEFAULT_ADMIN_ROLE) returns (string memory) {
        return _baseSuffix = newValue;
    }

    function mint(address to, uint256 typeId) public onlyRole(MINTER_ROLE) returns (uint) {
        require(typeId < _types.length, "Unknown token type");
        uint256 tokenId = ++_tokenIdCounter;
        _mint(to, tokenId);
        _tokensTypes[tokenId] = typeId;
        emit Mint(to, typeId, tokenId);
        return tokenId;
    }

    
    function burn(uint256 tokenId) public onlyRole(BURNER_ROLE) {
        address owner = _ownerOf(tokenId);
        if (!hasRole(BURNER_ROLE, _msgSender()) && _msgSender() != owner) {
            revert AccessControlUnauthorizedAccount(_msgSender(), BURNER_ROLE);
        }
        _burn(tokenId);
        emit Burn(owner, tokenId);
    }

    function balanceOf(address owner) public view returns (uint256) {
        if (owner == address(0)) {
            revert ERC721InvalidOwner(address(0));
        }
        return _balances[owner];
    }

    function ownerOf(uint256 tokenId) public view returns (address) {
        return _requireOwned(tokenId);
    }

    function name() public pure returns (string memory) {
        return _name;
    }

    function symbol() public pure returns (string memory) {
        return _symbol;
    }

    function approve(address to, uint256 tokenId) public {
        _approve(to, tokenId, _msgSender());
    }

    function getApproved(uint256 tokenId) public view returns (address) {
        _requireOwned(tokenId);

        return _getApproved(tokenId);
    }

    function transferFrom(address from, address to, uint256 tokenId) public {
        if (to == address(0)) {
            revert ERC721InvalidReceiver(address(0));
        }
        // Setting an "auth" arguments enables the `_isAuthorized` check which verifies that the token exists
        // (from != 0). Therefore, it is not needed to verify that the return value is not 0 here.
        address previousOwner = _update(to, tokenId, _msgSender());
        if (previousOwner != from) {
            revert ERC721IncorrectOwner(from, tokenId, previousOwner);
        }
    }

    function _ownerOf(uint256 tokenId) internal view returns (address) {
        return _owners[tokenId];
    }

    function _getApproved(uint256 tokenId) internal view returns (address) {
        return _tokenApprovals[tokenId];
    }

    function setTransfersDisabled(bool value) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _transfersDisabled = value;
    }

    function _isAuthorized(address owner, address spender, uint256 tokenId) internal view returns (bool) {
        return
            spender != address(0) &&
            (hasRole(OPERATOR_ROLE, _msgSender())
            || (owner == spender && !_transfersDisabled)
            || (_getApproved(tokenId) == spender && !_transfersDisabled));
    }

    function _checkAuthorized(address owner, address spender, uint256 tokenId) internal view {
        if (!_isAuthorized(owner, spender, tokenId)) {
            if (owner == address(0)) {
                revert ERC721NonexistentToken(tokenId);
            } else {
                revert ERC721InsufficientApproval(spender, tokenId);
            }
        }
    }

    function _increaseBalance(address account, uint128 value) internal {
        unchecked {
            _balances[account] += value;
        }
    }

    function _update(address to, uint256 tokenId, address auth) internal returns (address) {
        address from = _ownerOf(tokenId);

        // Perform (optional) operator check
        if (auth != address(0)) {
            _checkAuthorized(from, auth, tokenId);
        }

        // Execute the update
        if (from != address(0)) {
            // Clear approval. No need to re-authorize or emit the Approval event
            _approve(address(0), tokenId, address(0), false);

            unchecked {
                _balances[from] -= 1;
            }
            _ownerTokens[from].remove(tokenId);
        }

        if (to != address(0)) {
            unchecked {
                _balances[to] += 1;
            }
            _ownerTokens[to].add(tokenId);
        }

        _owners[tokenId] = to;

        emit Transfer(from, to, tokenId);

        return from;
    }

    function _mint(address to, uint256 tokenId) internal {
        if (to == address(0)) {
            revert ERC721InvalidReceiver(address(0));
        }
        address previousOwner = _update(to, tokenId, address(0));
        if (previousOwner != address(0)) {
            revert ERC721InvalidSender(address(0));
        }
    }

    function _burn(uint256 tokenId) internal {
        address previousOwner = _update(address(0), tokenId, address(0));
        if (previousOwner == address(0)) {
            revert ERC721NonexistentToken(tokenId);
        }
    }

    function _transfer(address from, address to, uint256 tokenId) internal {
        if (to == address(0)) {
            revert ERC721InvalidReceiver(address(0));
        }
        address previousOwner = _update(to, tokenId, address(0));
        if (previousOwner == address(0)) {
            revert ERC721NonexistentToken(tokenId);
        } else if (previousOwner != from) {
            revert ERC721IncorrectOwner(from, tokenId, previousOwner);
        }
    }

    function _approve(address to, uint256 tokenId, address auth) internal {
        _approve(to, tokenId, auth, true);
    }

    function _approve(address to, uint256 tokenId, address auth, bool emitEvent) internal virtual {
        // Avoid reading the owner unless necessary
        if (emitEvent || auth != address(0)) {
            address owner = _requireOwned(tokenId);

            // We do not use _isAuthorized because single-token approvals should not be able to call approve
            if (auth != address(0) && owner != auth) {
                revert ERC721InvalidApprover(auth);
            }

            if (emitEvent) {
                emit Approval(owner, to, tokenId);
            }
        }

        _tokenApprovals[tokenId] = to;
    }

    function _requireOwned(uint256 tokenId) internal view returns (address) {
        address owner = _ownerOf(tokenId);
        if (owner == address(0)) {
            revert ERC721NonexistentToken(tokenId);
        }
        return owner;
    }

    function isApprovedForAll(address, address) external pure returns (bool) {
        return false;
    }

    function safeTransferFrom(address from, address to, uint256 tokenId, bytes calldata data) public virtual {

    }

    function safeTransferFrom(address from, address to, uint256 tokenId) public {
        
    }

    function setApprovalForAll(address operator, bool approved) public {

    }
}