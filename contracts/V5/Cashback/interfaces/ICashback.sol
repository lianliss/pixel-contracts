//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface ICashback {
    function cashback(uint64 userId, uint fromAmount) external;
    function cashback(address account, uint fromAmount) external;
}