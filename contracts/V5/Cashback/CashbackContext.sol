//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/Proxy.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
 
/// @title Base context of CashbackContext
/// @author Danil Sakhinov
/// @notice Use to maintain the structural integrity of the proxies
contract CashbackContext is Proxy {
    address payable internal _receiver;
    IERC20 internal _erc20;
    IPXLsCoreV5 internal _core;
    uint[] internal _parentsPercents = [
        0
    ];
}