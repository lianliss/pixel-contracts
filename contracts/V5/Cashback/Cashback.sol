//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Cashback/CashbackContext.sol";
import "contracts/V5/Cashback/interfaces/ICashback.sol";

/// @notice Distributes the received payment between referrers
/// @dev All users of cashback must have role CLAIMER_ROLE
contract Cashback is CashbackContext, ICashback {

    bytes32 public constant CLAIMER_ROLE = keccak256("CLAIMER_ROLE");

    constructor(address coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(CLAIMER_ROLE, _msgSender());

        _core = IPXLsCoreV5(coreAddress);
        _receiver = payable(_msgSender());
    }

    function setCore(address coreAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV5(coreAddress);
    }

    function setReceiver(address receiverAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _receiver = payable(receiverAddress);
    }

    function setERC20(address erc20Address) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _erc20 = IERC20(erc20Address);
    }

    function _send(address payable to, uint amount) private {
        if (address(_erc20) == address(0)) {
            (bool sent,) = to.call{value: amount}("");
            require(sent, "Failed to send Ether");
        } else {
            _erc20.transfer(to, amount);
        }
    }

    function _getUser(uint64 userId) private view returns (address account, uint64 parentId) {
        (Storage memory user,,,,,,,,) = _core.getStorage(userId);
        return (user.account, user.parent);
    }

    function getBalance() public view returns (uint) {
        if (address(_erc20) == address(0)) {
            return address(this).balance;
        } else {
            return _erc20.balanceOf(address(this));
        }
    }

    event CashbackPayment(uint64 indexed originId, uint64 indexed referralId, uint64 indexed receiverId, uint amount);
    event RemainingPayment(uint64 indexed senderId, uint amount);
    function _onReceive(uint64 userId, uint amount) private {
        uint rest = amount;
        uint balance = getBalance();
        if (balance < rest) {
            rest = balance;
        }

        uint64 currentId = userId;
        (address currentAddress, uint64 currentParentId) = _getUser(userId);
        uint index;
        while (index < _parentsPercents.length
        && currentParentId != 0
        && rest > 0) {
            uint64 referralId = currentId;
            currentId = currentParentId;
            (currentAddress, currentParentId) = _getUser(currentId);
            uint currentAmount = rest * _parentsPercents[index] / _PERCENT_PRECISION;
            if (currentAmount > rest) {
                currentAmount = rest;
            }
            if (currentAmount > 0) {
                _send(payable(currentAddress), currentAmount);
                emit CashbackPayment(userId, referralId, currentId, currentAmount);
            }

            rest -= currentAmount;
            index++;
        }

        if (rest > 0) {
            _send(_receiver, rest);
            emit RemainingPayment(userId, rest);
        }
    }

    function cashback(uint64 userId, uint fromAmount) public onlyRole(CLAIMER_ROLE) {
        _onReceive(userId, fromAmount);
    }

    function cashback(address account, uint fromAmount) public onlyRole(CLAIMER_ROLE) {
        _onReceive(_core.getAddressId(account), fromAmount);
    }

    receive() external payable {}

    function flush() public onlyRole(DEFAULT_ADMIN_ROLE) {
        if (address(_erc20) != address(0)) {
            uint balance = _erc20.balanceOf(_msgSender());
            if (balance > 0) {
                _erc20.transfer(_msgSender(), balance);
            }
        }
        if (_msgSender().balance > 0) {
            (bool sent,) = _msgSender().call{value: _msgSender().balance}("");
            require(sent, "Failed to send Ether");
        }
    }

    function setParentsPercents(uint[] calldata newPercents) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _parentsPercents = newPercents;
    }

}