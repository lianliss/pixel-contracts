//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Slots/SlotsV5Structs.sol";
import "contracts/V5/NFT/NFTV5Structs.sol";

interface IPXLsSlotsV5 {

    event Recieved(address indexed account, uint amount);
    event SlotUnlocked(uint64 indexed userId, uint indexed slotTypeId, uint ercPrice, uint pixelPrice);
    event Equip(uint64 indexed userId, uint indexed slotTypeId, uint indexed typeId, uint tokenId, uint variantId);
    event Unequip(uint64 indexed userId, uint indexed slotTypeId, uint indexed typeId, uint tokenId);

    function getRegisteredParameters() external view returns (uint8);

    function getCustomReceiver(uint8 parameterId) external view returns (address);
    function getSlotsTypes() external view returns (SlotType[] memory);
    function getUserSlots(uint64 userId) external view returns (Slot[] memory, ItemType[] memory);
    function unlockSlot(uint8 slotTypeId) external;
    function unlockSlotSGB(uint8 slotTypeId) external payable;
    function equip(uint tokenId, uint8 slotTypeId, uint variantId, uint64 id) external;
    function equip(uint tokenId, uint8 slotTypeId, uint variantId) external;
    function unequip(uint8 slotTypeId) external;
    function unequip(uint8 slotTypeId, uint64 id) external;
    function calculate(uint64 userId) external;

}