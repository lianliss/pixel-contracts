//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IParameterReceiver {
    function updateParameter(uint64 userId, uint8 parameterId, uint24 multiplier, uint256 adder) external;
}