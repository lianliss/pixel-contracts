//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPXLsSlotsV5Migration {
    function migrate() external;
    function migrate(uint64 userId) external;
    function migrate(uint64[] calldata users) external;
}