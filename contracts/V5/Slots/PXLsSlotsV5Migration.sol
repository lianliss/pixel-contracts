//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Slots/SlotsV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5Migration.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5Migration.sol";

struct OldItemType {
    Modifier[][] variants;
    string tokenURI;
    string name;
    uint8[] slots;
    uint collectionId;
    uint8 rarity;
}

interface IOldNFT {
    function burn(uint256 tokenId) external;
    function getTokenType(uint256 tokenId) external view returns (OldItemType memory);
}

interface IOldSlots {
    function getUserSlots(uint64 userId) external view returns (Slot[] memory);
}

/// @title Pixel Shard Slots Migration
/// @author Danil Sakhinov
/// @notice Required PXLsSlotsV5 role PROXY_ROLE
/// @notice Required PXLsNFTV5 role MINTER_ROLE
/// @notice Required IPixelNFT role BURNER_ROLE
contract PXLsSlotsV5Migration is SlotsV5Context, ProxyImplementation, IPXLsSlotsV5Migration {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");
    IPXLsSlotsV5 private _oldSlots;
    IOldNFT private _oldNFT;
    IPXLsNFTV5Migration private _nftMigration;

    mapping (uint64 userId => bool isMigrated) public migrated;

    event UserSlotsMigration(uint64 indexed userId, uint nftAmount);
    event Equip(uint64 indexed userId, uint indexed slotTypeId, uint indexed typeId, uint tokenId, uint variantId);

    /// @param slotsAddress PXLsSlotsV5 new
    /// @param oldSlotsAddress PXLSlots old
    /// @param oldNFTAddress PixelNFT old
    /// @param coreAddress PXLsCoreV5 new for authorization
    /// @param nftMigrationAddress PXLsNFTV5Migration new for types mapping
    constructor(
        address slotsAddress,
        address oldSlotsAddress,
        address oldNFTAddress,
        address coreAddress,
        address nftMigrationAddress
        ) ProxyImplementation(slotsAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        _oldSlots = IPXLsSlotsV5(oldSlotsAddress);
        _oldNFT = IOldNFT(oldNFTAddress);
        _core = IPXLsCoreV5(coreAddress);
        _nftMigration = IPXLsNFTV5Migration(nftMigrationAddress);
    }

    /// Internal methods

    /// @notice Apply item modifiers without calculation
    function saveModifier(
        OldItemType memory itemType,
        Slot memory slot,
        uint64 userId,
        uint8 slotId
    ) internal {
        Modifier[] memory variant = itemType.variants[slot.variantId];
        for (uint modIndex; modIndex < variant.length; modIndex++) {
            Modifier memory mod = itemType.variants[slot.variantId][modIndex];
            _slotsMods[userId][slotId][mod.parameterId] = mod;
        }
    }


    /// Proxy methods

    function _migrateSlots(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xMigrateSlots(bytes)", encoded);
        if (isProxy) {
            (
                uint64 userId,
                address oldSlotsAddress,
                address oldNFTAddress,
                address nftMigrationAddress
            ) = abi.decode(encoded, (uint64, address, address, address));

            /// Prepare required contracts
            IOldSlots oldSlots = IOldSlots(oldSlotsAddress);
            IOldNFT oldNFT = IOldNFT(oldNFTAddress);
            IPXLsNFTV5Migration nftMigration = IPXLsNFTV5Migration(nftMigrationAddress);
            
            Slot[] memory oldUserSlots = oldSlots.getUserSlots(userId);
            for (uint8 slotId; slotId < oldUserSlots.length; slotId++) {
                Slot storage slot = _slots[userId][slotId];
                /// Enable slot if it unlocked in old contract
                slot.isEnabled = oldUserSlots[slotId].isEnabled;

                /// Get old tokenId and skip empty slots
                uint oldTokenId = oldUserSlots[slotId].tokenId;
                if (oldTokenId == 0) continue;

                /// Get token type
                OldItemType memory itemType = oldNFT.getTokenType(oldTokenId);
                uint typeId = nftMigration.getURIType(itemType.tokenURI); /// Get typeId by tokenURI

                /// Mint token to new Slots contract
                slot.tokenId = _nft.mint(
                    address(this),
                    typeId
                );
                slot.variantId = oldUserSlots[slotId].variantId;

                // Save modifier
                saveModifier(itemType, slot, userId, slotId);
                
                /// Burn old token
                // oldNFT.burn(oldTokenId);
                emit Equip(userId, slotId, typeId, slot.tokenId, slot.variantId);
            }

            /// Calculate and send modifies to dependent contracts
            IPXLsSlotsV5(address(this)).calculate(userId);
        }
    }
    /// @notice proxy entrance
    function xMigrateSlots(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _migrateSlots(encoded);
    }
    function _migrate(uint64 userId) internal {
        if (!migrated[userId]) {
            _migrateSlots(abi.encode(
                userId,
                address(_oldSlots),
                address(_oldNFT),
                address(_nftMigration)
            ));
            migrated[userId] = true;
        }
    }
    /// @notice public entrance
    function migrate() public {
        uint64 userId = _core.getAddressId(_msgSender());
        require (userId > 0, "Unauthorized user");
        _migrate(userId);
    }
    /// @notice public entrance
    function migrate(uint64 userId) public onlyRole(BACKEND_ROLE) {
        _migrate(userId);
    }
    /// @notice public entrance
    function migrate(uint64[] calldata users) public onlyRole(BACKEND_ROLE) {
        for (uint i; i < users.length; i++) {
            _migrate(users[i]);
        }
    }

}