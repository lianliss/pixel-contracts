//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Slots/SlotsV5Context.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5.sol";

/// @title Settings editor for Pixel Shard token
/// @author Danil Sakhinov
/// @notice Required PXLsCoreV5 role PROXY_ROLE
contract PXLsSlotsV5Settings is SlotsV5Context, ProxyImplementation {

    /// @param slotsAddress PXLsCoreV5
    constructor(address slotsAddress) ProxyImplementation(slotsAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }


    function _setERC20(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetERC20(bytes)", encoded);
        if (isProxy) {
            (address erc20) = abi.decode(encoded, (address));
            _erc20 = IERC20(erc20);
        }
    }
    /// @notice Proxy entrance
    function xSetERC20(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setERC20(encoded);
    }
    /// @notice public entrance
    function setERC20(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setERC20(abi.encode(newAddress));
    }


    function _setRegisteredParameters(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetRegisteredParameters(bytes)", encoded);
        if (isProxy) {
            (uint8 amount) = abi.decode(encoded, (uint8));
            _registeredParameters = amount;
        }
    }
    /// @notice Proxy entrance
    function xSetRegisteredParameters(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setRegisteredParameters(encoded);
    }
    /// @notice public entrance
    function setRegisteredParameters(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setRegisteredParameters(abi.encode(amount));
    }


    function _setReceiver(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetReceiver(bytes)", encoded);
        if (isProxy) {
            (address payable account) = abi.decode(encoded, (address));
            _receiver = account;
        }
    }
    /// @notice Proxy entrance
    function xSetReceiver(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setReceiver(encoded);
    }
    /// @notice public entrance
    function setReceiver(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setReceiver(abi.encode(newAddress));
    }


    function _setCoreAddress(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetCoreAddress(bytes)", encoded);
        if (isProxy) {
            (address newAddress) = abi.decode(encoded, (address));
            _core = IPXLsCoreV5(newAddress);
        }
    }
    /// @notice Proxy entrance
    function xSetCoreAddress(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setCoreAddress(encoded);
    }
    /// @notice public entrance
    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setCoreAddress(abi.encode(newAddress));
    }


    function _setChestAddress(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetChestAddress(bytes)", encoded);
        if (isProxy) {
            (address newAddress) = abi.decode(encoded, (address));
            _chest = IPXLsChestV5(newAddress);
        }
    }
    /// @notice Proxy entrance
    function xSetChestAddress(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setChestAddress(encoded);
    }
    /// @notice public entrance
    function setChestAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setChestAddress(abi.encode(newAddress));
    }


    function _setNFTAddress(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetNFTAddress(bytes)", encoded);
        if (isProxy) {
            (address newAddress) = abi.decode(encoded, (address));
            _nft = IPXLsNFTV5(newAddress);
        }
    }
    /// @notice Proxy entrance
    function xSetNFTAddress(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setNFTAddress(encoded);
    }
    /// @notice public entrance
    function setNFTAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setNFTAddress(abi.encode(newAddress));
    }


    function _createSlot(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xCreateSlot(bytes)", encoded);
        if (isProxy) {
            (
                string memory title, uint sgbPrice, uint pixelPrice, uint8 requireSlot
            ) = abi.decode(encoded, (string, uint, uint, uint8));
            SlotType storage newSlot = _slotTypes.push();
            newSlot.title = title;
            newSlot.sgbPrice = sgbPrice;
            newSlot.pixelPrice = pixelPrice;
            newSlot.requireSlot = requireSlot;
        }
    }
    /// @notice Proxy entrance
    function xCreateSlot(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _createSlot(encoded);
    }
    /// @notice public entrance
    function createSlot(
        string memory title, uint sgbPrice, uint pixelPrice, uint8 requireSlot
    ) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _createSlot(abi.encode(title, sgbPrice, pixelPrice, requireSlot));
    }

    function _updateSlot(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xUpdateSlot(bytes)", encoded);
        if (isProxy) {
            (
                uint slotTypeId, string memory title, uint sgbPrice, uint pixelPrice, uint8 requireSlot
            ) = abi.decode(encoded, (uint, string, uint, uint, uint8));
            SlotType storage slotType = _slotTypes[slotTypeId];
            slotType.title = title;
            slotType.sgbPrice = sgbPrice;
            slotType.pixelPrice = pixelPrice;
            slotType.requireSlot = requireSlot;
        }
    }
    /// @notice Proxy entrance
    function xUpdateSlot(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _updateSlot(encoded);
    }
    /// @notice public entrance
    function updateSlot(
        uint slotTypeId, string memory title, uint sgbPrice, uint pixelPrice, uint8 requireSlot
    ) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _updateSlot(abi.encode(slotTypeId, title, sgbPrice, pixelPrice, requireSlot));
    }

    
    function _setCashback(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetCashback(bytes)", encoded);
        if (isProxy) {
            (address newAddress) = abi.decode(encoded, (address));
            _cashback = ICashback(newAddress);
        }
    }
    /// @notice Proxy entrance
    function xSetCashback(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setCashback(encoded);
    }
    /// @notice public entrance
    function setCashback(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setCashback(abi.encode(newAddress));
    }


    function _setParameterReceiver(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetParameterReceiver(bytes)", encoded);
        if (isProxy) {
            (uint8 parameterId, address receiver) = abi.decode(encoded, (uint8, address));
            _customReceivers[parameterId] = IParameterReceiver(receiver);
        }
    }
    /// @notice Proxy entrance
    function xSetParameterReceiver(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setParameterReceiver(encoded);
    }
    /// @notice public entrance
    function setParameterReceiver(uint8 parameterId, address receiver) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setParameterReceiver(abi.encode(parameterId, receiver));
    }
}