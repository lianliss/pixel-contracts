//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Slots/SlotsV5Context.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5.sol";

/// @notice require PXLsCoreV5 role BURNER_ROLE
/// @notice require PXLsCoreV5 role MULTIPLIER_ROLE
/// @notice require PXLsChestV5 role MULTIPLIER_ROLE
/// @notice require PXLsNFTV5 role OPERATOR_ROLE
contract PXLsSlotsV5 is SlotsV5Context, IPXLsSlotsV5 {

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    constructor() Proxy() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _receiver = payable(_msgSender());

        // createSlot("Pocket 1", 15*10**18, 5*10**18, 0); // 0
        // createSlot("Pocket 2", 30*10**18, 10*10**18, 0); // 1
        // createSlot("Backpack", 45*10**18, 15*10**18, 1); // 2
        // createSlot("Pocket 3", 60*10**18, 20*10**18, 2); // 3
        // createSlot("Belt", 75*10**18, 25*10**18, 3); // 4
        // createSlot("Pocket 4", 90*10**18, 30*10**18, 4); // 5
        // createSlot("Artifact", 0, 1*10**18, 0); // 6
        // createSlot("Drill", 60*10**18, 20*10**18, 0); // 7
        // createSlot("Back", 1, 1, 0); // 8
        // createSlot("Loyalty Card", 15*10**18, 5*10**18, 0); // 9
        // createSlot("Smart Claimer", 500*10**18, 150*10**18, 0); // 10

    }

    receive() external payable {
        (bool sent,) = _receiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit Recieved(_msgSender(), msg.value);
    }

    function getRegisteredParameters() public view returns (uint8) {
        return _registeredParameters;
    }

    function getCustomReceiver(uint8 parameterId) public view returns (address) {
        return address(_customReceivers[parameterId]);
    }

    function getSlotsTypes() public view returns (SlotType[] memory) {
        return _slotTypes;
    }

    function getUserSlots(uint64 userId) public view returns (Slot[] memory, ItemType[] memory) {
        Slot[] memory slots = new Slot[](_slotTypes.length);
        uint[] memory typeIds = new uint[](_slotTypes.length);
        for (uint8 slotTypeId; slotTypeId < _slotTypes.length; slotTypeId++) {
            slots[slotTypeId] = _slots[userId][slotTypeId];
            typeIds[slotTypeId] = slots[slotTypeId].tokenId;
        }
        return (slots, _nft.getTokenTypes(typeIds));
    }

    function unlockSlot(uint8 slotTypeId) public {
        uint64 userId = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(userId) == _msgSender(), "Not authorized");
        Slot storage slot = _slots[userId][slotTypeId];
        require(!slot.isEnabled, "Slot is already unlocked");
        SlotType storage slotType = _slotTypes[slotTypeId];
        require(slotTypeId == 0
        || slotType.requireSlot == 0
        || _slots[userId][slotType.requireSlot].isEnabled == true, "Required slot is not unlocked");
        if (slotType.pixelPrice > 0) {
            _core.burn(_msgSender(), slotType.pixelPrice);
            slot.isEnabled = true;
            emit SlotUnlocked(userId, slotTypeId, 0, slotType.pixelPrice);
        } else {
            revert("Slot is unreachable");
        }
    }

    function unlockSlotSGB(uint8 slotTypeId) public payable {
        uint64 userId = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(userId) == _msgSender(), "Not authorized");
        Slot storage slot = _slots[userId][slotTypeId];
        require(!slot.isEnabled, "Slot is already unlocked");
        SlotType storage slotType = _slotTypes[slotTypeId];
        require(slotTypeId == 0
        || slotType.requireSlot == 0
        || _slots[userId][slotType.requireSlot].isEnabled == true, "Required slot is not unlocked");
        if (slotType.sgbPrice > 0) {
            if (address(_erc20) != address(0)) {
                require(_erc20.transferFrom(
                    _msgSender(),
                    _receiver,
                    slotType.sgbPrice
                ), "Can't transfer ERC20");
                emit Recieved(_msgSender(), slotType.sgbPrice);
            } else {
                require(msg.value >= slotType.sgbPrice, "Not enough SGB");
                (bool sent,) = _receiver.call{value: msg.value}("");
                require(sent, "Failed to send Ether");
                emit Recieved(_msgSender(), msg.value);
            }
            if (address(_cashback) != address(0)) {
                _cashback.cashback(userId, slotType.sgbPrice);
            }
            slot.isEnabled = true;
            emit SlotUnlocked(userId, slotTypeId, slotType.sgbPrice, 0);
        } else {
            revert("Slot is unreachable");
        }
    }

    function _equip(uint tokenId, uint8 slotTypeId, uint variantId, uint64 userId, address account) private {
        Slot storage slot = _slots[userId][slotTypeId];
        require(slot.isEnabled, "Slot is not enabled");
        require(slot.tokenId == 0, "The slot is already occupied by the item");
        require(_nft.ownerOf(tokenId) == account, "Not an item owner");
        ItemType memory itemType = _nft.getTokenType(tokenId);
        require(variantId < itemType.variants.length, "Unreachable variant");
        uint8 itemSlotIndex;
        while (itemSlotIndex < itemType.slots.length
        && itemType.slots[itemSlotIndex] != slotTypeId) {
            itemSlotIndex++;
        }
        require(itemSlotIndex < itemType.slots.length, "Wrong slot");
        // Transfer item
        _nft.transferFrom(account, address(this), tokenId);
        // Fill slot
        slot.tokenId = tokenId;
        slot.variantId = variantId;
        // Save modifier
        Modifier[] memory variant = itemType.variants[variantId];
        for (uint modIndex; modIndex < variant.length; modIndex++) {
            Modifier memory mod = itemType.variants[variantId][modIndex];
            _slotsMods[userId][slotTypeId][mod.parameterId] = mod;
        }
        // Calculate with other slots
        _calculate(userId);
        emit Equip(userId, slotTypeId, itemType.typeId, tokenId, variantId);
    }

    function equip(uint tokenId, uint8 slotTypeId, uint variantId) public {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");
        _equip(tokenId, slotTypeId, variantId, id, account);
    }

    function equip(uint tokenId, uint8 slotTypeId, uint variantId, uint64 id) public onlyRole(EDITOR_ROLE) {
        address account = _core.getIdAddress(id);
        _equip(tokenId, slotTypeId, variantId, id, account);
    }

    function _unequip(uint8 slotTypeId, uint64 userId, address account) private {
        Slot storage slot = _slots[userId][slotTypeId];
        uint tokenId = slot.tokenId;
        require(tokenId > 0 && _nft.ownerOf(tokenId) == address(this), "The slot is empty");
        ItemType memory itemType = _nft.getTokenType(tokenId);
        uint variantId = slot.variantId;
        // Transfer item
        if (itemType.disposable) {
            _nft.burn(tokenId);
        } else {
            _nft.transferFrom(address(this), account, tokenId);
        }
        // Clear slot
        slot.tokenId = 0;
        slot.variantId = 0;
        // Clear modifier
        Modifier[] memory variant = itemType.variants[variantId];
        for (uint modIndex; modIndex < variant.length; modIndex++) {
            uint8 parameterId = itemType.variants[variantId][modIndex].parameterId;
            delete _slotsMods[userId][slotTypeId][parameterId];
        }
        // Calculate with other slots
        _calculate(userId);
        emit Unequip(userId, slotTypeId, itemType.typeId, tokenId);
    }

    function unequip(uint8 slotTypeId) public {
        uint64 id = _core.getAddressId(_msgSender());
        address account = _core.getIdAddress(id);
        require(account == _msgSender(), "Not authorized");
        _unequip(slotTypeId, id, account);
    }

    function unequip(uint8 slotTypeId, uint64 id) public onlyRole(EDITOR_ROLE) {
        address account = _core.getIdAddress(id);
        _unequip(slotTypeId, id, account);
    }

    function _calculate(uint64 userId) internal {
        Modifier[] memory params = new Modifier[](_registeredParameters);
        bool storageChanged = false;
        bool chancesChanged = false;
        for (uint8 parameterId; parameterId < _registeredParameters; parameterId++) {
            params[parameterId].mul = uint24(PERCENT_PRECISION);
            for (uint8 slotTypeId; slotTypeId < _slotTypes.length; slotTypeId++) {
                Modifier memory mod = _slotsMods[userId][slotTypeId][parameterId];
                if (mod.mul > 0) {
                    params[parameterId].mul = uint24(uint256(params[parameterId].mul)
                            * uint256(mod.mul)
                            / PERCENT_PRECISION);
                }
                if (mod.add > 0) {
                    params[parameterId].add += mod.add;
                }
            }
            bool isParameterChanged =
                _updatedMods[userId][parameterId].mul != params[parameterId].mul
                || _updatedMods[userId][parameterId].add != params[parameterId].add;
            // Collect changes or send a new parameter to receiver
            if (parameterId >= 0 && parameterId <= PARAM_REF_PRICE) {
                storageChanged = storageChanged || isParameterChanged;
            } else if (parameterId >= PARAM_RARITY_COMMON && parameterId <= PARAM_RARITY_LEGENDARY) {
                chancesChanged = chancesChanged || isParameterChanged;
            } else if (address(_customReceivers[parameterId]) != address(0)) {
                if (isParameterChanged) {
                    _customReceivers[parameterId]
                    .updateParameter(userId, parameterId, params[parameterId].mul, params[parameterId].add);                    
                }
            }
            // Update _updatedMods
            _updatedMods[userId][parameterId].mul = params[parameterId].mul;
            _updatedMods[userId][parameterId].add = params[parameterId].add;
        }
        

        if (storageChanged) {
            _core.setMultipliers(
                userId,
                [
                    params[PARAM_SPEED_MUL].mul,
                    params[PARAM_SIZE_MUL].mul,
                    params[PARAM_REF_MUL].mul
                ],
                [
                    params[PARAM_SPEED_ADDER].add,
                    params[PARAM_SIZE_ADDER].add,
                    params[PARAM_REF_ADDER].add
                ],
                [
                    params[PARAM_SPEED_PRICE].mul,
                    params[PARAM_SIZE_PRICE].mul,
                    params[PARAM_REF_PRICE].mul
                ]
            );
        }
        if (chancesChanged) {
            _chest.setUserChances(userId, [
                params[PARAM_RARITY_PIXEL].mul,
                params[PARAM_RARITY_COMMON].mul,
                params[PARAM_RARITY_UNCOMMON].mul,
                params[PARAM_RARITY_RARE].mul,
                params[PARAM_RARITY_EPIC].mul,
                params[PARAM_RARITY_LEGENDARY].mul
            ]);
        }
    }

    function calculate(uint64 userId) public onlyRole(PROXY_ROLE) {
        _calculate(userId);
    }

}