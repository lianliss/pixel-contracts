//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Slots/SlotsV5Context.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5.sol";

/// @title Slots fixer
/// @author Danil Sakhinov
/// @notice Required PXLsSlotsV5 role PROXY_ROLE
contract SlotsCalculateSelf is SlotsV5Context, ProxyImplementation {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");
    address private _slotsAddress;

    /// @param slotsAddress PXLsCoreV5
    constructor(address slotsAddress, address coreAddress) ProxyImplementation(slotsAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        _slotsAddress = slotsAddress;
        _core = IPXLsCoreV5(coreAddress);
    }

    /// @notice Recalculate slots modifieds and send it to corresponding contracts
    /// @notice internal entrance
    function _calculate(uint64 userId) internal {
        IPXLsSlotsV5(_slotsAddress).calculate(userId);
    }
    /// @notice private entrance
    function calculate(uint64 userId) public onlyRole(BACKEND_ROLE) {
        _calculate(userId);
    }
    /// @notice public user entrance
    function calculateSelf() public {
        uint64 userId = _core.getAddressId(_msgSender());
        if (userId == 0 || _core.getIdAddress(userId) != _msgSender()) return;
        _calculate(userId);
    }



    /// @notice Clear and refill slots modifiers with a fresh data from NFTs
    /// then calculate and send it to corresponding contracts
    function _flushSlots(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xFlushSlots(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            for (uint8 slotTypeId; slotTypeId < _slotTypes.length; slotTypeId++) {
                Slot storage slot = _slots[userId][slotTypeId];
                /// Clear slots params
                for (uint8 parameterId; parameterId < _registeredParameters; parameterId++) {
                    delete _slotsMods[userId][slotTypeId][parameterId];
                }
                if (slot.tokenId != 0) {
                    /// Get slot NFT type if exists
                    ItemType memory itemType = _nft.getTokenType(slot.tokenId);
                    /// Check current variant exists
                    if (itemType.variants.length > slot.variantId) {
                        Modifier[] memory variant = itemType.variants[slot.variantId];
                        /// Fill slot params with NFT params of this variant
                        for (uint modIndex; modIndex < variant.length; modIndex++) {
                            Modifier memory mod = itemType.variants[slot.variantId][modIndex];
                            _slotsMods[userId][slotTypeId][mod.parameterId] = mod;
                        }
                    }
                }
            }
        }
    }
    /// @notice Proxy entrance
    function xFlushSlots(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _flushSlots(encoded);
    }
    /// @notice private entrance
    function flushSlots(uint64 userId) public onlyRole(BACKEND_ROLE) {
        _flushSlots(abi.encode(userId));
        _calculate(userId);
    }
    /// @notice public user entrance
    function flushSlotsSelf() public {
        uint64 userId = _core.getAddressId(_msgSender());
        if (userId == 0 || _core.getIdAddress(userId) != _msgSender()) return;
        _flushSlots(abi.encode(userId));
        _calculate(userId);
    }

    
    /// @notice Release user's slot if it possible
    function _unequipForce(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xUnequipForce(bytes)", encoded);
        if (isProxy) {
            (uint8 slotTypeId, uint64 userId) = abi.decode(encoded, (uint8, uint64));
            Slot storage slot = _slots[userId][slotTypeId];
            uint tokenId = slot.tokenId;
            if (tokenId == 0) return;
            if (_nft.ownerOf(tokenId) == address(this)) {
                /// Try to transfer item
                ItemType memory itemType = _nft.getTokenType(tokenId);
                if (itemType.disposable) {
                    try _nft.burn(tokenId) {} catch {}
                } else {
                    address account = _core.getIdAddress(userId);
                    try _nft.transferFrom(address(this), account, tokenId) {} catch {}
                }
            }
            /// Clear slot
            slot.tokenId = 0;
            slot.variantId = 0;
        }
    }
    /// @notice Proxy entrance
    function xUnequipForce(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _unequipForce(encoded);
    }
    /// @notice private entrance
    function unequipForce(uint8 slotTypeId, uint64 userId) public onlyRole(BACKEND_ROLE) {
        _unequipForce(abi.encode(slotTypeId, userId));
        _flushSlots(abi.encode(userId));
        _calculate(userId);
    }
    /// @notice public user entrance
    function unequipForceSelf(uint8 slotTypeId) public {
        uint64 userId = _core.getAddressId(_msgSender());
        if (userId == 0 || _core.getIdAddress(userId) != _msgSender()) return;
        _unequipForce(abi.encode(slotTypeId, userId));
        _flushSlots(abi.encode(userId));
        _calculate(userId);
    }

}