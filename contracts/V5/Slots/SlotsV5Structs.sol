//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct SlotType {
    string title;
    uint sgbPrice;
    uint pixelPrice;
    uint8 requireSlot;
}

struct Slot {
    uint tokenId;
    uint variantId;
    bool isEnabled;
}