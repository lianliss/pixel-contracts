//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Chest/interfaces/IPXLsChestV5.sol";
import "contracts/V5/Slots/interfaces/IParameterReceiver.sol";
import "contracts/V5/Slots/SlotsV5Structs.sol";
import "contracts/Proxy/Proxy.sol";
import "contracts/V5/Cashback/interfaces/ICashback.sol";

contract SlotsV5Context is Proxy {

    uint8 internal _registeredParameters = 17;
    mapping (uint8 parameterId => IParameterReceiver) public _customReceivers;
    SlotType[] internal _slotTypes;
    mapping (uint64 userId => mapping(uint8 slotTypeId => Slot slot)) internal _slots;
    mapping (uint64 userId => mapping(uint8 slotTypeId => mapping(uint8 parameterId => Modifier))) internal _slotsMods;
    mapping (uint64 userId => mapping(uint8 parameterId => Modifier)) internal _updatedMods;
    address payable internal _receiver;
    IPXLsCoreV5 internal _core;
    IPXLsChestV5 internal _chest;
    IPXLsNFTV5 internal _nft;
    IERC20 public _erc20;
    ICashback public _cashback;

}
