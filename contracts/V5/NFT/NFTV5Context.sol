//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/Proxy.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/libraries/PixelLibrary.sol";
import "contracts/V5/NFT/NFTV5Structs.sol";

/// @title Base context of PXLsNFTV5
/// @author Danil Sakhinov
/// @notice Use to maintain the structural integrity of the proxies
contract NFTV5Context is Proxy {
    using EnumerableSet for EnumerableSet.UintSet;

    ItemType[] internal _types;
    string[] internal _collections;
    mapping(uint256 tokenId => uint256) internal _tokensTypes;
    uint256 internal _tokenIdCounter;

    bool internal _transfersDisabled = true;

    mapping(uint256 tokenId => address) internal _owners;
    mapping(address owner => uint256) internal _balances;

    mapping(address owner => EnumerableSet.UintSet) internal _ownerTokens;
}