//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/NFT/NFTV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5Migration.sol";

interface INFTV5Editor {
    function createCollection(string calldata collectionName) external;
    function createType(
        string calldata typeName,
        string calldata _tokenURI,
        uint8[] calldata slots,
        uint collectionId,
        uint8 rarity,
        bool soulbound,
        bool disposable
    ) external;

    function addVariant(
        uint256 typeId,
        uint8[] calldata parameters,
        uint24[] calldata multipliers,
        uint256[] calldata adductors
    ) external;
}

struct ItemTypeV4 {
    Modifier[][] variants;
    string tokenURI;
    string name;
    uint8[] slots;
    uint collectionId;
    uint8 rarity;
}

interface INFTV4 {
    function burn(uint256 tokenId) external;
    function getCollections() external view returns (string[] memory);
    function getTypes(uint offset, uint limit) external view returns(ItemTypeV4[] memory);
    function getTypesLength() external view returns (uint);
    function getTokenType(uint256 tokenId) external view returns (ItemTypeV4 memory);
    function getOwnerTokensCount(address owner) external view returns (uint256);
    function getOwnerTokenAt(address owner, uint256 index) external view returns (uint256);
    function getOwnerTokens(address owner, uint offset, uint limit) external view returns (uint256[] memory, ItemTypeV4[] memory);
}

/// @title NFT Migration for Pixel Wallet
/// @author Danil Sakhinov
/// @notice Need V4 (PixelNFT) role BURNER_ROLE
/// @notice Need V5 (PXLsNFTV5) role MINTER_ROLE
/// @notice Need NFTV5Editor role EDITOR_ROLE
contract PXLsNFTV5Migration is NFTV5Context, ProxyImplementation, IPXLsNFTV5Migration {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");
    INFTV5Editor public editor;
    INFTV4 public v4;

    mapping(string tokenURI => uint typeId) public typesMapping;

    /// @param nftAddress PXLsNFTV5
    /// @param editorAddress NFTV5Editor
    /// @param v4Address PixelNFT
    constructor(
        address nftAddress,
        address editorAddress,
        address v4Address
    ) ProxyImplementation(nftAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        editor = INFTV5Editor(editorAddress);
        v4 = INFTV4(v4Address);
    }

    function getURIType(string calldata tokenURI) public view returns (uint) {
        return typesMapping[tokenURI];
    }

    function migrateCollections() public onlyRole(BACKEND_ROLE) {
        string[] memory collections = v4.getCollections();
        for (uint i; i < collections.length; i++) {
            editor.createCollection(collections[i]);
        }
    }

    function migrateTypes(uint limit) public onlyRole(BACKEND_ROLE) {
        IPXLsNFTV5 v5 = IPXLsNFTV5(address(getProxy()));
        require(v5.getCollections().length > 0, "Perform migrateCollections first");
        uint offset = v5.getTypesLength();

        uint length = v4.getTypesLength();
        require(length > offset, "No types left");
        if (limit < length) {
            length = limit;
        }
        ItemTypeV4[] memory types = v4.getTypes(offset, length);
        
        for (uint i; i < types.length; i++) {
            // Migrate type
            ItemTypeV4 memory current = types[i];
            editor.createType(
                current.name,
                current.tokenURI,
                current.slots,
                current.collectionId,
                current.rarity,
                false,
                false
            );

            // Set typeId
            uint typeId = offset + i;
            typesMapping[current.tokenURI] = typeId;

            // Migrate variants
            for (uint j; j < current.variants.length; j++) {
                uint paramsCount = current.variants[j].length;
                uint8[] memory parameters = new uint8[](paramsCount);
                uint24[] memory multipliers = new uint24[](paramsCount);
                uint256[] memory adductors = new uint256[](paramsCount);
                for (uint p; p < paramsCount; p++) {
                    parameters[p] = current.variants[j][p].parameterId;
                    multipliers[p] = current.variants[j][p].mul;
                    adductors[p] = current.variants[j][p].add;
                }

                editor.addVariant(typeId, parameters, multipliers, adductors);
            }
        }
    }

    function _migrate(address owner, uint limit) internal {
        IPXLsNFTV5 v5 = IPXLsNFTV5(address(getProxy()));
        uint length = v4.getOwnerTokensCount(owner);
        if (limit < length) {
            length = limit;
        }
        (uint256[] memory userTokens, ItemTypeV4[] memory userTypes) = v4.getOwnerTokens(owner, 0, length);

        for (uint i; i < length; i++) {
            uint typeId = typesMapping[userTypes[i].tokenURI];
            v5.mint(owner, typeId);
            v4.burn(userTokens[i]);
        }
    }

    function migrate(address owner, uint limit) public onlyRole(BACKEND_ROLE) {
        _migrate(owner, limit);
    }

    function migrate(uint limit) public {
        _migrate(_msgSender(), limit);
    }
    
}