//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/NFT/NFTV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";

/// @title NFT Editor for Pixel Wallet
/// @author Danil Sakhinov
/// @notice Need PXLsNFTV5 role PROXY_ROLE
contract PXLsNFTV5Editor is NFTV5Context, ProxyImplementation {

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    /// @param nftAddress PXLsNFTV5
    constructor(address nftAddress) ProxyImplementation(nftAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }

    event CollectionCreated(uint indexed collectionId, string collectionName);
    event TypeCreated(uint256 indexed typeId, string name);
    event TypeEdited(uint256 indexed typeId);
    event VariantAdded(uint256 indexed typeId, uint256 variantId);
    event VariantsCleared(uint256 indexed typeId);


    function _createCollection(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xCreateCollection(bytes)", encoded);
        if (isProxy) {
            (string memory collectionName) = abi.decode(encoded, (string));
            _collections.push(collectionName);
            emit CollectionCreated(_collections.length - 1, collectionName);
        }
    }
    /// @notice Proxy entrance
    function xCreateCollection(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _createCollection(encoded);
    }
    /// @notice public entrance
    function createCollection(string calldata collectionName) public onlyRole(EDITOR_ROLE) {
        _createCollection(abi.encode(collectionName));
    }


    function _createType(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xCreateType(bytes)", encoded);
        if (isProxy) {
            (
                string memory typeName,
                string memory _tokenURI,
                uint8[] memory slots,
                uint collectionId,
                uint8 rarity,
                bool soulbound,
                bool disposable
            ) = abi.decode(encoded, (string, string, uint8[], uint, uint8, bool, bool));
            ItemType storage newType = _types.push();

            newType.typeId = _types.length - 1;
            newType.tokenURI = _tokenURI;
            newType.name = typeName;
            newType.slots = slots;
            newType.collectionId = collectionId;
            newType.rarity = rarity;
            newType.soulbound = soulbound;
            newType.disposable = disposable;

            emit TypeCreated(newType.typeId, typeName);
        }
    }
    /// @notice Proxy entrance
    function xCreateType(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _createType(encoded);
    }
    /// @notice public entrance
    function createType(
        string calldata typeName,
        string calldata _tokenURI,
        uint8[] calldata slots,
        uint collectionId,
        uint8 rarity,
        bool soulbound,
        bool disposable
    ) public onlyRole(EDITOR_ROLE) {
        _createType(abi.encode(
            typeName,
            _tokenURI,
            slots,
            collectionId,
            rarity,
            soulbound,
            disposable
        ));
    }


    function _editType(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xEditType(bytes)", encoded);
        if (isProxy) {
            (
                uint256 typeId,
                string memory typeName,
                string memory _tokenURI,
                uint8[] memory slots,
                uint collectionId,
                uint8 rarity,
                bool soulbound,
                bool disposable
            ) = abi.decode(encoded, (uint256, string, string, uint8[], uint, uint8, bool, bool));
            ItemType storage currentType = _types[typeId];

            currentType.tokenURI = _tokenURI;
            currentType.name = typeName;
            currentType.slots = slots;
            currentType.collectionId = collectionId;
            currentType.rarity = rarity;
            currentType.soulbound = soulbound;
            currentType.disposable = disposable;

            emit TypeEdited(typeId);
        }
    }
    /// @notice Proxy entrance
    function xEditType(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _editType(encoded);
    }
    /// @notice public entrance
    function editType(
        uint256 typeId,
        string calldata typeName,
        string calldata _tokenURI,
        uint8[] calldata slots,
        uint collectionId,
        uint8 rarity,
        bool soulbound,
        bool disposable
    ) public onlyRole(EDITOR_ROLE) {
        _editType(abi.encode(
            typeId,
            typeName,
            _tokenURI,
            slots,
            collectionId,
            rarity,
            soulbound,
            disposable
        ));
    }


    function _addVariant(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xAddVariant(bytes)", encoded);
        if (isProxy) {
            (
                uint256 typeId,
                uint8[] memory parameters,
                uint24[] memory multipliers,
                uint256[] memory adductors
            ) = abi.decode(encoded, (uint256, uint8[], uint24[], uint256[]));

            ItemType storage t = _types[typeId];
            Modifier[] storage v = t.variants.push();
            for (uint8 i; i < parameters.length; i++) {
                v.push(Modifier(parameters[i], multipliers[i], adductors[i]));
            }
            emit VariantAdded(typeId, t.variants.length - 1);
        }
    }
    /// @notice Proxy entrance
    function xAddVariant(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _addVariant(encoded);
    }
    /// @notice public entrance
    function addVariant(
        uint256 typeId,
        uint8[] calldata parameters,
        uint24[] calldata multipliers,
        uint256[] calldata adductors
    ) public onlyRole(EDITOR_ROLE) {
        _addVariant(abi.encode(
            typeId,
            parameters,
            multipliers,
            adductors
        ));
    }


    function _clearVariants(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xClearVariants(bytes)", encoded);
        if (isProxy) {
            (uint256 typeId) = abi.decode(encoded, (uint256));
            delete _types[typeId].variants;
            emit VariantsCleared(typeId);
        }
    }
    /// @notice Proxy entrance
    function xClearVariants(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _clearVariants(encoded);
    }
    /// @notice public entrance
    function clearVariants(uint256 typeId) public onlyRole(EDITOR_ROLE) {
        _clearVariants(abi.encode(typeId));
    }


    function addEditors(address[] calldata editors) public onlyRole(DEFAULT_ADMIN_ROLE) {
        for (uint i; i < editors.length; i++) {
            _grantRole(EDITOR_ROLE, editors[i]);
        }
    }

}