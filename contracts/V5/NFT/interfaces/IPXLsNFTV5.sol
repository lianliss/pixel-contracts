//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/NFT/interfaces/IPXLsERC721.sol";
import "contracts/V5/NFT/NFTV5Structs.sol";
import "contracts/Proxy/IProxy.sol";
import "contracts/V5/NFT/interfaces/IPXLsERC721.sol";

interface IPXLsNFTV5 is IProxy, IPXLsERC721 {

    event Mint(address indexed owner, uint256 indexed typeId, uint256 tokenId);
    event Burn(address indexed owner, uint256 tokenId);

    function getCollections() external view returns (string[] memory);
    function getTypes() external view returns(ItemType[] memory);
    function getTypesLength() external view returns (uint);
    function getTokenType(uint256 tokenId) external view returns (ItemType memory);
    function getTokenTypes(uint256[] calldata tokenIds) external view returns (ItemType[] memory);
    function getOwnerTokensCount(address owner) external view returns (uint256);
    function getOwnerTokenAt(address owner, uint256 index) external view returns (uint256);
    function getOwnerTokens(address owner, uint offset, uint limit) external view returns (uint256[] memory, ItemType[] memory);
    function mint(address to, uint256 typeId) external returns (uint);
    function burn(uint256 tokenId) external;

}