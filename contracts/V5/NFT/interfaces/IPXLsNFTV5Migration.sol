//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPXLsNFTV5Migration {
    function getURIType(string calldata tokenURI) external view returns (uint);
    function migrate(address owner, uint limit) external;
    function migrate(uint limit) external;
}