//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/libraries/PixelLibrary.sol";
import "contracts/V5/NFT/PXLsERC721.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";

contract PXLsNFTV5 is PXLsERC721, IPXLsNFTV5 {
    using EnumerableSet for EnumerableSet.UintSet;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant OPERATOR_ROLE = keccak256("OPERATOR_ROLE");

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
    }

    function getCollections() public view returns (string[] memory) {
        return _collections;
    }

    function getTypesLength() public view returns (uint) {
        return _types.length;
    }

    function getTypes(uint offset, uint limit) public view returns(ItemType[] memory) {
        ItemType[] memory types = new ItemType[](limit);
        for (uint i; i < limit; i++) {
            types[i] = _types[i + offset];
        }
        return types;
    }

    function getTypes() public view returns(ItemType[] memory) {
        return _types;
    }

    function getTokenType(uint256 tokenId) public view returns (ItemType memory) {
        return _types[_tokensTypes[tokenId]];
    }

    function getTokenTypes(uint256[] calldata tokenIds) public view returns (ItemType[] memory) {
        ItemType[] memory types = new ItemType[](tokenIds.length);
        for (uint i; i < tokenIds.length; i++) {
            types[i] = _types[_tokensTypes[tokenIds[i]]];
        }
        return types;
    }

    function getOwnerTokensCount(address owner) public view returns (uint256) {
        return _ownerTokens[owner].length();
    }

    function getOwnerTokenAt(address owner, uint256 index) public view returns (uint256) {
        return _ownerTokens[owner].at(index);
    }

    function getOwnerTokens(address owner, uint offset, uint limit) public view returns (uint256[] memory, ItemType[] memory) {
        uint256 length = getOwnerTokensCount(owner);
        uint256 count = offset + limit > length
            ? length - offset
            : limit;
        uint256[] memory tokens = new uint256[](count);
        ItemType[] memory types = new ItemType[](count);
        for (uint256 i; i < count; i++) {
            tokens[i] = getOwnerTokenAt(owner, i + offset);
            types[i] = getTokenType(tokens[i]);
        }
        return (tokens, types);
    }

    function mint(address to, uint256 typeId) public onlyRole(MINTER_ROLE) returns (uint) {
        require(typeId < _types.length, "Unknown token type");
        uint256 tokenId = ++_tokenIdCounter;
        _mint(to, tokenId);
        _tokensTypes[tokenId] = typeId;
        emit Mint(to, typeId, tokenId);
        return tokenId;
    }

    
    function burn(uint256 tokenId) public onlyRole(BURNER_ROLE) {
        address owner = _ownerOf(tokenId);
        if (!hasRole(BURNER_ROLE, _msgSender()) && _msgSender() != owner) {
            revert AccessControlUnauthorizedAccount(_msgSender(), BURNER_ROLE);
        }
        _burn(tokenId);
        emit Burn(owner, tokenId);
    }

    function setTransfersDisabled(bool value) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _transfersDisabled = value;
    }

    function _isAuthorized(address owner, address spender, uint256 tokenId) internal view override returns (bool) {
        bool soulbound = getTokenType(tokenId).soulbound;
        return
            spender != address(0) &&
            (hasRole(OPERATOR_ROLE, _msgSender())
            || (owner == spender && !_transfersDisabled && !soulbound)
            || (_getApproved(tokenId) == spender && !_transfersDisabled) && !soulbound);
    }
}