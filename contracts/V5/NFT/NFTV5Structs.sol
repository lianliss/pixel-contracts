//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct Modifier {
    uint8 parameterId; // One of ACTION constant
    uint24 mul; // Multiplier with PERCENT_DECIMALS decimals
    uint256 add; // Adductor
}

struct ItemType {
    uint typeId;
    Modifier[][] variants; // Each variant can have a multiple modifiers
    string tokenURI;
    string name;
    uint8[] slots; // Purpose slots list
    uint collectionId;
    uint8 rarity;
    bool soulbound; // Soulbound NFTs can't be transfered
    bool disposable; // One time use - will be burned on unequip
}