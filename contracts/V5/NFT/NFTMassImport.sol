//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

struct Import {
    uint typeId;
    string typeName;
    string tokenURI;
    uint8[] slots;
    uint collectionId;
    uint8 rarity;
    uint8[] parameters;
    uint24[] multipliers;
    uint256[] adductors;
    bool soulbound;
    bool disposable;
}

interface INFTEditor {
    function createCollection(string calldata collectionName) external;
    function createType(
        string calldata typeName,
        string calldata _tokenURI,
        uint8[] calldata slots,
        uint collectionId,
        uint8 rarity,
        bool soulbound,
        bool disposable
    ) external;
    function addVariant(
        uint256 typeId,
        uint8[] calldata parameters,
        uint24[] calldata multipliers,
        uint256[] calldata adductors
    ) external;
}

/// @notice Need PXLsNFTV5Editor role EDITOR_ROLE
contract NFTMassImport is AccessControl {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    mapping (uint => uint) collectionMapping;
    mapping (uint => bool) isTypeMigrated;
    IPXLsNFTV5 _nft;
    INFTEditor _editor;
    
    constructor(address _nftAddress, address _nftEditor) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, address(this));

        _nft = IPXLsNFTV5(_nftAddress);
        _editor = INFTEditor(_nftEditor);
    }

    function addCollections(uint[] calldata indexes, string[] calldata collections) public onlyRole(BACKEND_ROLE) {
        require(indexes.length == collections.length, "Lengths is not match");
        string[] memory exists = _nft.getCollections();
        require(exists.length > 0, "Zero collection is not exists");

        for (uint i; i < collections.length; i++) {
            _editor.createCollection(collections[i]);
            collectionMapping[indexes[i]] = exists.length + i;
        }
    }

    function setCollectionMapping(uint sourceIndex, uint currentIndex) public onlyRole(BACKEND_ROLE) {
        collectionMapping[sourceIndex] = currentIndex;
    }

    function _addType(Import calldata item) private returns (bool) {
        if (isTypeMigrated[item.typeId]) return false;
        uint collectionId = collectionMapping[item.collectionId];
        if (collectionId == 0) return false;

        uint typeId = _nft.getTypesLength();
        _editor.createType(
            item.typeName,
            item.tokenURI,
            item.slots,
            collectionId,
            item.rarity,
            item.soulbound,
            item.disposable
        );
        _editor.addVariant(
            typeId,
            item.parameters,
            item.multipliers,
            item.adductors
        );
        isTypeMigrated[item.typeId] = true;
        return true;
    }

    function addType(Import calldata item) public onlyRole(BACKEND_ROLE) {
        require(_addType(item), "Wrong item");
    }

    function addTypes(Import[] calldata items) public onlyRole(BACKEND_ROLE) {
        for (uint i; i < items.length; i++) {
            _addType(items[i]);
        }
    }

}