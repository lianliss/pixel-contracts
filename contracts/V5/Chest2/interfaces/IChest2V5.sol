//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Chest2/Chest2V5Structs.sol";
import "contracts/Proxy/IProxy.sol";

interface IChest2V5 is IProxy {
    event CreateChest(uint indexed chestId, string title, string imageURI);
    event UpdateChest(uint indexed chestId, string title, string imageURI);
    event ItemDrop(address indexed account, uint indexed chestId, uint indexed typeId, uint tokenId);
    event TokenDrop(address indexed account, uint64 indexed userId, address indexed token, uint amount);
    event SendChest(address indexed account, uint indexed chestId);

    event IncreaseUserChest(address indexed account, uint indexed chestId);
    event DecreaseUserChest(address indexed account, uint indexed chestId);

    event ChestDropAdded(uint indexed chestId, uint dropIndex, uint ratio, bool isToken, bool isShard, address tokenAddress, uint amount, uint maxAmount, uint[] items);
    event ChestDropUpdated(uint indexed chestId, uint dropIndex, uint ratio, bool isToken, bool isShard, address tokenAddress, uint amount, uint maxAmount, uint[] items);
    event ChestDropDeleted(uint indexed chestId, uint dropIndex);
    event ChestDropCleared(uint indexed chestId, uint count);
    event ChestDropItemsAdded(uint indexed chestId, uint dropIndex, uint[] items);
    event ChestDropItemsCleared(uint indexed chestId, uint dropIndex, uint count);
}