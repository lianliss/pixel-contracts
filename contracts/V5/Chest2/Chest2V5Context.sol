//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/Proxy/Proxy.sol";
import "contracts/V5/Chest2/Chest2V5Structs.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Cashback/interfaces/ICashback.sol";
import "contracts/common/Multipool/interfaces/IMultipool.sol";

/// @title Base context of Chest2V5
/// @author Danil Sakhinov
/// @notice Use to maintain the structural integrity of the proxies
contract Chest2V5Context is Proxy {

    Chest[] internal _chests;
    IPXLsCoreV5 internal _core;
    IPXLsNFTV5 internal _nft;
    IMultipool internal _pool;

    uint constant internal HUNDRED_PERCENTS = 10000;

    mapping(address account => mapping(uint chestId => uint)) internal _userChestsAmounts;
    mapping(address account => EnumerableSet.UintSet) internal _userChests;

}