//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/V5/Chest2/Chest2V5Context.sol";
import "contracts/V5/Chest2/interfaces/IChest2V5.sol";

/// @notice Require PXLsCoreV5 role MINTER_ROLE
/// @notice Require PXLsNFTV5 role MINTER_ROLE
/// @notice Require Multipool role BACKEND_ROLE
contract Chest2V5 is Chest2V5Context, IChest2V5 {

    using EnumerableSet for EnumerableSet.UintSet;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    constructor(address coreAddress, address nftAddress, address poolAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());

        _core = IPXLsCoreV5(coreAddress);
        _nft = IPXLsNFTV5(nftAddress);
        _pool = IMultipool(poolAddress);
    }

    function setCoreAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV5(newAddress);
    }

    function setNFTAddress(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _nft = IPXLsNFTV5(newAddress);
    }

    function setMultipool(address newAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _pool = IMultipool(newAddress);
    }

    /// @notice Create a new chest only with specific title and image
    /// @param title - Chest name
    /// @param imageURI - Chest image URI
    /// @return A new chest index
    function createChest(string calldata title, string calldata imageURI) public onlyRole(EDITOR_ROLE) returns (uint) {
        _chests.push();
        uint chestId = _chests.length - 1;
        _chests[chestId].chestId = chestId;
        _chests[chestId].title = title;
        _chests[chestId].imageURI = imageURI;

        emit CreateChest(chestId, title, imageURI);
        return chestId;
    }

    /// @notice Updates chest title and image
    /// @param chestId - index of the chest
    /// @param title - Chest name
    /// @param imageURI - Chest image URI
    function updateChest(uint chestId, string calldata title, string calldata imageURI) public onlyRole(EDITOR_ROLE) {
        _chests[chestId].title = title;
        _chests[chestId].imageURI = imageURI;

        emit UpdateChest(chestId, title, imageURI);
    }

    /// @notice Add a new drop element to the chest
    /// @param chestId - index of the chest
    /// @param drop - new drop structure
    /// @return new drop index
    function addChestDrop(uint chestId, Reward calldata drop) public onlyRole(EDITOR_ROLE) returns (uint) {
        uint dropIndex = _chests[chestId].drop.length;
        _chests[chestId].drop.push(drop);
        emit ChestDropAdded(
            chestId,
            dropIndex,
            drop.ratio,
            drop.isToken,
            drop.isShard,
            drop.tokenAddress,
            drop.amount,
            drop.maxAmount,
            drop.items
        );
        return dropIndex;
    }

    /// @notice Rewrite one chest drop
    /// @param chestId - index of the chest
    /// @param dropIndex - index of the drop inside the chest
    /// @param drop - new drop structure
    /// @return updated drop index
    function updateChestDrop(uint chestId, uint dropIndex, Reward calldata drop) public onlyRole(EDITOR_ROLE) returns (uint) {
        require(dropIndex < _chests[chestId].drop.length, "The drop is not that long");
        _chests[chestId].drop[dropIndex] = drop;
        emit ChestDropUpdated(
            chestId,
            dropIndex,
            drop.ratio,
            drop.isToken,
            drop.isShard,
            drop.tokenAddress,
            drop.amount,
            drop.maxAmount,
            drop.items
        );
        return dropIndex;
    }

    /// @notice Delete one chest drop with sperific index
    /// @param chestId - index of the chest
    /// @param dropIndex - index of the drop inside the chest
    /// @return Deleted drop structure
    function deleteChestDrop(uint chestId, uint dropIndex) public onlyRole(EDITOR_ROLE) returns (Reward memory) {
        require(dropIndex < _chests[chestId].drop.length, "The drop is not that long");
        Reward memory drop = _chests[chestId].drop[dropIndex];
        for (uint i = dropIndex; i < _chests[chestId].drop.length - 1; i++) {
            _chests[chestId].drop[i] = _chests[chestId].drop[i + 1];
        }
        _chests[chestId].drop.pop();
        emit ChestDropDeleted(chestId, dropIndex);
        return drop;
    }

    /// @notice Cleard all drop from the chest
    /// @param chestId - index of the chest
    /// @return Count of deleted drops
    function deleteChestDrop(uint chestId) public onlyRole(EDITOR_ROLE) returns (uint) {
        uint count = _chests[chestId].drop.length;
        delete _chests[chestId].drop;
        emit ChestDropCleared(chestId, count);
        return count;
    }

    /// @notice Push a new NFT typeId's to the end of specific drop items
    /// @param chestId - index of the chest
    /// @param dropIndex - index of the drop inside the chest
    /// @param items - Array of NFT typeIs's
    /// @return A new length of items array
    function addChestDropItems(uint chestId, uint dropIndex, uint[] calldata items) public onlyRole(EDITOR_ROLE) returns (uint) {
        for (uint i; i < items.length; i++) {
            _chests[chestId].drop[dropIndex].items.push(items[i]);
        }
        emit ChestDropItemsAdded(chestId, dropIndex, items);
        return _chests[chestId].drop[dropIndex].items.length;
    }

    /// @notice Delete all NFT typeId's from a specific drop
    /// @param chestId - index of the chest
    /// @param dropIndex - index of the drop inside the chest
    /// @return Count of deleted items
    function deleteChestDropItems(uint chestId, uint dropIndex) public onlyRole(EDITOR_ROLE) returns (uint) {
        uint count = _chests[chestId].drop[dropIndex].items.length;
        delete _chests[chestId].drop[dropIndex].items;
        emit ChestDropItemsCleared(chestId, dropIndex, count);
        return count;
    }

    function getChest(uint chestId) public view returns (Chest memory) {
        return _chests[chestId];
    }

    function getChestsCount() public view returns (uint) {
        return _chests.length;
    }

    function getChests() public view returns (Chest[] memory) {
        return _chests;
    }

    function getChests(uint offset, uint limit) public view returns (Chest[] memory) {
        Chest[] memory chests;
        if (offset >= _chests.length) {
            chests = new Chest[](0);
            return chests;
        }
        uint rest = _chests.length - offset < limit
            ? _chests.length - offset
            : limit;
            
        chests = new Chest[](rest);
        for (uint chestId = offset; chestId < offset + limit && chestId < _chests.length; chestId++) {
            chests[chestId - offset] = _chests[chestId];
        }
        return chests;
    }

    function getChestDropsCount(uint chestId) public view returns (uint) {
        return _chests[chestId].drop.length;
    }

    function getChestDrop(uint chestId, uint dropIndex) public view returns (Reward memory) {
        return _chests[chestId].drop[dropIndex];
    }

    function _getChance(uint chestId, uint rarity) public view returns (uint) {
        Chest storage chest = _chests[chestId];
        return chest.drop[rarity].ratio;
    }

    uint private _randomCounter;
    function _pseudoRandom(uint mod) public returns (uint) {
        _randomCounter++;
        return uint(keccak256(abi.encodePacked(block.prevrandao, block.timestamp, _randomCounter)))
            % mod;
    }

    function _rollRarity(uint chestId) public returns (uint) {
        uint rand = _pseudoRandom(HUNDRED_PERCENTS);
        uint chance;
        if (_chests[chestId].drop.length > 1) {
            for (uint rarity = _chests[chestId].drop.length - 1; rarity >= 0; rarity--) {
                chance += _getChance(chestId, rarity);
                if (rand <= chance) {
                    return rarity;
                }
            }
        }
        return 0;
    }

    function _rollDrop(uint chestId, address account) internal {
        Chest storage chest = _chests[chestId];
        uint rarity = _rollRarity(chestId);
        Reward storage drop = chest.drop[rarity];
        if (drop.isToken || drop.isShard) {
            uint amount = drop.maxAmount > 0 && drop.maxAmount > drop.amount
                ? _pseudoRandom(drop.maxAmount + 1) + drop.amount
                : drop.amount;
            uint64 userId = _core.getAddressId(account);
            if (drop.isToken) {
                _pool.send(drop.tokenAddress, account, amount);
            } else {
                IPXLsCoreV5(drop.tokenAddress).mintForOne(userId, amount);
            }
            emit TokenDrop(account, userId, drop.tokenAddress, amount);
        } else {
            uint itemIndex = _pseudoRandom(drop.items.length);
            uint typeId = drop.items[itemIndex];
            uint tokenId = _nft.mint(account, typeId);
            emit ItemDrop(account, chestId, typeId, tokenId);
        }
    }

    function sendChest(uint chestId, address account) public onlyRole(MINTER_ROLE) {
        _rollDrop(chestId, account);
        emit SendChest(account, chestId);
    }

    function _addUserChest(address account, uint chestId) internal returns (uint) {
        require(chestId < _chests.length, "Unknown chest");
        _userChests[account].add(chestId);
        _userChestsAmounts[account][chestId]++;
        emit IncreaseUserChest(account, chestId);
        return _userChestsAmounts[account][chestId];
    }

    function _removeUserChest(address account, uint chestId) internal returns (uint) {
        require(_userChestsAmounts[account][chestId] > 0, "The user does not have the chest");
        if (--_userChestsAmounts[account][chestId] == 0) {
            _userChests[account].remove(chestId);
        }
        emit DecreaseUserChest(account, chestId);
        return _userChestsAmounts[account][chestId];
    }

    function giveTheChest(uint chestId, address[] calldata accounts) public onlyRole(MINTER_ROLE) {
        for (uint i; i < accounts.length; i++) {
            _addUserChest(accounts[i], chestId);
        }
    }

    function giveTheChestToUser(uint chestId, uint64[] calldata userId) public onlyRole(MINTER_ROLE) {
        for (uint i; i < userId.length; i++) {
            _addUserChest(_core.getIdAddress(userId[i]), chestId);
        }
    }

    function claimFreeChest(uint chestId) public {
        _removeUserChest(_msgSender(), chestId);
        _rollDrop(chestId, _msgSender());
        emit SendChest(_msgSender(), chestId);
    }

    function getUserChestsCount(address account) public view returns (uint) {
        return _userChests[account].length();
    }

    function getUserChests(address account) public view returns (UserChest[] memory) {
        UserChest[] memory chest = new UserChest[](_userChests[account].length());
        for (uint i; i < _userChests[account].length(); i++) {
            chest[i].chestId = _userChests[account].at(i);
            chest[i].amount = _userChestsAmounts[account][chest[i].chestId];
        }
        return chest;
    }

    function getUserChests(address account, uint offset, uint limit) public view returns (UserChest[] memory) {
        UserChest[] memory chests;
        uint length = _userChests[account].length();
        if (offset >= length) {
            chests = new UserChest[](0);
            return chests;
        }
        uint rest = length - offset < limit
            ? length - offset
            : limit;
            
        chests = new UserChest[](rest);
        for (uint k = offset; k < offset + limit && k < length; k++) {
            uint index = k - offset;
            chests[index].chestId = _userChests[account].at(k);
            chests[index].amount = _userChestsAmounts[account][chests[index].chestId];
        }
        return chests;
    }

    function getUserChestsWithData(address account) public view returns (UserChestData[] memory) {
        UserChestData[] memory chest = new UserChestData[](_userChests[account].length());
        for (uint i; i < _userChests[account].length(); i++) {
            chest[i].chestId = _userChests[account].at(i);
            chest[i].amount = _userChestsAmounts[account][chest[i].chestId];
            chest[i].chest = _chests[chest[i].chestId];
        }
        return chest;
    }

    function getUserChestsWithData(address account, uint offset, uint limit) public view returns (UserChestData[] memory) {
        UserChestData[] memory chests;
        uint length = _userChests[account].length();
        if (offset >= length) {
            chests = new UserChestData[](0);
            return chests;
        }
        uint rest = length - offset < limit
            ? length - offset
            : limit;
            
        chests = new UserChestData[](rest);
        for (uint k = offset; k < offset + limit && k < length; k++) {
            uint index = k - offset;
            chests[index].chestId = _userChests[account].at(k);
            chests[index].amount = _userChestsAmounts[account][chests[index].chestId];
            chests[index].chest = _chests[chests[index].chestId];
        }
        return chests;
    }

}