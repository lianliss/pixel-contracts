//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct Reward {
    uint ratio;
    bool isToken;
    bool isShard; /// Will use "mint" instead of Multipool.send()
    uint[] items;
    address tokenAddress;
    uint amount; /// token amount or min token amount
    uint maxAmount; /// max token amount; "amount" param must be set
}

struct Chest {
    uint chestId;
    string title;
    string imageURI;
    Reward[] drop;
}

struct UserChest {
    uint chestId;
    uint amount;
}

struct UserChestData {
    uint chestId;
    uint amount;
    Chest chest;
}