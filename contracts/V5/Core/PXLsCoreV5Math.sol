//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Structs.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5Math.sol";

/// @title Base math of PXLsCoreV5
/// @author Danil Sakhinov
/// @notice Use for mining calculations
contract PXLsCoreV5Math is IPXLsCoreV5Math {

    BaseParams private _params = BaseParams(
        1 * 10**16,
        4 * _PRECISION,
        uint24(25 * _PERCENT_PRECISION),
        uint24(_PERCENT_PRECISION),
        5000,
        24000
    );

    // Referral rewards to parents
    uint[2] public _parentsPercents = [
        2000, // 20% to first parent
        500 // 5% to second ancestor
    ];

    mapping(address => bool) private _owners;

    constructor() {
        _owners[msg.sender] = true;
    }

    modifier onlyOwner() {
        require(_owners[msg.sender], "Sender is not a Storage Owner");
        _;
    }

    function getParams() public view returns (BaseParams memory) {
        return _params;
    }

    function getParentsPercents() public view returns (uint[2] memory) {
        return _parentsPercents;
    }
    
    function _getMining(Storage memory user) internal view returns (uint) {
        uint miningMultiplicator = user.miningMultiplicator == 0
            ? _PERCENT_PRECISION
            : user.miningMultiplicator;
        return _params.speed
        * miningMultiplicator / _PERCENT_PRECISION
        * _params.halvingRatio / _PERCENT_PRECISION;
    }

    function getSpeed(Storage memory user) public view returns (uint) {
        return (user.speedLevel + 1) * _getMining(user)
        * user.speedMultiplicator / _PERCENT_PRECISION
        / _HOUR
            + user.speedAdder;
    }

    function getSpeedPrice(Storage memory user) public view returns (uint) {
        uint nextLevel = user.speedLevel + 2;
        return nextLevel**2
        * _params.difficulty / _PERCENT_PRECISION
        * _params.droneAverage / _PERCENT_PRECISION
        * _getMining(user)
        * user.speedPrice / _PERCENT_PRECISION;
    }

    function getSize(Storage memory user) public view returns (uint) {
        return (user.sizeLevel + 1) * _getMining(user)
        * _params.size / _PRECISION
        * user.sizeMultiplicator / _PERCENT_PRECISION
            + user.sizeAdder;
    }

    function getSizePrice(Storage memory user) public view returns (uint) {
        uint nextLevel = user.sizeLevel + 2;
        return nextLevel**2
        * _params.difficulty / _PERCENT_PRECISION
        * _params.droneAverage / _PERCENT_PRECISION
        * _getMining(user)
        * _params.size / _PRECISION
        * user.sizePrice / _PERCENT_PRECISION;
    }

    function getReferralSize(Storage memory user)
    public view returns (uint) {
        uint8 currentLevel = user.referralLevel + 1;
        return (
            currentLevel * _getMining(user)
            * _params.size / _PRECISION
            + _PRECISION * 2
            + (_parentsPercents[0] * _PRECISION / _PERCENT_PRECISION * currentLevel)
            + (_parentsPercents[1] * _PRECISION / _PERCENT_PRECISION * currentLevel)
        )
        * user.referralMultiplicator / _PERCENT_PRECISION
            + user.referralAdder;
    }

    function getReferralPrice(Storage memory user)
    public view returns (uint) {
        uint nextLevel = user.referralLevel + 2;
        return nextLevel**2
        * _params.difficulty / _PERCENT_PRECISION
        * _params.droneAverage / _PERCENT_PRECISION
        * _getMining(user)
        * _params.size / _PRECISION
        * (_PERCENT_PRECISION + _parentsPercents[0] * nextLevel) / _PERCENT_PRECISION
        * (_PERCENT_PRECISION + _parentsPercents[1] * nextLevel) / _PERCENT_PRECISION
        * user.referralPrice / _PERCENT_PRECISION;
    }

    function getMinedTokens(Storage memory user) public view returns (uint) {
        uint limit = getSize(user);
        uint time = block.timestamp - user.claimTimestamp;
        uint amount = getSpeed(user)
            * time;
        return amount > limit && limit != 0
            ? limit
            : amount;
    }


    /// Setters

    function setBurnRate(uint24 burnRate) public onlyOwner() {
        _params.burnRate = burnRate;
    }

    function setHalvingRatio(uint24 ratio) public onlyOwner() {
        _params.halvingRatio = ratio;
    }

    function setBaseSpeed(uint speed) public onlyOwner() {
        _params.speed = speed;
    }

    function setBaseSize(uint size) public onlyOwner() {
        _params.size = size;
    }

    function setDiffuculty(uint24 value) public onlyOwner() {
        _params.difficulty = value;
    }

    function setDroneAverage(uint24 value) public onlyOwner() {
        _params.droneAverage = value;
    }

    function setParentsPercents(uint[2] calldata _percents) public onlyOwner() {
        _parentsPercents = _percents;
    }

    function setOwnership(address account, bool value) public onlyOwner() {
        _owners[account] = value;
    }
}