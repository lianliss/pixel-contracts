//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/Proxy.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5Math.sol";
 
/// @title Context of PXLsCoreV5
/// @author Danil Sakhinov
/// @notice Use to maintain the structural integrity of the proxies
contract CoreV5Context is Proxy {

    mapping(uint64 userId => Storage) internal _storages;
    mapping(address account => uint64 userId) internal _accounts;
    mapping(uint64 userId => uint amount) internal _balances;
    uint internal _totalSupply;

    uint internal _claimed;
    uint internal _burned;

    bool internal _claimsDisabled;
    bool internal _refersDisabled;

    IPXLsCoreV5Math internal _math;

    uint internal _activityRewardSeconds = _DAY * 7;

}