//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";

/// @title Settings editor for Pixel Shard token
/// @author Danil Sakhinov
/// @notice Required PXLsCoreV5 role PROXY_ROLE
contract PXLsCoreV5Settings is CoreV5Context, ProxyImplementation {

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    /// @param coreAddress PXLsCoreV5
    constructor(address coreAddress) ProxyImplementation(coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }

    event SetClaimsDisabled(address caller, bool value);
    event SetRefersDisabled(address caller, bool value);
    event SetStorageDisabled(uint64 indexed id, address caller, bool value);


    function _setClaimsDisabled(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetClaimsDisabled(bytes)", encoded);
        if (isProxy) {
            (bool newValue) = abi.decode(encoded, (bool));
            _claimsDisabled = newValue;
            emit SetClaimsDisabled(_msgSender(), newValue);
        }
    }
    /// @notice Proxy entrance
    function xSetClaimsDisabled(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setClaimsDisabled(encoded);
    }
    /// @notice public entrance
    function setClaimsDisabled(bool newValue) public onlyRole(EDITOR_ROLE) {
        _setClaimsDisabled(abi.encode(newValue));
    }

    function _setRefersDisabled(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetRefersDisabled(bytes)", encoded);
        if (isProxy) {
            (bool newValue) = abi.decode(encoded, (bool));
            _refersDisabled = newValue;
            emit SetRefersDisabled(_msgSender(), newValue);
        }
    }
    /// @notice Proxy entrance
    function xSetRefersDisabled(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setRefersDisabled(encoded);
    }
    /// @notice public entrance
    function setRefersDisabled(bool newValue) public onlyRole(EDITOR_ROLE) {
        _setRefersDisabled(abi.encode(newValue));
    }

    function _setStorageDisabled(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetStorageDisabled(bytes)", encoded);
        if (isProxy) {
            (uint64 userId, bool newValue) = abi.decode(encoded, (uint64, bool));
            _storages[userId].disabled = newValue;
        }
    }
    /// @notice Proxy entrance
    function xSetStorageDisabled(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setStorageDisabled(encoded);
    }
    /// @notice public entrance
    function setStorageDisabled(uint64 userId, bool newValue) public onlyRole(EDITOR_ROLE) {
        _setStorageDisabled(abi.encode(userId, newValue));
    }

    function _setNewMath(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetNewMath(bytes)", encoded);
        if (isProxy) {
            (address newMathContract) = abi.decode(encoded, (address));
            _math = IPXLsCoreV5Math(newMathContract);
        }
    }
    /// @notice Proxy entrance
    function xSetNewMath(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setNewMath(encoded);
    }
    /// @notice public entrance
    function setNewMath(address newMathContract) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setNewMath(abi.encode(newMathContract));
    }

    function _setActivitySeconds(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetActivitySeconds(bytes)", encoded);
        if (isProxy) {
            (uint value) = abi.decode(encoded, (uint));
            _activityRewardSeconds = value;
        }
    }
    /// @notice Proxy entrance
    function xSetActivitySeconds(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setActivitySeconds(encoded);
    }
    /// @notice public entrance
    function setActivitySeconds(uint value) public onlyRole(EDITOR_ROLE) {
        _setActivitySeconds(abi.encode(value));
    }
}