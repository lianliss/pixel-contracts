//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5Migration.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5Migration.sol";

struct OldStorage {
    address account;
    uint64 parent;
    bool disabled;
    uint claimTimestamp;
    uint claimed;
    uint refStorage;
    uint8 sizeLevel;
    uint8 speedLevel;
    uint8 referralLevel;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint24 referralMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    uint256 referralAdder;
    uint24 speedPrice;
    uint24 sizePrice;
    uint24 referralPrice;
    uint activity;
}

interface IPXLsCoreV4 {
    function getStorage(uint64 id) external view returns (
        OldStorage memory user,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
    );
    function getAddressId(address account) external view returns (uint64);
}

/// @title Pixel Shard Core Migration
/// @author Danil Sakhinov
/// @notice Required PXLsCoreV5 role PROXY_ROLE
/// @notice Required PXLsSlotsV5Migration role BACKEND_ROLE
contract PXLsCoreV5Migration is CoreV5Context, ProxyImplementation, IPXLsCoreV5Migration {

    IPXLsCoreV4 private _oldCore;
    IPXLsSlotsV5Migration private _slots;
    mapping (uint64 userId => bool isMigrated) public migrated;

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    /// @param coreAddress PXLsCoreV5 new
    /// @param oldCoreAddress PXLsCoreV4 old
    /// @param slotsMigrationAddress IPXLsSlotsV5Migration
    constructor(
        address coreAddress,
        address oldCoreAddress,
        address slotsMigrationAddress
    ) ProxyImplementation(coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        _oldCore = IPXLsCoreV4(oldCoreAddress);
        _slots = IPXLsSlotsV5Migration(slotsMigrationAddress);
    }

    function _migrateStorage(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xMigrateStorage(bytes)", encoded);
        if (isProxy) {
            (
                uint64 userId,
                address account,
                uint64 parent,
                bool disabled,
                uint claimTimestamp,
                uint claimed,
                uint refStorage,
                uint8 sizeLevel,
                uint8 speedLevel,
                uint8 referralLevel,
                uint activity
            ) = abi.decode(encoded, (
                uint64,
                address,
                uint64,
                bool,
                uint,
                uint,
                uint,
                uint8,
                uint8,
                uint8,
                uint
            ));
            
            Storage storage user = _storages[userId];
            user.account = account;
            user.parent = parent;
            user.disabled = disabled;
            user.claimTimestamp = claimTimestamp;
            user.claimed = claimed;
            user.refStorage = refStorage;
            user.sizeLevel = sizeLevel;
            user.speedLevel = speedLevel;
            user.referralLevel = referralLevel;
            user.activity = activity;

            user.speedMultiplicator = uint24(_PERCENT_PRECISION);
            user.sizeMultiplicator = uint24(_PERCENT_PRECISION);
            user.referralMultiplicator = uint24(_PERCENT_PRECISION);
            user.speedPrice = uint24(_PERCENT_PRECISION);
            user.sizePrice = uint24(_PERCENT_PRECISION);
            user.referralPrice = uint24(_PERCENT_PRECISION);
            user.miningMultiplicator = uint24(_PERCENT_PRECISION);

            _accounts[account] = userId;
        }
    }
    /// @notice Proxy entrance
    function xMigrateStorage(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _migrateStorage(encoded);
    }


    function _migrateBalance(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xMigrateBalance(bytes)", encoded);
        if (isProxy) {
            (uint64 userId, uint balance) = abi.decode(encoded, (uint64, uint));
            _balances[userId] = balance;
        }
    }
    /// @notice Proxy entrance
    function xMigrateBalance(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _migrateBalance(encoded);
    }


    function _migrate(uint64 userId) internal {
        (
            OldStorage memory user,,,,,,uint balance,
        ) = _oldCore.getStorage(userId);
        _migrateStorage(abi.encode(
            userId,
            user.account,
            user.parent,
            user.disabled,
            user.claimTimestamp,
            user.claimed,
            user.refStorage,
            user.sizeLevel,
            user.speedLevel,
            user.referralLevel,
            user.activity
        ));
        _migrateBalance(abi.encode(
            userId,
            balance
        ));
        //_slots.migrate(userId);
        migrated[userId] = true;

        emit UserMigrated(userId, user.account);
    }

    /// @notice public entrance
    /// @dev Prohibits to migrate twice
    function migrate() public {
        uint64 userId = IPXLsCoreV4(_oldCore).getAddressId(_msgSender());
        require (userId > 0, "Unauthorized user");
        require (!migrated[userId], "User already migrated to V5");

        _migrate(userId);
    }

    /// @notice public entrance
    /// @dev Allow to migrate twice (or mode times)
    function migrate(uint64 userId) public onlyRole(BACKEND_ROLE) {
        require (!migrated[userId], "User already migrated to V5");

        _migrate(userId);
    }

    /// @notice public entrance
    /// @dev Skips migrated users
    function migrate(uint64[] calldata userId) public onlyRole(BACKEND_ROLE) {
        for (uint i; i < userId.length; i++) {
            if (migrated[userId[i]]) continue;
            _migrate(userId[i]);
        }
    }
    
}