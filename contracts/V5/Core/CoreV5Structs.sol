//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

uint8 constant _decimals = 18;
uint constant _PRECISION = 10**_decimals;
uint constant _PERCENT_DECIMALS = 4;
uint constant _PERCENT_PRECISION = 10**_PERCENT_DECIMALS;
uint constant _INITIAL_SUPPLY = 2768888888 * 10**(_decimals - 2);
uint constant _HOUR = 60 * 60;
uint constant _DAY = _HOUR * 24;

struct Storage {
    address account;
    uint64 parent;
    bool disabled;
    uint claimTimestamp;
    uint claimed;
    uint refStorage;
    uint8 sizeLevel;
    uint8 speedLevel;
    uint8 referralLevel;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint24 referralMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    uint256 referralAdder;
    uint24 speedPrice;
    uint24 sizePrice;
    uint24 referralPrice;
    uint activity;
    uint24 miningMultiplicator;
}

struct BaseParams {
    uint speed;
    uint size;
    uint24 difficulty;
    uint24 halvingRatio;
    uint24 burnRate;
    uint24 droneAverage;
}