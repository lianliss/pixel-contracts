//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Context.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Slots/interfaces/IParameterReceiver.sol";

/// @title Pixel Shard token
/// @author Danil Sakhinov
/// @notice Like ERC20 but transfers disabled
/// @notice Only Pixel Wallet users allowed
contract PXLsCoreV5 is CoreV5Context, IPXLsCoreV5, IParameterReceiver {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    bytes32 public constant GRANTER_ROLE = keccak256("GRANTER_ROLE");
    bytes32 public constant CLAIMER_ROLE = keccak256("CLAIMER_ROLE");

    string private constant _symbol = "PXLs";
    string private constant _name = "Pixel Shard";

    /// @param coreMathAddress CoreV5Math
    constructor(address coreMathAddress) Proxy() {
        _math = IPXLsCoreV5Math(coreMathAddress);

        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(GRANTER_ROLE, _msgSender());
        _grantRole(CLAIMER_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
        _grantRole(MULTIPLIER_ROLE, _msgSender());
        
        _totalSupply = _INITIAL_SUPPLY;
    }

    modifier onlyStorageOwner(uint64 userId) {
        require(_storages[userId].account == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    function setMultipliers(uint64 userId, uint24[3] calldata mult, uint256[3] calldata add, uint24[3] calldata price) public onlyRole(MULTIPLIER_ROLE) {
        _claimReward(userId, 0);
        Storage storage user = _storages[userId];
        user.speedMultiplicator = mult[0];
        user.sizeMultiplicator = mult[1];
        user.referralMultiplicator = mult[2];
        user.speedAdder = add[0];
        user.sizeAdder = add[1];
        user.referralAdder = add[2];
        user.speedPrice = price[0];
        user.sizePrice = price[1];
        user.referralPrice = price[2];
    }

    /// @notice Burn storage tokens
    /// @param userId Wallet ID
    /// @param value Tokens amount to burn
    function _burn(uint64 userId, uint256 value) internal {
        require(_balances[userId] >= value, "Can't burn more");
        uint burnValue = value * _math.getParams().burnRate / _PERCENT_PRECISION;
        uint reinvestValue = value - burnValue;
        _totalSupply -= burnValue;
        _balances[userId] -= value;
        _burned += burnValue;
        emit Burn(userId, _storages[userId].account, burnValue);
        if (reinvestValue > 0) {
            emit Reinvest(userId, _storages[userId].account, reinvestValue);
        }
    }

    /// @notice The administrator can reset the storage
    /// @param userId Wallet ID
    function burnStorage(uint64 userId) public onlyRole(BURNER_ROLE) {
        Storage storage user = _storages[userId];
        uint balance = _balances[userId];
        _burn(userId, balance);
        user.sizeLevel = 0;
        user.speedLevel = 0;
        user.referralLevel = 0;
        user.refStorage = 0;
        emit Transfer(user.account, address(0), balance);
    }

    /// @notice Burner contract can burn a specific amount of tokens from an account
    /// @param account Storage owner
    /// @param amount Tokens amount to burn
    function burn(address account, uint amount) public onlyRole(BURNER_ROLE) {
        uint64 id = getAddressId(account);
        require(_balances[id] >= amount, "Not enough funds");
        require(_storages[id].account == account, "Address have no storage");
        _burn(id, amount);
        emit Transfer(account, address(0), amount);
    }

    /// @notice Admin can change a storage parent
    /// @param userId Wallet ID
    /// @param parent New parent
    function changeParent(uint64 userId, uint64 parent) public onlyRole(GRANTER_ROLE) {
        _storages[userId].parent = parent;
        emit SetParent(userId, parent);
    }

    /// @notice Transfer tokens to storage account
    /// @param userId Wallet ID
    /// @param amount Tokens amount to claim
    function _claim(uint64 userId, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _storages[userId].claimed += amount;
        _balances[userId] += amount;
    }

    function _fillRef(uint64 userId, uint amount) internal returns (uint) {
        Storage storage user = _storages[userId];
        uint referralSize = _math.getReferralSize(user);
        if (referralSize <= user.refStorage) {
            return 0;
        }
        uint limit = referralSize - user.refStorage;
        uint value = limit < amount
            ? limit
            : amount;
        if (value > 0) {
            user.refStorage += value;
        }
        return value;
    }

    /// @notice ERC20 format
    function totalSupply() public view virtual returns (uint256) {
        return _totalSupply;
    }

    /// @notice ERC20 format
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /// @notice ERC20 format
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /// @notice ERC20 format
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /// @notice Gets account balance
    /// @param account Address linked to storage
    /// @return Tokens amount
    /// @dev Balances linked to Wallet ID only
    function balanceOf(address account) public view virtual returns (uint256) {
        uint64 userId = getAddressId(account);
        return _storages[userId].account == account
            ? _balances[userId]
            : 0;
    }

    /// @notice Gets Wallet ID linked to a storage
    /// @param account Address linked to storage
    /// @return Wallet ID
    function getAddressId(address account) public view returns (uint64) {
        return _accounts[account];
    }

    function getIdAddress(uint64 userId) public view returns (address) {
        return _storages[userId].account;
    }

    function _updateActivity(Storage storage user) internal {
        uint claimSeconds = block.timestamp - user.claimTimestamp;
        if (claimSeconds < _DAY) {
            user.activity += claimSeconds;
        } else {
            if (claimSeconds < _activityRewardSeconds) {
                user.activity = 0;
            }
        }
    }

    /// @notice Claim reward by storage
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function _claimReward(uint64 userId, uint additional) internal {
        Storage storage current = _storages[userId];
        uint mined = _math.getMinedTokens(current) + additional;
        if (mined == 0) return;
        require(!current.disabled, "Storage disabled");
        _claim(userId, mined);
        _updateActivity(current);
        current.claimTimestamp = block.timestamp;
        emit Claim(userId, current.account, mined);
        emit Transfer(address(this), current.account, mined);

        uint index = 0;
        uint64 previous = userId;
        uint[2] memory parentsPercents = _math.getParentsPercents();
        while (index < parentsPercents.length
        && current.parent != 0
            && !_refersDisabled) {
            uint64 receiver = current.parent;
            current = _storages[receiver];
            uint reward = mined * parentsPercents[index] / _PERCENT_PRECISION;
            if (_claimed + reward <= _totalSupply && current.account != address(0)) {
                uint filled = _fillRef(receiver, reward);
                if (filled > 0) {
                    emit ReferReward(userId, receiver, previous, current.account, filled);
                }
            }
            index++;
            previous = receiver;
        }
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function claimReward(uint64 userId) public onlyStorageOwner(userId) {
        require(!_claimsDisabled, "Claims disabled");
        _claimReward(userId, 0);
    }

    function claimReferral(uint64 userId) public onlyStorageOwner(userId) {
        require(!_claimsDisabled, "Claims disabled");
        Storage storage user = _storages[userId];
        require(!user.disabled, "Storage disabled");
        uint mined = user.refStorage;
        if (mined == 0) return;
        _claim(userId, mined);
        user.refStorage = 0;
        emit ClaimReferral(userId, user.account, mined);
        emit Transfer(address(this), user.account, mined);
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function claimFor(uint64 userId) public onlyRole(CLAIMER_ROLE) {
        _claimReward(userId, 0);
    }

    function claimForRef(uint64 userId) public onlyRole(CLAIMER_ROLE) {
        Storage storage user = _storages[userId];
        require(!user.disabled, "Storage disabled");
        uint mined = user.refStorage;
        if (mined == 0) return;
        _claim(userId, mined);
        user.refStorage = 0;
        emit Claim(userId, user.account, mined);
        emit Transfer(address(this), user.account, mined);
    }

    /// @notice Mint amount of tokens to storage and distribute it to refers
    /// @param userId Wallet ID
    /// @param amount Amount of tokens to mint
    /// @dev Calls claim
    function mintWithClaim(uint64 userId, uint amount) public onlyRole(MINTER_ROLE) {
        _claimReward(userId, amount);
    }

    /// @notice Mint amount of tokens only for one account
    /// @param userId Wallet ID
    /// @param amount Amount of tokens to mint
    function mintForOne(uint64 userId, uint amount) public onlyRole(MINTER_ROLE) {
        _claim(userId, amount);
        emit Transfer(address(this), _storages[userId].account, amount);
    }

    /// @notice Promotes storage to a new size level
    /// @param userId Wallet ID
    /// @param levelPrice Promoting cost
    function _promoteSizeLevel(uint64 userId, uint levelPrice) internal {
        Storage storage user = _storages[userId];
        _claimReward(userId, 0);
        user.sizeLevel++;
        emit LevelUpSize(userId, user.account, user.sizeLevel, levelPrice);
    }

    /// @notice Admin can promote storage size for free
    /// @param userId Wallet ID
    function promoteSizeLevel(uint64 userId) public onlyRole(GRANTER_ROLE) {
        _promoteSizeLevel(userId, 0);
    }

    /// @notice Storage owner can buy size level up
    /// @param userId Wallet ID
    function buySizeLevel(uint64 userId) public onlyStorageOwner(userId) {
        Storage storage user = _storages[userId];
        uint levelPrice = _math.getSizePrice(user);
        require(balanceOf(user.account) >= levelPrice, "Not enough funds");

        _burn(userId, levelPrice);
        _promoteSizeLevel(userId, levelPrice);
        emit Transfer(user.account, address(0), levelPrice);
    }

    /// @notice Promotes storage to a new speed level
    /// @param userId Wallet ID
    /// @param levelPrice Promoting cost
    function _promoteSpeedLevel(uint64 userId, uint levelPrice) internal {
        Storage storage user = _storages[userId];
        _claimReward(userId, 0);
        user.speedLevel++;
        emit LevelUpSpeed(userId, user.account, user.speedLevel, levelPrice);
    }

    /// @notice Admin can promote storage speed for free
    /// @param userId Wallet ID
    function promoteSpeedLevel(uint64 userId) public onlyRole(GRANTER_ROLE) {
        _promoteSpeedLevel(userId, 0);
    }

    /// @notice Storage owner can buy speed level up
    /// @param userId Wallet ID
    function buySpeedLevel(uint64 userId) public onlyStorageOwner(userId) {
        Storage storage user = _storages[userId];
        uint levelPrice = _math.getSpeedPrice(user);
        require(balanceOf(user.account) >= levelPrice, "Not enough funds");

        _burn(userId, levelPrice);
        _promoteSpeedLevel(userId, levelPrice);
        emit Transfer(user.account, address(0), levelPrice);
    }

    /// @notice Promotes storage to a new speed level
    /// @param userId Wallet ID
    /// @param levelPrice Promoting cost
    function _promoteReferralLevel(uint64 userId, uint levelPrice) internal {
        Storage storage user = _storages[userId];
        user.referralLevel++;
        emit LevelUpReferral(userId, user.account, user.referralLevel, levelPrice);
    }

    /// @notice Admin can promote storage speed for free
    /// @param userId Wallet ID
    function promoteReferralLevel(uint64 userId) public onlyRole(GRANTER_ROLE) {
        _promoteReferralLevel(userId, 0);
    }

    /// @notice Storage owner can buy speed level up
    /// @param userId Wallet ID
    function buyReferralLevel(uint64 userId) public onlyStorageOwner(userId) {
        Storage storage user = _storages[userId];
        uint levelPrice = _math.getReferralPrice(user);
        require(balanceOf(user.account) >= levelPrice, "Not enough funds");

        _burn(userId, levelPrice);
        _promoteReferralLevel(userId, levelPrice);
        emit Transfer(user.account, address(0), levelPrice);
    }

    /// @notice Access controller can create storage for Pixel Wallet user on his address
    /// @param userId Wallet ID
    /// @param account Wallet address
    /// @param parent Pixel Wallet inviter
    function grantAccess(uint64 userId, address account, uint64 parent) public onlyRole(GRANTER_ROLE) {
        _accounts[account] = userId;
        _storages[userId].account = account;
        if (_storages[userId].claimTimestamp == 0) {
            _storages[userId].claimTimestamp = block.timestamp;
            _storages[userId].speedMultiplicator = uint24(_PERCENT_PRECISION);
            _storages[userId].sizeMultiplicator = uint24(_PERCENT_PRECISION);
            _storages[userId].referralMultiplicator = uint24(_PERCENT_PRECISION);
            _storages[userId].speedPrice = uint24(_PERCENT_PRECISION);
            _storages[userId].sizePrice = uint24(_PERCENT_PRECISION);
            _storages[userId].referralPrice = uint24(_PERCENT_PRECISION);
            _storages[userId].miningMultiplicator = uint24(_PERCENT_PRECISION);
        }
        if (parent != 0 && parent != userId) {
            _storages[userId].parent = parent;
            emit SetParent(userId, parent);
        }
        emit GrantAccess(userId, account, _msgSender());
    }

    /// @notice Returns total claimed amount by storages
    /// @return Claimed tokens amount
    function totalClaimed() public view virtual returns (uint) {
        return _claimed;
    }

    /// @notice Returns total burned amount by storages
    /// @return Burned tokens amount
    function totalBurned() public view virtual returns (uint) {
        return _burned;
    }

    /// @notice Storage info getter
    /// @param userId Wallet ID
    function getStorage(uint64 userId) public view returns (
        Storage memory user,
        BaseParams memory base,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
    ) {
        Storage storage _user = _storages[userId];
        user = _user;
        base = _math.getParams();
        prices[0] = _math.getSpeedPrice(_user);
        prices[1] = _math.getSizePrice(_user);
        prices[2] = _math.getReferralPrice(_user);
        mined = _math.getMinedTokens(_user);
        sizeLimit = _math.getSize(_user);
        rewardPerSecond = _math.getSpeed(_user);
        refLimit = _math.getReferralSize(_user);
        balance = _balances[userId];
        coins = user.account.balance;
    }

    /// @notice Admins can burn amount tokens from total supply
    /// @param amount Amount of tokens to burn
    function burnTotalSupply(uint amount) public onlyRole(BURNER_ROLE) {
        _totalSupply -= amount;
        emit BurnTotalSupply(_msgSender(), amount);
    }

    function updateParameter(uint64 userId, uint8, uint24 multiplier, uint256) public onlyRole(MULTIPLIER_ROLE) {
        _claimReward(userId, 0);
        _storages[userId].miningMultiplicator = multiplier;
    }

}