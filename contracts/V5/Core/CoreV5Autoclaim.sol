//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5Events.sol";

/// @title Pixel Shard Core for Autoclaim bot
/// @author Danil Sakhinov
/// @dev Fix of ClaimReferral event inside _claimForRef
/// @notice Required PXLsCoreV5 role PROXY_ROLE
/// @notice Required PXLsCoreV5 role CLAIMER_ROLE
contract CoreV5Autoclaim is CoreV5Context, ProxyImplementation, IPXLsCoreV5Events {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    constructor(address coreAddress) ProxyImplementation(coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }

    /// Internal methods

    function _claim(uint64 userId, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _storages[userId].claimed += amount;
        _balances[userId] += amount;
    }

    function _claimForRef(uint64 userId) internal {
        Storage storage user = _storages[userId];
        require(!user.disabled, "Storage disabled");
        uint mined = user.refStorage;
        if (mined == 0) return;
        _claim(userId, mined);
        user.refStorage = 0;
        emit ClaimReferral(userId, user.account, mined);
        emit Transfer(address(this), user.account, mined);
    }

    /// External methods

    function claimFor(uint64 userId) public onlyRole(BACKEND_ROLE) {
        IPXLsCoreV5(address(getProxy())).claimFor(userId);
    }

    function _claimForRefImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xClaimForRef(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            _claimForRef(userId);
        }
    }
    function xClaimForRef(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _claimForRefImplementation(encoded);
    }
    function claimForRef(uint64 userId) public onlyRole(BACKEND_ROLE) {
        _claimForRefImplementation(abi.encode(userId));
    }

}