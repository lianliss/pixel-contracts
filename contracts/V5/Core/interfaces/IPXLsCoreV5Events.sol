//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPXLsCoreV5Events {

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Claim(uint64 indexed userId, address account, uint amount);
    event ClaimReferral(uint64 indexed userId, address account, uint amount);
    event Burn(uint64 indexed userId, address account, uint amount);
    event BurnTotalSupply(address account, uint amount);
    event ReferReward(uint64 indexed claimerId, uint64 indexed id, uint64 indexed child, address account, uint amount);
    event LevelUpSize(uint64 indexed userId, address account, uint level, uint price);
    event LevelUpSpeed(uint64 indexed userId, address account, uint level, uint price);
    event LevelUpReferral(uint64 indexed userId, address account, uint level, uint price);
    event GrantAccess(uint64 indexed userId, address account, address controller);
    event SetParent(uint64 indexed userId, uint64 indexed parent);
    event Reinvest(uint64 indexed userId, address account, uint value);
    
}