//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface ICoreV5Auth {
    function getAddressId(address account) external view returns (uint64);
    function getIdAddress(uint64 id) external view returns (address);
}