//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Structs.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5Events.sol";
import "contracts/V5/Core/interfaces/ICoreV5Auth.sol";
import "contracts/Proxy/IProxy.sol";

interface IPXLsCoreV5 is IPXLsCoreV5Events, ICoreV5Auth, IProxy {

    function getStorage(uint64 id) external view returns (
        Storage memory user,
        BaseParams memory base,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
        );

    function mintForOne(uint64 userId, uint amount) external;
    function burn(address account, uint amount) external;
    function setMultipliers(uint64 userId, uint24[3] calldata mult, uint256[3] calldata add, uint24[3] calldata price) external;
    function claimFor(uint64 userId) external;
    function claimForRef(uint64 userId) external;
}