//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Structs.sol";

interface IPXLsCoreV5Math {
    function getParams() external view returns (BaseParams memory);
    function getParentsPercents() external view returns (uint[2] memory);
    function getSpeed(Storage memory user) external view returns (uint);
    function getSpeedPrice(Storage memory user) external view returns (uint);
    function getSize(Storage memory user) external view returns (uint);
    function getSizePrice(Storage memory user) external view returns (uint);
    function getReferralSize(Storage memory user)
    external view returns (uint);
    function getReferralPrice(Storage memory user)
    external view returns (uint);
    function getMinedTokens(Storage memory user) external view returns (uint);
}