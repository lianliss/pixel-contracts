//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPXLsCoreV5Migration {

    event UserMigrated(uint64 indexed userId, address indexed account);

    function migrate() external;
    function migrate(uint64 userId) external;
    function migrate(uint64[] calldata userId) external;
}