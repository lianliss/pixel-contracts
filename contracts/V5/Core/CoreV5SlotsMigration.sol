//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";

struct V4Storage {
    address account;
    uint64 parent;
    bool disabled;
    uint claimTimestamp;
    uint claimed;
    uint refStorage;
    uint8 sizeLevel;
    uint8 speedLevel;
    uint8 referralLevel;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint24 referralMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    uint256 referralAdder;
    uint24 speedPrice;
    uint24 sizePrice;
    uint24 referralPrice;
    uint activity;
}

struct V5Storage {
    address account;
    uint64 parent;
    bool disabled;
    uint claimTimestamp;
    uint claimed;
    uint refStorage;
    uint8 sizeLevel;
    uint8 speedLevel;
    uint8 referralLevel;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint24 referralMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    uint256 referralAdder;
    uint24 speedPrice;
    uint24 sizePrice;
    uint24 referralPrice;
    uint activity;
    uint24 miningMultiplicator;
}

struct BaseParams {
    uint speed;
    uint size;
    uint24 difficulty;
    uint24 halvingRatio;
    uint24 burnRate;
}

struct Slot {
    uint tokenId;
    uint variantId;
    bool isEnabled;
}

struct Modifier {
    uint8 parameterId; // One of ACTION constant
    uint24 mul; // Multiplier with PERCENT_DECIMALS decimals
    uint256 add; // Adductor
}

struct ItemType {
    uint typeId;
    Modifier[][] variants; // Each variant can have a multiple modifiers
    string tokenURI;
    string name;
    uint8[] slots; // Purpose slots list
    uint collectionId;
    uint8 rarity;
    bool soulbound; // Soulbound NFTs can't be transfered
    bool disposable; // One time use - will be burned on unequip
}

interface ICoreMigration {
    function migrated(uint64 userId) external view returns (bool isMigrated);
    function migrate(uint64[] calldata userId) external;
}

interface ISlotsMigration {
    function migrated(uint64 userId) external view returns (bool isMigrated);
    function migrate(uint64[] calldata userId) external;
}

interface INFTMigration {
    function migrate(address owner, uint limit) external;
}

interface IV4Core {
    function getIdAddress(uint64 userId) external view returns (address);
    function getStorage(uint64 id) external view returns (
        V4Storage memory user,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
    );
}

interface IV4Slots {
    function getUserSlots(uint64 userId) external view returns (Slot[] memory);
}

interface IV5Core {
    function getIdAddress(uint64 userId) external view returns (address);
    function getStorage(uint64 id) external view returns (
        V5Storage memory user,
        BaseParams memory base,
        uint[3] memory prices,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint refLimit,
        uint balance,
        uint coins
    );
}

interface IV5Slots {
    function getUserSlots(uint64 userId) external view returns (Slot[] memory, ItemType[] memory);
}

interface IV4NFT {
    function getOwnerTokensCount(address owner) external view returns (uint256);
}

contract V5BackendMigration is AccessControl {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    ICoreMigration private _coreMigration;
    ISlotsMigration private _slotsMigration;
    INFTMigration private _nftMigration;

    IV4Core private _coreOld;
    IV4Slots private _slotsOld;
    IV4NFT private _nftOld;
    IV5Core private _core;
    IV5Slots private _slots;

    uint64[] public usersToCheck;
    uint64[] public usersToMigrate;
    address[] public nftOwners;

    mapping (uint64 userId => bool isMigrated) public migrated;

    constructor (
        address coreMigrationAddress,
        address slotsMigrationAddress,
        address nftMigrationAddress,
        address v4CoreAddress,
        address v4SlotsAddress,
        address v4NFTAddress,
        address v5CoreAddress,
        address v5SlotsAddress
    ) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());

        _coreMigration = ICoreMigration(coreMigrationAddress);
        _slotsMigration = ISlotsMigration(slotsMigrationAddress);
        _nftMigration = INFTMigration(nftMigrationAddress);

        _coreOld = IV4Core(v4CoreAddress);
        _slotsOld = IV4Slots(v4SlotsAddress);
        _nftOld = IV4NFT(v4NFTAddress);

        _core = IV5Core(v5CoreAddress);
        _slots = IV5Slots(v5SlotsAddress);
    }

    function getNeedToMigrateCore(uint64 userId) public view returns (bool needMigrate) {
        address v5Address = _core.getIdAddress(userId);
        address v4Address = _coreOld.getIdAddress(userId);

        bool isV4 = v4Address != address(0);
        needMigrate = isV4 && v5Address == address(0);
    }

    function getHaveV4Slots(uint64 userId) public view returns (bool haveSlots, bool haveItems) {
        Slot[] memory slots = _slotsOld.getUserSlots(userId);
        for (uint i; i < slots.length; i++) {
            haveSlots = haveSlots || slots[i].isEnabled;
            haveItems = haveItems || slots[i].tokenId > 0;
        }
    }

    function getHaveV5Slots(uint64 userId) public view returns (bool haveSlots, bool haveItems) {
        (Slot[] memory slots,) = _slots.getUserSlots(userId);
        for (uint i; i < slots.length; i++) {
            haveSlots = haveSlots || slots[i].isEnabled;
            haveItems = haveItems || slots[i].tokenId > 0;
        }
    }

    function _migrate(uint64 userId) internal returns (bool skip) {
        if (migrated[userId]) {
            return true;
        }
        
        uint64[] memory users = new uint64[](1);
        users[0] = userId;
        _coreMigration.migrate(users);

        (bool haveOldSlots,) = getHaveV4Slots(userId);
        if (haveOldSlots) {
            _slotsMigration.migrate(users);
        }
        migrated[userId] = true;
        return false;
    }

    function nftOwnersLength() public view returns (uint) {
        return nftOwners.length;
    }

    function fillUsersToCheck(uint64[] calldata users) public onlyRole(BACKEND_ROLE) {
        for (uint i; i < users.length; i++) {
            if (migrated[users[i]]) {
                continue;
            }
            if (!_coreMigration.migrated(users[i])) {
                usersToCheck.push(users[i]);
            }
        }
    }

    function getUsersToCheckLength() public view returns (uint) {
        return usersToCheck.length;
    }

    function getUsersToMigrateLength() public view returns (uint) {
        return usersToMigrate.length;
    }

    function clearUsersToCheck() public onlyRole(BACKEND_ROLE) {
        delete usersToCheck;
    }

    function fillUsersToMigrage(uint limit) public onlyRole(BACKEND_ROLE) {
        uint realLimit = usersToCheck.length < limit
            ? usersToCheck.length
            : limit;
        for (uint i; i < realLimit; i++) {
            if (usersToCheck.length == 0) {
                return;
            }
            uint64 userId = usersToCheck[usersToCheck.length - 1];
            usersToCheck.pop();

            address owner = _coreOld.getIdAddress(userId);
            if (owner != address(0) && _nftOld.getOwnerTokensCount(owner) > 0) {
                nftOwners.push(owner);
            }

            bool needCore = getNeedToMigrateCore(userId);
            if (needCore) {
                usersToMigrate.push(userId);
            }
        }
    }

    function migrate(uint limit) public onlyRole(BACKEND_ROLE) {
        uint realLimit = usersToMigrate.length < limit
            ? usersToMigrate.length
            : limit;
        for (uint i; i < realLimit; i++) {
            if (usersToMigrate.length == 0) {
                return;
            }
            uint64 userId = usersToMigrate[usersToMigrate.length - 1];
            usersToMigrate.pop();
            _migrate(userId);
        }
    }

    function migrateNft(uint limit) public onlyRole(BACKEND_ROLE) {
        uint realLimit = nftOwners.length < limit
            ? nftOwners.length
            : limit;
        for (uint i; i < realLimit; i++) {
            if (nftOwners.length == 0) {
                return;
            }
            address owner = nftOwners[nftOwners.length - 1];
            nftOwners.pop();
            uint count = _nftOld.getOwnerTokensCount(owner);
            if (count == 0) {
                continue;
            }
            _nftMigration.migrate(owner, count);
        }
    }

}