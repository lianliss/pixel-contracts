//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

uint8 constant RARITY_COUNT = 6;

struct Recipe {
    uint recipeTypeId;
    uint botTypeId;
    uint8[RARITY_COUNT] requirements;
    uint8[] slots;
    uint price;
    address tokenAddress;
    bool isMinerToken;
}

/// @notice Require PXLsNFTV5 role MINTER_ROLE
/// @notice Require PXLsNFTV5 role BURNER_ROLE
/// @notice Require PXLsCoreV5 role BURNER_ROLE
contract AutoclaimCraft is AccessControl, Pausable {

    using EnumerableSet for EnumerableSet.UintSet;

    EnumerableSet.UintSet private _botsTypes;
    mapping (uint botTypeId => Recipe recipe) private _botsRecipes;
    mapping (uint recipeTypeId => uint botTypeId) private _recipesBots;

    IPXLsNFTV5 private _nft;
    IPXLsCoreV5 private _core;
    address private _receiver;
    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    constructor(
        address coreAddress,
        address nftAddress
        ) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());

        _core = IPXLsCoreV5(coreAddress);
        _nft = IPXLsNFTV5(nftAddress);
        _receiver = _msgSender();
    }

    event RecipeAdded(uint indexed recipeTypeId, uint indexed botTypeId, uint8[RARITY_COUNT] requirements, uint8[] slots, uint price, address tokenAddress, bool isMinerToken);
    event RecipeDeleted(uint indexed recipeTypeId, uint indexed botTypeId);
    event BotCrafted(uint indexed recipeTypeId, uint indexed botTypeId, address indexed account, uint[] tokensBurned, uint[] typesBurned, uint8[RARITY_COUNT] raritiesBurned, uint payed, address tokenAddress, bool isMinerToken);

    function setReceiver(address receiverAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _receiver = receiverAddress;
    }

    function addRecipe(
        uint recipeTypeId,
        uint botTypeId,
        uint8[RARITY_COUNT] calldata requirements,
        uint8[] calldata slots,
        uint price,
        address tokenAddress,
        bool isMinerToken
    ) public onlyRole(EDITOR_ROLE) {
        require(!_botsTypes.contains(botTypeId), "Bot recipe already exists");
        require(requirements[0] == 0, "Can't require ZERO rarity");
        _botsTypes.add(botTypeId);
        _botsRecipes[botTypeId] = Recipe(
            recipeTypeId,
            botTypeId,
            requirements,
            slots,
            price,
            tokenAddress,
            isMinerToken
        );
        _recipesBots[recipeTypeId] = botTypeId;
        emit RecipeAdded(recipeTypeId, botTypeId, requirements, slots, price, tokenAddress, isMinerToken);
    }

    function deleteRecipeByRecipeTypeId(uint recipeTypeId) public onlyRole(EDITOR_ROLE) {
        uint botTypeId = _recipesBots[recipeTypeId];
        require(_botsTypes.contains(botTypeId), "Recipe is not exists");
        _botsTypes.remove(botTypeId);
        _recipesBots[recipeTypeId] = 0;
        emit RecipeDeleted(recipeTypeId, botTypeId);
    }

    function deleteRecipeByBotTypeId(uint botTypeId) public onlyRole(EDITOR_ROLE) {
        require(_botsTypes.contains(botTypeId), "Recipe is not exists");
        uint recipeTypeId = _botsRecipes[botTypeId].recipeTypeId;
        _botsTypes.remove(botTypeId);
        _recipesBots[recipeTypeId] = 0;
        emit RecipeDeleted(recipeTypeId, botTypeId);
    }

    function getRecipeByBotTypeId(uint botTypeId) public view returns (Recipe memory) {
        return _botsRecipes[botTypeId];
    }

    function getRecipeByRecipeTypeId(uint recipeTypeId) public view returns (Recipe memory) {
        return _botsRecipes[_recipesBots[recipeTypeId]];
    }

    function getRecipes() public view returns (Recipe[] memory) {
        Recipe[] memory recipes = new Recipe[](_botsTypes.length());
        for (uint index; index < _botsTypes.length(); index++) {
            recipes[index] = _botsRecipes[_botsTypes.at(index)];
        }
        return recipes;
    }

    function _getTokenTypeId(uint tokenId) private view returns (uint) {
        ItemType memory itemType = _nft.getTokenType(tokenId);
        return itemType.typeId;
    }

    function _payRecipe(uint price, address tokenAddress, bool isMinerToken) internal {
        if (isMinerToken) {
            IPXLsCoreV5(tokenAddress).burn(_msgSender(), price);
        } else if (tokenAddress == address(0)) {
            require(msg.value >= price, "Not enough Ether");
            (bool sent,) = _receiver.call{value: msg.value}("");
            require(sent, "Failed to send Ether");
        } else {
            require(IERC20(tokenAddress).transferFrom(
                _msgSender(),
                _receiver,
                price
            ), "Can't transfer ERC20");
        }
    }

    function craft(uint recipeTokenId, uint[] calldata tokensIds) public payable whenNotPaused {
        _pause();

        uint recipeTypeId = _getTokenTypeId(recipeTokenId);
        uint botTypeId = _recipesBots[recipeTypeId];
        require(
            _botsTypes.contains(botTypeId)
            && _botsRecipes[botTypeId].recipeTypeId == recipeTypeId,
            "Recipe is not exists");

        /// Burn recipe
        require(_nft.ownerOf(recipeTokenId) == _msgSender(), "Sender is not an item owner");
        _nft.burn(recipeTokenId);

        /// Burn tokens
        uint[] memory itemTypesIds = new uint[](tokensIds.length);
        uint8[RARITY_COUNT] memory requirements = _botsRecipes[botTypeId].requirements;
        {
            uint8[] storage slots = _botsRecipes[botTypeId].slots;
            ItemType[] memory itemTypes = _nft.getTokenTypes(tokensIds);
            for (uint index; index < itemTypes.length; index++) {
                itemTypesIds[index] = itemTypes[index].typeId;
                require(requirements[itemTypes[index].rarity] > 0, "Unrequired token rarity found");
                require(_nft.ownerOf(tokensIds[index]) == _msgSender(), "Sender is not an item owner");

                /// Chest slots requirements
                if (slots.length > 0) {
                    bool inSlots = false;
                    for (uint s; s < slots.length; s++) {
                        for (uint itemSlot; itemSlot < itemTypes[index].slots.length; itemSlot++) {
                            if (slots[s] == itemTypes[index].slots[itemSlot]) {
                                inSlots = true;
                                break;
                            }
                        }
                        if (inSlots) {
                            break;
                        }
                    }
                    require(inSlots, "Unrequired token slot found");
                }
                _nft.burn(tokensIds[index]);
                --requirements[itemTypes[index].rarity];
            }
        }

        /// Burn shards
        uint shardsAmount = _botsRecipes[botTypeId].price;
        _payRecipe(shardsAmount, _botsRecipes[botTypeId].tokenAddress, _botsRecipes[botTypeId].isMinerToken);

        /// Check requirements
        for (uint index = 1; index < RARITY_COUNT; index++) {
            require(requirements[index] == 0, "The requirements are not met");
        }

        _nft.mint(_msgSender(), botTypeId);
        emit BotCrafted(
            recipeTypeId,
            botTypeId,
            _msgSender(),
            tokensIds,
            itemTypesIds,
            _botsRecipes[botTypeId].requirements,
            shardsAmount,
            _botsRecipes[botTypeId].tokenAddress,
            _botsRecipes[botTypeId].isMinerToken
        );

        _unpause();
    }

}