//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/V5/Payable/Payable.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";

struct BotParams {
    uint typeId;
    uint durationSeconds;
    uint sessionClaims;
    uint24 txPriceMod;
    bool isTimeSession;
    uint sessionSeconds;
    uint startPrice;
}

struct ChainTxPrice {
    uint chainId;
    uint txPrice;
}

/// @title Autoclaim Bot
/// @author Danil Sakhinov
/// @notice Required PayableMath role OPERATOR_ROLE
/// @notice Required PayableMath role MULTIPLIER_ROLE
/// @notice Required IPXLsNFTV5 role OPERATOR_ROLE
/// @notice Required IPXLsNFTV5 role BURNER_ROLE
/// @notice Required IPXLsCoreV5 role CLAIMER_ROLE
/// @notice Required IPXLsCoreV5 role BURNER_ROLE
/// @notice Required SecondMiner role CLAIMER_ROLE
contract AutoclaimNFT is Payable {

    using EnumerableSet for EnumerableSet.UintSet;
    using EnumerableSet for EnumerableSet.AddressSet;

    IPXLsNFTV5 private _nft;
    IPXLsCoreV5 private _core;
    EnumerableSet.UintSet private _botTypes;
    EnumerableSet.UintSet private _chains;
    EnumerableSet.AddressSet private _miners;
    mapping (address minerAddress => bool hasReferral) private _hasReferral;
    mapping (uint typeId => BotParams params) private _botTypeParams;
    mapping (uint tokenId => BotParams params) private _botParams;
    mapping (uint tokenId => uint64 userId) private _botOwner;
    mapping (uint64 userId => uint tokenId) private _userEquipped;
    mapping (uint64 userId => uint deposit) public userDeposit;
    mapping (uint64 userId => uint intervalSeconds) public userInterval;
    mapping (uint64 userId => uint startTimestamp) public userStartTimestamp;
    mapping (uint chainId => uint txPrice) private _chainTxPrice;

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    /// @param nftAddress PXLsNFTV5
    constructor(
        address coreAddress,
        address nftAddress,
        address payableMathAddress
        ) Payable(payableMathAddress, coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());

        _nft = IPXLsNFTV5(nftAddress);
        _core = IPXLsCoreV5(coreAddress);
    }

    event AddMiner(address indexed minerAddress, bool hasReferral);
    event RemoveMiner(address indexed minerAddress);
    event AddChainTxPrice(uint indexed chainId, uint txPrice);
    event RemoveChainTxPrice(uint indexed chainId);
    event GasCompensation(address indexed txSender, address erc20, uint amount);
    event SetBotType(uint indexed typeId, uint durationSeconds, uint sessionClaims, uint24 txPriceMod, bool isTimeSession, uint sessionSeconds, uint startPrice);
    event UnsetBotType(uint indexed typeId);
    event BotEquip(uint64 indexed userId, address indexed account, uint indexed typeId, uint tokenId, uint durationSeconds, uint sessionClaims, bool isTimeSession, uint sessionSeconds);
    event BotUnequip(uint64 indexed userId, address indexed account, uint indexed typeId, uint tokenId);
    event Deposit(uint64 indexed userId, address indexed account, address erc20, uint depositSize);
    event Withdraw(uint64 indexed userId, address indexed account, address erc20, uint withdrawSize);
    event BotStarted(uint64 indexed userId, address indexed account, uint indexed typeId, uint tokenId, uint durationSeconds, uint sessionClaims, uint startTime, uint intervalSeconds, uint depositSize, bool isTimeSession, uint sessionSeconds, uint startPrice);
    event BotStopped(uint64 indexed userId, address indexed account, uint indexed typeId, uint tokenId, uint stopTime, uint workTime, uint withdrawSize, uint8 reason);
    event ClaimMiner(uint64 indexed userId, address indexed account, address indexed miner, bool isReferral);
    event ClaimError(uint64 indexed userId, address indexed account, address indexed miner, bool isReferral);
    event BotClaimForUser(uint64 indexed userId, address indexed account, uint indexed typeId, uint tokenId, uint durationLeft, uint claimsLeft, uint txPrice);

    function setCore(address coreAddress) public override onlyRole(DEFAULT_ADMIN_ROLE) {
        _payableAuth = ICoreV5Auth(coreAddress);
        _core = IPXLsCoreV5(coreAddress);
        emit SetCore(coreAddress);
    }

    function addMiner(address minerAddress, bool hasReferral) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _miners.add(minerAddress);
        _hasReferral[minerAddress] = hasReferral;
        emit AddMiner(minerAddress, hasReferral);
    }

    function removeMiner(address minerAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _miners.remove(minerAddress);
        emit RemoveMiner(minerAddress);
    }

    function addChainTxPrice(uint chainId, uint txPrice) public onlyRole(BACKEND_ROLE) {
        _chains.add(chainId);
        _chainTxPrice[chainId] = txPrice;
        emit AddChainTxPrice(chainId, txPrice);
    }

    function removeChainTxPrice(uint chainId) public onlyRole(BACKEND_ROLE) {
        _chains.remove(chainId);
        emit RemoveChainTxPrice(chainId);
    }

    function getChainsTxPrices() public view returns (ChainTxPrice[] memory) {
        ChainTxPrice[] memory prices = new ChainTxPrice[](_chains.length());
        for (uint index; index < _chains.length(); index++) {
            prices[index].chainId = _chains.at(index);
            prices[index].txPrice = _chainTxPrice[prices[index].chainId];
        }
        return prices;
    }

    function _getChainsTxPricesSum() private view returns (uint) {
        uint sum;
        for (uint index; index < _chains.length(); index++) {
            uint chainId = _chains.at(index);
            sum += _chainTxPrice[chainId];
        }
        return sum;
    }

    function getMiners() public view returns (address[] memory, bool[] memory) {
        address[] memory miners = new address[](_miners.length());
        bool[] memory hasReferral = new bool[](_miners.length());
        for (uint index; index < _miners.length(); index++) {
            miners[index] = _miners.at(index);
            hasReferral[index] = _hasReferral[miners[index]];
        }
        return (miners, hasReferral);
    }

    /// @notice Set NFT typeId as Autoclaim Bot
    /// @param typeId - NFT typeId
    /// @param durationSeconds - Bot work time
    /// @param sessionClaims - Bot claims count per one work session if isTimeSession = false
    /// @param txPriceMod - Payable transaction multiplier with 4 digits of precision (10000 = 100%)
    /// @param isTimeSession - Is bot type works with time-limit sessions (false if claims-limit sessions)
    /// @param sessionSeconds - Work session in seconds if isTimeSession = true
    /// @param startPrice - Start price in PXLs
    function setBotType(
        uint typeId,
        uint durationSeconds,
        uint sessionClaims,
        uint24 txPriceMod,
        bool isTimeSession,
        uint sessionSeconds,
        uint startPrice
        ) public onlyRole(EDITOR_ROLE) {
        if (!_botTypes.contains(typeId)) {
            _botTypes.add(typeId);
        }
        _botTypeParams[typeId].typeId = typeId;
        _botTypeParams[typeId].durationSeconds = durationSeconds;
        _botTypeParams[typeId].sessionClaims = sessionClaims;
        _botTypeParams[typeId].txPriceMod = txPriceMod;
        _botTypeParams[typeId].isTimeSession = isTimeSession;
        _botTypeParams[typeId].sessionSeconds = sessionSeconds;
        _botTypeParams[typeId].startPrice = startPrice;
        emit SetBotType(typeId, durationSeconds, sessionClaims, txPriceMod, isTimeSession, sessionSeconds, startPrice);
    }

    function unsetBot(uint typeId) public onlyRole(EDITOR_ROLE) {
        require(_botTypes.contains(typeId), "NFT is not an Autoclaim bot");
        _botTypes.remove(typeId);
        emit UnsetBotType(typeId);
    }

    function getBotTypes() public view returns (BotParams[] memory) {
        BotParams[] memory bots = new BotParams[](_botTypes.length());
        for (uint index; index < _botTypes.length(); index++) {
            bots[index] = _botTypeParams[_botTypes.at(index)];
        }
        return bots;
    }

    function equip(uint tokenId) public {
        require(_nft.ownerOf(tokenId) == _msgSender(), "Not an item owner");
        uint64 userId = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(userId) == _msgSender(), "Not authorized");
        require(_userEquipped[userId] == 0, "The slot is already occupied by the item");
        ItemType memory tokenType = _nft.getTokenType(tokenId);
        require(_botTypes.contains(tokenType.typeId), "Item is not Autoclaim Bot");

        _nft.transferFrom(_msgSender(), address(this), tokenId);
        _botParams[tokenId] = _botTypeParams[tokenType.typeId];
        _botOwner[tokenId] = userId;
        _userEquipped[userId] = tokenId;
        if (_botParams[tokenId].txPriceMod > 0) {
            _payableMath.updateBotMod(userId, _botParams[tokenId].txPriceMod);
        }
        emit BotEquip(userId, _msgSender(), tokenType.typeId, tokenId, _botParams[tokenId].durationSeconds, _botParams[tokenId].sessionClaims, _botParams[tokenId].isTimeSession, _botParams[tokenId].sessionSeconds);
    }

    /// @dev stop reason = 1
    function _unequip(uint64 userId) private {
        uint tokenId = _userEquipped[userId];
        require(tokenId > 0 && _nft.ownerOf(tokenId) == address(this), "Bot is not equipped");
        if (userStartTimestamp[userId] > 0) {
            _stop(userId, 1);
        }
        _nft.burn(tokenId);
        _userEquipped[userId] = 0;
        BotParams storage params = _botParams[tokenId];
        delete _botParams[tokenId];
        _payableMath.updateBotMod(userId, uint24(_PERCENT_PRECISION));
        emit BotUnequip(userId, _core.getIdAddress(userId), params.typeId, tokenId);
    }

    function unequip() public {
        uint64 userId = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(userId) == _msgSender(), "Not authorized");
        _unequip(userId);
    }

    function unequipForUser(uint64 userId) public onlyRole(BACKEND_ROLE) {
        _unequip(userId);
    }

    function getTimingDepositSize(uint64 userId, uint intervalSeconds) public view returns (uint) {
        uint tokenId = _userEquipped[userId];
        if (tokenId == 0) return 0;
        BotParams storage botParams = _botParams[tokenId];
        uint price;
        uint claimsPerDay = 24 hours / intervalSeconds;
        uint sessionClaims = botParams.isTimeSession
            ? botParams.sessionSeconds / intervalSeconds
            : botParams.sessionClaims;

        /// Today claims
        uint todayTimeLeft = 24 hours - block.timestamp % 24 hours;
        if (todayTimeLeft > botParams.durationSeconds) {
            todayTimeLeft = botParams.durationSeconds;
        }
        uint todayClaims = todayTimeLeft / intervalSeconds;
        if (todayClaims > sessionClaims) {
            todayClaims = sessionClaims;
        }
        price += _payableMath.estimateDayTransactionsPrice(userId, todayClaims, _payableMath.getNextTransactionIndex(userId));
        price += _getChainsTxPricesSum() * todayClaims;
        if (claimsPerDay == 0 || todayClaims == sessionClaims) {
            return price;
        }

        /// Usual daily claims by full day
        uint claimsLeft = (botParams.durationSeconds - todayTimeLeft) / intervalSeconds;
        if (claimsLeft > sessionClaims - todayClaims) {
            claimsLeft = sessionClaims - todayClaims;
        }
        uint fullDays = claimsLeft / claimsPerDay;
        price += _payableMath.estimateDayTransactionsPrice(userId, claimsPerDay, 0) * fullDays;
        price += _getChainsTxPricesSum() * claimsPerDay * fullDays;

        /// Last not full day claims
        claimsLeft = claimsLeft % claimsPerDay;
        price += _payableMath.estimateDayTransactionsPrice(userId, claimsLeft, 0);
        price += _getChainsTxPricesSum() * claimsLeft;
        return price;
    }

    function _sendTxGasCompensation() private returns (uint) {
        uint txPrice = _getChainsTxPricesSum();
        if (address(_payableErc20) != address(0)) {
            require(_payableErc20.transfer(
                _msgSender(),
                txPrice
            ), "Can't transfer ERC20");
        } else {
            require(address(this).balance >= txPrice, "Not enough coins");
            (bool sent,) = _msgSender().call{value: txPrice}("");
            require(sent, "Failed to send Ether");
        }
        emit GasCompensation(_msgSender(), address(_payableErc20), txPrice);
        return txPrice;
    }

    function start(uint intervalSeconds) public payable {
        uint64 userId = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(userId) == _msgSender(), "Not authorized");
        uint tokenId = _userEquipped[userId];
        require(tokenId > 0, "Bot is not equipped");
        BotParams storage botParams = _botParams[tokenId];
        require(botParams.durationSeconds > intervalSeconds, "Bot resource exhausted");
        userInterval[userId] = intervalSeconds;
        uint depositSize = getTimingDepositSize(userId, intervalSeconds);
        if (address(_payableErc20) != address(0)) {
            require(_payableErc20.transferFrom(
                _msgSender(),
                address(this),
                depositSize
            ), "Can't transfer ERC20");
            userDeposit[userId] = depositSize;
            emit Deposit(userId, _msgSender(), address(_payableErc20), depositSize);
        } else {
            require(msg.value >= depositSize, "Not enough coins");
            userDeposit[userId] = msg.value;
            emit Deposit(userId, _msgSender(), address(_payableErc20), msg.value);
        }

        _core.burn(_msgSender(), botParams.startPrice);

        userStartTimestamp[userId] = block.timestamp;
        emit BotStarted(
            userId,
            _msgSender(),
            botParams.typeId,
            tokenId,
            botParams.durationSeconds,
            botParams.sessionClaims,
            block.timestamp,
            intervalSeconds,
            userDeposit[userId],
            botParams.isTimeSession,
            botParams.sessionSeconds,
            botParams.startPrice
        );
    }

    function _stop(uint64 userId, uint8 reason) private {
        require(userStartTimestamp[userId] > 0, "Bot is not started");
        address account = _core.getIdAddress(userId);
        uint tokenId = _userEquipped[userId];
        require(tokenId > 0, "Bot is not equipped");
        BotParams storage botParams = _botParams[tokenId];
        // Withdraw deposit
        uint depositSize = userDeposit[userId];
        if (address(_payableErc20) != address(0)) {
            if (depositSize > _payableErc20.balanceOf(address(this))) {
                depositSize = _payableErc20.balanceOf(address(this));
            }
            require(_payableErc20.transfer(
                account,
                depositSize
            ), "Can't transfer ERC20");
        } else {
            if (depositSize > address(this).balance) {
                depositSize = address(this).balance;
            }
            (bool sent,) = account.call{value: depositSize}("");
            require(sent, "Failed to send Ether");
        }
        // Reset Bot session Durability
        botParams.sessionClaims = _botTypeParams[botParams.typeId].sessionClaims;
        botParams.sessionSeconds = _botTypeParams[botParams.typeId].sessionSeconds;
        // Decrease bot duration
        uint workTime = block.timestamp - userStartTimestamp[userId];
        if (workTime > botParams.durationSeconds) {
            botParams.durationSeconds = 0;
        } else {
            botParams.durationSeconds -= workTime;
        }
        // Clear user params
        userDeposit[userId] = 0;
        userStartTimestamp[userId] = 0;
        userInterval[userId] = 0;
        // Events
        emit Withdraw(userId, account, address(_payableErc20), depositSize);
        emit BotStopped(
            userId,
            account,
            botParams.typeId,
            tokenId,
            block.timestamp,
            workTime,
            depositSize,
            reason
        );
    }

    /// @dev stop reason = 0
    function stop() public {
        uint64 userId = _core.getAddressId(_msgSender());
        require(_core.getIdAddress(userId) == _msgSender(), "Not authorized");
        _stop(userId, 0);
    }

    /// @dev stop reason = 2
    function stopForUser(uint64 userId) public onlyRole(BACKEND_ROLE) {
        _stop(userId, 2);
    }

    /// @dev stop reason = 3 if user deposit is too low
    /// @dev stop reason = 4 if session parameter is too low
    /// @dev stop reason = 5 if duration is too low
    /// @dev stop reason = 6 if session is finished
    /// @dev stop reason = 7 if duration is finished
    function claimForUser(uint64 userId) public onlyRole(BACKEND_ROLE) {
        require(userStartTimestamp[userId] > 0, "Bot is not started");
        address account = _core.getIdAddress(userId);
        uint tokenId = _userEquipped[userId];
        require(tokenId > 0, "Bot is not equipped");
        BotParams storage botParams = _botParams[tokenId];
        uint txPrice = _payableMath.getNextTransactionPrice(userId);
        uint workTime = block.timestamp - userStartTimestamp[userId];

        /// Stop before claims
        if (txPrice > userDeposit[userId]) {
            _stop(userId, 3);
            return;
        }
        if (botParams.isTimeSession) {
            if (workTime > botParams.sessionSeconds) {
                _stop(userId, 4);
                return;
            }
        } else {
            if (botParams.sessionClaims == 0) {
                _stop(userId, 4);
                return;
            }
        }
        if (botParams.durationSeconds < workTime) {
            _stop(userId, 5);
            return;
        }

        /// Transfer payable transaction
        userDeposit[userId] -= txPrice;
        if (address(_payableErc20) != address(0)) {
            require(_payableErc20.transfer(
                _payableReceiver,
                txPrice
            ), "Can't transfer ERC20");
        } else {
            require(address(this).balance >= txPrice, "Not enough coins");
            (bool sent,) = _payableReceiver.call{value: txPrice}("");
            require(sent, "Failed to send Ether");
        }
        /// Emit Payable transaction
        _payableMath.emitTransaction(userId);
        /// Send compensation to executer
        userDeposit[userId] -= _sendTxGasCompensation();

        /// Claim miners
        for (uint index; index < _miners.length(); index++) {
            IPXLsCoreV5 miner = IPXLsCoreV5(_miners.at(index));
            try miner.claimFor(userId) {
                emit ClaimMiner(userId, account, address(miner), false);
            } catch {
                emit ClaimError(userId, account, address(miner), false);
            }
            /// Claim referral if exists
            if (_hasReferral[address(miner)]) {
                try miner.claimForRef(userId) {
                    emit ClaimMiner(userId, account, address(miner), true);
                } catch {
                    emit ClaimError(userId, account, address(miner), true);
                }
            }
        }

        if (!botParams.isTimeSession) {
            --botParams.sessionClaims;
        }
        emit BotClaimForUser(
            userId,
            account,
            botParams.typeId,
            tokenId,
            botParams.durationSeconds - workTime,
            botParams.sessionClaims,
            txPrice
        );

        /// Stop after claims
        if (botParams.isTimeSession) {
            if (workTime + userInterval[userId] > botParams.sessionSeconds) {
                _stop(userId, 6);
            }
        } else {
            if (botParams.sessionClaims == 0) {
                _stop(userId, 6);
            }
        }
        if (botParams.durationSeconds < workTime + userInterval[userId]) {
            _stop(userId, 7);
        }
    }

    function getUserSlot(uint64 userId) public view returns(
        BotParams memory botParams,
        uint tokenId,
        uint deposit,
        uint intervalSeconds,
        uint startTimestamp
    ) {
        tokenId = _userEquipped[userId];
        botParams = _botParams[tokenId];
        if (userStartTimestamp[userId] > 0) {
            uint workTime = block.timestamp - userStartTimestamp[userId];
            if (workTime > botParams.durationSeconds) {
                botParams.durationSeconds = 0;
            } else {
                botParams.durationSeconds -= workTime;
            }
        }
        deposit = userDeposit[userId];
        intervalSeconds = userInterval[userId];
        startTimestamp = userStartTimestamp[userId];
    }

}