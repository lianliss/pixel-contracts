//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/Proxy.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Chest/ChestV5Structs.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Cashback/interfaces/ICashback.sol";
 
/// @title Base context of PXLsChestV5
/// @author Danil Sakhinov
/// @notice Use to maintain the structural integrity of the proxies
contract ChestV5Context is Proxy {

    Chest[] internal _chests;
    MarketItem[] internal _market;
    mapping(uint64 id => uint24[RARITY_COUNT]) internal _chances;
    IPXLsCoreV5 internal _core;
    IPXLsNFTV5 internal _nft;
    address payable internal _receiver;

    uint internal _baseValue = PERCENT_PRECISION;
    uint internal _minPXLsBonus;
    uint internal _maxPXLsBonus = 2 * 10**18;
    IERC20 public _erc20;
    ICashback public _cashback;

}