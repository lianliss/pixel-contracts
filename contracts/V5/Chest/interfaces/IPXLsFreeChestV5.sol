//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPXLsFreeChestV5 {
    event FreeChestClaimed(uint64 indexed userId, address indexed account, uint indexed chestId);
    event WeeklyChestClaimed(uint64 indexed userId, address indexed account, uint indexed chestId);
    
    function claimWeeklyChest() external;
    function claimFreeChest(uint chestId) external;
    function setUsersFreeChest(uint64[] calldata userId, uint chestId) external;
    function getUserFreeChests(uint64 userId) external view returns (uint[] memory);
}