//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/libraries/PixelLibrary.sol";
import "contracts/V5/Chest/ChestV5Structs.sol";
import "contracts/Proxy/IProxy.sol";

interface IPXLsChestV5 is IProxy {
    event CreateChest(uint indexed chestId, string title, uint minPixels, uint maxPixels);
    event UpdateChest(uint indexed chestId, string title, uint minPixels, uint maxPixels);
    event ItemDrop(address indexed account, uint indexed chestId, uint indexed typeId, uint tokenId);
    event BonusDrop(address indexed account, uint64 indexed userId, uint amount);
    event BuyChestPixel(address indexed account, uint indexed marketItem, uint indexed chestId, uint price);
    event BuyChestSGB(address indexed account, uint indexed marketItem, uint indexed chestId, uint price);
    event SendChest(address indexed account, uint indexed chestId);

    function createChest(
        uint[RARITY_COUNT] calldata ratios,
        uint minPixels,
        uint maxPixels,
        string calldata title
    ) external returns (uint);

    function updateChest(
        uint chestId,
        uint[RARITY_COUNT] calldata ratios,
        uint minPixels,
        uint maxPixels,
        string calldata title
    ) external;
    
    function setUserChances(uint64 userId, uint24[RARITY_COUNT] calldata chances) external;
    function sendChest(uint chestId, address account) external;
    function getChestsCount() external view returns (uint);
    function addChestItem(uint chestIndex, uint slotIndex, uint rarity, uint typeId) external returns (uint);
    function addChestItems(uint chestIndex, uint slotIndex, uint rarity, uint[] calldata typeId) external;
}