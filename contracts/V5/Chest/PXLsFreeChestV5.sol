//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Chest/interfaces/IPXLsChestV5.sol";
import "contracts/V5/Chest/interfaces/IPXLsFreeChestV5.sol";

/// @title Weekly Chest claimer
/// @author Danil Sakhinov
/// @notice Require PXLsCoreV5 role PROXY_ROLE
/// @notice Require PXLsChestV5 role MINTER_ROLE
contract PXLsFreeChestV5 is CoreV5Context, ProxyImplementation, IPXLsFreeChestV5 {

    IPXLsChestV5 private _chest;
    uint public weeklyChestId;
    mapping(uint64 id => mapping(uint chestId => bool)) public userFreeChestId;

    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    /// @param coreAddress PXLsCoreV5
    /// @param chestAddress PXLsChestV5
    constructor(
        address coreAddress,
        address chestAddress
    ) ProxyImplementation(coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        _chest = IPXLsChestV5(chestAddress);
    }

    error TooShortActivity(uint);

    function _clearActivity(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xClearActivity(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            if (_storages[userId].activity >= _activityRewardSeconds) {
                _storages[userId].activity = 0;
            } else {
                revert TooShortActivity(_storages[userId].activity);
            }
        }
    }
    /// @notice proxy entrance
    function xClearActivity(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _clearActivity(encoded);
    }
    function claimWeeklyChest() public {
        IPXLsCoreV5 core = IPXLsCoreV5(address(getProxy()));
        uint64 userId = core.getAddressId(_msgSender());
        require(userId > 0, "Unauthorized");

        _clearActivity(abi.encode(userId));
        _chest.sendChest(weeklyChestId, _msgSender());
        emit WeeklyChestClaimed(userId, _msgSender(), weeklyChestId);
    }

    function claimFreeChest(uint chestId) public {
        IPXLsCoreV5 core = IPXLsCoreV5(address(getProxy()));
        uint64 userId = core.getAddressId(_msgSender());
        require(userId > 0, "Unauthorized");

        require(userFreeChestId[userId][chestId] == true, "Free chest not available");
        userFreeChestId[userId][chestId] = false;
        _chest.sendChest(chestId, _msgSender());
        emit FreeChestClaimed(userId, _msgSender(), chestId);
    }

    function setUsersFreeChest(uint64[] calldata userId, uint chestId) public onlyRole(EDITOR_ROLE) {
        for (uint i; i < userId.length; i++) {
            userFreeChestId[userId[i]][chestId] = true;
        }
    }

    function getUserFreeChests(uint64 userId) public view returns (uint[] memory) {
        uint chestsCount = _chest.getChestsCount();
        uint[] memory chests = new uint[](chestsCount);
        for (uint chestId; chestId < chestsCount; chestId++) {
            if (userFreeChestId[userId][chestId] == true) {
                chests[chestId] = chestId;
            }
        }
        return chests;
    }

    function setWeeklyChest(uint chestId) public onlyRole(EDITOR_ROLE) {
        weeklyChestId = chestId;
    }

}