//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/libraries/PixelLibrary.sol";

struct Chest {
    uint[][RARITY_COUNT][] items;
    uint[RARITY_COUNT] ratios;
    uint chestId;
    uint minPixels;
    uint maxPixels;
    string title;
}

struct MarketItem {
    uint marketId;
    uint chestId;
    uint count;
    uint pixelPrice;
    uint sgbPrice;
}