//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/V5/Payable/interfaces/IPayableMath.sol";
import "contracts/V5/Slots/interfaces/IParameterReceiver.sol";

contract PayableMath is IPayableMath, IParameterReceiver, AccessControl {

    bytes32 public constant OPERATOR_ROLE = keccak256("OPERATOR_ROLE");
    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    mapping (uint64 userId => uint lastTransactionTimestamp) private _lastTransaction;
    mapping (uint64 userId => uint transactionsToday) private _transactions;
    mapping (uint64 userId => uint24 percents) public userSlotsMod;
    mapping (uint64 userId => uint24 percents) public userBotMod;
    mapping (uint64 userId => uint24 percents) public userExtraMod;

    uint constant private PERCENT_PRECISION = 10000;

    uint public baseTransactionGas = 4433670000000000;

    uint constant COOLDOWN_PERIOD = 24 hours;

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(OPERATOR_ROLE, _msgSender());
        _grantRole(MULTIPLIER_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(OPERATOR_ROLE, address(this));
    }

    function _getIsNewDay(uint64 userId) private view returns (bool) {
        uint lastDay = _lastTransaction[userId] / COOLDOWN_PERIOD;
        uint today = block.timestamp / COOLDOWN_PERIOD;
        return today != lastDay;
    }

    function getLastTransactionTimestamp(uint64 userId) public view returns (uint) {
        return _lastTransaction[userId];
    }

    function getNextTransactionIndex(uint64 userId) public view returns (uint) {
        if (_getIsNewDay(userId)) {
            return 0;
        } else {
            return _transactions[userId];
        }
    }

    function _getTransactionPrice(uint64 userId, uint index) private view returns (uint) {
        uint price = index * (index + 1) / 2 * baseTransactionGas;
        if (userSlotsMod[userId] > 0) {
            price = price * uint(userSlotsMod[userId]) / PERCENT_PRECISION;
        }
        if (userBotMod[userId] > 0) {
            price = price * uint(userBotMod[userId]) / PERCENT_PRECISION;
        }
        if (userExtraMod[userId] > 0) {
            price = price * uint(userExtraMod[userId]) / PERCENT_PRECISION;
        }
        return price;
    }

    function getNextTransactionPrice(uint64 userId) public view returns (uint) {
        uint index = getNextTransactionIndex(userId);
        return _getTransactionPrice(userId, index);
    }

    function estimateDayTransactionsPrice(uint64 userId, uint count, uint startIndex) public view returns (uint) {
        uint price;
        for (uint index = startIndex; index < count + startIndex; index++) {
            price += _getTransactionPrice(userId, index);
        }
        return price;
    }

    function estimateDayTransactionsPrice(uint count, uint startIndex) public view returns (uint) {
        return estimateDayTransactionsPrice(0, count, startIndex);
    }

    function estimateDayTransactionsPrice(uint count) public view returns (uint) {
        return estimateDayTransactionsPrice(count, 0);
    }

    function emitTransaction(uint64 userId) public onlyRole(OPERATOR_ROLE) {
        _transactions[userId] = getNextTransactionIndex(userId) + 1;
        _lastTransaction[userId] = block.timestamp;
    }

    function setBaseTransactionGas(uint newGasAmount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseTransactionGas = newGasAmount;
        emit UpdateBaseTransactionGas(newGasAmount);
    }

    function flushUserTransactions(uint64 userId) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _transactions[userId] = 0;
    }

    function updateParameter(uint64 userId, uint8, uint24 multiplier, uint256) public onlyRole(MULTIPLIER_ROLE) {
        userSlotsMod[userId] = multiplier;
    }

    function updateBotMod(uint64 userId, uint24 multiplier) public onlyRole(MULTIPLIER_ROLE) {
        userBotMod[userId] = multiplier;
    }

    function updateExtraMod(uint64 userId, uint24 multiplier) public onlyRole(BACKEND_ROLE) {
        userExtraMod[userId] = multiplier;
    }

}