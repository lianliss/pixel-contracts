//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IPayableMath {
    
    function getLastTransactionTimestamp(uint64 userId) external view returns (uint);
    function getNextTransactionIndex(uint64 userId) external view returns (uint);
    function getNextTransactionPrice(uint64 userId) external view returns (uint);

    function estimateDayTransactionsPrice(uint64 userId, uint count, uint startIndex) external view returns (uint);
    function estimateDayTransactionsPrice(uint count, uint startIndex) external view returns (uint);
    function estimateDayTransactionsPrice(uint count) external view returns (uint);

    function emitTransaction(uint64 userId) external;

    function updateBotMod(uint64 userId, uint24 multiplier) external;
    function updateExtraMod(uint64 userId, uint24 multiplier) external;

    event UpdateBaseTransactionGas(uint newGasAmount);

}