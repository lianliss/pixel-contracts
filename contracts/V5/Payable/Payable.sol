//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Payable/interfaces/IPayableMath.sol";
import "contracts/V5/Core/interfaces/ICoreV5Auth.sol";

abstract contract Payable is AccessControl {

    IPayableMath public _payableMath;
    IERC20 public _payableErc20;
    address public _payableReceiver;
    ICoreV5Auth public _payableAuth;

    constructor(address mathAddress, address coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _payableMath = IPayableMath(mathAddress);
        _payableReceiver = _msgSender();
        _payableAuth = ICoreV5Auth(coreAddress);
    }

    event PayableTransaction(address sender, uint64 userId, uint amount);
    event SetERC20(address newAddress);
    event SetMath(address newAddress);
    event SetReceiver(address newAddress);
    event SetCore(address newAddress);

    function setERC20(address newERC20) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _payableErc20 = IERC20(newERC20);
        emit SetERC20(newERC20);
    }

    function setPayableMath(address mathAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _payableMath = IPayableMath(mathAddress);
        emit SetMath(mathAddress);
    }

    function setReceiver(address receiverAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _payableReceiver = receiverAddress;
        emit SetReceiver(receiverAddress);
    }

    function setCore(address coreAddress) public virtual onlyRole(DEFAULT_ADMIN_ROLE) {
        _payableAuth = ICoreV5Auth(coreAddress);
        emit SetCore(coreAddress);
    }

    receive() external payable {
        (bool sent,) = _payableReceiver.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        emit PayableTransaction(_msgSender(), _payableAuth.getAddressId(_msgSender()), msg.value);
    }

    function _payTransaction(uint64 userId) internal {
        uint price = _payableMath.getNextTransactionPrice(userId);
        if (address(_payableErc20) == address(0)) {
            require(msg.value >= price, "Not enough Ether");
            (bool sent,) = _payableReceiver.call{value: msg.value}("");
            require(sent, "Failed to send Ether");
            emit PayableTransaction(_msgSender(), userId, msg.value);
        } else {
            require(_payableErc20.transferFrom(
                _msgSender(),
                _payableReceiver,
                price
            ), "Can't transfer ERC20");
            emit PayableTransaction(_msgSender(), userId, price);
        }
        _payableMath.emitTransaction(userId);
    }

}