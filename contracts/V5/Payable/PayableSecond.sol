//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/SecondMiner/SecondMinerV5Context.sol";
import "contracts/V5/SecondMiner/interfaces/ISecondMinerV5.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Payable/Payable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

/// @title Second Miner with Payable transactions
/// @author Danil Sakhinov
/// @notice Required SecondMinerV5 role PROXY_ROLE
/// @notice Required PayableMath role OPERATOR_ROLE
contract PayableSecond is SecondMinerV5Context, ProxyImplementation, IPXLSecondMinerV5, Payable {

    bool public payableClaimsDisabled = false;

    constructor(
        address secondMinerAddress,
        address coreAddress,
        address payableMathAddress
        )
    ProxyImplementation(secondMinerAddress)
    Payable(payableMathAddress, coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }

    function setPayableClaimsDisabled(bool newValue) public onlyRole(DEFAULT_ADMIN_ROLE) {
        payableClaimsDisabled = newValue;
    }

    modifier onlyStorageOwner(uint64 userId) {
        require(_payableAuth.getIdAddress(userId) == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    /// Internal core functions

    function _getCoreStorage(uint64 id) internal view returns (Storage memory) {
        (
            Storage memory _storage,,,,,,,,
        ) = _core.getStorage(id);
        return _storage;
    }

    function _getSpeed(Storage memory coreUser, uint64 id) internal view returns (uint) {
        SecondStorage storage user = _users[id];
        uint24 mult = user.speedMultiplicator == 0
            ? uint24(_PERCENT_PRECISION)
            : user.speedMultiplicator;
        if (_parameters[SPEED_PARAMETER] == 0) {
            mult = coreUser.speedMultiplicator;
        }
        uint256 adder = _parameters[SPEED_PARAMETER] == 0
            ? coreUser.speedAdder
            : user.speedAdder;
        return (coreUser.speedLevel + 1) * baseSpeed
        * mult / _PERCENT_PRECISION
        / _HOUR
        + adder;
    }

    function _getSize(Storage memory coreUser, uint64 id) internal view returns (uint) {
        SecondStorage storage user = _users[id];
        uint24 mult = user.sizeMultiplicator == 0
            ? uint24(_PERCENT_PRECISION)
            : user.sizeMultiplicator;
        if (_parameters[SIZE_PARAMETER] == 0) {
            mult = coreUser.sizeMultiplicator;
        }
        uint256 adder = _parameters[SIZE_PARAMETER] == 0
            ? coreUser.sizeAdder
            : user.sizeAdder;
        return (coreUser.sizeLevel + 1) * baseSpeed
        * baseSize / _PRECISION
        * mult / _PERCENT_PRECISION
            + adder;
    }

    function _claim(uint64 id, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _users[id].claimed += amount;
        _balances[id] += amount;
    }

    function _checkRequirements(uint64 id) internal view returns (bool) {
        Storage memory coreUser = _getCoreStorage(id);
        return coreUser.sizeLevel >= minSizeLevel
        && coreUser.speedLevel >= minSpeedLevel;
    }

    function _getMinedTokens(uint64 id) internal view returns (uint) {
        SecondStorage storage user = _users[id];
        Storage memory coreUser = _getCoreStorage(id);
        uint limit = _getSize(coreUser, id);
        if (user.claimTimestamp == 0) {
            return 0;
        }
        uint time = block.timestamp - user.claimTimestamp;
        uint amount = _getSpeed(coreUser, id)
            * time;
        return amount > limit && limit != 0
            ? limit
            : amount;
    }

    function _allowMining(uint64 id) internal returns (bool) {
        SecondStorage storage user = _users[id];
        require(!user.disabled, "This token mining is disabled for this user");
        if (user.claimTimestamp == 0) {
            Storage memory coreUser = _getCoreStorage(id);
            require(coreUser.sizeLevel >= minSizeLevel, "Minimum storage level required");
            require(coreUser.speedLevel >= minSpeedLevel, "Minimum drill level required");
            user.claimTimestamp = block.timestamp;
        }
        return true;
    }

    function _claimReward(uint64 id, uint additional) internal {
        require(!_claimsDisabled, "Claims disabled");
        uint mined = _getMinedTokens(id) + additional;
        SecondStorage storage current = _users[id];
        if (current.claimTimestamp == 0) {
            _allowMining(id);
            return;
        }
        if (mined == 0) return;
        Storage memory coreUser = _getCoreStorage(id);
        require(!coreUser.disabled, "Storage disabled");
        require(!current.disabled, "This mining is disabled for this user");
        _claim(id, mined);
        current.claimTimestamp = block.timestamp;
        emit Claim(id, coreUser.account, mined);
        emit Transfer(address(this), coreUser.account, mined);

        uint index = 0;
        uint64 previous = id;
        while (index < parentsPercents.length
        && coreUser.parent != 0
            && !_refersDisabled) {
            uint64 receiver = coreUser.parent;
            coreUser = _getCoreStorage(receiver);
            uint reward = mined * parentsPercents[index] / _PERCENT_PRECISION;
            if (_claimed + reward <= _totalSupply
                && coreUser.account != address(0)
                && !_users[receiver].disabled) {
                _claim(receiver, reward);
                emit ReferReward(id, receiver, previous, coreUser.account, reward);
            }
            index++;
            previous = receiver;
        }
    }


    /// External core functions

    function _claimRewardImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xClaimReward(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            _claimReward(userId, 0);
        }
    }
    function xClaimReward(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _claimRewardImplementation(encoded);
    }
    function claimReward(uint64 userId) public payable onlyStorageOwner(userId) {
        require(!payableClaimsDisabled, "Claims disabled");
        _payTransaction(userId);
        _claimRewardImplementation(abi.encode(userId));
    }

}