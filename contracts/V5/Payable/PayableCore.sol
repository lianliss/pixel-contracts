//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/CoreV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Payable/Payable.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5Events.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Slots/interfaces/IPXLsSlotsV5.sol";

/// @title Core and Slots with Payable transactions
/// @author Danil Sakhinov
/// @notice Required PXLsCoreV5 role PROXY_ROLE
/// @notice Required PXLsSlotsV5 role PROXY_ROLE
/// @notice Required PXLsSlotsV5 role EDITOR_ROLE
/// @notice Required PayableMath role OPERATOR_ROLE
contract PayableCore is CoreV5Context, ProxyImplementation, IPXLsCoreV5Events, Payable {

    bool public payableClaimsDisabled = false;
    IPXLsSlotsV5 public _slots;

    /// @param coreAddress PXLsCoreV5
    constructor(
        address coreAddress,
        address payableMathAddress,
        address slotsAddress
        )
    ProxyImplementation(coreAddress)
    Payable(payableMathAddress, coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());

        _slots = IPXLsSlotsV5(slotsAddress);
    }

    function setPayableClaimsDisabled(bool newValue) public onlyRole(DEFAULT_ADMIN_ROLE) {
        payableClaimsDisabled = newValue;
    }

    modifier onlyStorageOwner(uint64 userId) {
        require(_payableAuth.getIdAddress(userId) == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    /// Internal core functions

    function _burn(uint64 userId, uint256 value) internal {
        require(_balances[userId] >= value, "Can't burn more");
        uint burnValue = value * _math.getParams().burnRate / _PERCENT_PRECISION;
        uint reinvestValue = value - burnValue;
        _totalSupply -= burnValue;
        _balances[userId] -= value;
        _burned += burnValue;
        emit Burn(userId, _storages[userId].account, burnValue);
        if (reinvestValue > 0) {
            emit Reinvest(userId, _storages[userId].account, reinvestValue);
        }
    }

    function _claim(uint64 userId, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _storages[userId].claimed += amount;
        _balances[userId] += amount;
    }

    function _fillRef(uint64 userId, uint amount) internal returns (uint) {
        Storage storage user = _storages[userId];
        uint referralSize = _math.getReferralSize(user);
        if (referralSize <= user.refStorage) {
            return 0;
        }
        uint limit = referralSize - user.refStorage;
        uint value = limit < amount
            ? limit
            : amount;
        if (value > 0) {
            user.refStorage += value;
        }
        return value;
    }

    function _claimReward(uint64 userId, uint additional) internal {
        Storage storage current = _storages[userId];
        uint mined = _math.getMinedTokens(current) + additional;
        if (mined == 0) return;
        require(!current.disabled, "Storage disabled");
        _claim(userId, mined);
        current.claimTimestamp = block.timestamp;
        emit Claim(userId, current.account, mined);
        emit Transfer(address(this), current.account, mined);

        uint index = 0;
        uint64 previous = userId;
        uint[2] memory parentsPercents = _math.getParentsPercents();
        while (index < parentsPercents.length
        && current.parent != 0
            && !_refersDisabled) {
            uint64 receiver = current.parent;
            current = _storages[receiver];
            uint reward = mined * parentsPercents[index] / _PERCENT_PRECISION;
            if (_claimed + reward <= _totalSupply && current.account != address(0)) {
                uint filled = _fillRef(receiver, reward);
                if (filled > 0) {
                    emit ReferReward(userId, receiver, previous, current.account, filled);
                }
            }
            index++;
            previous = receiver;
        }
    }

    function _promoteSizeLevel(uint64 userId, uint levelPrice) internal {
        Storage storage user = _storages[userId];
        _claimReward(userId, 0);
        user.sizeLevel++;
        emit LevelUpSize(userId, user.account, user.sizeLevel, levelPrice);
    }

    function _promoteSpeedLevel(uint64 userId, uint levelPrice) internal {
        Storage storage user = _storages[userId];
        _claimReward(userId, 0);
        user.speedLevel++;
        emit LevelUpSpeed(userId, user.account, user.speedLevel, levelPrice);
    }

    function _promoteReferralLevel(uint64 userId, uint levelPrice) internal {
        Storage storage user = _storages[userId];
        user.referralLevel++;
        emit LevelUpReferral(userId, user.account, user.referralLevel, levelPrice);
    }


    /// External core functions

    function _claimRewardImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xClaimReward(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            _claimReward(userId, 0);
        }
    }
    function xClaimReward(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _claimRewardImplementation(encoded);
    }
    function claimReward(uint64 userId) public payable onlyStorageOwner(userId) {
        require(!payableClaimsDisabled, "Claims disabled");
        _payTransaction(userId);
        _claimRewardImplementation(abi.encode(userId));
    }


    
    function _claimReferralImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xClaimReferral(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            Storage storage user = _storages[userId];
            require(!user.disabled, "Storage disabled");
            uint mined = user.refStorage;
            if (mined == 0) return;
            _claim(userId, mined);
            user.refStorage = 0;
            emit ClaimReferral(userId, user.account, mined);
            emit Transfer(address(this), user.account, mined);
        }
    }
    function xClaimReferral(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _claimReferralImplementation(encoded);
    }
    function claimReferral(uint64 userId) public payable onlyStorageOwner(userId) {
        require(!payableClaimsDisabled, "Claims disabled");
        _payTransaction(userId);
        _claimReferralImplementation(abi.encode(userId));
    }



    function _buySizeLevelImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xBuySizeLevel(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            Storage storage user = _storages[userId];
            uint levelPrice = _math.getSizePrice(user);
            require(IERC20(address(this)).balanceOf(user.account) >= levelPrice, "Not enough funds");

            _burn(userId, levelPrice);
            _promoteSizeLevel(userId, levelPrice);
            emit Transfer(user.account, address(0), levelPrice);
        }
    }
    function xBuySizeLevel(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _buySizeLevelImplementation(encoded);
    }
    function buySizeLevel(uint64 userId) public payable onlyStorageOwner(userId) {
        _payTransaction(userId);
        _buySizeLevelImplementation(abi.encode(userId));
    }



    function _buySpeedLevelImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xBuySpeedLevel(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            Storage storage user = _storages[userId];
            uint levelPrice = _math.getSpeedPrice(user);
            require(IERC20(address(this)).balanceOf(user.account) >= levelPrice, "Not enough funds");

            _burn(userId, levelPrice);
            _promoteSpeedLevel(userId, levelPrice);
            emit Transfer(user.account, address(0), levelPrice);
        }
    }
    function xBuySpeedLevel(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _buySpeedLevelImplementation(encoded);
    }
    function buySpeedLevel(uint64 userId) public payable onlyStorageOwner(userId) {
        _payTransaction(userId);
        _buySpeedLevelImplementation(abi.encode(userId));
    }



    function _buyReferralLevelImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xBuyReferralLevel(bytes)", encoded);
        if (isProxy) {
            (uint64 userId) = abi.decode(encoded, (uint64));
            Storage storage user = _storages[userId];
            uint levelPrice = _math.getReferralPrice(user);
            require(IERC20(address(this)).balanceOf(user.account) >= levelPrice, "Not enough funds");

            _burn(userId, levelPrice);
            _promoteReferralLevel(userId, levelPrice);
            emit Transfer(user.account, address(0), levelPrice);
        }
    }
    function xBuyReferralLevel(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _buyReferralLevelImplementation(encoded);
    }
    function buyReferralLevel(uint64 userId) public payable onlyStorageOwner(userId) {
        _payTransaction(userId);
        _buyReferralLevelImplementation(abi.encode(userId));
    }



    /// Slots

    function equip(uint tokenId, uint8 slotTypeId, uint variantId) public payable {
        uint64 userId = _payableAuth.getAddressId(_msgSender());
        address account = _payableAuth.getIdAddress(userId);
        require(account == _msgSender(), "Not authorized");
        _payTransaction(userId);
        _slots.equip(tokenId, slotTypeId, variantId, userId);
    }

    function unequip(uint8 slotTypeId) public payable {
        uint64 userId = _payableAuth.getAddressId(_msgSender());
        address account = _payableAuth.getIdAddress(userId);
        require(account == _msgSender(), "Not authorized");
        _payTransaction(userId);
        _slots.unequip(slotTypeId, userId);
    }

}