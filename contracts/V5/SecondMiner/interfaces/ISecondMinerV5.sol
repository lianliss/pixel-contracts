//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/IProxy.sol";
import "contracts/V5/SecondMiner/SecondMinerV5Structs.sol";

interface IPXLSecondMinerV5 is IProxy {

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Claim(uint64 indexed id, address account, uint amount);
    event Burn(uint64 indexed id, address account, uint amount);
    event BurnTotalSupply(address account, uint amount);
    event ReferReward(uint64 indexed claimerId, uint64 indexed id, uint64 indexed child, address account, uint amount);
    event SetAccessController(address controller);
    event SetBurner(address controller);
    event SetClaimsDisabled(address caller, bool value);
    event SetRefersDisabled(address caller, bool value);
    event SetStorageDisabled(uint64 indexed id, address caller, bool value);
    event Reinvest(uint64 indexed id, address account, uint value);

}