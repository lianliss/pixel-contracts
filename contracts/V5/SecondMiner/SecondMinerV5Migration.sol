//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/SecondMiner/SecondMinerV5Context.sol";
import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";

struct OldStorage {
    uint claimTimestamp;
    uint claimed;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    bool disabled;
}

interface IOldSecondMiner {
    function getStorage(uint64 id) external view returns (
        OldStorage memory user,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint balance,
        uint coins
    );
}

contract SecondMinerV5Migration is SecondMinerV5Context, ProxyImplementation {

    mapping (uint64 userId => bool isMigrated) public migrated;
    IOldSecondMiner public _old;
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    constructor(
        address secondMinerAddress,
        address oldSecondMinerAddress,
        address coreAddress
        ) ProxyImplementation(secondMinerAddress) {
        _old = IOldSecondMiner(oldSecondMinerAddress);
        _core = IPXLsCoreV5(coreAddress);

        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }

    function _migrateStorage(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xMigrateStorage(bytes)", encoded);
        if (isProxy) {
            (uint64 userId, address oldAddress) = abi.decode(encoded, (uint64, address));
            IOldSecondMiner old = IOldSecondMiner(oldAddress);
            (
                OldStorage memory user,,,, uint balance,
            ) = old.getStorage(userId);

            _balances[userId] = balance;
            _users[userId] = SecondStorage(
                user.claimTimestamp,
                user.claimed,
                user.speedMultiplicator,
                user.sizeMultiplicator,
                user.speedAdder,
                user.sizeAdder,
                user.disabled
            );
        }
    }
    /// @notice Proxy entrance
    function xMigrateStorage(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _migrateStorage(encoded);
    }

    event UserSecondStorageMigrated(uint64 indexed userId);
    function _migrate(uint64 userId) private {
        (
            OldStorage memory user,,,,,
        ) = _old.getStorage(userId);

        if (user.claimTimestamp > 0) {
            _migrateStorage(abi.encode(userId, address(_old)));
        }

        migrated[userId] = true;
        emit UserSecondStorageMigrated(userId);
    }

    /// @notice public entrance
    /// @dev Prohibits to migrate twice
    function migrate() public {
        uint64 userId = _core.getAddressId(_msgSender());
        require (_core.getIdAddress(userId) == _msgSender(), "Unauthorized");
        require (!migrated[userId], "User already migrated to V5");
        _migrate(userId);
    }

    /// @notice public entrance
    /// @dev Allow to migrate twice (or mode times)
    function migrate(uint64 userId) public onlyRole(BACKEND_ROLE) {
        require (!migrated[userId], "User already migrated to V5");

        _migrate(userId);
    }

    /// @notice public entrance
    /// @dev Skips migrated users
    function migrate(uint64[] calldata userId) public onlyRole(BACKEND_ROLE) {
        for (uint i; i < userId.length; i++) {
            if (migrated[userId[i]]) continue;
            _migrate(userId[i]);
        }
    }

}