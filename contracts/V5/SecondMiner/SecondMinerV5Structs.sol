//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct SecondStorage {
    uint claimTimestamp;
    uint claimed;
    uint24 speedMultiplicator;
    uint24 sizeMultiplicator;
    uint256 speedAdder;
    uint256 sizeAdder;
    bool disabled;
}