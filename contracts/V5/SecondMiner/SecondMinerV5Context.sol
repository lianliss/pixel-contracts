//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/Proxy.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/SecondMiner/SecondMinerV5Structs.sol";

contract SecondMinerV5Context is Proxy {

    mapping(uint64 => uint) internal _balances;

    string internal _symbol;
    string internal _name;
    uint internal _totalSupply;
    IPXLsCoreV5 internal _core;

    uint internal constant SPEED_PARAMETER = 0;
    uint internal constant SIZE_PARAMETER = 1;

    uint internal _claimed;
    uint internal _burned;

    bool internal _claimsDisabled;
    bool internal _refersDisabled;

    uint internal _difficulty = 10;
    uint internal _burnRate = _PERCENT_PRECISION;

    uint8 public minSpeedLevel = 0;
    uint8 public minSizeLevel = 0;

    // Tokens per hour
    uint public baseSpeed = 2 * 10**15; // 0.002 PXLs
    uint public baseSize = 6 * _PRECISION;

    mapping (uint64 userId => SecondStorage) internal _users;
    uint8[2] internal _parameters;

    // Referral rewards to parents
    uint[] parentsPercents = [
        100, // 1% to first parent
        0 // 0% to second parent
    ];

}