//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/FloatMiner/interfaces/IFloatMiner.sol";
import "contracts/V5/FloatMiner/FloatMinerContext.sol";
import "contracts/V5/Slots/interfaces/IParameterReceiver.sol";

/// @title Float Pixel Shard miner
/// @author Danil Sakhinov
/// @notice Like ERC20 but transfers disabled
/// @notice Only Pixel Wallet users allowed
contract FloatMiner is FloatMinerContext, IFloatMiner, IParameterReceiver {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    bytes32 public constant CLAIMER_ROLE = keccak256("CLAIMER_ROLE");
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    constructor(
        string memory tokenSymbol,
        string memory tokenName,
        uint initialSupply,
        address coreAddress
        ) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(CLAIMER_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
        _grantRole(MULTIPLIER_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _totalSupply = initialSupply;
        _symbol = tokenSymbol;
        _name = tokenName;
        _core = IPXLsCoreV5(coreAddress);
    }

    function setCore(address coreAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV5(coreAddress);
    }

    function _getCoreStorage(uint64 id) internal view returns (Storage memory) {
        (
            Storage memory _storage,,,,,,,,
        ) = _core.getStorage(id);
        return _storage;
    }

    modifier onlyStorageOwner(uint64 id) {
        require(_getCoreStorage(id).account == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    function setBurnRate(uint burnRate) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _burnRate = burnRate;
    }

    function setActiveHolders(uint newValue) public onlyRole(BACKEND_ROLE) {
        holdersActive = newValue;
        emit ActiveHoldersSet(newValue);
    }

    function setUserKarmaMod(uint64 userId, uint24 multiplier) public onlyRole(BACKEND_ROLE) {
        _users[userId].karmaMultiplicator = multiplier;
        emit UserKarmaSet(userId, multiplier);
    }

    function _getHolders() internal view returns (uint) {
        return holdersCount == 0 ? 1 : holdersCount;
    }

    function _getActiveHolders() internal view returns (uint) {
        return holdersActive == 0 ? _getHolders() : holdersActive;
    }

    function getMiningCurrent(uint64 userId) public view returns (uint) {
        uint256 userKarma = uint256(_users[userId].karmaMultiplicator);
        uint256 karma = userKarma == 0 ? _PERCENT_PRECISION : userKarma;
        return baseSpeed * _getHolders() / _getActiveHolders() * karma / _PERCENT_PRECISION;
    }

    function _getSpeed(Storage memory coreUser, uint64 userId) internal view returns (uint) {
        FloatStorage storage user = _users[userId];
        uint24 mult = user.speedMultiplicator == 0
            ? uint24(_PERCENT_PRECISION)
            : user.speedMultiplicator;
        if (_parameters[SPEED_PARAMETER] == 0) {
            mult = coreUser.speedMultiplicator;
        }
        uint256 adder = _parameters[SPEED_PARAMETER] == 0
            ? coreUser.speedAdder
            : user.speedAdder;
        return (coreUser.speedLevel + 1) * getMiningCurrent(userId)
        * mult / _PERCENT_PRECISION
        / _HOUR
        + adder;
    }

    function _getSize(Storage memory coreUser, uint64 userId) internal view returns (uint) {
        FloatStorage storage user = _users[userId];
        uint24 mult = user.sizeMultiplicator == 0
            ? uint24(_PERCENT_PRECISION)
            : user.sizeMultiplicator;
        if (_parameters[SIZE_PARAMETER] == 0) {
            mult = coreUser.sizeMultiplicator;
        }
        uint256 adder = _parameters[SIZE_PARAMETER] == 0
            ? coreUser.sizeAdder
            : user.sizeAdder;
        return (coreUser.sizeLevel + 1) * baseSpeed
        * baseSize / _PRECISION
        * mult / _PERCENT_PRECISION
            + adder;
    }

    /// @notice Burn storage tokens
    /// @param userId Wallet ID
    /// @param value Tokens amount to burn
    function _burn(uint64 userId, uint256 value) internal {
        require(_balances[userId] >= value, "Can't burn more");
        uint burnValue = value * _burnRate / _PERCENT_PRECISION;
        uint reinvestValue = value - burnValue;
        _totalSupply -= burnValue;
        _balances[userId] -= value;
        _burned += burnValue;
        address userAddress = _core.getIdAddress(userId);
        emit Burn(userId, userAddress, burnValue);
        if (reinvestValue > 0) {
            emit Reinvest(userId, userAddress, reinvestValue);
        }
    }

    /// @notice The administrator can reset the storage
    /// @param userId Wallet ID
    function burnStorage(uint64 userId) public onlyRole(BURNER_ROLE) {
        Storage memory user = _getCoreStorage(userId);
        uint balance = _balances[userId];
        _burn(userId, balance);
        emit Transfer(user.account, address(0), balance);
    }

    /// @notice Burner contract can burn a specific amount of tokens from an account
    /// @param account Storage owner
    /// @param amount Tokens amount to burn
    function burn(address account, uint amount) public onlyRole(BURNER_ROLE) {
        uint64 id = _core.getAddressId(account);
        Storage memory coreUser = _getCoreStorage(id);
        require(_balances[id] >= amount, "Not enough funds");
        require(coreUser.account == account, "Address have no storage");
        _burn(id, amount);
        emit Transfer(account, address(0), amount);
    }

    /// @notice Transfer tokens to storage account
    /// @param id Wallet ID
    /// @param amount Tokens amount to claim
    function _claim(uint64 id, uint amount) internal {
        require(_claimed + amount <= _totalSupply, "Supply limit exceeded");
        _claimed += amount;
        _users[id].claimed += amount;
        _balances[id] += amount;
    }

    /// @notice ERC20 format
    function totalSupply() public view virtual returns (uint256) {
        return _totalSupply;
    }

    /// @notice ERC20 format
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /// @notice ERC20 format
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /// @notice ERC20 format
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /// @notice Gets account balance
    /// @param account Address linked to storage
    /// @return Tokens amount
    /// @dev Balances linked to Wallet ID only
    function balanceOf(address account) public view virtual returns (uint256) {
        uint64 id = _core.getAddressId(account);
        return _core.getIdAddress(id) == account
            ? _balances[id]
            : 0;
    }

    function _allowMining(uint64 userId) internal returns (bool) {
        FloatStorage storage user = _users[userId];
        require(!user.disabled, "This token mining is disabled for this user");
        if (user.claimTimestamp == 0) {
            Storage memory coreUser = _getCoreStorage(userId);
            require(coreUser.sizeLevel >= minSizeLevel, "Minimum storage level required");
            require(coreUser.speedLevel >= minSpeedLevel, "Minimum drill level required");
            user.claimTimestamp = block.timestamp;
            holdersCount++;
        }
        return true;
    }

    function _checkRequirements(uint64 userId) internal view returns (bool) {
        Storage memory coreUser = _getCoreStorage(userId);
        return coreUser.sizeLevel >= minSizeLevel
        && coreUser.speedLevel >= minSpeedLevel;
    }

    /// @notice Gets mined tokens since last claim
    /// @param userId Wallet ID
    /// @return Tokens amount
    function _getMinedTokens(uint64 userId) internal view returns (uint) {
        FloatStorage storage user = _users[userId];
        Storage memory coreUser = _getCoreStorage(userId);
        uint limit = _getSize(coreUser, userId);
        if (user.claimTimestamp == 0) {
            return 0;
        }
        uint time = block.timestamp - user.claimTimestamp;
        uint amount = _getSpeed(coreUser, userId)
            * time;
        return amount > limit && limit != 0
            ? limit
            : amount;
    }

    /// @notice Claim reward by storage
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function _claimReward(uint64 userId, uint additional) internal {
        require(!_claimsDisabled, "Claims disabled");
        uint mined = _getMinedTokens(userId) + additional;
        FloatStorage storage current = _users[userId];
        if (current.claimTimestamp == 0) {
            _allowMining(userId);
            return;
        }
        if (mined == 0) return;
        Storage memory coreUser = _getCoreStorage(userId);
        require(!coreUser.disabled, "Storage disabled");
        require(!current.disabled, "This mining is disabled for this user");
        _claim(userId, mined);
        current.claimTimestamp = block.timestamp;
        emit Claim(userId, coreUser.account, mined);
        emit Transfer(address(this), coreUser.account, mined);

        uint index = 0;
        uint64 previous = userId;
        while (index < parentsPercents.length
        && coreUser.parent != 0
            && !_refersDisabled) {
            uint64 receiver = coreUser.parent;
            coreUser = _getCoreStorage(receiver);
            uint reward = mined * parentsPercents[index] / _PERCENT_PRECISION;
            if (_claimed + reward <= _totalSupply
                && coreUser.account != address(0)
                && !_users[receiver].disabled) {
                _claim(receiver, reward);
                emit ReferReward(userId, receiver, previous, coreUser.account, reward);
            }
            index++;
            previous = receiver;
        }
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function claimReward(uint64 userId) public onlyStorageOwner(userId) {
        _claimReward(userId, 0);
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function claimFor(uint64 userId) public onlyRole(CLAIMER_ROLE) {
        _claimReward(userId, 0);
    }

    /// @notice Mint amount of tokens to storage and distribute it to refers
    /// @param userId Wallet ID
    /// @param amount Amount of tokens to mint
    /// @dev Calls claim
    function mintWithClaim(uint64 userId, uint amount) public onlyRole(MINTER_ROLE) {
        _claimReward(userId, amount);
    }

    /// @notice Mint amount of tokens only for one account
    /// @param userId Wallet ID
    /// @param amount Amount of tokens to mint
    function mintForOne(uint64 userId, uint amount) public onlyRole(MINTER_ROLE) {
        _claim(userId, amount);
        emit Transfer(address(this), _core.getIdAddress(userId), amount);
    }

    /// @notice Returns total claimed amount by storages
    /// @return Claimed tokens amount
    function totalClaimed() public view virtual returns (uint) {
        return _claimed;
    }

    /// @notice Returns total burned amount by storages
    /// @return Burned tokens amount
    function totalBurned() public view virtual returns (uint) {
        return _burned;
    }

    /// @notice Contract owner can disable claims
    function disableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = true;
        emit SetClaimsDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable claims again
    function enableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = false;
        emit SetClaimsDisabled(_msgSender(), false);
    }

    /// @notice Contract owner can disable parents rewards
    function disableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = true;
        emit SetRefersDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable parents rewards again
    function enableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = false;
        emit SetRefersDisabled(_msgSender(), false);
    }

    /// @notice Contract owner set base tokens per hour value
    function setBaseSpeed(uint speed) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSpeed = speed;
    }

    function setBaseSize(uint size) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSize = size;
    }

    function setDiffuculty(uint value) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _difficulty = value;
    }

    function setMinLevels(uint8 minSpeed, uint8 minSize) public onlyRole(DEFAULT_ADMIN_ROLE) {
        minSpeedLevel = minSpeed;
        minSizeLevel = minSize;
    }

    /// @notice Owner can set a new parents percents
    /// @param _percents Array of percents
    function setParentsPercents(uint[2] calldata _percents) public onlyRole(DEFAULT_ADMIN_ROLE) {
        parentsPercents = _percents;
    }

    /// @notice Storage info getter
    /// @param userId Wallet ID
    function getStorage(uint64 userId) public view returns (
        FloatStorage memory user,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint balance,
        uint coins
    ) {
        FloatStorage storage _user = _users[userId];
        Storage memory coreUser = _getCoreStorage(userId);
        user = _user;
        mined = _getMinedTokens(userId);
        sizeLimit = _getSize(coreUser, userId);
        rewardPerSecond = _getSpeed(coreUser, userId);
        balance = _balances[userId];
        coins = coreUser.account.balance;
    }

    /// @notice Admins can burn amount tokens from total supply
    /// @param amount Amount of tokens to burn
    function burnTotalSupply(uint amount) public onlyRole(BURNER_ROLE) {
        _totalSupply -= amount;
        emit BurnTotalSupply(_msgSender(), amount);
    }

    function setParameters(uint8[2] calldata parameters) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _parameters = parameters;
    }

    function updateParameter(uint64 userId, uint8 parameterId, uint24 multiplier, uint256 adder) public onlyRole(MULTIPLIER_ROLE) {
        FloatStorage storage user = _users[userId];
        if (_checkRequirements(userId)) {
            _claimReward(userId, 0);
        }
        if (parameterId == _parameters[SPEED_PARAMETER]) {
            user.speedMultiplicator = multiplier;
            user.speedAdder = adder;
        } else if (parameterId == _parameters[SIZE_PARAMETER]) {
            user.sizeMultiplicator = multiplier;
            user.sizeAdder = adder;
        }
    }

    function disableMining(uint64 userId) public onlyRole(MULTIPLIER_ROLE) {
        FloatStorage storage user = _users[userId];
        user.disabled = true;
        user.claimTimestamp = 0;
    }

    function enableMining(uint64 userId) public onlyRole(MULTIPLIER_ROLE) {
        FloatStorage storage user = _users[userId];
        user.disabled = false;
        user.claimTimestamp = block.timestamp;
    }

}