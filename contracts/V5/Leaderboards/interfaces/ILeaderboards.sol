// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "contracts/Proxy/IProxy.sol";
import "contracts/V5/Leaderboards/LeaderboardsStructs.sol";

interface ILeaderboards is IProxy {

    event BoardAdded(uint indexed boardId);
    event BoardRemoved(uint indexed boardId);
    event BoardLimitUpdated(uint indexed boardId, uint limit);
    event RewardSent(uint indexed boardId, uint64 indexed userId, uint amount);

    function addBoard() external returns (uint boardId);
    function addBoard(uint boardId) external;
    function setBoardLimit(uint boardId, uint limit) external;
    function addBoardWithLimit(uint limit) external;
    function addBoardWithLimit(uint boardId, uint limit) external;
    function removeBoard(uint boardId) external;

    function getBoards() external view returns (uint[] memory);
    function getLimits() external view returns (LimitsLeft[] memory);

    function send(uint boardId, uint64 userId, uint amount) external;
    function send(uint boardId, address to, uint amount) external;
    function sendBatch(uint boardId, uint64[] calldata userId, uint[] calldata amount)
    external
    returns (BatchSendResult[] memory);
    function sendBatch(uint boardId, address[] calldata to, uint[] calldata amount)
    external
    returns (BatchSendResult[] memory);

}