// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

uint8 constant NO_ERROR = 0;
uint8 constant ERROR_ALREADY_RECEIVED = 1;
uint8 constant ERROR_LIMIT = 2;

struct BatchSendResult {
    uint64 userId;
    uint amount;
    bool isAlreadyReceivedError;
    bool isBoardLimitError;
    bool isSent;
}

struct LimitsLeft {
    uint boardId;
    uint limit;
}