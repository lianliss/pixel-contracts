// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/Proxy/Proxy.sol";
import "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import "contracts/V5/Leaderboards/LeaderboardsStructs.sol";

contract LeaderboardsContext is Proxy {

    IPXLsCoreV5 internal _core;

    EnumerableSet.UintSet internal _boards;
    uint internal _boardsCounter;
    mapping(uint boardId => uint limit) internal _limits;
    mapping(uint boardId => mapping(uint userId => bool isReceived)) public isUserReceived;

}