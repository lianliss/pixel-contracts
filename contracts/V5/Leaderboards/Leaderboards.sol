// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "contracts/V5/Leaderboards/LeaderboardsContext.sol";
import "contracts/V5/Leaderboards/interfaces/ILeaderboards.sol";

/// @dev Require IPXLsCoreV5 role MINTER_ROLE
contract Leaderboards is LeaderboardsContext, ILeaderboards {

    using EnumerableSet for EnumerableSet.UintSet;

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");
    bytes32 public constant EDITOR_ROLE = keccak256("EDITOR_ROLE");

    constructor(address coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, _msgSender());
        _grantRole(EDITOR_ROLE, address(this));

        _core = IPXLsCoreV5(coreAddress);
    }

    function setCoreAddress(address coreAddress)
    public
    onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = IPXLsCoreV5(coreAddress);
    }

    function _addBoard(uint boardId)
    private {
        require(_boards.add(boardId), "Adding failed");
        emit BoardAdded(boardId);
    }

    function addBoard(uint boardId)
    public
    onlyRole(EDITOR_ROLE) {
        if (boardId >= _boardsCounter) {
            _boardsCounter = boardId + 1;
        }
        _addBoard(boardId);
    }

    function addBoard() public onlyRole(EDITOR_ROLE)
    returns (uint boardId) {
        _addBoard(_boardsCounter++);
        return _boardsCounter;
    }

    function setBoardLimit(uint boardId, uint limit)
    public
    onlyRole(EDITOR_ROLE) {
        _limits[boardId] = limit;
        emit BoardLimitUpdated(boardId, limit);
    }

    function addBoardWithLimit(uint limit)
    public
    onlyRole(EDITOR_ROLE) {
        uint boardId = addBoard();
        setBoardLimit(boardId, limit);
    }

    function addBoardWithLimit(uint boardId, uint limit)
    public
    onlyRole(EDITOR_ROLE) {
        addBoard(boardId);
        setBoardLimit(boardId, limit);
    }

    function removeBoard(uint boardId)
    public
    onlyRole(EDITOR_ROLE) {
        require(_boards.remove(boardId), "Removing failed");
        emit BoardRemoved(boardId);
    }

    function getBoards()
    public view
    returns (uint[] memory) {
        return _boards.values();
    }

    function getLimits()
    public view
    returns (LimitsLeft[] memory) {
        LimitsLeft[] memory result = new LimitsLeft[](_boards.length());
        for (uint i; i < result.length; i++) {
            result[i].boardId = _boards.at(i);
            result[i].limit = _limits[i];
        }
        return result;
    }

    function _sendReward(uint boardId, uint64 userId, uint amount)
    private
    returns (uint8 errorCode) {
        if (isUserReceived[boardId][userId]) {
            return ERROR_ALREADY_RECEIVED;
        }
        if (_limits[boardId] > amount) {
            return ERROR_LIMIT;
        }
        _core.mintForOne(userId, amount);
        isUserReceived[boardId][userId] = true;
        emit RewardSent(boardId, userId, amount);
        return 0;
    }

    function send(uint boardId, uint64 userId, uint amount)
    public
    onlyRole(BACKEND_ROLE) {
        uint8 errorCode = _sendReward(boardId, userId, amount);
        require (errorCode != ERROR_ALREADY_RECEIVED, "User already received this reward");
        require (errorCode != ERROR_LIMIT, "Board limit reached");
    }

    function send(uint boardId, address to, uint amount)
    public
    onlyRole(BACKEND_ROLE) {
        uint8 errorCode = _sendReward(boardId, _core.getAddressId(to), amount);
        require (errorCode != ERROR_ALREADY_RECEIVED, "User already received this reward");
        require (errorCode != ERROR_LIMIT, "Board limit reached");
    }

    function sendBatch(uint boardId, uint64[] calldata userId, uint[] calldata amount)
    public
    onlyRole(BACKEND_ROLE) 
    returns (BatchSendResult[] memory) {
        require(userId.length == amount.length, "Users and amounts lengths does not match");
        BatchSendResult[] memory result = new BatchSendResult[](userId.length);

        for (uint i; i < userId.length; i++) {
            uint8 errorCode = _sendReward(boardId, userId[i], amount[i]);
            result[i].userId = userId[i];
            result[i].amount = amount[i];
            result[i].isAlreadyReceivedError = errorCode == ERROR_ALREADY_RECEIVED;
            result[i].isBoardLimitError = errorCode == ERROR_LIMIT;
            result[i].isSent = errorCode == NO_ERROR;
        }
        return result;
    }

    function sendBatch(uint boardId, address[] calldata to, uint[] calldata amount)
    public
    onlyRole(BACKEND_ROLE) 
    returns (BatchSendResult[] memory) {
        require(to.length == amount.length, "Users and amounts lengths does not match");
        BatchSendResult[] memory result = new BatchSendResult[](to.length);

        for (uint i; i < to.length; i++) {
            uint64 userId = _core.getAddressId(to[i]);
            uint8 errorCode = _sendReward(boardId, userId, amount[i]);
            result[i].userId = userId;
            result[i].amount = amount[i];
            result[i].isAlreadyReceivedError = errorCode == ERROR_ALREADY_RECEIVED;
            result[i].isBoardLimitError = errorCode == ERROR_LIMIT;
            result[i].isSent = errorCode == NO_ERROR;
        }
        return result;
    }

}