// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

uint constant _PERCENT_DECIMALS = 4;
uint constant _PERCENT_PRECISION = 10**_PERCENT_DECIMALS;



struct Order {
    // Order ID
    bytes32 id;
    // Owner of the NFT
    address seller;
    // NFT registry address
    address nftAddress;
    // Price (in wei) for the published item
    uint256 price;
    // Time when this sale ends
    uint256 expiresAt;
}

struct Offer {
    // Offer ID
    bytes32 id;
    // Buyer address
    address buyer;
    // NFT registry address
    address nftAddress;
    // Price (in wei)
    uint256 price;
    // Time when this offer ends
    uint256 expiresAt;
}