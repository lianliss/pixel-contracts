// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "contracts/V5/Marketplace/MarketV5Structs.sol";
import "contracts/Proxy/IProxy.sol";

interface IMarketplaceV5 is IProxy {
    
    event OrderCreated(
        bytes32 id,
        uint256 indexed assetId,
        address indexed seller,
        address nftAddress,
        uint256 priceInWei,
        uint256 expiresAt
    );
    event OrderSuccessful(
        bytes32 id,
        uint256 indexed assetId,
        address indexed seller,
        address nftAddress,
        uint256 totalPrice,
        address indexed buyer
    );
    event OrderCancelled(bytes32 id, uint256 indexed assetId, address indexed seller, address nftAddress);

    event OfferCreated(
        bytes32 id,
        uint256 indexed assetId,
        address indexed buyer,
        address nftAddress,
        uint256 priceInWei,
        uint256 expiresAt
    );
    event OfferSuccessful(
        bytes32 id,
        uint256 indexed assetId,
        address indexed buyer,
        address nftAddress,
        uint256 totalPrice,
        address indexed seller
    );
    event OfferCancelled(bytes32 id, uint256 indexed assetId, address indexed buyer, address nftAddress);

    event ChangedPublicationFee(uint256 publicationFee);
    event ChangedOrderFee(uint256 orderFee);
    event FeesCollectorSet(address indexed oldFeesCollector, address indexed newFeesCollector);
    event NftCollectionAdded(address indexed nftAddress);
    event NftCollectionRemoved(address indexed nftAddress);

    event UpdateUserOrderFeeMultiplier(uint64 indexed userId, uint24 multiplier);
    event UpdateUserPublicationFeeMultiplier(uint64 indexed userId, uint24 multiplier);

}