// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "contracts/V5/Marketplace/MarketV5Context.sol";
import "contracts/V5/Marketplace/interfaces/IMarketplaceV5.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Slots/interfaces/IParameterReceiver.sol";

contract MarketplaceV5 is MarketV5Context, IMarketplaceV5, IParameterReceiver, Pausable {

    using Address for address;
    using EnumerableSet for EnumerableSet.AddressSet;

    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    
    /**
     * @dev Initialize this contract. Acts as a constructor
     * @param _acceptedToken - Address of the ERC20 accepted for this marketplace
     * @param _orderFee - fees collector with 4 digits of precision
     */
    constructor(
        address coreAddress,
        address _acceptedToken,
        uint256 _orderFee
    ) {

        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());

        core = ICoreV5Auth(coreAddress);
        acceptedToken = IERC20(_acceptedToken);
        setFeesCollector(_msgSender());
        setOrderFee(_orderFee);
    }

    /**
     * @dev Sets the publication fee that's charged to users to publish items
     * @param _publicationFee - Fee amount with 4 digits of precision this contract charges to publish an item
     */
    function setPublicationFee(uint256 _publicationFee) external onlyRole(DEFAULT_ADMIN_ROLE) {
        publicationFee = _publicationFee;
        emit ChangedPublicationFee(publicationFee);
    }

    function setErc20Token(address erc20Address) external onlyRole(DEFAULT_ADMIN_ROLE) {
        acceptedToken = IERC20(erc20Address);
    }

    /**
     * @dev Sets the share cut for the fees collector of the contract that's
     *  charged to the seller on a successful sale
     * @param newOrderFee - fees for the collector
     */
    function setOrderFee(uint256 newOrderFee) public onlyRole(DEFAULT_ADMIN_ROLE) {
        require(
            newOrderFee <= _PERCENT_PRECISION,
            "Marketplace#setFeesCollectorCutPerMillion: FEES_MUST_BE_BETWEEN_0_AND_10000"
        );

        orderFee = newOrderFee;
        emit ChangedOrderFee(newOrderFee);
    }

    /**
     * @notice Set the fees collector
     * @param _newFeesCollector - fees collector
     */
    function setFeesCollector(address _newFeesCollector) public onlyRole(DEFAULT_ADMIN_ROLE) {
        require(_newFeesCollector != address(0), "Marketplace#setFeesCollector: INVALID_FEES_COLLECTOR");

        emit FeesCollectorSet(feesCollector, _newFeesCollector);
        feesCollector = _newFeesCollector;
    }

    /**
     * @notice Add new nft collection
     * @param nftCollection - nft address
     */
    function addNftCollection(address nftCollection) public onlyRole(DEFAULT_ADMIN_ROLE) {
        require(_nftCollectionList.add(nftCollection), "Marketplace#addNftCollection: INVALID_NFT_ADDRESS");
        emit NftCollectionAdded(nftCollection);
    }

    /**
     * @notice Add new nft collection
     * @param nftCollection - nft address
     */
    function removeNftCollection(address nftCollection) public onlyRole(DEFAULT_ADMIN_ROLE) {
        require(_nftCollectionList.remove(nftCollection), "Marketplace#removeNftCollection: INVALID_NFT_ADDRESS");
        emit NftCollectionRemoved(nftCollection);
    }

    function nftCollectionList() public view returns (address[] memory) {
        return _nftCollectionList.values();
    }

    /**
     * @dev Creates a new order
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order (in hours)
     */
    function createOrder(
        address nftAddress,
        uint256 assetId,
        uint256 priceInWei,
        uint256 expiresAt
    ) public payable whenNotPaused {
        _createOrder(nftAddress, assetId, priceInWei, expiresAt);
    }

    /**
     * @dev Cancel an already published order
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function cancelOrder(address nftAddress, uint256 assetId) public whenNotPaused {
        _cancelOrder(nftAddress, assetId);
    }

    /**
     * @dev Executes the sale for a published NFT
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     * @param price - Order price
     */
    function executeOrder(address nftAddress, uint256 assetId, uint256 price) public payable whenNotPaused {
        if (address(acceptedToken) == address(0)) price = msg.value;
        _executeOrder(nftAddress, assetId, price);
    }

    function _getAccountFee(address account, uint feeParameterIndex) private view returns (uint) {
        uint fee = feeParameterIndex == 0
            ? orderFee
            : publicationFee;
        uint64 userId = core.getAddressId(account);
        if (userId > 0 && _parameters[feeParameterIndex] > 0) {
            uint mult = uint(userMultiplicators[userId][feeParameterIndex]);
            if (mult > 0) {
                fee = fee * mult / _PERCENT_PRECISION;
            }
        }
        return fee;
    }

    /**
     * @dev Creates a new order
     * @param nftAddress - Non fungible registry address
     * @param assetId - ID of the published NFT
     * @param priceInWei - Price in Wei for the supported coin
     * @param expiresAt - Duration of the order
     */
    function _createOrder(address nftAddress, uint256 assetId, uint256 priceInWei, uint256 expiresAt) internal {
        _requireNFT(nftAddress);

        address sender = _msgSender();

        IPXLsNFTV5 nftRegistry = IPXLsNFTV5(nftAddress);
        address assetOwner = nftRegistry.ownerOf(assetId);

        try nftRegistry.getTokenType(assetId) returns (ItemType memory itemType) {
            require(!itemType.soulbound, "Marketplace#_createOrder: SOULBOUND_ITEM");
        } catch (bytes memory) {}

        require(sender == assetOwner, "Marketplace#_createOrder: NOT_ASSET_OWNER");
        require(priceInWei > 0, "Price should be bigger than 0");
        require(expiresAt > block.timestamp + 1 minutes, "Marketplace#_createOrder: INVALID_EXPIRES_AT");

        bytes32 orderId = keccak256(abi.encodePacked(block.timestamp, assetOwner, assetId, nftAddress, priceInWei));

        orderByAssetId[nftAddress][assetId] = Order({
            id: orderId,
            seller: assetOwner,
            nftAddress: nftAddress,
            price: priceInWei,
            expiresAt: expiresAt
        });

        // Check if there's a publication fee and
        // transfer the amount to marketplace owner
        if (publicationFee > 0) {
            _transferFund(
                sender,
                feesCollector,
                priceInWei * _getAccountFee(sender, PUBLICATION_PARAMETER) / _PERCENT_PRECISION,
                "Marketplace#_createOrder: TRANSFER_FEE_FAILED"
            );
        }

        // Transfer asset
        nftRegistry.transferFrom(sender, address(this), assetId);

        emit OrderCreated(orderId, assetId, assetOwner, nftAddress, priceInWei, expiresAt);
    }

    /**
     * @dev Cancel an already published order
     *  can only be canceled by seller or the contract owner
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     */
    function _cancelOrder(address nftAddress, uint256 assetId) internal {
        address sender = _msgSender();
        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_cancelOrder: INVALID_ORDER");
        require(order.seller == sender
        || hasRole(DEFAULT_ADMIN_ROLE, sender), "Marketplace#_cancelOrder: UNAUTHORIZED_USER");

        // Transfer asset
        IPXLsNFTV5 nftRegistry = IPXLsNFTV5(nftAddress);
        nftRegistry.transferFrom(address(this), sender, assetId);

        bytes32 orderId = order.id;
        address orderSeller = order.seller;
        address orderNftAddress = order.nftAddress;
        delete orderByAssetId[nftAddress][assetId];

        emit OrderCancelled(orderId, assetId, orderSeller, orderNftAddress);
    }

    /**
     * @dev Executes the sale for a published NFT
     * @param nftAddress - Address of the NFT registry
     * @param assetId - ID of the published NFT
     * @param price - Order price
     */
    function _executeOrder(address nftAddress, uint256 assetId, uint256 price) internal {
        address sender = _msgSender();

        IPXLsNFTV5 nftRegistry = IPXLsNFTV5(nftAddress);

        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_executeOrder: ASSET_NOT_FOR_SALE");

        require(order.seller != address(0), "Marketplace#_executeOrder: INVALID_SELLER");
        require(order.seller != sender, "Marketplace#_executeOrder: SENDER_IS_SELLER");
        require(order.price == price, "Marketplace#_executeOrder: PRICE_MISMATCH");
        require(block.timestamp < order.expiresAt, "Marketplace#_executeOrder: ORDER_EXPIRED");

        delete orderByAssetId[nftAddress][assetId];

        uint fee = _getAccountFee(sender, ORDER_FEE_PARAMETER);
        uint256 feesCollectorShareAmount = (price * fee) / _PERCENT_PRECISION;

        // Fees collector share
        {
            if (feesCollectorShareAmount > 0) {
                _transferFund(
                    sender,
                    feesCollector,
                    feesCollectorShareAmount,
                    "Marketplace#_executeOrder: TRANSFER_FEES_TO_FEES_COLLECTOR_FAILED"
                );
            }
        }

        // Transfer sale amount to seller
        _transferFund(
            sender,
            order.seller,
            price - feesCollectorShareAmount,
            "Marketplace#_executeOrder: TRANSFER_AMOUNT_TO_SELLER_FAILED"
        );

        // Transfer asset owner
        nftRegistry.transferFrom(address(this), sender, assetId);

        emit OrderSuccessful(order.id, assetId, order.seller, nftAddress, price, sender);
    }

    function _requireNFT(address nftAddress) internal view {
        require(_nftCollectionList.contains(nftAddress), "Marketplace#_requireNFT: INVALID_NFT_ADDRESS");
    }

    function _transferFund(address from, address to, uint256 amount, string memory errMsg) internal {
        if (address(acceptedToken) == address(0)) {
            if (to == address(this)) {
                require(msg.value == amount, errMsg);
            } else {
                (bool sent, ) = payable(to).call{ value: amount }("");
                require(sent, errMsg);
            }
        } else {
            require(acceptedToken.transferFrom(from, to, amount), errMsg);
        }
    }

    function updateParameter(uint64 userId, uint8 parameterId, uint24 multiplier, uint256) public onlyRole(MULTIPLIER_ROLE) {
        if (parameterId == _parameters[ORDER_FEE_PARAMETER]) {
            userMultiplicators[userId][ORDER_FEE_PARAMETER] = multiplier;
            emit UpdateUserOrderFeeMultiplier(userId, multiplier);
        } else if (parameterId == _parameters[PUBLICATION_PARAMETER]) {
            userMultiplicators[userId][PUBLICATION_PARAMETER] = multiplier;
            emit UpdateUserPublicationFeeMultiplier(userId, multiplier);
        }
    }

    function setCoreAddress(address newCoreAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        core = ICoreV5Auth(newCoreAddress);
    }
}
