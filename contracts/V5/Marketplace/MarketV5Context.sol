// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "contracts/Proxy/Proxy.sol";
import "contracts/V5/Marketplace/MarketV5Structs.sol";
import "contracts/V5/Core/interfaces/ICoreV5Auth.sol";

contract MarketV5Context is Proxy {
    
    using Address for address;
    using EnumerableSet for EnumerableSet.AddressSet;

    IERC20 public acceptedToken;
    ICoreV5Auth public core;
    EnumerableSet.AddressSet internal _nftCollectionList;

    // From ERC721 registry assetId to Order (to avoid asset collision)
    mapping(address => mapping(uint256 => Order)) public orderByAssetId;
    // From ERC721 registry assetId to Offer (to avoid asset collision)
    mapping(address => mapping(uint256 => Offer)) public offerByAssetId;

    address public feesCollector;

    uint256 public orderFee;
    uint256 public publicationFee;

    uint8[2] internal _parameters; /// orderFee and publicationFee parameters
    mapping(uint64 userId => uint24[2] mult) public userMultiplicators;

    uint internal constant ORDER_FEE_PARAMETER = 0;
    uint internal constant PUBLICATION_PARAMETER = 1;

}