// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "contracts/V5/Marketplace/MarketV5Context.sol";
import "contracts/V5/Marketplace/interfaces/IMarketplaceV5.sol";
import "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import "contracts/V5/Slots/interfaces/IParameterReceiver.sol";
import "contracts/Proxy/ProxyImplementation.sol";

interface IMarket {
    function orderByAssetId(address, uint256) external view returns (Order memory);
}

contract MarketplaceV5Wrap is ProxyImplementation, MarketV5Context, IMarketplaceV5, Pausable {

    using Address for address;
    using EnumerableSet for EnumerableSet.AddressSet;

    bytes32 public constant MULTIPLIER_ROLE = keccak256("MULTIPLIER_ROLE");
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");
    IMarket private _market;
    mapping(uint64 userId => uint extraMod) public extraMod;

    constructor(
        address coreAddress,
        address _acceptedToken,
        uint256,
        address _marketAddress
    ) ProxyImplementation(_marketAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());

        core = ICoreV5Auth(coreAddress);
        acceptedToken = IERC20(_acceptedToken);
        _market = IMarket(_marketAddress);
        setFeesCollector(_msgSender());
    }

    function setExtraMod(uint64 userId, uint multiplier) public onlyRole(BACKEND_ROLE) {
        extraMod[userId] = multiplier;
    }

    function setFeesCollector(address _newFeesCollector) public onlyRole(DEFAULT_ADMIN_ROLE) {
        feesCollector = _newFeesCollector;
    }

    function _getAccountFee(address account, uint feeParameterIndex, uint extra) private view returns (uint) {
        uint fee = feeParameterIndex == 0
            ? orderFee
            : publicationFee;
        uint64 userId = core.getAddressId(account);
        if (userId > 0 && _parameters[feeParameterIndex] > 0) {
            uint mult = uint(userMultiplicators[userId][feeParameterIndex]);
            if (mult > 0) {
                fee = fee * mult / _PERCENT_PRECISION;
            }
        }
        if (extra > 0) {
            fee = fee * extra / _PERCENT_PRECISION;
        }
        return fee;
    }

    function _createOrder(address sender, address nftAddress, uint256 assetId, uint256 priceInWei, uint256 expiresAt)
    internal 
    returns (uint256)
    {
        _requireNFT(nftAddress);

        uint256 feeSize = 0;

        IPXLsNFTV5 nftRegistry = IPXLsNFTV5(nftAddress);
        address assetOwner = nftRegistry.ownerOf(assetId);

        try nftRegistry.getTokenType(assetId) returns (ItemType memory itemType) {
            require(!itemType.soulbound, "Marketplace#_createOrder: SOULBOUND_ITEM");
        } catch (bytes memory) {}

        require(sender == assetOwner, "Marketplace#_createOrder: NOT_ASSET_OWNER");
        require(priceInWei > 0, "Price should be bigger than 0");
        require(expiresAt > block.timestamp + 1 minutes, "Marketplace#_createOrder: INVALID_EXPIRES_AT");

        bytes32 orderId = keccak256(abi.encodePacked(block.timestamp, assetOwner, assetId, nftAddress, priceInWei));

        orderByAssetId[nftAddress][assetId] = Order({
            id: orderId,
            seller: assetOwner,
            nftAddress: nftAddress,
            price: priceInWei,
            expiresAt: expiresAt
        });

        // Check if there's a publication fee and
        // transfer the amount to marketplace owner
        if (publicationFee > 0) {
            feeSize = priceInWei * _getAccountFee(sender, PUBLICATION_PARAMETER, 0) / _PERCENT_PRECISION;
        }

        // Transfer asset
        nftRegistry.transferFrom(sender, address(this), assetId);

        emit OrderCreated(orderId, assetId, assetOwner, nftAddress, priceInWei, expiresAt);
        return feeSize;
    }

    function _cancelOrder(address sender, address nftAddress, uint256 assetId) internal {
        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_cancelOrder: INVALID_ORDER");
        require(order.seller == sender
        || hasRole(DEFAULT_ADMIN_ROLE, sender), "Marketplace#_cancelOrder: UNAUTHORIZED_USER");

        // Transfer asset
        IPXLsNFTV5 nftRegistry = IPXLsNFTV5(nftAddress);
        nftRegistry.transferFrom(address(this), sender, assetId);

        bytes32 orderId = order.id;
        address orderSeller = order.seller;
        address orderNftAddress = order.nftAddress;
        delete orderByAssetId[nftAddress][assetId];

        emit OrderCancelled(orderId, assetId, orderSeller, orderNftAddress);
    }

    function _executeOrder(address sender, address nftAddress, uint256 assetId, uint256 price, uint256 extra)
    internal
    returns (address seller, uint256 amountToSend, uint256 feeToSend)
    {

        IPXLsNFTV5 nftRegistry = IPXLsNFTV5(nftAddress);

        Order memory order = orderByAssetId[nftAddress][assetId];

        require(order.id != 0, "Marketplace#_executeOrder: ASSET_NOT_FOR_SALE");

        require(order.seller != address(0), "Marketplace#_executeOrder: INVALID_SELLER");
        require(order.seller != sender, "Marketplace#_executeOrder: SENDER_IS_SELLER");
        require(order.price == price, "Marketplace#_executeOrder: PRICE_MISMATCH");
        require(block.timestamp < order.expiresAt, "Marketplace#_executeOrder: ORDER_EXPIRED");

        delete orderByAssetId[nftAddress][assetId];
        seller = order.seller;

        {
            uint fee = _getAccountFee(order.seller, ORDER_FEE_PARAMETER, extra);
            feeToSend = (price * fee) / _PERCENT_PRECISION;
            if (feeToSend > 0) {
                amountToSend = price - feeToSend;
            } else {
                amountToSend = price;
            }
            /// Transfers will be executed in Proxy contract
        }

        // Transfer asset owner
        nftRegistry.transferFrom(address(this), sender, assetId);

        emit OrderSuccessful(order.id, assetId, order.seller, nftAddress, price, sender);
    }

    function _requireNFT(address nftAddress) internal view {
        require(_nftCollectionList.contains(nftAddress), "Marketplace#_requireNFT: INVALID_NFT_ADDRESS");
    }

    function _transferFund(address from, address to, uint256 amount, string memory errMsg) internal {
        if (address(acceptedToken) == address(0)) {
            if (to == address(this)) {
                require(msg.value == amount, errMsg);
            } else {
                (bool sent, ) = payable(to).call{ value: amount }("");
                require(sent, errMsg);
            }
        } else {
            require(acceptedToken.transferFrom(from, to, amount), errMsg);
        }
    }




    function _createOrderImplementation(bytes memory encoded) internal returns(bytes memory) {
        (bool isProxy, bytes memory ctxResult) = routedDelegate("xCreateOrder(bytes)", encoded);
        if (isProxy) {
            (
                address sender,
                address nftAddress,
                uint256 assetId,
                uint256 priceInWei,
                uint256 expiresAt
            ) = abi.decode(encoded, (address, address, uint256, uint256, uint256));
            uint feeSize = _createOrder(sender, nftAddress, assetId, priceInWei, expiresAt);
            return abi.encode(feeSize);
        } else {
            return ctxResult;
        }
    }
    function xCreateOrder(bytes memory encoded) public onlyRole(PROXY_ROLE) returns(bytes memory) {
        return _createOrderImplementation(encoded);
    }
    function createOrder(
        address nftAddress,
        uint256 assetId,
        uint256 priceInWei,
        uint256 expiresAt
    ) public payable whenNotPaused {

        /// Execute by Context contract
        address sender = _msgSender();
        bytes memory ctxResult = _createOrderImplementation(abi.encode(sender, nftAddress, assetId, priceInWei, expiresAt));
        (,,uint256 feeSize) = abi.decode(ctxResult, (uint256,uint256,uint256));

        /// Make transfer by Proxy contract
        if (feeSize > 0) {
            _transferFund(
                _msgSender(),
                feesCollector,
                feeSize,
                "Marketplace#_createOrder: TRANSFER_FEE_FAILED"
            );
        }
    }


    function _cancelOrderImplementation(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xCancelOrder(bytes)", encoded);
        if (isProxy) {
            (
                address sender,
                address nftAddress,
                uint256 assetId
            ) = abi.decode(encoded, (address, address, uint256));
            _cancelOrder(sender, nftAddress, assetId);
        }
    }
    function xCancelOrder(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _cancelOrderImplementation(encoded);
    }
    function cancelOrder(address nftAddress, uint256 assetId) public whenNotPaused {
        address sender = _msgSender();
        _cancelOrderImplementation(abi.encode(sender, nftAddress, assetId));
    }



    function _executeOrderImplementation(bytes memory encoded) internal returns(bytes memory) {
        (bool isProxy, bytes memory ctxResult) = routedDelegate("xExecuteOrder(bytes)", encoded);
        if (isProxy) {
            (   
                address sender,
                address nftAddress,
                uint256 assetId,
                uint256 price,
                uint256 extra
            ) = abi.decode(encoded, (address, address, uint256, uint256, uint256));
            (address seller, uint amountToSend, uint feeToSend) = _executeOrder(sender, nftAddress, assetId, price, extra);
            return abi.encode(seller, amountToSend, feeToSend);
        } else {
            return ctxResult;
        }
    }
    function xExecuteOrder(bytes memory encoded) public onlyRole(PROXY_ROLE) returns(bytes memory) {
        return _executeOrderImplementation(encoded);
    }
    function executeOrder(address nftAddress, uint256 assetId, uint256 price) public payable whenNotPaused {
        address sender = _msgSender();
        if (address(acceptedToken) == address(0)) price = msg.value;
        uint64 sellerUserId = core.getAddressId(_market.orderByAssetId(nftAddress, assetId).seller);
        uint extra = extraMod[sellerUserId];

        /// Execute by Context contract
        bytes memory ctxResult = _executeOrderImplementation(abi.encode(sender, nftAddress, assetId, price, extra));
        (,,address seller, uint256 amountToSend, uint256 feeToSend) = abi.decode(ctxResult, (uint256,uint256,address,uint256,uint256));

        /// Make transfers by Proxy contract
        if (amountToSend > 0) {
            _transferFund(
                _msgSender(),
                seller,
                amountToSend,
                "Marketplace#_executeOrder: TRANSFER_AMOUNT_TO_SELLER_FAILED"
            );
        }
        if (feeToSend > 0) {
            _transferFund(
                _msgSender(),
                feesCollector,
                feeToSend,
                "Marketplace#_executeOrder: TRANSFER_FEES_TO_FEES_COLLECTOR_FAILED"
            );
        }
    }

}