//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/Proxy/ProxyImplementation.sol";
import "contracts/V5/Marketplace/MarketV5Context.sol";

/// @title Settings editor for MarketplaceV5
/// @author Danil Sakhinov
/// @notice Required MarketplaceV5 role PROXY_ROLE
contract MarketV5Settings is MarketV5Context, ProxyImplementation {

    constructor(address marketAddress) ProxyImplementation(marketAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(PROXY_ROLE, _msgSender());
    }

    function _setParametersIndexes(bytes memory encoded) internal {
        (bool isProxy,) = routedDelegate("xSetParametersIndexes(bytes)", encoded);
        if (isProxy) {
            (uint8[2] memory parameterId) = abi.decode(encoded, (uint8[2]));
            _parameters = parameterId;
        }
    }
    /// @notice Proxy entrance
    function xSetParametersIndexes(bytes memory encoded) public onlyRole(PROXY_ROLE) {
        _setParametersIndexes(encoded);
    }
    /// @notice public entrance
    function setParametersIndexes(uint8[2] calldata parameterId) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _setParametersIndexes(abi.encode(parameterId));
    }

}