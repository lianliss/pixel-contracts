//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "contracts/common/Multipool/interfaces/IMultipool.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/V5/Core/interfaces/ICoreV5Auth.sol";

struct Payment {
    uint paymentId;
    uint64 userId;
    address userAddress;
    address tokenAddress;
    uint tokenAmount;
    bool isPayed;
    bool isCancel;
    string chargeId;
}

contract StarsPayments is AccessControl {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");
    IMultipool private _pool;
    ICoreV5Auth private _auth;
    mapping(uint paymentId => Payment payment) public payments;
    mapping(string chargeId => uint paymentId) public charges;

    constructor(address poolAddress, address coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());

        _pool = IMultipool(poolAddress);
        _auth = ICoreV5Auth(coreAddress);
    }

    receive() external payable {}

    event Send(address erc20, address indexed to, uint amount);
    event CreatedOrder(uint indexed paymentId, uint64 indexed userId, address indexed userAddress, address tokenAddress, uint tokenAmount);
    event ExecutedOrder(uint indexed paymentId, uint64 indexed userId, address indexed userAddress, address tokenAddress, uint tokenAmount, string chargeId);
    event CancelledOrder(uint indexed paymentId, uint64 indexed userId, address indexed userAddress, address tokenAddress, uint tokenAmount);

    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    function getBalance(address erc20Address) public view returns (uint) {
        if (erc20Address == address(0)) {
            return getBalance();
        } else {
            return IERC20(erc20Address).balanceOf(address(this));
        }
    }

    function getChargePayment(string calldata chargeId) public view returns (Payment memory) {
        uint paymentId = charges[chargeId];
        return payments[paymentId];
    }

    function getChargeUserId(string calldata chargeId) public view returns (uint64) {
        return getChargePayment(chargeId).userId;
    }

    function getChargeUserAddress(string calldata chargeId) public view returns (address) {
        return getChargePayment(chargeId).userAddress;
    }

    function _send(address erc20Address, address to, uint amount) private {
        if (erc20Address == address(0)) {
            require(getBalance() >= amount, "Not enough funds");
            (bool sent,) = to.call{value: amount}("");
            require(sent, "Failed to send Ether");
            emit Send(erc20Address, to, amount);
        } else {
            IERC20 erc20 = IERC20(erc20Address);
            require(getBalance(erc20Address) >= amount, "Not enough funds");
            require(erc20.transfer(to, amount), "Can't transfer ERC20");
            emit Send(erc20Address, to, amount);
        }
    }

    function createOrder(
        uint paymentId,
        uint64 userId,
        address tokenAddress,
        uint tokenAmount
    ) public onlyRole(BACKEND_ROLE) {
        Payment storage payment = payments[paymentId];
        require(payment.paymentId == 0, "Payment already exists");

        payment.paymentId = paymentId;
        payment.userId = userId;
        payment.userAddress = _auth.getIdAddress(userId);
        payment.tokenAddress = tokenAddress;
        payment.tokenAmount = tokenAmount;

        if (tokenAddress == address(0)) {
            _pool.send(address(this), tokenAmount);
        } else {
            _pool.send(tokenAddress, address(this), tokenAmount);
        }

        emit CreatedOrder(paymentId, userId, payment.userAddress, tokenAddress, tokenAmount);
    }

    function executeOrder(uint paymentId, string calldata chargeId) public onlyRole(BACKEND_ROLE) {
        Payment storage payment = payments[paymentId];
        require(payment.paymentId != 0, "Payment is not exists");
        require(!payment.isPayed, "Payment already executed");
        require(!payment.isCancel, "Payment has been cancelled");

        payment.isPayed = true;
        payment.chargeId = chargeId;
        charges[chargeId] = paymentId;
        _send(payment.tokenAddress, payment.userAddress, payment.tokenAmount);

        emit ExecutedOrder(paymentId, payment.userId, payment.userAddress, payment.tokenAddress, payment.tokenAmount, chargeId);
    }

    function cancelOrder(uint paymentId) public onlyRole(BACKEND_ROLE) {
        Payment storage payment = payments[paymentId];
        require(payment.paymentId != 0, "Payment is not exists");
        require(!payment.isPayed, "Payment has been executed");
        require(!payment.isCancel, "Payment already cancelled");

        payment.isCancel = true;
        _send(payment.tokenAddress, address(_pool), payment.tokenAmount);

        emit CancelledOrder(paymentId, payment.userId, payment.userAddress, payment.tokenAddress, payment.tokenAmount);
    }

}