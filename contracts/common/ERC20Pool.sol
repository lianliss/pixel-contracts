//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract ERC20Pool is AccessControl {

    IERC20 public erc20;
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    constructor(address erc20Address) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        
        erc20 = IERC20(erc20Address);
    }

    event SendERC20(address erc20, address indexed to, uint amount);
    event Deposit(address erc20, address indexed from, uint amount);

    function getBalance() public view returns (uint) {
        return erc20.balanceOf(address(this));
    }

    function send(address to, uint amount) public onlyRole(BACKEND_ROLE) {
        require(getBalance() >= amount, "Not enough funds");
        require(erc20.transfer(to, amount), "Can't transfer ERC20");
        emit SendERC20(address(erc20), to, amount);
    }

    function _withdraw(uint amount) private {
        require(getBalance() >= amount, "Not enough funds");
        require(erc20.transfer(_msgSender(), amount), "Can't transfer ERC20");
        emit SendERC20(address(erc20), _msgSender(), amount);
    }

    function withdraw(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _withdraw(amount);
    }

    function withdraw() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _withdraw(getBalance());
    }

    function deposit(uint amount) public {
        erc20.transferFrom(_msgSender(), address(this), amount);
        emit Deposit(address(erc20), _msgSender(), amount);
    }

}