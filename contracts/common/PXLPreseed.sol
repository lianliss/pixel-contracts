// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/// @title Hello Pixel Preseed Round contract
/// @author Danil Sakhinov
contract PXLPreseed is Ownable {

    struct User {
        uint deposit;
        uint depositTimestamp;
        string name;
        string contact;
    }
    mapping (address => User) public users;
    address[] private _usersList;
    address private _receiver;
    uint private _invested;

    IERC20 public USDT;

    uint constant WAD = 10 ** 6;
    uint public minUserAmount = 2000 * WAD; // The minimum amount for which a user can make a purchase
    uint public maxUserAmount = 10000 * WAD; // The maximum amount for which a user can make a purchase
    uint public maxInvestments = 138000 * WAD; // Maximum total investments

    event BuyPXL(address sender, uint amount);
    event SetReceiver(address receiver);
    event SetMinDeposit(uint amount);
    event SetMaxDeposit(uint amount);
    event SetMaxInvestments(uint amount);

    constructor (IERC20 usdtAddress, address receiver) Ownable(_msgSender()) {
        USDT = IERC20(usdtAddress);
        setReceiver(receiver);
    }

    function invest(uint amount, string calldata name, string calldata contact) public {
        User storage user = users[_msgSender()];
        require(amount >= minUserAmount, "Too small deposit");
        require(amount <= maxUserAmount - user.deposit, "Too big deposit");
        require(amount <= maxInvestments - _invested, "");

        USDT.transferFrom(_msgSender(), _receiver, amount);
        if (user.deposit == 0) {
            _usersList.push(_msgSender());
        }
        user.deposit += amount;
        _invested += amount;
        user.depositTimestamp = block.timestamp;
        user.name = name;
        user.contact = contact;

        emit BuyPXL(_msgSender(), amount);
    }

    function invested() public view returns(uint) {
        return _invested;
    }

    /// @notice Sets the minimum of user deposit. Available only for contract Owner
    /// @param amount BUSD amount
    function setMinUserAmount(uint amount) public onlyOwner {
        minUserAmount = amount;
        emit SetMinDeposit(amount);
    }

    /// @notice Sets the maximum of user deposit. Available only for contract Owner
    /// @param amount BUSD amount
    function setMaxUserAmount(uint amount) public onlyOwner {
        maxUserAmount = amount;
        emit SetMaxDeposit(amount);
    }

    function setMaxInvestments(uint amount) public onlyOwner {
        maxInvestments = amount;
        emit SetMaxInvestments(amount);
    }

    function setReceiver(address receiver) public onlyOwner {
        _receiver = receiver;
        emit SetReceiver(receiver);
    }

    function getDeposit(address account) public view returns(User memory) {
        return users[account];
    }

    function getDeposits() public view returns(address[] memory, User[] memory) {
        User[] memory data = new User[](_usersList.length);
        for (uint i; i < _usersList.length; i++) {
            User storage user = users[_usersList[i]];
            data[i] = User({
                deposit: user.deposit,
                depositTimestamp: user.depositTimestamp,
                name: user.name,
                contact: user.contact
            });
        }
        return (_usersList, data);
    }

    function getPreseedData() public view returns(uint, uint, uint, uint, uint) {
        return(
            minUserAmount,
            maxUserAmount,
            maxInvestments,
            invested(),
            _usersList.length
        );
    }
}