//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/common/Multipool/interfaces/IMultipool.sol";

contract Multipool is IMultipool, AccessControl {

    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
    }

    receive() external payable {
        emit Deposit(address(0), _msgSender(), msg.value);
    }

    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    function getBalance(address erc20Address) public view returns (uint) {
        if (erc20Address == address(0)) {
            return getBalance();
        } else {
            return IERC20(erc20Address).balanceOf(address(this));
        }
    }

    function _send(address erc20Address, address to, uint amount) private {
        if (erc20Address == address(0)) {
            require(getBalance() >= amount, "Not enough funds");
            (bool sent,) = to.call{value: amount}("");
            require(sent, "Failed to send Ether");
            emit Send(erc20Address, to, amount);
        } else {
            IERC20 erc20 = IERC20(erc20Address);
            require(getBalance(erc20Address) >= amount, "Not enough funds");
            require(erc20.transfer(to, amount), "Can't transfer ERC20");
            emit Send(erc20Address, to, amount);
        }
    }

    function send(address erc20Address, address to, uint amount) public onlyRole(BACKEND_ROLE) {
        _send(erc20Address, to, amount);
    }

    function send(address to, uint amount) public onlyRole(BACKEND_ROLE) {
        _send(address(0), to, amount);
    }

    function withdraw(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _send(address(0), _msgSender(), amount);
    }

    function withdraw(address erc20Address, uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _send(erc20Address, _msgSender(), amount);
    }

    function withdrawAll() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _send(address(0), _msgSender(), getBalance());
    }

    function withdrawAll(address erc20Address) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _send(erc20Address, _msgSender(), getBalance());
    }

    function deposit(address erc20Address, uint amount) public {
        IERC20(erc20Address).transferFrom(_msgSender(), address(this), amount);
        emit Deposit(erc20Address, _msgSender(), amount);
    }

    function deposit() public payable {
        emit Deposit(address(0), _msgSender(), msg.value);
    }

}