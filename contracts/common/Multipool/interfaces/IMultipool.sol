//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

interface IMultipool {

    event Send(address erc20, address indexed to, uint amount);
    event Deposit(address erc20, address indexed from, uint amount);

    function getBalance() external view returns (uint);
    function getBalance(address erc20Address) external view returns (uint);
    function send(address erc20Address, address to, uint amount) external;
    function send(address to, uint amount) external;
    function deposit(address erc20Address, uint amount) external;
    function deposit() external payable;

}