//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

contract SendGas {

    receive() external payable {
        (bool sent,) = msg.sender.call{value: msg.value}("");
        require(sent, "Failed to send Gas");
    }

    function send(address payable receiver) public payable {
        (bool sent,) = receiver.call{value: msg.value}("");
        require(sent, "Failed to send Gas");
    }

}