//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V4/interfaces/IPXLsCoreV4.sol";

contract Stats {
    struct StorageStats {
        uint64 id;
        address accountAddress;
        uint lastClaim;
        uint8 sizeLevel;
        uint8 speedLevel;
        uint8 referralLevel;
        uint claimed;
        uint mined;
        uint refStorage;
        uint sizeLimit;
        uint refLimit;
        uint rewardPerSecond;
        uint256 shardsBalance;
        uint256 sgbBalance;
    }

    IPXLsCoreV4 public PXLs = IPXLsCoreV4(0x39838abf66af299401030e1B82be7848301FbB94);

    function getStats(uint64[] calldata ids) public view returns(StorageStats[] memory) {
        StorageStats[] memory stats = new StorageStats[](ids.length);
        for (uint i; i < ids.length; i++) {
            uint64 id = ids[i];
            (
                Storage memory user,
                ,
                uint mined,
                uint sizeLimit,
                uint rewardPerSecond,
                uint refLimit,
                uint balance,
                uint coins
            ) = PXLs.getStorage(id);
            stats[i] = StorageStats({
                id: id,
                accountAddress: user.account,
                lastClaim: user.claimTimestamp,
                sizeLevel: user.sizeLevel,
                speedLevel: user.speedLevel,
                referralLevel: user.referralLevel,
                claimed: user.claimed,
                mined: mined,
                refStorage: user.refStorage,
                sizeLimit: sizeLimit,
                refLimit: refLimit,
                rewardPerSecond: rewardPerSecond,
                shardsBalance: balance,
                sgbBalance: coins
            });
        }
        return stats;
    }
}