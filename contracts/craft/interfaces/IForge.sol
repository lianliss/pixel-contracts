// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import { UD60x18 } from "@prb/math/src/UD60x18.sol";

interface IForge {
    /// @notice Rarities of the collection.
    enum Rarity {
        NONE,      // Placeholder for undefined value
        COMMON,    // 1
        UNCOMMON,  // 2
        RARE,      // 3
        EPIC,      // 4
        LEGENDARY  // 5
    }

    /// @notice Emitted when craft is completed.
    event CraftCompleted(address crafter, uint256 craftedTokenId, uint256 typeId, Rarity rarity);

    /// @notice Emitted when token type IDs are added to a rarity category.
    event TypeIdsAdded(Rarity indexed rarity, uint256[] tokenTypeIds);

    /// @notice Emitted when token type IDs are removed from a rarity category.
    event TypeIdsRemoved(Rarity indexed rarity, uint256[] tokenTypeIds);

    /// @notice Emitted when a random token type ID is selected from a rarity category.
    event RandomTypeIdSelected(Rarity indexed rarity, uint256 typeId);

    /// @notice Emitted when a slot parameter is updated.
    event ParameterUpdated(address indexed user, uint8 parameterId, uint24 multiplier, uint256 adder);

    /// @notice Emitted when a receiver parameter ID is updated.
    event ParameterIdUpdated(string parameterName, uint8 newParameterId);

    /**
     * @notice Crafts a new NFT with a random rarity based on the provided materials.
     * @param pixelDustAmount Amount of PIXEL_DUST to burn for crafting.
     * @param blendTokenIds Array of BLEND NFT token IDs to burn for crafting.
     */
    function craft(uint256 pixelDustAmount, uint256[] calldata blendTokenIds) external;

    /**
     * @notice Calculates the distribution of rarity for crafting based on inputs.
     * @param pixelDustAmount Amount of PIXEL_DUST used for crafting.
     * @param blendTokenIds Array of BLEND NFT token IDs.
     * @param user The address of the user.
     * @return distribution Array representing the rarity distribution weights.
     */
    function calculateRarityDistribution(
        uint256 pixelDustAmount,
        uint256[] calldata blendTokenIds,
        address user
    ) external view returns (UD60x18[5] memory distribution);

    /**
     * @notice Sets the parameter ID for slot 0.
     * @param parameterId New parameter ID.
     */
    function setSlot0ParameterId(uint8 parameterId) external;

    /**
     * @notice Sets the parameter ID for slot 1.
     * @param parameterId New parameter ID.
     */
    function setSlot1ParameterId(uint8 parameterId) external;

    /**
     * @notice Adds multiple token type IDs to a specific rarity category.
     * @dev Only callable by users with the `DEFAULT_ADMIN_ROLE`.
     * @param tokenTypeIds An array of token type IDs to add.
     * @param rarity The index of the rarity category to which the IDs should be added.
     */
    function addTypeIdsByRarity(uint256[] calldata tokenTypeIds, Rarity rarity) external;

    /**
     * @notice Removes multiple token type IDs from a specific rarity category.
     * @dev Only callable by users with the `DEFAULT_ADMIN_ROLE`.
     * @param tokenTypeIds An array of token type IDs to remove.
     * @param rarity The index of the rarity category from which the IDs should be removed.
     */
    function removeTypeIdsByRarity(uint256[] calldata tokenTypeIds, Rarity rarity) external;

    /**
     * @notice Retrieves the total count of token type IDs in a specific rarity category.
     * @param rarity The index of the rarity category to query.
     * @return The total number of token type IDs in the specified rarity category.
     */
    function getTotalTypeIdsByRarity(Rarity rarity) external view returns (uint256);

    /**
     * @notice Retrieves a paginated list of token type IDs from a specific rarity category.
     * @param rarity The index of the rarity category to query.
     * @param offset The starting index of the pagination.
     * @param limit The maximum number of items to return.
     * @return An array of token type IDs from the specified rarity category.
     */
    function getTypeIdsByRarityPaginated(
        Rarity rarity,
        uint256 offset,
        uint256 limit
    ) external view returns (uint256[] memory);

    /**
     * @notice Retrieves the slot multiplier for a user and slot number.
     * @param user The address of the user.
     * @param slotNumber The slot number to query.
     * @return The slot multiplier as a UD60x18 value.
     */
    function slotMultiplier(address user, uint256 slotNumber) external view returns (UD60x18);

    /**
     * @notice Retrieves the IDs for slot 0 and slot 1 parameters.
     * @return slot0parameterId ID of the parameter for slot 0.
     */
    function slot0ParameterId() external view returns (uint8);

    /**
     * @notice Retrieves the IDs for slot 0 and slot 1 parameters.
     * @return slot1parameterId ID of the parameter for slot 1.
     */
    function slot1ParameterId() external view returns (uint8);
}
