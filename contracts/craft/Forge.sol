// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import { UD60x18, ud } from "@prb/math/src/UD60x18.sol";
import { EnumerableSet } from "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import { IERC721 } from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import { ERC20Burnable as IERC20Burnable} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";
import { IParameterReceiver } from "contracts/V5/Slots/interfaces/IParameterReceiver.sol";
import { IPXLsNFTV5 } from "contracts/V5/NFT/interfaces/IPXLsNFTV5.sol";
import { ItemType } from "contracts/V5/NFT/NFTV5Structs.sol";
import { ICoreV5Auth } from "contracts/V5/Core/interfaces/ICoreV5Auth.sol";
import { IForge } from "contracts/craft/interfaces/IForge.sol";
import "contracts/craft/interfaces/IPXLDust.sol";

/**
 * @title Forge
 * @dev A contract for crafting NFTs with configurable rarity and slot-based multipliers.
 */
contract Forge is IForge, IParameterReceiver, AccessControl {
    using EnumerableSet for EnumerableSet.UintSet;

    /// @notice Reference to the core contract for user authentication and address mapping.
    ICoreV5Auth immutable public CORE;

    /// @notice ERC20 token used as a crafting material.
    IPXLDust immutable public PIXEL_DUST;

    /// @notice Collection of SLOT NFTs.
    IPXLsNFTV5 immutable public SLOT_COLLECTION;

    /// @notice Collection of BLEND NFTs.
    IPXLsNFTV5 immutable public BLEND_COLLECTION;

    /// @notice Collection of CRAFTED NFTs.
    IPXLsNFTV5 immutable public CRAFTED_COLLECTION;

    /// @notice Base value for calculations.
    UD60x18 immutable private _BASE = ud(100e3);

    /// @notice Base multiplier for SLOT NFTs.
    UD60x18 immutable private _BASE_SLOT_MULTIPLIER = ud(1.01e18);

    /// @notice Multipliers for various BLEND rarities.
    UD60x18 immutable private _BLEND_MULTIPLIER_COMMON = ud(1.02e18);
    UD60x18 immutable private _BLEND_MULTIPLIER_UNCOMMON = ud(1.04e18);
    UD60x18 immutable private _BLEND_MULTIPLIER_RARE = ud(1.06e18);
    UD60x18 immutable private _BLEND_MULTIPLIER_EPIC = ud(1.08e18);
    UD60x18 immutable private _BLEND_MULTIPLIER_LEGENDARY = ud(1.1e18);

    /// @notice Base blend multipliers for higher rarities.
    UD60x18 immutable private _BASE_BLEND_MULTIPLIER_UNCOMMON = ud(1.8e18);
    UD60x18 immutable private _BASE_BLEND_MULTIPLIER_RARE = ud(1.6e18);
    UD60x18 immutable private _BASE_BLEND_MULTIPLIER_EPIC = ud(1.4e18);

    /// @notice Role for editing SLOT parameters.
    bytes32 public constant SLOT_EDITOR_ROLE = keccak256("SLOT_EDITOR_ROLE");

    /// @notice Role for editing main settings parameters.
    bytes32 public constant SETTINGS_EDITOR_ROLE = keccak256("SETTINGS_EDITOR_ROLE");

    /// @notice ID for slot 0 parameter.
    uint8 public slot0ParameterId;

    /// @notice ID for slot 1 parameter.
    uint8 public slot1ParameterId;

    /// @notice Counter to ensure unique seeds even within the same tx.
    uint256 private _randomSeedCounter;

    /// @notice Lookup mapping for token type by rarity.
    mapping(Rarity => EnumerableSet.UintSet) private _typeIdsByRarity;

    /// @notice Mapping to track slot multipliers for each user and slot.
    mapping(address user => mapping(uint256 slotNumber => UD60x18)) public slotMultiplier;

    /**
     * @dev Initializes the Forge contract with the provided dependencies.
     * @param core Address of the core contract for user authentication.
     * @param pixelDust Address of the PIXEL_DUST token contract.
     * @param blendCollection Address of the BLEND NFT collection contract.
     * @param slotCollection Address of the SLOT NFT collection contract.
     * @param craftedCollection Address of the CRAFTED NFT collection contract.
     */
    constructor(
        address core,
        address pixelDust,
        address blendCollection,
        address slotCollection,
        address craftedCollection
    ){
        CORE = ICoreV5Auth(core);
        PIXEL_DUST = IPXLDust(pixelDust);
        SLOT_COLLECTION = IPXLsNFTV5(slotCollection);
        BLEND_COLLECTION = IPXLsNFTV5(blendCollection);
        CRAFTED_COLLECTION = IPXLsNFTV5(craftedCollection);
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(SETTINGS_EDITOR_ROLE, _msgSender());
    }

    /**
     * @notice Crafts a new NFT with a random rarity based on the provided materials.
     * @param pixelDustAmount Amount of PIXEL_DUST to burn for crafting.
     * @param blendTokenIds Array of BLEND NFT token IDs to burn for crafting.
     */
    function craft(uint256 pixelDustAmount, uint256[] calldata blendTokenIds) external {
        require(pixelDustAmount >= 1e18, "Not enough dust");
        Rarity rarity = _getRandomCategory(pixelDustAmount, blendTokenIds);
        for (uint256 i = 0; i < blendTokenIds.length; i++) BLEND_COLLECTION.burn(blendTokenIds[i]);
        PIXEL_DUST.burn(_msgSender(), pixelDustAmount);
        uint256 typeId = _getRandomTypeIdByRarity(rarity);
        uint256 tokenId = CRAFTED_COLLECTION.mint(_msgSender(), typeId);
        emit CraftCompleted(_msgSender(), tokenId, typeId, rarity);
    }

    /**
     * @notice Calculates the distribution of rarity for crafting based on inputs.
     * @param pixelDustAmount Amount of PIXEL_DUST used for crafting.
     * @param blendTokenIds Array of BLEND NFT token IDs.
     * @return distribution Array representing the rarity distribution weights.
     * @param user The address of the user.
     */
    function calculateRarityDistribution(
        uint256 pixelDustAmount,
        uint256[] calldata blendTokenIds,
        address user
    ) external view returns (UD60x18[5] memory distribution) {
        return _calculateRarityDistribution(pixelDustAmount, blendTokenIds, user);
    }

    /**
     * @notice Updates a parameter for a specific user slot.
     * @param userId User ID to identify the user.
     * @param parameterId ID of the parameter to update.
     * @param multiplier Multiplier value for the parameter.
     * @param adder Unused parameter in this implementation.
     */
    // solc-ignore-next-line unused-param
    function updateParameter(uint64 userId, uint8 parameterId, uint24 multiplier, uint256 adder)
        external
        onlyRole(SLOT_EDITOR_ROLE)
    {
        address user = CORE.getIdAddress(userId);
        if(parameterId == slot0ParameterId) slotMultiplier[user][0] = ud(multiplier) / ud(10000);
        if(parameterId == slot1ParameterId) slotMultiplier[user][1] = ud(multiplier) / ud(10000);
        emit ParameterUpdated(user, parameterId, multiplier, adder);
    }

    /**
     * @notice Sets the parameter ID for slot 0.
     * @param parameterId New parameter ID.
     */
    function setSlot0ParameterId(uint8 parameterId) external onlyRole(SETTINGS_EDITOR_ROLE) {
        slot0ParameterId = parameterId;
        emit ParameterIdUpdated("Slot0", parameterId);
    }

    /**
     * @notice Sets the parameter ID for slot 1.
     * @param parameterId New parameter ID.
     */
    function setSlot1ParameterId(uint8 parameterId) external onlyRole(SETTINGS_EDITOR_ROLE) {
        slot1ParameterId = parameterId;
        emit ParameterIdUpdated("Slot1", parameterId);
    }

    /**
     * @notice Adds multiple token type IDs to a specific rarity category.
     * @dev Only callable by users with the `SETTINGS_EDITOR_ROLE`.
     * @param tokenTypeIds An array of token type IDs to add.
     * @param rarity The index of the rarity category to which the IDs should be added.
     */
    function addTypeIdsByRarity(uint256[] calldata tokenTypeIds, Rarity rarity)
        external
        onlyRole(SETTINGS_EDITOR_ROLE)
    {
        for (uint256 i = 0; i < tokenTypeIds.length; i++) {
            _typeIdsByRarity[rarity].add(tokenTypeIds[i]);
        }
        emit TypeIdsAdded(rarity, tokenTypeIds);
    }

    /**
     * @notice Removes multiple token type IDs from a specific rarity category.
     * @dev Only callable by users with the `SETTINGS_EDITOR_ROLE`.
     * @param tokenTypeIds An array of token type IDs to remove.
     * @param rarity The index of the rarity category from which the IDs should be removed.
     */
    function removeTypeIdsByRarity(uint256[] calldata tokenTypeIds, Rarity rarity)
        external
        onlyRole(SETTINGS_EDITOR_ROLE)
    {
        for (uint256 i = 0; i < tokenTypeIds.length; i++) {
            require(_typeIdsByRarity[rarity].contains(tokenTypeIds[i]), "Invalid type ID");
            _typeIdsByRarity[rarity].remove(tokenTypeIds[i]);
        }
        emit TypeIdsRemoved(rarity, tokenTypeIds);
    }

    /**
     * @notice Retrieves the total count of token type IDs in a specific rarity category.
     * @param rarity The index of the rarity category to query.
     * @return The total number of token type IDs in the specified rarity category.
     */
    function getTotalTypeIdsByRarity(Rarity rarity) external view returns (uint256) {
        return _typeIdsByRarity[rarity].length();
    }

    /**
     * @notice Retrieves a paginated list of token type IDs from a specific rarity category.
     * @param rarity The index of the rarity category to query.
     * @param offset The starting index of the pagination.
     * @param limit The maximum number of items to return.
     * @return An array of token type IDs from the specified rarity category.
     */
    function getTypeIdsByRarityPaginated(
        Rarity rarity,
        uint256 offset,
        uint256 limit
    ) external view returns (uint256[] memory) {
        uint256 totalItems = _typeIdsByRarity[rarity].length();
        require(offset < totalItems, "Offset out of bounds");

        uint256 itemsToReturn = (offset + limit > totalItems) ? totalItems - offset : limit;

        uint256[] memory paginatedItems = new uint256[](itemsToReturn);
        for (uint256 i = 0; i < itemsToReturn; i++) {
            paginatedItems[i] = _typeIdsByRarity[rarity].at(offset + i);
        }
        return paginatedItems;
    }

    /**
     * @notice Calculates the distribution of rarities for crafting based on the amount of PIXEL_DUST and blend tokens.
     * @dev The distribution is influenced by slot multipliers, blend token rarity, and the amount of PIXEL_DUST.
     * @param pixelDustAmount The amount of PIXEL_DUST used for crafting.
     * @param blendTokenIds An array of blend token IDs used in the crafting process.
     * @param user The address of the user.
     * @return distribution An array of UD60x18 values representing the probability distribution for each rarity:
     *                      [Common, Uncommon, Rare, Epic, Legendary].
     */
    function _calculateRarityDistribution(
        uint256 pixelDustAmount,
        uint256[] calldata blendTokenIds,
        address user
    ) private view returns (UD60x18[5] memory distribution) {
        UD60x18 baseMultiplier = _calculateSlotBaseMultiplier(user);
        (UD60x18 uncommonDivider, UD60x18 rareDivider, UD60x18 epicDivider) = _calculateBlendDividers(blendTokenIds);
        distribution[0] = _BASE / (baseMultiplier.pow(ud(pixelDustAmount)));
        distribution[1] = (_BASE - distribution[0]) / uncommonDivider;
        distribution[2] = (_BASE - (distribution[0] + distribution[1])) / rareDivider;
        distribution[3] = (_BASE - (distribution[0] + distribution[1] + distribution[2])) / epicDivider;
        distribution[4] = _BASE - (distribution[0] + distribution[1] + distribution[2] + distribution[3]);
    }

    /**
     * @notice Calculates the multipliers (dividers) for each rarity based on blend token rarities.
     * @dev Uses the quantity of tokens per rarity to calculate the overall multiplier for each category.
     * @param blendTokenIds An array of blend token IDs used in the crafting process.
     * @return uncommonDivider The calculated divider for Uncommon rarity.
     * @return rareDivider The calculated divider for Rare rarity.
     * @return epicDivider The calculated divider for Epic rarity.
     */
    function _calculateBlendDividers(uint256[] calldata blendTokenIds)
        private
        view
        returns (UD60x18, UD60x18, UD60x18)
    {
        uint256 quantityCommon;
        uint256 quantityUncommon;
        uint256 quantityRare;
        uint256 quantityEpic;
        uint256 quantityLegendary;
        for (uint256 i = 0; i < blendTokenIds.length; i++) {
            Rarity rarity = _getBlendRarityByTokenId(blendTokenIds[i]);
            require(BLEND_COLLECTION.ownerOf(blendTokenIds[i]) == _msgSender(), "Invalid token owner");
            require(rarity >= Rarity.COMMON && rarity <= Rarity.LEGENDARY, "Invalid nft in blend");
            if(rarity == Rarity.COMMON) quantityCommon++;
            if(rarity == Rarity.UNCOMMON) quantityUncommon++;
            if(rarity == Rarity.RARE) quantityRare++;
            if(rarity == Rarity.EPIC) quantityEpic++;
            if(rarity == Rarity.LEGENDARY) quantityLegendary++;
        }
        UD60x18 multipiler =
            ((quantityCommon > 0 ? _BLEND_MULTIPLIER_COMMON : ud(1)).powu(quantityCommon)) *
            ((quantityUncommon > 0 ? _BLEND_MULTIPLIER_UNCOMMON : ud(1)).powu(quantityUncommon)) *
            ((quantityRare > 0 ? _BLEND_MULTIPLIER_RARE : ud(1)).powu(quantityRare)) *
            ((quantityEpic > 0 ? _BLEND_MULTIPLIER_EPIC : ud(1)).powu(quantityEpic)) *
            ((quantityLegendary > 0 ? _BLEND_MULTIPLIER_LEGENDARY : ud(1)).powu(quantityLegendary));
        return (
            _BASE_BLEND_MULTIPLIER_UNCOMMON * multipiler,
            _BASE_BLEND_MULTIPLIER_RARE * multipiler,
            _BASE_BLEND_MULTIPLIER_EPIC * multipiler
            );
    }

    /**
     * @notice Calculates base multiplier for slots.
     * @dev This function uses user-specific slot multipliers and combines them with the base multiplier.
     * @param user The address of the user.
     * @return The resulting base multiplier as a UD60x18 value.
     */
    function _calculateSlotBaseMultiplier(address user) private view returns (UD60x18) {
        UD60x18 slot0Multiplier = slotMultiplier[user][0];
        UD60x18 slot1Multiplier = slotMultiplier[user][1];
        return _BASE_SLOT_MULTIPLIER *
            (slot0Multiplier != ud(0) ? slot0Multiplier : ud(1e18)) *
            (slot1Multiplier != ud(0) ? slot1Multiplier : ud(1e18));
    }

    /**
     * @notice Retrieves the rarity of a token by its ID.
     * @dev Uses the BLEND_COLLECTION to fetch token details.
     * @param tokenId The ID of the token.
     * @return The rarity of the token as a uint8 value.
     */
    function _getBlendRarityByTokenId(uint256 tokenId) private view returns (Rarity) {
        ItemType memory itemType = BLEND_COLLECTION.getTokenType(tokenId);
        return Rarity(itemType.rarity);
    }

    /**
     * @notice Determines a random category (rarity) based on input values.
     * @dev Combines rarity distribution and random seed to select a category.
     * @param pixelDustAmount The amount of PIXEL_DUST used for crafting.
     * @param blendTokenIds The IDs of blend tokens involved in the crafting process.
     * @return The selected category as a uint8 value.
     */
    function _getRandomCategory(uint256 pixelDustAmount, uint256[] calldata blendTokenIds)
        private
        returns (Rarity)
    {
        UD60x18[5] memory distribution = _calculateRarityDistribution(pixelDustAmount, blendTokenIds, _msgSender());
        UD60x18 totalWeight ;
        for (uint256 i = 0; i < distribution.length; i++) {
            totalWeight = totalWeight + distribution[i];
        }

        UD60x18 randomValue = ud(_getPseudoRandomSeed()) % totalWeight;
        UD60x18 cumulativeSum ;
        for (uint8 i = 0; i < distribution.length; i++) {
            cumulativeSum = cumulativeSum + distribution[i];
            if (randomValue < cumulativeSum) {
                return Rarity(i+1);
            }
        }
        revert("Invalid category");
    }

    /**
     * @notice Selects a random token type ID from a specific rarity category.
     * @dev The randomness is based on a pseudo-random seed and is not cryptographically secure.
     * @param rarity The index of the rarity category from which to select the token type ID.
     * @return A randomly selected token type ID from the specified rarity category.
     */
    function _getRandomTypeIdByRarity(Rarity rarity) private returns (uint256) {
        require(_typeIdsByRarity[rarity].length() > 0, "No types in the set");
        uint256 randomIndex = _getPseudoRandomSeed() % _typeIdsByRarity[rarity].length();
        return _typeIdsByRarity[rarity].at(randomIndex);
    }

    /**
     * @notice Generates a pseudo-random seed.
     * @dev Combines multiple block parameters, sender address, and a counter for randomness.
     * @return A pseudo-random uint256 seed.
     */
    function _getPseudoRandomSeed() private returns (uint256) {
        _randomSeedCounter++;
        return uint256(
            keccak256(
                abi.encodePacked(
                    block.prevrandao,
                    block.timestamp,
                    block.number,
                    _msgSender(),
                    _randomSeedCounter
                )
            )
        );
    }
}
