//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

struct Storage {
    address account;
    uint64 parent;
    bool disabled;
    uint claimTimestamp;
    uint claimed;
}

interface ICoinMining {

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Claim(uint64 indexed id, address account, uint amount);
    event ReferReward(uint64 indexed claimerId, uint64 indexed id, uint64 indexed child, address account, uint amount);
    event GrantAccess(uint64 indexed id, address account, address controller);
    event SetParent(uint64 indexed id, uint64 indexed parent);
    event SetAccessController(address controller);
    event SetClaimsDisabled(address caller, bool value);
    event SetRefersDisabled(address caller, bool value);
    event SetStorageDisabled(uint64 indexed id, address caller, bool value);

    function getStorage(uint64 id) external view returns (
        Storage memory user,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint balance,
        uint coins
        );

    function getAddressId(address account) external view returns (uint64);
    function getIdAddress(uint64 id) external view returns (address);
    function mintForOne(uint64 id, uint amount) external;
}