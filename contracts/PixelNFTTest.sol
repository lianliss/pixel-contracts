//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V4/interfaces/IPixelNFT.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract PixelNFTTest is AccessControl {
    
    IPixelNFT public _nft;

    constructor (address nftAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _nft = IPixelNFT(nftAddress);
    }

    function mint(uint typeId, uint amount, address receiver) public onlyRole(DEFAULT_ADMIN_ROLE) {
        for (uint i; i < amount; i++) {
            _nft.mint(receiver, typeId);
        }       
    }

    function mint(uint typeId, uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        mint(typeId, amount, _msgSender());
    }

}