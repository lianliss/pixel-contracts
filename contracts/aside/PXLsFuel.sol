//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "contracts/V5/Core/interfaces/ICoreV5Auth.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

/// @notice Gas distribution based on PXLsCoreV5 authorization
contract PXLsFuel is AccessControl {

    struct GasData {
        uint gasAmount;
        uint minGasLimit;
        uint balance;
        uint nextUserOpportunity;
        uint nextAddressOpportunity;
        bool isAvailable;
    }

    ICoreV5Auth private _core;
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    uint public gasAmount = 10**6;
    uint public minGasLimit = 10**4;
    uint public userGasTimeout = 15 minutes;
    uint public addressGasTimeout = 30 days;

    mapping(uint64 userId => uint timestamp) public nextUserOpportunity;
    mapping(address account => uint timestamp) public nextAddressOpportunity;

    constructor(address coreAddress) {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());

        _core = ICoreV5Auth(coreAddress);
    }

    event Recieved(address from, uint amount);
    event Sent(address to, uint amount);
    event Release(address admin, uint amount);

    receive() external payable {
        emit Recieved(_msgSender(), msg.value);
    }

    function _sendGas(address to, uint amount) private {
        (bool sent,) = to.call{value: amount}("");
        require(sent, "Failed to send Ether");
        emit Sent(to, amount);
    }

    function _auth(address account, uint64 userId) private view {
        require(
            userId > 0
            && _core.getIdAddress(userId) == account
            && _core.getAddressId(account) == userId,
            "Unauthorized user"
        );
    }

    function _limits(address account, uint64 userId) private {
        require(account.balance < minGasLimit, "User have gas");
        require(block.timestamp >= nextUserOpportunity[userId], "User time limit");
        require(block.timestamp >= nextAddressOpportunity[account], "Address time limit");

        nextUserOpportunity[userId] = block.timestamp + userGasTimeout;
        nextAddressOpportunity[account] = block.timestamp + addressGasTimeout;
    }

    function sendGas(address to) public onlyRole(BACKEND_ROLE) {
        uint64 userId = _core.getAddressId(to);
        _auth(to, userId);
        _limits(to, userId);
        _sendGas(to, gasAmount);
    }

    function sendGas(uint64 userId) public onlyRole(BACKEND_ROLE) {
        address to = _core.getIdAddress(userId);
        _auth(to, userId);
        _limits(to, userId);
        _sendGas(to, gasAmount);
    }

    function release() public onlyRole(DEFAULT_ADMIN_ROLE) {
        uint amount = address(this).balance;
        _sendGas(_msgSender(), amount);
        emit Release(_msgSender(), amount);
    }

    function release(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _sendGas(_msgSender(), amount);
        emit Release(_msgSender(), amount);
    }

    function setCoreAddress(address coreAddress) public onlyRole(DEFAULT_ADMIN_ROLE) {
        _core = ICoreV5Auth(coreAddress);
    }

    function setGasAmount(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        gasAmount = amount;
    }

    function setMinGasLimit(uint limit) public onlyRole(DEFAULT_ADMIN_ROLE) {
        minGasLimit = limit;
    }

    function setUserGasTimeout(uint timeout) public onlyRole(DEFAULT_ADMIN_ROLE) {
        userGasTimeout = timeout;
    }

    function setAddressGasTimeout(uint timeout) public onlyRole(DEFAULT_ADMIN_ROLE) {
        addressGasTimeout = timeout;
    }

    function flushUser(uint64 userId) public onlyRole(DEFAULT_ADMIN_ROLE) {
        nextUserOpportunity[userId] = 0;
    }

    function flushAddress(address account) public onlyRole(DEFAULT_ADMIN_ROLE) {
        nextAddressOpportunity[account] = 0;
    }

    function flush(uint64 userId, address account) public onlyRole(DEFAULT_ADMIN_ROLE) {
        nextUserOpportunity[userId] = 0;
        nextAddressOpportunity[account] = 0;
    }

    function getData(address account) public view returns (GasData memory) {
        uint64 userId = _core.getAddressId(account);
        return GasData(
            gasAmount,
            minGasLimit,
            account.balance,
            nextUserOpportunity[userId],
            nextAddressOpportunity[account],
            block.timestamp > nextUserOpportunity[userId]
                && block.timestamp > nextAddressOpportunity[account]
                && account.balance < minGasLimit
                && address(this).balance >= gasAmount
        );
    }

}