//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract CoinBalances {

    function getBalances(address[] calldata accounts) public view returns (uint[] memory) {
        uint[] memory balances = new uint[](accounts.length);

        for (uint i; i < accounts.length; i++) {
            balances[i] = accounts[i].balance;
        }

        return balances;
    }

    function getTokenBalances(address tokenAddress, address[] calldata accounts) public view returns (uint[] memory) {
        uint[] memory balances = new uint[](accounts.length);
        IERC20 token = IERC20(tokenAddress);

        for (uint i; i < accounts.length; i++) {
            balances[i] = token.balanceOf(accounts[i]);
        }

        return balances;
    }

}