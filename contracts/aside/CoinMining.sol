//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "contracts/interfaces/ICoinMining.sol";

/// @title Swisstronik Miniapp
/// @notice Only Wallet users allowed
contract CoinMining is ICoinMining, AccessControl {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
    bytes32 public constant GRANTER_ROLE = keccak256("GRANTER_ROLE");
    bytes32 public constant CLAIMER_ROLE = keccak256("CLAIMER_ROLE");

    mapping(uint64 => Storage) private _storages;
    mapping(address => uint64) private _accounts;

    uint8 constant private _decimals = 18;

    uint private constant _PRECISION = 10**_decimals;
    uint private constant _PERCENT_DECIMALS = 4;
    uint private constant _PERCENT_PRECISION = 10**_PERCENT_DECIMALS;
    uint private constant _INITIAL_SUPPLY = 2768888888 * 10**(_decimals - 2);
    uint private constant _HOUR = 60 * 60;
    uint private constant _DAY = _HOUR * 24;

    uint private _claimed;

    bool private _claimsDisabled;
    bool private _refersDisabled;

    string private _name = "Swisstronik Coin";
    string private _symbol = "SWTR";

    // Tokens per hour
    uint baseSpeed = 1 * 10**17;
    uint baseSize = 1 * _PRECISION;

    // Referral rewards to parents
    uint[] parentsPercents = [
        1000, // 10% to first parent
        500 // 5% to second ancestor
    ];

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(GRANTER_ROLE, _msgSender());
        _grantRole(CLAIMER_ROLE, _msgSender());
        _grantRole(MINTER_ROLE, _msgSender());
        _grantRole(BURNER_ROLE, _msgSender());
    }

    modifier onlyStorageOwner(uint64 id) {
        require(_storages[id].account == _msgSender(), "Sender is not a Storage Owner");
        _;
    }

    event Recieved(address indexed account, uint amount);
    receive() external payable {
        emit Recieved(_msgSender(), msg.value);
    }
    function deposit() public payable {
        emit Recieved(_msgSender(), msg.value);
    }

    event Release(address indexed account, uint amount);
    function release(uint amount) public onlyRole(DEFAULT_ADMIN_ROLE) {
        (bool sent,) = _msgSender().call{value: amount}("");
        require(sent, "Failed to send Ether");
        emit Release(_msgSender(), amount);
    }
    function release() public onlyRole(DEFAULT_ADMIN_ROLE) {
        uint amount = totalSupply();
        (bool sent,) = _msgSender().call{value: amount}("");
        require(sent, "Failed to send Ether");
        emit Release(_msgSender(), amount);
    }

    function _getSpeed() internal view returns (uint) {
        return totalSupply() > 0
            ? baseSpeed / _HOUR
            : 0;
    }

    function _getSize() internal view returns (uint) {
        return baseSize <= totalSupply()
            ? baseSize
            : totalSupply();
    }

    /// @notice Admin can change a storage parent
    /// @param id Wallet ID
    /// @param parent New parent
    function changeParent(uint64 id, uint64 parent) public onlyRole(GRANTER_ROLE) {
        _storages[id].parent = parent;
        emit SetParent(id, parent);
    }

    /// @notice Transfer coins to storage account
    /// @param id Wallet ID
    /// @param amount Coins amount to claim
    function _claim(uint64 id, uint amount) internal returns (uint) {
        uint currentAmount = totalSupply() >= amount
            ? amount
            : totalSupply();
        _claimed += currentAmount;
        _storages[id].claimed += currentAmount;
        if (currentAmount > 0) {
            (bool sent,) = _storages[id].account.call{value: currentAmount}("");
            require(sent, "Failed to send Ether");
        }
        return currentAmount;
    }

    /// @notice ERC20 format
    function totalSupply() public view virtual returns (uint256) {
        return address(this).balance;
    }

    /// @notice ERC20 format
    function decimals() public view virtual returns (uint8) {
        return _decimals;
    }

    /// @notice ERC20 format
    function name() public view virtual returns (string memory) {
        return _name;
    }

    /// @notice ERC20 format
    function symbol() public view virtual returns (string memory) {
        return _symbol;
    }

    /// @notice Gets account balance
    /// @param account Address linked to storage
    /// @return Tokens amount
    /// @dev Balances linked to Wallet ID only
    function balanceOf(address account) public view virtual returns (uint256) {
        uint64 userId = getAddressId(account);
        return userId > 0
            ? account.balance + _getMinedTokens(userId)
            : account.balance;
    }

    /// @notice Gets Wallet ID linked to a storage
    /// @param account Address linked to storage
    /// @return Wallet ID
    function getAddressId(address account) public view returns (uint64) {
        return _accounts[account];
    }

    function getIdAddress(uint64 userId) public view returns (address) {
        return _storages[userId].account;
    }

    /// @notice Gets mined tokens since last claim
    /// @param userId Wallet ID
    /// @return Tokens amount
    function _getMinedTokens(uint64 userId) internal view returns (uint) {
        Storage storage user = _storages[userId];
        uint limit = _getSize();
        uint time = block.timestamp - user.claimTimestamp;
        uint amount = _getSpeed() * time;
        return amount > limit && limit != 0
            ? limit
            : amount;
    }

    /// @notice Claim reward by storage
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function _claimReward(uint64 userId, uint additional) internal {
        require(!_claimsDisabled, "Claims disabled");
        uint mined = _getMinedTokens(userId) + additional;
        if (mined == 0) return;
        Storage storage current = _storages[userId];
        require(!current.disabled, "Storage disabled");
        uint claimed = _claim(userId, mined);
        current.claimTimestamp = block.timestamp;
        emit Claim(userId, current.account, claimed);
        emit Transfer(address(this), current.account, claimed);

        uint index = 0;
        uint64 previous = userId;
        while (index < parentsPercents.length
        && current.parent != 0
            && !_refersDisabled) {
            uint64 receiver = current.parent;
            current = _storages[receiver];
            uint reward = claimed * parentsPercents[index] / _PERCENT_PRECISION;
            if (reward > 0 && current.account != address(0)) {
                uint claimedRefer = _claim(receiver, reward);
                if (claimedRefer > 0) {
                    emit ReferReward(userId, receiver, previous, current.account, claimedRefer);
                }
            }
            index++;
            previous = receiver;
        }
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function claimReward(uint64 userId) public onlyStorageOwner(userId) {
        _claimReward(userId, 0);
    }

    /// @notice Storage owner can claim his reward
    /// @notice It also sends rewards to parents
    /// @param userId Wallet ID
    function claimFor(uint64 userId) public onlyRole(CLAIMER_ROLE) {
        _claimReward(userId, 0);
    }

    /// @notice Mint amount of tokens to storage and distribute it to refers
    /// @param userId Wallet ID
    /// @param amount Amount of tokens to mint
    /// @dev Calls claim
    function mintWithClaim(uint64 userId, uint amount) public onlyRole(MINTER_ROLE) {
        _claimReward(userId, amount);
    }

    /// @notice Mint amount of tokens only for one account
    /// @param userId Wallet ID
    /// @param amount Amount of tokens to mint
    function mintForOne(uint64 userId, uint amount) public onlyRole(MINTER_ROLE) {
        _claim(userId, amount);
        emit Transfer(address(this), _storages[userId].account, amount);
    }

    /// @notice Access controller can create storage for Pixel Wallet user on his address
    /// @param userId Wallet ID
    /// @param account Wallet address
    /// @param parent Pixel Wallet inviter
    function grantAccess(uint64 userId, address account, uint64 parent) public onlyRole(GRANTER_ROLE) {
        _accounts[account] = userId;
        _storages[userId].account = account;
        if (_storages[userId].claimTimestamp == 0) {
            _storages[userId].claimTimestamp = block.timestamp;
        }
        if (parent != 0 && parent != userId) {
            _storages[userId].parent = parent;
            emit SetParent(userId, parent);
        }
        emit GrantAccess(userId, account, _msgSender());
    }

    /// @notice Admins can disable storage
    /// @param userId Wallet ID
    function disableStorage(uint64 userId) public onlyRole(GRANTER_ROLE) {
        _storages[userId].disabled = true;
        emit SetStorageDisabled(userId, _msgSender(), true);
    }

    /// @notice Admins can enable storage again
    /// @param userId Wallet ID
    function enableStorage(uint64 userId) public onlyRole(GRANTER_ROLE) {
        _storages[userId].disabled = false;
        emit SetStorageDisabled(userId, _msgSender(), false);
    }

    /// @notice Returns total claimed amount by storages
    /// @return Claimed tokens amount
    function totalClaimed() public view virtual returns (uint) {
        return _claimed;
    }

    /// @notice Contract owner can disable claims
    function disableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = true;
        emit SetClaimsDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable claims again
    function enableClaims() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _claimsDisabled = false;
        emit SetClaimsDisabled(_msgSender(), false);
    }

    /// @notice Contract owner can disable parents rewards
    function disableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = true;
        emit SetRefersDisabled(_msgSender(), true);
    }

    /// @notice Contract owner can enable parents rewards again
    function enableRefers() public onlyRole(DEFAULT_ADMIN_ROLE) {
        _refersDisabled = false;
        emit SetRefersDisabled(_msgSender(), false);
    }

    /// @notice Contract owner set base tokens per hour value
    function setBaseSpeed(uint speed) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSpeed = speed;
    }

    function setBaseSize(uint size) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseSize = size;
    }

    /// @notice Owner can set a new parents percents
    /// @param _percents Array of percents
    function setParentsPercents(uint[2] calldata _percents) public onlyRole(DEFAULT_ADMIN_ROLE) {
        parentsPercents = _percents;
    }

    /// @notice Storage info getter
    /// @param userId Wallet ID
    function getStorage(uint64 userId) public view returns (
        Storage memory user,
        uint mined,
        uint sizeLimit,
        uint rewardPerSecond,
        uint balance,
        uint coins
    ) {
        Storage storage _user = _storages[userId];
        user = _user;
        mined = _getMinedTokens(userId);
        sizeLimit = _getSize();
        rewardPerSecond = _getSpeed();
        balance = balanceOf(_user.account);
        coins = user.account.balance;
    }

}