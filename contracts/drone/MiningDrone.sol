// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";
import { IParameterReceiver } from "contracts/V5/Slots/interfaces/IParameterReceiver.sol";
import { IPXLsCoreV5 } from "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import { IPXLsCoreV5Math, BaseParams, Storage } from "contracts/V5/Core/interfaces/IPXLsCoreV5Math.sol";
import { IMiningDrone, Mission } from "contracts/drone/interfaces/IMiningDrone.sol";
import "contracts/craft/interfaces/IPXLDust.sol";

uint8 constant _decimals = 18;
uint constant _PRECISION = 10**_decimals;
uint constant _PERCENT_DECIMALS = 4;
uint constant _PERCENT_PRECISION = 10**_PERCENT_DECIMALS;
uint constant _HOUR = 60 * 60;

/**
 * @title MiningDrone
 * @notice A smart contract for managing mining drones that mine resources for users.
 * @dev Implements drone dispatching, mission tracking, resource rewards, and user upgrades.
 */
contract MiningDrone is IMiningDrone, AccessControl {
    /// @notice The PIXEL Core V5 contract instance (immutable).
    IPXLsCoreV5 public immutable PIXEL;

    /// @notice The ERC20 token used for mining rewards (immutable).
    IPXLDust public immutable PIXEL_DUST;

    /// @notice The core contract instance (immutable).
    IPXLsCoreV5 public immutable CORE;

    /// @notice The core math contract instance (immutable).
    IPXLsCoreV5Math public immutable CORE_MATH;

    /// @notice Role for editing SLOT parameters.
    bytes32 public constant SLOT_EDITOR_ROLE = keccak256("SLOT_EDITOR_ROLE");

    /// @notice Role for editing main settings parameters.
    bytes32 public constant SETTINGS_EDITOR_ROLE = keccak256("SETTINGS_EDITOR_ROLE");

    /// @notice Role for setting KARMA.
    bytes32 public constant BACKEND_ROLE = keccak256("BACKEND_ROLE");

    /// @notice ID for drone speed parameter.
    uint8 public paramIdDroneSpeed = 21;

    /// @notice ID for drone duration parameter.
    uint8 public paramIdDroneDurationSeconds = 22;

    /// @notice ID for drone missions count parameter.
    uint8 public paramIdDroneMissionsCount = 23;

    /// @notice ID for slot 0 parameter.
    uint8 public paramIdUpgradeCostMultiplier = 24;

    /// @notice Mapping of user addresses to their mine levels.
    mapping(address => uint256) public mineLevel;

    /// @notice Mapping of user addresses to their active missions.
    mapping(address => Mission) public missions;

    /// @notice Mapping of user addresses to slot multipliers for each slot.
    mapping(address userAddress => mapping(uint8 paramIndex => uint256 value)) public userParams;

    mapping(address userAddress => uint256 value) public userKarma;

    /**
     * @dev Initializes the contract, granting roles and setting external contract dependencies.
     * @param pixelDust Address of the PIXEL_DUST ERC20 token.
     * @param coreV5 Address of the Core V5 contract.
     * @param coreV5Math Address of the Core V5 Math contract.
     * @param slotsV5 Address with the SLOT_EDITOR_ROLE permissions.
     */
    constructor(address pixelDust, address coreV5, address coreV5Math, address slotsV5) {
        PIXEL = IPXLsCoreV5(coreV5);
        PIXEL_DUST = IPXLDust(pixelDust);
        CORE = IPXLsCoreV5(coreV5);
        CORE_MATH = IPXLsCoreV5Math(coreV5Math);
        _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _grantRole(SETTINGS_EDITOR_ROLE, _msgSender());
        _grantRole(BACKEND_ROLE, _msgSender());
        _grantRole(SLOT_EDITOR_ROLE, slotsV5);
    }

    /**
     * @notice Dispatches a drone for a mining mission.
     * @dev Sets mission parameters and marks the drone as in use.
     * Requirements:
     * - The drone must not currently be in operation.
     */
    function dispatchDrone() external {
        Mission memory mission = missions[_msgSender()];
        require(block.timestamp >= mission.dispatchTime + mission.durationSeconds, "Drone in operation");
        if (mission.dispatchTime > 0) require(mission.claimed, "Drone needs to be returned");
        require(userParams[_msgSender()][paramIdDroneMissionsCount] > 0, "The drone's resource has expired");

        missions[_msgSender()] = Mission({
            dustPerHour: _calculateMiningSpeed(_msgSender()),
            durationSeconds: userParams[_msgSender()][paramIdDroneDurationSeconds],
            dispatchTime: block.timestamp,
            dispatchesLeft: userParams[_msgSender()][paramIdDroneMissionsCount],
            claimed: false
        });
        userParams[_msgSender()][paramIdDroneMissionsCount]--;
        emit DroneDispatched(
            _msgSender(),
            missions[_msgSender()].dustPerHour,
            missions[_msgSender()].durationSeconds,
            missions[_msgSender()].dispatchTime,
            userParams[_msgSender()][paramIdDroneMissionsCount]
        );
    }

    /**
     * @notice Returns the drone and claims the reward for the completed mission.
     * @dev Transfers mined resources to the user.
     * Requirements:
     * - The mission must be complete.
     * - The reward must not already be claimed.
     */
    function returnDroneAndClaimReward() external {
        Mission memory mission = missions[_msgSender()];
        require(block.timestamp >= mission.dispatchTime + mission.durationSeconds, "Drone in operation");
        require(!missions[_msgSender()].claimed, "Already claimed");
        uint256 reward = _calculateFinalReward(mission.dustPerHour, mission.durationSeconds);
        missions[_msgSender()].claimed = true;
        PIXEL_DUST.mint(_msgSender(), reward);
        emit RewardClaimed(_msgSender(), reward);
    }

    /**
     * @notice Upgrades the user's mine to the next level.
     * @dev Burns the required PIXEL tokens and increments the mine level.
     */
    function upgradeMine() external {
        uint256 upgradePrice = _calculateMineUpgradePrice(_msgSender());
        mineLevel[_msgSender()] += 1;
        PIXEL.burn(_msgSender(), upgradePrice);
        emit MineUpgraded(_msgSender(), mineLevel[_msgSender()], upgradePrice);
    }

    /**
     * @notice Updates a parameter for a specific user slot.
     * @param userId User ID to identify the user.
     * @param parameterId ID of the parameter to update.
     * @param multiplier Multiplier value for the parameter.
     * @param adder Unused parameter in this implementation.
     */
    // solc-ignore-next-line unused-param
    function updateParameter(uint64 userId, uint8 parameterId, uint24 multiplier, uint256 adder)
        external
        onlyRole(SLOT_EDITOR_ROLE)
    {
        address user = CORE.getIdAddress(userId);
        if (parameterId == paramIdUpgradeCostMultiplier) userParams[user][paramIdUpgradeCostMultiplier] = multiplier;

        if (parameterId == paramIdDroneSpeed
        || parameterId == paramIdDroneDurationSeconds
        || parameterId == paramIdDroneMissionsCount) {
            if (adder == 0) {
                /// Kill the mission if the drone is removed from the slot
                missions[user].dispatchTime = 0;
                missions[user].claimed = false;
            }
        }

        if (parameterId == paramIdDroneSpeed) userParams[user][paramIdDroneSpeed] = adder;
        if (parameterId == paramIdDroneDurationSeconds) userParams[user][paramIdDroneDurationSeconds] = adder;
        if (parameterId == paramIdDroneMissionsCount) userParams[user][paramIdDroneMissionsCount] = adder;
        emit ParameterUpdated(user, parameterId, multiplier, adder);
    }

    /**
     * @notice Sets the parameter ID for slot 0.
     * @param parameterId New parameter ID.
     */
    function setParameterIdUpgradeCostMultiplier(uint8 parameterId) external onlyRole(SETTINGS_EDITOR_ROLE) {
        paramIdUpgradeCostMultiplier = parameterId;
        emit ParameterIdUpdated("Slot0", parameterId);
    }

    /**
     * @notice Sets the parameter ID for the drone's speed.
     * @param parameterId New parameter ID.
     */
    function setParameterIdDroneSpeed(uint8 parameterId) external onlyRole(SETTINGS_EDITOR_ROLE) {
        paramIdDroneSpeed = parameterId;
        emit ParameterIdUpdated("DroneSpeed", parameterId);
    }

    /**
     * @notice Sets the parameter ID for the drone's duration.
     * @param parameterId New parameter ID.
     */
    function setParameterIdDroneDuration(uint8 parameterId) external onlyRole(SETTINGS_EDITOR_ROLE) {
        paramIdDroneDurationSeconds = parameterId;
        emit ParameterIdUpdated("DroneDuration", parameterId);
    }

    /**
     * @notice Calculates the mining speed for a specific user.
     * @param user The address of the user.
     * @return dustPerHour The calculated mining speed in PIXEL_DUST per hour.
     */
    function calculateMiningSpeed(address user) external view returns (uint256 dustPerHour) {
        return _calculateMiningSpeed(user);
    }

    /**
     * @notice Calculates the upgrade price for the user's mine.
     * @param user The address of the user.
     * @return The price in PIXEL tokens to upgrade the user's mine.
     */
    function calculateMineUpgradePrice(address user) external view returns (uint256) {
        return _calculateMineUpgradePrice(user);
    }

    function getMiningCurrent(address userAddress) private view returns (uint256) {
        (Storage memory user, BaseParams memory base, , , , , , , ) = CORE.getStorage(CORE.getAddressId(userAddress));
        uint miningMultiplicator = user.miningMultiplicator == 0
            ? _PERCENT_PRECISION
            : user.miningMultiplicator;

        return base.speed
        * miningMultiplicator / _PERCENT_PRECISION
        * base.halvingRatio / _PERCENT_PRECISION;
    }

    function _calculateMiningSpeed(address user) private view returns (uint256 dustPerHour) {
        uint256 miningCurrent = getMiningCurrent(user);
        uint256 droneSpeed = userParams[user][paramIdDroneSpeed];
        
        uint256 karma = userKarma[user] == 0 ? _PERCENT_PRECISION : userKarma[user];

        return droneSpeed * _calculateMineMultiplier(miningCurrent) / _PRECISION
            * karma / _PERCENT_PRECISION;
    }

    function _calculateMineMultiplier(uint256 miningCurrent) private view returns (uint256) {
        return _PRECISION + ((mineLevel[_msgSender()] + 1) * miningCurrent);
    }

    function _calculateMineUpgradePrice(address user) private view returns (uint256) {
        uint256 level = mineLevel[user] + 2;
        BaseParams memory params = CORE_MATH.getParams();
        uint256 multiplier = userParams[user][paramIdUpgradeCostMultiplier] == 0
            ? _PERCENT_PRECISION
            : userParams[user][paramIdUpgradeCostMultiplier];
        return ((level * params.difficulty) * (level * params.speed)) / _PERCENT_PRECISION
            * multiplier / _PERCENT_PRECISION;
    }

    function _calculateFinalReward(uint256 dustPerHour, uint256 durationSeconds) private pure returns (uint256) {
        return dustPerHour / _HOUR * durationSeconds;
    }

    function setUserKarmaMod(address userAddress, uint256 mult) public onlyRole(BACKEND_ROLE) {
        userKarma[userAddress] = mult;
    }

    function getUserMission(address userAddress) public view returns (Mission memory mission, uint256 upgradePrice) {
        mission = missions[userAddress];
        upgradePrice = _calculateMineUpgradePrice(userAddress);
    }
    
}
