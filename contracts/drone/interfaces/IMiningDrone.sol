// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.24;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { IParameterReceiver } from "contracts/V5/Slots/interfaces/IParameterReceiver.sol";
import { IPXLsCoreV5 } from "contracts/V5/Core/interfaces/IPXLsCoreV5.sol";
import { IPXLsCoreV5Math, BaseParams } from "contracts/V5/Core/interfaces/IPXLsCoreV5Math.sol";
import "contracts/craft/interfaces/IPXLDust.sol";

struct Mission {
    uint256 dustPerHour;
    uint256 durationSeconds;
    uint256 dispatchTime;
    uint256 dispatchesLeft;
    bool claimed;
}

/**
 * @title IMiningDrone
 * @dev Interface for interacting with the MiningDrone contract.
 */
interface IMiningDrone is IParameterReceiver {
    /// @notice Events
    event DroneDispatched(address indexed user, uint256 dustPerHour, uint256 durationSeconds, uint256 dispatchTime, uint256 dispatchesLeft);
    event RewardClaimed(address indexed user, uint256 reward);
    event MineUpgraded(address indexed user, uint256 newLevel, uint256 cost);
    event ParameterUpdated(address indexed user, uint8 parameterId, uint24 multiplier, uint256 adder);
    event ParameterIdUpdated(string parameterName, uint8 newParameterId);

    /**
     * @notice Dispatches a drone for a mining mission.
     * @dev The drone starts a mission with parameters defined by the user.
     * @dev Emits no events.
     * @dev Requirements:
     * - The drone must not be currently in operation.
     */
    function dispatchDrone() external;

    /**
     * @notice Returns the drone and claims the mining reward.
     * @dev Transfers mined resources (PIXEL_DUST) to the user.
     * @dev Emits no events.
     * @dev Requirements:
     * - The mission must be complete.
     * - Rewards must not already be claimed.
     */
    function returnDroneAndClaimReward() external;

    /**
     * @notice Upgrades the user's mine to a higher level.
     * @dev Burns PIXEL tokens as the cost for the upgrade.
     * @dev Emits no events.
     */
    function upgradeMine() external;

    /**
     * @notice Sets the parameter ID for slot 0.
     * @param parameterId The new parameter ID for slot 0.
     * @dev Only callable by an address with SETTINGS_EDITOR_ROLE.
     */
    function setParameterIdUpgradeCostMultiplier(uint8 parameterId) external;

    /**
     * @notice Sets the parameter ID for the drone's speed.
     * @param parameterId The new parameter ID for drone speed.
     * @dev Only callable by an address with SETTINGS_EDITOR_ROLE.
     */
    function setParameterIdDroneSpeed(uint8 parameterId) external;

    /**
     * @notice Sets the parameter ID for the drone's duration.
     * @param parameterId The new parameter ID for drone duration.
     * @dev Only callable by an address with SETTINGS_EDITOR_ROLE.
     */
    function setParameterIdDroneDuration(uint8 parameterId) external;

    /**
     * @notice Calculates the mining speed for a user.
     * @param user The address of the user.
     * @return dustPerHour The calculated mining speed in PIXEL_DUST per hour.
     * @dev Pure function that does not modify state.
     */
    function calculateMiningSpeed(address user) external view returns (uint256 dustPerHour);

    /**
     * @notice Calculates the cost to upgrade the user's mine.
     * @param user The address of the user.
     * @return The cost in PIXEL tokens.
     * @dev Pure function that does not modify state.
     */
    function calculateMineUpgradePrice(address user) external view returns (uint256);

    /**
     * @notice Returns the PIXEL Core V5 contract address.
     * @return The address of the PIXEL Core V5 contract.
     */
    function PIXEL() external view returns (IPXLsCoreV5);

    /**
     * @notice Returns the PIXEL_DUST ERC20 token address.
     * @return The address of the PIXEL_DUST token.
     */
    function PIXEL_DUST() external view returns (IPXLDust);

    /**
     * @notice Returns the Core V5 Math contract address.
     * @return The address of the Core V5 Math contract.
     */
    function CORE_MATH() external view returns (IPXLsCoreV5Math);

    /**
     * @notice Returns the user's current mine level.
     * @param user The address of the user.
     * @return The current level of the user's mine.
     */
    function mineLevel(address user) external view returns (uint256);

    // /**
    //  * @notice Returns the current mission of a user.
    //  * @param user The address of the user.
    //  * @return The mission details.
    //  */
    // function missions(address user) external view returns (Mission memory);

    /**
     * @notice Returns the multiplier for a user's slot parameter.
     * @param userAddress The address of the user.
     * @param paramIndex The slot ID.
     * @return The value for the specified slot.
     */
    function userParams(address userAddress, uint8 paramIndex) external view returns (uint256);
}
